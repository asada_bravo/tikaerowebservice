Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Security
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Xml.Schema
Imports System.Text
Imports System.Web.Services.Description
Imports System.Data
Imports System.IO
Imports TikAero.DAL.BusinessObjects
Imports TikAero.WebService.Helper
Imports System.Text.RegularExpressions
Imports System.Net.Mail
Imports ADODB
Imports System.Runtime.InteropServices
Imports System.Xml.Linq
Imports System.Data.OleDb

<SoapDocumentService(SoapBindingUse.Literal,
                     SoapParameterStyle.Wrapped,
                     RoutingStyle:=SoapServiceRoutingStyle.SoapAction),
                        System.Web.Services.WebService(Name:="TikAero XML web service",
                                Description:="Input XML request document and return response document",
                                Namespace:="http://www.tiksystems.com/TikAeroWebService")>
Partial Public Class AgentService
    Inherits System.Web.Services.WebService

    Public AgentHeader As AgentAuthHeader

#Region " Web Services Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()
        'Add your own initialization code after the InitializeComponent() call

    End Sub

    'Required by the Web Services Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Web Services Designer
    'It can be modified using the Web Services Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        'CODEGEN: This procedure is required by the Web Services Designer
        'Do not modify it using the code editor.
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region

#Region " Lersun "
    Private Function AuthenticatePassport(ByVal strCode As String,
                                          ByVal strPassport As String) As Boolean
        Try
            Dim strPassword As String = FormsAuthentication.HashPasswordForStoringInConfigFile(strCode.ToUpper, "SHA1")
            Dim clsDeCrypt As New clsCrypto
            Dim strNPassword As String = clsDeCrypt.clsCrypto(strCode.ToUpper, strPassword, True)
            clsDeCrypt = Nothing

            If strNPassword = strPassport Then
                Return True
            Else
                Return False
            End If
        Catch
        End Try
    End Function

    Private Function CreatePassport(ByVal strCode As String) As String

        Try
            Dim strSHA1 As String = FormsAuthentication.HashPasswordForStoringInConfigFile(strCode, "SHA1")
            Dim clsEnCrypt As New clsCrypto
            Dim strPassword As String = clsEnCrypt.clsCrypto(strCode, strSHA1, True)
            clsEnCrypt = Nothing
            Return strPassword
        Catch
            CreatePassport = ""
        End Try
    End Function
#End Region

#Region "Webservice Method"

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetBookingHistory"),
WebMethod(Description:="Return Booking History",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetBookingHistory(ByVal strBookingId As String) As DataSet

        Dim clsRunCom As New clsRunComplus
        Dim rsHeader As ADODB.Recordset = Session("rsHeader")
        Dim rsSegments As ADODB.Recordset = Session("rsSegments")
        Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
        Dim rsRemarks As ADODB.Recordset = Session("rsRemarks")
        Dim rsPayments As ADODB.Recordset = Session("rsPayments")
        Dim rsMappings As ADODB.Recordset = Session("rsMappings")
        Dim rsServices As ADODB.Recordset = Session("rsServices")
        Dim rsTaxes As ADODB.Recordset = Session("rsTaxes")
        Dim rsFees As ADODB.Recordset = Session("rsFees")
        Dim rsQuotes As ADODB.Recordset = Session("rsQuotes")

        'Clear Recordset before create new.
        ClearBookingRecordset(rsHeader, rsSegments, rsPassengers, rsRemarks, rsPayments, rsMappings, rsServices, rsTaxes, rsFees, rsQuotes)
        Using ds As DataSet = clsRunCom.GetBookingHistory(strBookingId,
                                                          rsHeader,
                                                          rsSegments,
                                                          rsPassengers,
                                                          rsRemarks,
                                                          rsPayments,
                                                          rsMappings,
                                                          rsServices,
                                                          rsTaxes,
                                                          rsFees,
                                                          rsQuotes)
            Dim rsHeaderClone As New ADODB.Recordset
            Dim rsSegmentsClone As New ADODB.Recordset
            Dim rsPassengersClone As New ADODB.Recordset
            Dim rsRemarksClone As New ADODB.Recordset
            Dim rsPaymentsClone As New ADODB.Recordset
            Dim rsMappingsClone As New ADODB.Recordset
            Dim rsServicesClone As New ADODB.Recordset
            Dim rsTaxesClone As New ADODB.Recordset
            Dim rsFeesClone As New ADODB.Recordset
            Dim rsQuotesClone As New ADODB.Recordset
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport
                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim fr As New FabricateRecordset

                        fr.CopyAdoRecordset(rsHeader, rsHeaderClone)
                        Session("rsHeader") = rsHeader

                        fr.CopyAdoRecordset(rsSegments, rsSegmentsClone)
                        Session("rsSegments") = rsSegments

                        fr.CopyAdoRecordset(rsPassengers, rsPassengersClone)
                        Session("rsPassenger") = rsPassengers

                        fr.CopyAdoRecordset(rsRemarks, rsRemarksClone)
                        Session("rsRemarks") = rsRemarks

                        fr.CopyAdoRecordset(rsPayments, rsPaymentsClone)
                        Session("rsPayments") = rsPayments

                        fr.CopyAdoRecordset(rsMappings, rsMappingsClone)
                        Session("rsMappings") = rsMappings

                        fr.CopyAdoRecordset(rsServices, rsServicesClone)
                        Session("rsServices") = rsServices

                        fr.CopyAdoRecordset(rsTaxes, rsTaxesClone)
                        Session("rsTaxes") = rsTaxes

                        fr.CopyAdoRecordset(rsFees, rsFeesClone)
                        Session("rsFees") = rsFees

                        fr.CopyAdoRecordset(rsQuotes, rsQuotesClone)
                        Session("rsQuotes") = rsQuotes

                        CopyRecordsetsToDataset(rsHeaderClone,
                                                rsSegmentsClone,
                                                rsPassengersClone,
                                                rsRemarksClone,
                                                rsPaymentsClone,
                                                rsMappingsClone,
                                                rsTaxesClone,
                                                rsQuotesClone,
                                                rsFeesClone,
                                                rsServicesClone,
                                                ds)

                    End If
                End If
            Catch e As Exception
            Finally
                ClearBookingRecordset(rsHeaderClone,
                                      rsSegmentsClone,
                                      rsPassengersClone,
                                      rsRemarksClone,
                                      rsPaymentsClone,
                                      rsMappingsClone,
                                      rsServicesClone,
                                      rsTaxesClone,
                                      rsFeesClone,
                                      rsQuotesClone)
            End Try
            Return ds
        End Using

    End Function


    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetBinRangeSearch"),
WebMethod(Description:="Return  Activities",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetBinRangeSearch(ByVal strCardType As String, ByVal strStatusCode As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing

            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then


                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetBinRangeSearch(strCardType, strStatusCode)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "BinRangeSearch", objOut)
                    End If
                End If
            Catch e As Exception

            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetFlightDailyCount"),
WebMethod(Description:="Return  Activities",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetFlightDailyCount(ByVal dtFrom As Date, ByVal dtTo As Date, ByVal strFrom As String, ByVal strTo As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing

            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetFlightDailyCount(dtFrom, dtTo, strFrom, strTo)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "FlightDailyCount", objOut)
                    End If
                End If
            Catch e As Exception

            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using

    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetFlightDailyCountXML"),
WebMethod(Description:="Return  Activities",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetFlightDailyCountXML(ByVal dtFrom As Date, ByVal dtTo As Date, ByVal strFrom As String, ByVal strTo As String, ByVal strToken As String) As String
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (String.IsNullOrEmpty(strToken)) Then
                    Return ""
                Else
                    If IsTokenValid(strToken) = True Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetFlightDailyCount(dtFrom, dtTo, strFrom, strTo)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "FlightDailyCount", objOut)
                        objOut.DataSetName = "FlightDailyCounts"
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try

            If objOut.Tables.Count > 0 Then
                Return objOut.GetXml
            Else
                Return String.Empty
            End If
        End Using

    End Function


    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/AccuralQuote"),
WebMethod(Description:="Return  AccuralQuote",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function AccuralQuote(ByVal XmlPassengers As String, ByVal XmlMapping As String, ByVal strClientId As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing

            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then


                        Dim clsRunCom As New clsRunComplus

                        Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
                        Dim rsMappings As ADODB.Recordset = Session("rsMappings")

                        Dim MappingsUpdate As New MappingsUpdate
                        Dim Passengers As New Passengers
                        Dim r As New Helper.RecordSet

                        Passengers = Serialization.Deserialize(XmlPassengers, Passengers)

                        XmlMapping = XmlMapping.Replace("Mapping", "MappingUpdate")
                        MappingsUpdate = Serialization.Deserialize(XmlMapping, MappingsUpdate)

                        r.FillPassengers(rsPassengers, Passengers)
                        r.FillMapping(rsMappings, MappingsUpdate)


                        rsA = clsRunCom.AccuralQuote(rsPassengers, rsMappings, strClientId)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "AccuralQuote", objOut)
                    End If
                End If
            Catch e As Exception

            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/AgencyDetails"),
         WebMethod(Description:="Returns agency detail",
         EnableSession:=True)>
    Public Function AgencyDetails(ByVal webAgencyCode As String,
                                   ByVal agentUser As String,
                                   ByVal agentPassword As String,
                                   ByRef passport As String,
                                   ByVal agencyCode As String) As String

        Dim objOut As New XmlDocument
        Dim rsA As ADODB.Recordset = Nothing
        Dim strAgencyDetails As String

        Try
            Dim clsRunCom As New clsRunComplus
            rsA = clsRunCom.GetAgencySessionProfile(webAgencyCode, String.Empty)

            If rsA Is Nothing Then
                Throw New System.Exception("No found agency information")
            Else
                Dim bFound As Boolean = False

                Do While rsA.EOF = False
                    Dim AgencyLogonDB As String = System.Convert.ToString(rsA.Fields("agency_logon").Value)
                    Dim AgencyPasswordDB As String = System.Convert.ToString(rsA.Fields("agency_password").Value)
                    If (AgencyLogonDB.ToUpper = agentUser.ToUpper) And (AgencyPasswordDB.ToUpper = agentPassword.ToUpper) Then
                        bFound = True
                        Exit Do
                    Else
                        rsA.MoveNext()
                    End If
                Loop
                If bFound = False Then
                    strAgencyDetails = "0"
                Else
                    strAgencyDetails = String.Empty
                    If agencyCode.Length = 0 Or webAgencyCode.ToUpper().Equals(agencyCode.ToUpper()) Then
                        passport = CreatePassport(webAgencyCode.ToUpper)
                    Else
                        Helper.Check.ClearRecordset(rsA)
                        rsA = clsRunCom.GetAgencySessionProfile(agencyCode, String.Empty)
                        If rsA.EOF = False Then
                            passport = CreatePassport(agencyCode.ToUpper)
                        End If
                    End If

                    If IsNothing(rsA) = False Then
                        rsA.Filter = 0
                        If rsA.EOF = False Then
                            If rsA.Fields("default_user_account_id").Value Is System.DBNull.Value Then
                                Session("CreateById") = System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")
                            Else
                                Session("CreateById") = rsA.Fields("default_user_account_id").Value
                            End If
                        End If
                        strAgencyDetails = RecordSetToXML(rsA, "TikAero", "Agent")
                    End If
                End If
            End If
        Catch e As Exception
            strAgencyDetails = "Error: " & e.Message
            Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(e, webAgencyCode)
        Finally
            Helper.Check.ClearRecordset(rsA)
        End Try

        Return strAgencyDetails
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetActivities"),
WebMethod(Description:="Return  Activities",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetActivities(ByVal AgencyCode As String, ByVal RemarkType As String, ByVal Nickname As String, ByVal TimelimitFrom As Date, ByVal TimelimitTo As Date, ByVal PendingOnly As Boolean, ByVal IncompleteOnly As Boolean, ByVal IncludeRemarks As Boolean) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing

            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetActivities(AgencyCode, RemarkType, Nickname, TimelimitFrom, TimelimitTo, PendingOnly, IncompleteOnly, IncludeRemarks)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "ActivitySearch", objOut)
                    End If
                End If
            Catch e As Exception

            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ValidCreditCard"),
WebMethod(Description:="Valid CreditCard(",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ValidCreditCard(ByVal BookingId As String, ByVal XmlMapping As String, ByVal XmlFees As String, ByVal XmlPayments As String, ByVal XmlSegments As String, ByVal securityToken As String, ByVal authenticationToken As String, ByVal commerceIndicator As String, ByVal strRecordLocator As String) As DataSet
        Using objOut As New DataSet
            Dim Allocations As Allocations
            Dim ErrorId As String = String.Empty
            Dim ResponseCode As String = String.Empty
            Dim FabricateRecordset As New FabricateRecordset

            Dim rsA As ADODB.Recordset = Nothing
            Dim rsVoucher As ADODB.Recordset = Nothing
            Dim rsPayments As ADODB.Recordset = Nothing
            Dim rsAllocation As ADODB.Recordset = FabricateRecordset.FabricatePaymentAllocationRecordset
            Dim rsHeader As ADODB.Recordset = Nothing
            Dim rsRemarks As ADODB.Recordset = Nothing
            Dim rsServices As ADODB.Recordset = Nothing
            Dim rsTaxes As ADODB.Recordset = Nothing
            Dim rsQuotes As ADODB.Recordset = Nothing

            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim AgencyCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport
                    Dim AgencyCurrencyRcd As String = AgentHeader.AgencyCurrencyRcd
                    If (AuthenticatePassport(AgencyCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus

                        Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
                        Dim rsSegments As ADODB.Recordset = Session("rsSegments")
                        Dim rsMappings As ADODB.Recordset = Session("rsMappings")
                        Dim rsFees As ADODB.Recordset = Session("rsFees")

                        Dim Itinerary As New Itinerary
                        Dim MappingsUpdate As New MappingsUpdate
                        Dim PaymentsUpdate As New PaymentsUpdate

                        Dim Status As Boolean = clsRunCom.GetEmpty(AgentHeader.AgencyCode, AgentHeader.AgencyCurrencyRcd, , , , , rsPayments)

                        Itinerary = Serialization.Deserialize(XmlSegments, Itinerary)
                        XmlPayments = XmlPayments.Replace("Payment", "PaymentUpdate")
                        PaymentsUpdate = Serialization.Deserialize(XmlPayments, PaymentsUpdate)

                        ErrorId = PaymentsUpdate(0).booking_payment_id.ToString

                        Dim DefaultCreateById As String

                        Dim Mappings As New Mappings
                        Dim Fees As New Fees
                        Dim iInfantCount As Integer

                        'Find Payment allocation
                        Mappings = Serialization.Deserialize(XmlMapping, Mappings)
                        Fees = Serialization.Deserialize(XmlFees, Fees)
                        Allocations = GetPaymentAllocations(Mappings, Fees, True, Nothing)

                        XmlMapping = XmlMapping.Replace("Mapping", "MappingUpdate")
                        MappingsUpdate = Serialization.Deserialize(XmlMapping, MappingsUpdate)
                        Dim r As New Helper.RecordSet
                        With r
                            If Not Session("CreateById") Is Nothing AndAlso Not Session("CreateById") = String.Empty Then
                                DefaultCreateById = Session("CreateById")
                                .DefaultCreateById = New Guid(DefaultCreateById)
                            Else
                                If IsGuid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")) Then
                                    Session("CreateById") = System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")
                                End If
                                DefaultCreateById = Session("CreateById")
                                r.DefaultCreateById = New Guid(DefaultCreateById)
                            End If
                            .Fill(rsAllocation, Allocations, False)
                            .Fill(rsPayments, PaymentsUpdate)
                            .FillMapping(rsMappings, MappingsUpdate)
                        End With
                        rsPassengers.Filter = "passenger_type_rcd = 'INF'"
                        iInfantCount = rsPassengers.RecordCount
                        rsPassengers.Filter = 0
                        If InfantExcessLimit(rsSegments, iInfantCount) = False Then
                            rsA = clsRunCom.ValidCreditCard(rsPayments,
                                                            rsSegments,
                                                            rsAllocation,
                                                            rsVoucher,
                                                            rsMappings,
                                                            securityToken,
                                                            authenticationToken,
                                                            commerceIndicator,
                                                            strRecordLocator)

                            If rsA Is Nothing Then
                                ResponseCode = "No rs*"
                                Return Nothing
                            ElseIf rsA("ResponseCode").Value = "APPROVED" Then
                                ResponseCode = "APPROVED"

                                Call clsRunCom.GetBooking(BookingId,
                                                          rsHeader,
                                                          rsSegments,
                                                          rsPassengers,
                                                          rsRemarks,
                                                          rsPayments,
                                                          rsMappings,
                                                          rsServices,
                                                          rsTaxes,
                                                          rsFees,
                                                          rsQuotes)

                                Session("rsFees") = rsFees

                                Call InputRecordsetToDataset(rsA, "Payments", objOut)

                            ElseIf rsA("ResponseCode").Value = "MANUAL" Then
                                ResponseCode = "MANUAL"
                                Call InputRecordsetToDataset(rsA, "Manual", objOut, "MANUAL")
                            Else
                                ResponseCode = System.Convert.ToString(rsA("ResponseCode").Value)
                                Call InputRecordsetToDataset(rsA, "PaymentError", objOut, "PaymentError")
                            End If
                            clsRunCom = Nothing
                        Else
                            CreateErrorDataset(objOut, "200", "Infant over limit")
                        End If
                    End If
                End If

            Catch e As Exception
                Dim Serialization As New Helper.Serialization
                Mail.Send(ErrorHelper.RenderCcDebugMessage(ErrorId & " ERROR ", ResponseCode & " " & e.Message, "", "", "", XmlPayments, "", "", "", "", "", ""), ErrorId & " ERROR ", System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), Nothing)
            Finally

                Helper.Check.ClearRecordset(rsA)
                Helper.Check.ClearRecordset(rsVoucher)
                Helper.Check.ClearRecordset(rsPayments)
                Helper.Check.ClearRecordset(rsAllocation)
                Helper.Check.ClearRecordset(rsHeader)
                Helper.Check.ClearRecordset(rsRemarks)
                Helper.Check.ClearRecordset(rsServices)
                Helper.Check.ClearRecordset(rsTaxes)
                Helper.Check.ClearRecordset(rsQuotes)
            End Try

            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ValidCreditCardAdjust"),
    WebMethod(Description:="Adjust Valid CreditCard(",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ValidCreditCardAdjust(ByVal BookingId As String,
                                          ByVal XmlMapping As String,
                                          ByVal XmlFees As String,
                                          ByVal XmlPayments As String,
                                          ByVal XmlSegments As String,
                                          ByVal securityToken As String,
                                          ByVal authenticationToken As String,
                                          ByVal commerceIndicator As String,
                                          ByVal strRecordLocator As String,
                                          ByVal XmlPaymentFees As String) As DataSet
        Using objOut As New DataSet
            Dim Allocations As Allocations
            Dim ErrorSubject As String = String.Empty
            Dim ErrorId As String = String.Empty
            Dim ResponseCode As String = String.Empty
            Dim FabricateRecordset As New FabricateRecordset

            Dim rsAllocation As ADODB.Recordset = FabricateRecordset.FabricatePaymentAllocationRecordset
            Dim rsA As ADODB.Recordset = Nothing
            Dim rsVoucher As ADODB.Recordset = Nothing
            Dim rsPayments As ADODB.Recordset = Nothing

            Dim rsHeader As ADODB.Recordset = Nothing
            Dim rsRemarks As ADODB.Recordset = Nothing
            Dim rsServices As ADODB.Recordset = Nothing
            Dim rsTaxes As ADODB.Recordset = Nothing
            Dim rsQuotes As ADODB.Recordset = Nothing

            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim AgencyCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport
                    Dim AgencyCurrencyRcd As String = AgentHeader.AgencyCurrencyRcd
                    If (AuthenticatePassport(AgencyCode, strPassport)) Then
                        Dim Serialization As New Helper.Serialization


                        Dim clsRunCom As New clsRunComplus

                        Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
                        Dim rsSegments As ADODB.Recordset = Session("rsSegments")
                        Dim rsMappings As ADODB.Recordset = Session("rsMappings")
                        Dim rsFees As ADODB.Recordset = Session("rsFees")


                        Dim Itinerary As New Itinerary
                        Dim MappingsUpdate As New MappingsUpdate
                        Dim PaymentsUpdate As New PaymentsUpdate

                        Dim Status As Boolean = clsRunCom.GetEmpty(AgentHeader.AgencyCode, AgentHeader.AgencyCurrencyRcd, , , , , rsPayments)

                        Itinerary = Serialization.Deserialize(XmlSegments, Itinerary)
                        XmlPayments = XmlPayments.Replace("Payment", "PaymentUpdate")
                        PaymentsUpdate = Serialization.Deserialize(XmlPayments, PaymentsUpdate)

                        ErrorId = PaymentsUpdate(0).booking_payment_id.ToString

                        Dim DefaultCreateById As String

                        Dim Mappings As New Mappings
                        Dim Fees As New Fees
                        Dim iInfantCount As Integer

                        Dim TotalFees As New Fees

                        'Find Payment allocation
                        Mappings = Serialization.Deserialize(XmlMapping, Mappings)
                        Fees = Serialization.Deserialize(XmlFees, Fees)

                        'Find Payment Allocation.
                        If XmlPaymentFees.Length > 0 Then
                            TotalFees = Serialization.Deserialize(XmlPaymentFees, TotalFees)
                        End If

                        Allocations = GetPaymentAllocations(Mappings, Fees, True, TotalFees)
                        'Allocations = GetPaymentAllocations(Mappings, Fees)

                        XmlMapping = XmlMapping.Replace("Mapping", "MappingUpdate")
                        MappingsUpdate = Serialization.Deserialize(XmlMapping, MappingsUpdate)
                        Dim r As New Helper.RecordSet
                        With r
                            If Not Session("CreateById") Is Nothing AndAlso Not Session("CreateById") = String.Empty Then
                                DefaultCreateById = Session("CreateById")
                                .DefaultCreateById = New Guid(DefaultCreateById)
                            Else
                                If IsGuid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")) Then
                                    Session("CreateById") = System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")
                                End If
                                DefaultCreateById = Session("CreateById")
                                r.DefaultCreateById = New Guid(DefaultCreateById)
                            End If

                            .Fill(rsAllocation, Allocations, False)
                            .Fill(rsPayments, PaymentsUpdate)
                            .FillMapping(rsMappings, MappingsUpdate)
                            .FillFees(rsFees, Fees)

                            If .FeeChange(rsFees, Fees) Then
                                .FillCreateDate(rsFees)
                                .FillAccountBy(rsFees)
                            End If
                            If .SegmentChange(rsSegments, Itinerary) Then
                                .FillCreateDate(rsSegments)
                            End If
                        End With
                        rsPassengers.Filter = "passenger_type_rcd = 'INF'"
                        iInfantCount = rsPassengers.RecordCount
                        rsPassengers.Filter = 0
                        If InfantExcessLimit(rsSegments, iInfantCount) = False Then
                            rsA = clsRunCom.ValidCreditCard(rsPayments, rsSegments, rsAllocation, rsVoucher, rsMappings, securityToken, authenticationToken, commerceIndicator, strRecordLocator, rsFees)

                            If rsA Is Nothing Then
                                ResponseCode = "No rs*"
                            ElseIf rsA("ResponseCode").Value = "APPROVED" Then
                                ResponseCode = "APPROVED"

                                Call clsRunCom.GetBooking(BookingId, rsHeader, rsSegments, rsPassengers, rsRemarks, rsPayments, rsMappings, rsServices, rsTaxes, rsFees, rsQuotes)

                                Session("rsFees") = rsFees

                                Call InputRecordsetToDataset(rsA, "Payments", objOut)

                            ElseIf rsA("ResponseCode").Value = "MANUAL" Then
                                ResponseCode = "MANUAL"
                                Call InputRecordsetToDataset(rsA, "Manual", objOut, "MANUAL")
                            Else
                                ResponseCode = System.Convert.ToString(rsA("ResponseCode").Value)
                                Call InputRecordsetToDataset(rsA, "PaymentError", objOut, "PaymentError")
                            End If
                            clsRunCom = Nothing
                        Else
                            CreateErrorDataset(objOut, "200", "Infant over limit")
                        End If
                    End If
                End If

            Catch e As Exception
                Dim Serialization As New Helper.Serialization
                Mail.Send(ErrorHelper.RenderCcDebugMessage(ErrorId & " ERROR ", ResponseCode & " " & e.Message, "", "", "", XmlPayments, "", "", "", "", "", ""), ErrorId & " ERROR ", System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), Nothing)
            Finally

                Helper.Check.ClearRecordset(rsAllocation)
                Helper.Check.ClearRecordset(rsA)
                Helper.Check.ClearRecordset(rsVoucher)
                Helper.Check.ClearRecordset(rsPayments)

                Helper.Check.ClearRecordset(rsHeader)
                Helper.Check.ClearRecordset(rsRemarks)
                Helper.Check.ClearRecordset(rsServices)
                Helper.Check.ClearRecordset(rsTaxes)
                Helper.Check.ClearRecordset(rsQuotes)
            End Try

            If IsNothing(objOut) = False Then
                If objOut.Tables.Count > 0 AndAlso objOut.Tables(0).Rows.Count > 0 Then
                    Return objOut
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/CompleteRemark"),
WebMethod(Description:="Complete Remark",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function CompleteRemark(ByVal XmlRemarks As String, ByVal RemarkId As String, ByVal UserId As String) As Boolean
        Dim bResult As Boolean
        Dim rsRemark As New ADODB.Recordset
        rsRemark = Nothing
        Try
            If (AgentHeader Is Nothing) Then
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus

                    If RemarkId.Trim.Length = 0 Then
                        Dim Remarks As New Remarks
                        rsRemark = New ADODB.Recordset
                        With rsRemark
                            .Fields.Append("id", ADODB.DataTypeEnum.adVarChar, 100)
                            .Fields.Append("booking_remark_id", ADODB.DataTypeEnum.adVarChar, 100)
                            .Open()
                        End With
                        Dim DefaultCreateById As String

                        Remarks = Serialization.Deserialize(XmlRemarks, Remarks)
                        Dim r As New Helper.RecordSet
                        With r
                            If Not Session("CreateById") Is Nothing AndAlso Not Session("CreateById") = String.Empty Then
                                DefaultCreateById = Session("CreateById")
                                r.DefaultCreateById = New Guid(DefaultCreateById)
                            Else
                                If IsGuid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")) Then
                                    Session("CreateById") = System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")
                                End If
                                DefaultCreateById = Session("CreateById")
                                r.DefaultCreateById = New Guid(DefaultCreateById)
                            End If
                            .Fill(rsRemark, Remarks)
                        End With
                    End If
                    bResult = clsRunCom.CompleteRemark(rsRemark, RemarkId, UserId)
                    clsRunCom = Nothing
                End If
            End If
        Catch e As Exception
            bResult = False
        Finally
            Helper.Check.ClearRecordset(rsRemark)
        End Try

        Return bResult
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ReleaseFlightInventorySession"),
WebMethod(Description:="Get Client Bookings",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ReleaseFlightInventorySession(ByVal BookingId As String) As Boolean
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then

                    'Dim rsA As ADODB.Recordset
                    Dim clsRunCom As New clsRunComplus

                    Return clsRunCom.ReleaseFlightInventorySession(BookingId)

                    clsRunCom = Nothing
                End If
            End If
        Catch e As Exception
            Return False
        End Try
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetTicketSales"),
WebMethod(Description:="Get Ticket Sales",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetTicketSales(ByVal AgencyCode As String, ByVal UserId As String, ByVal Origin As String, ByVal Destination As String, ByVal Airline As String, ByVal FlightNumber As String, ByVal FlightFrom As Date, ByVal FlightTo As Date, ByVal TicketingFrom As Date, ByVal TicketingTo As Date, ByVal PassengerType As String, ByVal Language As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetTicketSales(AgencyCode, UserId, Origin, Destination, Airline, FlightNumber, FlightFrom, FlightTo, TicketingFrom, TicketingTo, PassengerType, Language)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "TicketSales", "TicketSale", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetBookings"),
WebMethod(Description:="Get Client Bookings",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetBookings(ByVal Airline As String, ByVal FlightNumber As String, ByVal FlightId As String, ByVal FlightFrom As Date, ByVal FlightTo As Date, ByVal RecordLocator As String, ByVal Origin As String, ByVal Destination As String, ByVal PassengerName As String, ByVal SeatNumber As String, ByVal TicketNumber As String, ByVal PhoneNumber As String, ByVal AgencyCode As String, ByVal ClientNumber As String, ByVal MemberNumber As String, ByVal ClientId As String, ByVal ShowHistory As Boolean, ByVal Language As String, ByVal bIndividual As Boolean, ByVal bGroup As Boolean, ByVal CreateFrom As Date, ByVal CreateTo As Date) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetBookings(Airline, FlightNumber, FlightId, FlightFrom, FlightTo, RecordLocator, Origin, Destination, PassengerName, SeatNumber, TicketNumber, PhoneNumber, AgencyCode, ClientNumber, MemberNumber, ClientId, ShowHistory, String.Empty, bIndividual, bGroup, CreateFrom, CreateTo)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "ClientBookings", "ClientBooking", objOut)
                    End If
                End If
            Catch e As Exception

            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function


    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/BooingLogon"),
WebMethod(Description:="Logon with RecordLocator",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function BookingLogon(ByRef RecordLocator As String, ByRef NameOrPhone As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport
                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.BookingLogon(RecordLocator, NameOrPhone)

                        Call InputRecordsetToDataset(rsA, "Booking", "Client", objOut)
                        clsRunCom = Nothing
                    End If
                End If
            Catch e As Exception

            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetClient"),
WebMethod(Description:="Logon with RecordLocator",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetClient(ByVal clientId As String, ByVal clientNumber As String, ByVal passengerId As String) As String
        Dim strResult As String = String.Empty

        Dim rsClient As ADODB.Recordset = Nothing
        Dim rsRemarks As ADODB.Recordset = Nothing
        Try
            If (AgentHeader Is Nothing) Then
                strResult = String.Empty
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport
                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus
                    If String.IsNullOrEmpty(clientId) = False AndAlso IsGuid(clientId) = False Then
                        clientId = String.Empty
                    ElseIf String.IsNullOrEmpty(passengerId) = False AndAlso IsGuid(passengerId) = False Then
                        passengerId = String.Empty
                    End If
                    Call clsRunCom.GetClient(clientId, clientNumber, passengerId, rsClient, rsRemarks)
                    strResult = RecordSetToXML(rsClient, "Booking", "Client")
                    clsRunCom = Nothing
                End If
            End If
        Catch e As Exception
            strResult = ""
        Finally
            Helper.Check.ClearRecordset(rsClient)
            Helper.Check.ClearRecordset(rsRemarks)
        End Try

        Return strResult
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/TravelAgentLogon"),
WebMethod(Description:="TravelAgentLogon Logon",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function TravelAgentLogon(ByVal agencyCode As String, ByVal agentLogon As String, ByVal agentPassword As String) As String
        Dim strResult As String = String.Empty
        Dim rs As ADODB.Recordset = Nothing
        Dim rsUser As ADODB.Recordset = Nothing
        Try
            If (AgentHeader Is Nothing) Then
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then

                    Dim clsRunCom As New clsRunComplus

                    rs = clsRunCom.TravelAgentLogon(agencyCode, agentLogon, agentPassword)
                    If rs.RecordCount > 0 Then

                        Dim UserID As String = rs("user_account_id").Value

                        rsUser = clsRunCom.UserRead(UserID)
                        Session("CreateById") = UserID.ToString
                        strResult = RecordSetToXML(rsUser, "UsersList", "UserList")
                        clsRunCom = Nothing
                    End If
                End If
            End If
        Catch e As Exception

        Finally
            Helper.Check.ClearRecordset(rs)
            Helper.Check.ClearRecordset(rsUser)
        End Try
        Return strResult
    End Function


    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ClientLogon"),
WebMethod(Description:="Client Logon",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ClientLogon(ByVal ClientNumber As String, ByVal ClientPassword As String) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.ClientLogon(ClientNumber, ClientPassword)
                        Call InputRecordsetToDataset(rsA, "Booking", "Client", objOut)

                        clsRunCom = Nothing
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetFormOfPayments"),
WebMethod(Description:="Return Form Of Payments",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetFormOfPayments(ByVal strLanguage As String) As String

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing

            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetFormOfPayments(strLanguage)

                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "GetFormOfPayments", objOut)
                    End If
                End If
            Catch e As Exception

            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            If Not objOut Is Nothing Then
                objOut.DataSetName = "FormOfPayments"
                Return objOut.GetXml()
            Else
                Return String.Empty
            End If
        End Using

    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetFormOfPaymentSubTypes"),
WebMethod(Description:="Return Form Of Payments",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetFormOfPaymentSubTypes(ByVal strType As String, ByVal strLanguage As String) As String
        Dim objOut As DataSet
        objOut = ReadFormOfPaymentSubTypes(strType, strLanguage)

        Return objOut.GetXml
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetPaymentSubTypes"),
WebMethod(Description:="Return Form Of Payments",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetPaymentSubTypes(ByVal strType As String, ByVal strLanguage As String) As DataSet
        Dim objOut As DataSet
        objOut = ReadFormOfPaymentSubTypes(strType, strLanguage)

        Return objOut
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetFormOfPaymentSubtypeFees"),
WebMethod(Description:="Return Form Of Payments",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetFormOfPaymentSubtypeFees(ByVal FormOfPayment As String, ByVal FormOfPaymentSubtype As String, ByVal CurrencyRcd As String, ByVal Agency As String, ByVal FeeDate As Date) As DataSet
        Dim rsA As ADODB.Recordset = Nothing
        Using objOut As New DataSet
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetFormOfPaymentSubtypeFees(FormOfPayment, FormOfPaymentSubtype, CurrencyRcd, Agency, FeeDate)

                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "Booking", "Fee", objOut)
                    End If
                End If
            Catch e As Exception
                Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(e, "Input : FormOfPayment " & FormOfPayment & "<br/>" &
                                                                                                                                ", FormOfPaymentSubtype " & FormOfPaymentSubtype & "<br/>" &
                                                                                                                                ", CurrencyRcd " & CurrencyRcd & "<br/>" &
                                                                                                                                ", Agency " & Agency & "<br/>" &
                                                                                                                                ", FeeDate " & FeeDate.ToString & "<br/>")
                Throw
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetAgencySessionProfile"),
WebMethod(Description:="Return Agency Session Profile",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetAgencySessionProfile(ByVal AgencyCode As String, ByVal UserAccountID As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport
                    If (AuthenticatePassport(strCode, strPassport)) Then
                        If String.IsNullOrEmpty(UserAccountID) = False AndAlso IsGuid(UserAccountID) = False Then
                        Else
                            Dim clsRunCom As New clsRunComplus
                            rsA = clsRunCom.GetAgencySessionProfile(AgencyCode, UserAccountID)
                            clsRunCom = Nothing
                            Call InputRecordsetToDataset(rsA, "TikAero", "Agent", objOut)
                        End If
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/PassengerTitels"),
WebMethod(Description:="Return Passenger Titels",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetPassengerTitles(ByVal strLanguage As String) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus
                        rsA = clsRunCom.GetPassengerTitles(strLanguage)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "PassengerTitels", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetDocumentType"),
WebMethod(Description:="Return Document Type",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetDocumentType(ByVal strLanguage As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetDocumentType(strLanguage)

                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "DocumentType", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetBooking"),
WebMethod(Description:="Return Booking Information",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetBooking(ByVal BookingId As String) As DataSet
        Using ds As New DataSet
            Dim rsHeaderClone As ADODB.Recordset = Nothing
            Dim rsSegmentsClone As ADODB.Recordset = Nothing
            Dim rsPassengersClone As ADODB.Recordset = Nothing
            Dim rsRemarksClone As ADODB.Recordset = Nothing
            Dim rsPaymentsClone As ADODB.Recordset = Nothing
            Dim rsMappingsClone As ADODB.Recordset = Nothing
            Dim rsServicesClone As ADODB.Recordset = Nothing
            Dim rsTaxesClone As ADODB.Recordset = Nothing
            Dim rsFeesClone As ADODB.Recordset = Nothing
            Dim rsQuotesClone As ADODB.Recordset = Nothing

            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport
                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus
                        Dim rsHeader As ADODB.Recordset = Session("rsHeader")
                        Dim rsSegments As ADODB.Recordset = Session("rsSegments")
                        Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
                        Dim rsRemarks As ADODB.Recordset = Session("rsRemarks")
                        Dim rsPayments As ADODB.Recordset = Session("rsPayments")
                        Dim rsMappings As ADODB.Recordset = Session("rsMappings")
                        Dim rsServices As ADODB.Recordset = Session("rsServices")
                        Dim rsTaxes As ADODB.Recordset = Session("rsTaxes")
                        Dim rsFees As ADODB.Recordset = Session("rsFees")
                        Dim rsQuotes As ADODB.Recordset = Session("rsQuotes")

                        'Clear Recordset before create new.
                        ClearBookingRecordset(rsHeader, rsSegments, rsPassengers, rsRemarks, rsPayments, rsMappings, rsServices, rsTaxes, rsFees, rsQuotes)

                        Call clsRunCom.GetBooking(BookingId, rsHeader, rsSegments, rsPassengers, rsRemarks, rsPayments, rsMappings, rsServices, rsTaxes, rsFees, rsQuotes)

                        rsHeaderClone = New ADODB.Recordset
                        rsSegmentsClone = New ADODB.Recordset
                        rsPassengersClone = New ADODB.Recordset
                        rsRemarksClone = New ADODB.Recordset
                        rsPaymentsClone = New ADODB.Recordset
                        rsMappingsClone = New ADODB.Recordset
                        rsServicesClone = New ADODB.Recordset
                        rsTaxesClone = New ADODB.Recordset
                        rsFeesClone = New ADODB.Recordset
                        rsQuotesClone = New ADODB.Recordset

                        Dim fr As New FabricateRecordset

                        fr.CopyAdoRecordset(rsHeader, rsHeaderClone)
                        Session("rsHeader") = rsHeader

                        fr.CopyAdoRecordset(rsSegments, rsSegmentsClone)
                        Session("rsSegments") = rsSegments

                        fr.CopyAdoRecordset(rsPassengers, rsPassengersClone)
                        Session("rsPassenger") = rsPassengers

                        fr.CopyAdoRecordset(rsRemarks, rsRemarksClone)
                        Session("rsRemarks") = rsRemarks

                        fr.CopyAdoRecordset(rsPayments, rsPaymentsClone)
                        Session("rsPayments") = rsPayments

                        fr.CopyAdoRecordset(rsMappings, rsMappingsClone)
                        Session("rsMappings") = rsMappings

                        fr.CopyAdoRecordset(rsServices, rsServicesClone)
                        Session("rsServices") = rsServices

                        fr.CopyAdoRecordset(rsTaxes, rsTaxesClone)
                        Session("rsTaxes") = rsTaxes

                        fr.CopyAdoRecordset(rsFees, rsFeesClone)
                        Session("rsFees") = rsFees

                        fr.CopyAdoRecordset(rsQuotes, rsQuotesClone)
                        Session("rsQuotes") = rsQuotes

                        CopyRecordsetsToDataset(rsHeaderClone, rsSegmentsClone, rsPassengersClone, rsRemarksClone, rsPaymentsClone, rsMappingsClone, rsTaxesClone, rsQuotesClone, rsFeesClone, rsServicesClone, ds)
                        Dim BookingState As New Booking
                        Serialization.FillBooking(ds, BookingState)
                        Session("BookingState") = BookingState
                    End If
                End If
            Catch e As Exception
            Finally
                ClearBookingRecordset(rsHeaderClone,
                                      rsSegmentsClone,
                                      rsPassengersClone,
                                      rsRemarksClone,
                                      rsPaymentsClone,
                                      rsMappingsClone,
                                      rsServicesClone,
                                      rsTaxesClone,
                                      rsFeesClone,
                                      rsQuotesClone)
            End Try
            Return ds
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/AddInfant"),
    WebMethod(Description:="Add Infant to current booking",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function AddInfant(ByVal strBookingID As String,
                              ByVal strAgencyCode As String,
                              ByVal GroupBooking As Boolean,
                              ByVal bNoVat As Boolean) As DataSet

        Using ds As New DataSet
            Dim rsSegmentsClone As ADODB.Recordset = Nothing
            Dim rsPassengersClone As ADODB.Recordset = Nothing
            Dim rsMappingsClone As ADODB.Recordset = Nothing
            Dim rsTaxesClone As ADODB.Recordset = Nothing
            Dim rsQuotesClone As ADODB.Recordset = Nothing
            Dim rsQuote As ADODB.Recordset = Nothing

            Try
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport
                Dim FabricateRecordset As New FabricateRecordset

                If (AuthenticatePassport(strCode, strPassport)) Then

                    Dim clsRunCom As New clsRunComplus

                    Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
                    Dim rsSegments As ADODB.Recordset = Session("rsSegments")
                    Dim rsMappings As ADODB.Recordset = Session("rsMappings")
                    Dim rsTaxes As ADODB.Recordset = Session("rsTaxes")
                    Dim rsServices As ADODB.Recordset = Session("rsServices")

                    Dim bSuccess As Boolean

                    bSuccess = clsRunCom.AddInfant(rsPassengers, rsSegments, rsMappings, rsTaxes, rsServices, rsQuote, "{" & strBookingID & "}", strAgencyCode, , GroupBooking, bNoVat)

                    rsSegmentsClone = New ADODB.Recordset
                    rsPassengersClone = New ADODB.Recordset
                    rsMappingsClone = New ADODB.Recordset
                    rsTaxesClone = New ADODB.Recordset
                    rsQuotesClone = New ADODB.Recordset

                    Dim fr As New FabricateRecordset

                    fr.CopyAdoRecordset(rsSegments, rsSegmentsClone)
                    Session("rsSegments") = rsSegments

                    fr.CopyAdoRecordset(rsPassengers, rsPassengersClone)
                    Session("rsPassenger") = rsPassengers

                    fr.CopyAdoRecordset(rsMappings, rsMappingsClone)
                    Session("rsMappings") = rsMappings

                    Session("rsServices") = rsServices

                    fr.CopyAdoRecordset(rsTaxes, rsTaxesClone)
                    Session("rsTaxes") = rsTaxes

                    Call CopyRecordsetsToDataset(Nothing, rsSegmentsClone, rsPassengersClone, Nothing, Nothing, rsMappingsClone, rsTaxesClone, rsQuote, Nothing, Nothing, ds)

                    Dim Booking As New Booking
                    Serialization.FillBooking(ds, Booking)
                    Session("BookingState") = Booking
                    clsRunCom = Nothing
                End If
            Catch ex As Exception

            Finally
                Helper.Check.ClearRecordset(rsSegmentsClone)
                Helper.Check.ClearRecordset(rsPassengersClone)
                Helper.Check.ClearRecordset(rsMappingsClone)
                Helper.Check.ClearRecordset(rsTaxesClone)
                Helper.Check.ClearRecordset(rsQuotesClone)
                Helper.Check.ClearRecordset(rsQuote)
            End Try
            Return ds
        End Using
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/CalculateBookingCreateFees"),
WebMethod(Description:="Calculate booking create fee",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function CalculateBookingCreateFees(ByVal AgencyCode As String,
                                            ByVal strCurrency As String,
                                            ByVal strBookingId As String,
                                            ByVal XmlHeader As String,
                                            ByVal XmlSegments As String,
                                            ByVal XmlPassengers As String,
                                            ByVal XmlFees As String,
                                            ByVal XmlRemarks As String,
                                            ByVal XmlPayments As String,
                                            ByVal XmlMapping As String,
                                            ByVal XMLServices As String,
                                            ByVal XmlTaxes As String,
                                            ByVal strLanguage As String,
                                            ByVal bNoVat As Boolean) As String

        Return CalculateFees(AgencyCode,
                             strCurrency,
                             strBookingId,
                             XmlHeader,
                             XmlSegments,
                             XmlPassengers,
                             XmlFees,
                             XmlRemarks,
                             XmlPayments,
                             XmlMapping,
                             XMLServices,
                             XmlTaxes,
                             "CREATEBOOKING",
                             strLanguage,
                             bNoVat)

    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/CalculateBookingChangeFees"),
WebMethod(Description:="Calculate booking change fee",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function CalculateBookingChangeFees(ByVal AgencyCode As String,
                                            ByVal strCurrency As String,
                                            ByVal strBookingId As String,
                                            ByVal XmlHeader As String,
                                            ByVal XmlSegments As String,
                                            ByVal XmlPassengers As String,
                                            ByVal XmlFees As String,
                                            ByVal XmlRemarks As String,
                                            ByVal XmlPayments As String,
                                            ByVal XmlMapping As String,
                                            ByVal XMLServices As String,
                                            ByVal XmlTaxes As String,
                                            ByVal strLanguage As String,
                                            ByVal bNoVat As Boolean) As String
        Return CalculateFees(AgencyCode,
                             strCurrency,
                             strBookingId,
                             XmlHeader,
                             XmlSegments,
                             XmlPassengers,
                             XmlFees,
                             XmlRemarks,
                             XmlPayments,
                             XmlMapping,
                             XMLServices,
                             XmlTaxes,
                             "UPDATEBOOKING",
                             strLanguage,
                             bNoVat)

    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/CalculateNameChangeFees"),
WebMethod(Description:="Calculate name change fee",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function CalculateNameChangeFees(ByVal AgencyCode As String,
                                        ByVal strCurrency As String,
                                        ByVal strBookingId As String,
                                        ByVal XmlHeader As String,
                                        ByVal XmlSegments As String,
                                        ByVal XmlPassengers As String,
                                        ByVal XmlFees As String,
                                        ByVal XmlRemarks As String,
                                        ByVal XmlPayments As String,
                                        ByVal XmlMapping As String,
                                        ByVal XMLServices As String,
                                        ByVal XmlTaxes As String,
                                        ByVal bBookingCreate As Boolean,
                                        ByVal bBookingChange As Boolean,
                                        ByVal bNameChange As Boolean,
                                        ByVal bSeatAssign As Boolean,
                                        ByVal bSpecialService As Boolean,
                                        ByVal strLanguage As String,
                                        ByVal bNoVat As Boolean) As String

        Return CalculateFees(AgencyCode,
                             strCurrency,
                             strBookingId,
                             XmlHeader,
                             XmlSegments,
                             XmlPassengers,
                             XmlFees,
                             XmlRemarks,
                             XmlPayments,
                             XmlMapping,
                             XMLServices,
                             XmlTaxes,
                             "NAMECHANGE",
                             strLanguage,
                             bNoVat)
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/CalculateSeatAssignmentFees"),
WebMethod(Description:="Calculate seat assignment fee",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function CalculateSeatAssignmentFees(ByVal AgencyCode As String,
                                            ByVal strCurrency As String,
                                            ByVal strBookingId As String,
                                            ByVal XmlHeader As String,
                                            ByVal XmlSegments As String,
                                            ByVal XmlPassengers As String,
                                            ByVal XmlFees As String,
                                            ByVal XmlRemarks As String,
                                            ByVal XmlPayments As String,
                                            ByVal XmlMapping As String,
                                            ByVal XMLServices As String,
                                            ByVal XmlTaxes As String,
                                            ByVal strLanguage As String,
                                            ByVal bNoVat As Boolean) As String

        Return CalculateFees(AgencyCode,
                             strCurrency,
                             strBookingId,
                             XmlHeader,
                             XmlSegments,
                             XmlPassengers,
                             XmlFees,
                             XmlRemarks,
                             XmlPayments,
                             XmlMapping,
                             XMLServices,
                             XmlTaxes,
                             "SEATASSIGN",
                             strLanguage,
                             bNoVat)
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/CalculateSpecialServiceFees"),
WebMethod(Description:="Calculate Fee for special service",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function CalculateSpecialServiceFees(ByVal AgencyCode As String,
                                            ByVal strCurrency As String,
                                            ByVal strBookingId As String,
                                            ByVal XmlHeader As String,
                                            ByVal XMLServices As String,
                                            ByVal XmlFees As String,
                                            ByVal XmlRemarks As String,
                                            ByVal XmlMapping As String,
                                            ByVal strLanguage As String,
                                            ByVal bNoVat As Boolean) As String

        Return CalculateFees(AgencyCode,
                             strCurrency,
                             strBookingId,
                             XmlHeader,
                             String.Empty,
                             String.Empty,
                             XmlFees,
                             XmlRemarks,
                             String.Empty,
                             XmlMapping,
                             XMLServices,
                             String.Empty,
                             "SSR",
                             strLanguage,
                             bNoVat)
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/CalculateNewFees"),
WebMethod(Description:="Calculate Fee",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function CalculateNewFees(ByVal strBookingId As String,
                                ByVal AgencyCode As String,
                                ByVal XmlHeader As String,
                                ByVal XmlSegments As String,
                                ByVal XmlPassengers As String,
                                ByVal XmlPayments As String,
                                ByVal XmlRemarks As String,
                                ByVal XmlMapping As String,
                                ByVal XmlFees As String,
                                ByVal XmlTaxes As String,
                                ByVal XmlQuotes As String,
                                ByVal XMLServices As String,
                                ByVal strCurrency As String,
                                ByVal bBookingCreate As Boolean,
                                ByVal bBookingChange As Boolean,
                                ByVal bNameChange As Boolean,
                                ByVal bSeatAssign As Boolean,
                                ByVal strLanguage As String,
                                ByVal bNoVat As Boolean) As String

        Dim strCalType As String = String.Empty

        If bBookingCreate = True Then
            strCalType = "CREATEBOOKING"
        ElseIf bBookingChange = True Then
            strCalType = "UPDATEBOOKING"
        ElseIf bNameChange = True Then
            strCalType = "NAMECHANGE"
        ElseIf bSeatAssign = True Then
            strCalType = "SEATASSIGN"
        End If

        Return CalculateFees(AgencyCode,
                             strCurrency,
                             strBookingId,
                             XmlHeader,
                             XmlSegments,
                             XmlPassengers,
                             XmlFees,
                             XmlRemarks,
                             XmlPayments,
                             XmlMapping,
                             XMLServices,
                             XmlTaxes,
                             strCalType,
                             strLanguage,
                             bNoVat)
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/TikSqlDBWebServices/TestWebService"),
      WebMethod(Description:="Public Function TestWebService(ByVal TestReference As String) As String",
      EnableSession:=True)>
    Public Function TestWebService(ByVal TestReference As String) As String
        Dim ADODBRecordset As ADODB.Recordset = Nothing
        Dim VersionApplication As String = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString()
        Dim Test As String = ""
        Dim TestReturn() As String = {"", "", "", "", ""}
        Dim TikProcessBooking As tikAeroProcess.Booking = Nothing
        TestReturn(0) = "Webservice Version: " & VersionApplication
        Try
            Test = "TikProcess"
            If TikProcessBooking Is Nothing Then
                If System.Configuration.ConfigurationManager.AppSettings("ComServer") <> "" Then
                    TikProcessBooking = CreateObject("tikAeroProcess.Booking", System.Configuration.ConfigurationManager.AppSettings("ComServer"))
                Else
                    TikProcessBooking = New tikAeroProcess.Booking
                End If
            End If

            TikProcessBooking.GetEmpty(ADODBRecordset)

            If ADODBRecordset Is Nothing Then
                TestReturn(1) = "Call TikProcessBooking: " & "Nothing"
            ElseIf ADODBRecordset.EOF Then
                TestReturn(1) = "Call TikProcessBooking: " & "EOF"
            ElseIf ADODBRecordset.BOF Then
                TestReturn(1) = "Call TikProcessBooking: " & "BOF"
            Else
                TestReturn(1) = "Call TikProcessBooking: " & ADODBRecordset.RecordCount & "Rows "
            End If

            Test = "ServerName"
            TestReturn(2) = "Read ServerName : " & System.Configuration.ConfigurationManager.AppSettings("ComServer")


            Return Join(TestReturn, "; ")
        Catch e As Exception
            Return "Error" & e.Message & " On: " & Test & " " & Join(TestReturn, ";")
        Finally
            If Not ADODBRecordset Is Nothing Then
                ADODBRecordset = Nothing
            End If
        End Try
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetItinerary"),
WebMethod(Description:="Get Itinerary as XML",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetItinerary(ByVal BookingId As String, ByVal PassengerId As String, ByVal strLanguageCode As String) As String
        Dim ItineraryXml As String = String.Empty
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport
                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus
                    ItineraryXml = clsRunCom.GetXml(BookingId, PassengerId, strLanguageCode)
                End If
            End If
        Catch e As Exception
            Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(e, BookingId)
        End Try
        Return ItineraryXml
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetSeatMap"),
WebMethod(Description:="Return Seatmaps",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetSeatMap(ByVal Origin As String, ByVal Destination As String, ByVal FlightId As String, ByVal BoardingClass As String, ByVal BookingClass As String, ByVal strLanguage As String) As DataSet
        Using ds As New DataSet
            Dim rs As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus

                        rs = clsRunCom.GetSeatMap(Origin, Destination, FlightId, BoardingClass, BookingClass, strLanguage)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rs, "SeatMap", ds)
                    End If
                End If
            Catch e As Exception
                ds.Namespace = e.Message
            Finally
                Helper.Check.ClearRecordset(rs)
            End Try
            Return ds
        End Using
    End Function


    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetSeatMapLayout"),
WebMethod(Description:="Return Seatmap Layout",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetSeatMapLayout(ByVal strFlightId As String,
                                     ByVal strOrigin As String,
                                     ByVal strDestination As String,
                                     ByVal strBoardingClass As String,
                                     ByVal strConfiguration As String,
                                     ByVal strLanguage As String) As DataSet
        Using ds As New DataSet
            Dim bResult As Boolean
            Dim rsCompartment As ADODB.Recordset = Nothing
            Dim rsSeatmap As ADODB.Recordset = Nothing
            Dim rsAttribute As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus
                        bResult = clsRunCom.GetSeatMapLayout(strFlightId,
                                                             strOrigin,
                                                             strDestination,
                                                             strBoardingClass,
                                                             strConfiguration,
                                                             strLanguage,
                                                             rsCompartment,
                                                             rsSeatmap,
                                                             rsAttribute)
                        clsRunCom = Nothing

                        Call InputRecordsetToDataset(rsCompartment, "Compartment", ds)
                        Call InputRecordsetToDataset(rsSeatmap, "Seatmap", ds)
                        Call InputRecordsetToDataset(rsAttribute, "Attribute", ds)
                    End If
                End If
            Catch e As Exception
                ds.Namespace = e.Message
            Finally
                Helper.Check.ClearRecordset(rsCompartment)
                Helper.Check.ClearRecordset(rsSeatmap)
                Helper.Check.ClearRecordset(rsAttribute)
            End Try
            Return ds
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetPassengerDetails"),
WebMethod(Description:="GetPassengerDetails(",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetPassengerDetails(ByVal PassengerId As String,
                                    ByVal BookingSegmentId As String,
                                    ByVal FlightId As String,
                                    ByVal xmlCheckinPassengers As String,
                                    ByVal bPassenger As Boolean,
                                    ByVal bRemarks As Boolean,
                                    ByVal bServices As Boolean,
                                    ByVal bBaggage As Boolean,
                                    ByVal bSegment As Boolean,
                                    ByVal bFee As Boolean,
                                    ByVal bBookingDetails As Boolean,
                                    ByVal LanguageCode As String,
                                    ByVal strOrigin As String) As String
        Using ds As New DataSet

            Dim rsCheckinPassengers As ADODB.Recordset = Nothing
            Dim rsSegmentsClone As ADODB.Recordset = Nothing
            Dim rsPassengersClone As ADODB.Recordset = Nothing
            Dim rsRemarksClone As ADODB.Recordset = Nothing
            Dim rsMappingsClone As ADODB.Recordset = Nothing
            Dim rsServicesClone As ADODB.Recordset = Nothing
            Dim rsFeesClone As ADODB.Recordset = Nothing
            Dim rsBaggage As ADODB.Recordset = Nothing

            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim FabricateRecordset As New FabricateRecordset
                        rsCheckinPassengers = FabricateRecordset.FabricateMappingRecordset

                        Dim r As New Helper.RecordSet
                        Dim RecordSet As New Helper.RecordSet
                        With RecordSet
                            Dim CheckInPassengers As New CheckInPassengers
                            Serialization.AddCheckInPassenger(xmlCheckinPassengers, CheckInPassengers)
                            .Fill(rsCheckinPassengers, CheckInPassengers, False)
                        End With

                        Dim clsRunCom As New clsRunComplus
                        Dim rsMappings As ADODB.Recordset = Nothing
                        Dim rsSegments As ADODB.Recordset = Nothing
                        Dim rsPassengers As ADODB.Recordset = Nothing
                        Dim rsRemarks As ADODB.Recordset = Nothing
                        Dim rsServices As ADODB.Recordset = Nothing
                        Dim rsFees As ADODB.Recordset = Nothing

                        rsMappings = clsRunCom.GetPassengerDetails(PassengerId,
                                                                   BookingSegmentId,
                                                                   FlightId,
                                                                   rsCheckinPassengers,
                                                                   rsPassengers,
                                                                   rsRemarks,
                                                                   rsServices,
                                                                   rsBaggage,
                                                                   rsSegments,
                                                                   rsFees,
                                                                   bPassenger,
                                                                   bRemarks,
                                                                   bServices,
                                                                   bBaggage,
                                                                   bSegment,
                                                                   bFee,
                                                                   bBookingDetails,
                                                                   LanguageCode,
                                                                   strOrigin)
                        clsRunCom = Nothing

                        rsSegmentsClone = New ADODB.Recordset
                        rsPassengersClone = New ADODB.Recordset
                        rsRemarksClone = New ADODB.Recordset
                        rsMappingsClone = New ADODB.Recordset
                        rsServicesClone = New ADODB.Recordset
                        rsFeesClone = New ADODB.Recordset

                        Dim fr As New FabricateRecordset

                        fr.CopyAdoRecordset(rsSegments, rsSegmentsClone)
                        Session("rsSegments") = rsSegments

                        fr.CopyAdoRecordset(rsPassengers, rsPassengersClone)
                        Session("rsPassenger") = rsPassengers

                        fr.CopyAdoRecordset(rsRemarks, rsRemarksClone)
                        Session("rsRemarks") = rsRemarks

                        fr.CopyAdoRecordset(rsMappings, rsMappingsClone)
                        Session("rsMappings") = rsMappings

                        fr.CopyAdoRecordset(rsServices, rsServicesClone)
                        Session("rsServices") = rsServices

                        fr.CopyAdoRecordset(rsFees, rsFeesClone)
                        Session("rsFees") = rsFees

                        CopyRecordsetsToDataset(Nothing,
                                                rsSegmentsClone,
                                                rsPassengersClone,
                                                rsRemarksClone,
                                                Nothing,
                                                rsMappingsClone,
                                                Nothing,
                                                Nothing,
                                                rsFeesClone,
                                                rsServicesClone,
                                                ds)
                    End If
                End If
            Catch e As Exception
                If System.Configuration.ConfigurationManager.AppSettings("ErrorMailEnabled").ToUpper = "TRUE" Then
                    Dim ErrorSubject As String = System.Configuration.ConfigurationManager.AppSettings("ErrorSubject") & System.Convert.ToString(Date.Now)
                    'Mail.Send(ErrorHelper.RenderDebugMessage("CheckInSave", e.Message, "", xmlBookingSegment, xmlPassengers, "", xmlRemarks, XmlMapping, xmlFee, "", "", ""), ErrorSubject, System.Configuration.ConfigurationSettings.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationSettings.AppSettings("ErrorSenderEmail"), Nothing)
                End If
            Finally
                Helper.Check.ClearRecordset(rsSegmentsClone)
                Helper.Check.ClearRecordset(rsPassengersClone)
                Helper.Check.ClearRecordset(rsRemarksClone)
                Helper.Check.ClearRecordset(rsMappingsClone)
                Helper.Check.ClearRecordset(rsServicesClone)
                Helper.Check.ClearRecordset(rsFeesClone)
                Helper.Check.ClearRecordset(rsCheckinPassengers)
                Helper.Check.ClearRecordset(rsBaggage)
            End Try
            If IsNothing(ds) = False Then
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count Then
                    Return ds.GetXml
                Else
                    Return String.Empty
                End If
            Else
                Return String.Empty
            End If
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/CheckInSave"),
WebMethod(Description:="CheckInSave",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function CheckInSave(ByVal xmlMapping As String,
                            ByVal xmlBaggageTag As String,
                            ByVal xmlSeatAssignment As String,
                            ByVal xmlPassengers As String,
                            ByVal xmlService As String,
                            ByVal xmlRemarks As String,
                            ByVal xmlBookingSegment As String,
                            ByVal xmlFee As String) As Boolean

        Dim bResult As Boolean
        Dim rsBaggageTag As ADODB.Recordset = Nothing
        Dim rsSeatAssignment As ADODB.Recordset = Nothing
        Dim rsSegment As ADODB.Recordset = Nothing
        Dim rsPassengers As ADODB.Recordset = Nothing
        Dim rsRemarks As ADODB.Recordset = Nothing
        Dim rsServices As ADODB.Recordset = Nothing
        Dim rsFees As ADODB.Recordset = Nothing

        Try
            If (AgentHeader Is Nothing) Then
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then

                    Dim rsMappping As ADODB.Recordset = Session("rsMappings")

                    Dim r As New Helper.RecordSet
                    Dim DefaultCreateById As String

                    Dim Mappings As New Mappings
                    Dim Passengers As New Passengers
                    Dim Remarks As New Remarks
                    Dim MappingsUpdate As New MappingsUpdate
                    Dim Fees As New Fees

                    xmlMapping = xmlMapping.Replace("Mapping", "MappingUpdate")
                    MappingsUpdate = Serialization.Deserialize(xmlMapping, MappingsUpdate)

                    With r
                        If Not Session("CreateById") Is Nothing AndAlso Not Session("CreateById") = String.Empty Then
                            DefaultCreateById = Session("CreateById")
                            r.DefaultCreateById = New Guid(DefaultCreateById)
                        Else
                            If IsGuid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")) Then
                                Session("CreateById") = System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")
                            End If
                            DefaultCreateById = Session("CreateById")
                            r.DefaultCreateById = New Guid(DefaultCreateById)
                        End If

                        'Fill Check-in informatin.
                        If .UpdateMapping(rsMappping, MappingsUpdate) = True Then
                            Dim clsRunCom As New clsRunComplus

                            bResult = clsRunCom.CheckInSave(rsMappping, rsBaggageTag, rsSeatAssignment, rsPassengers, rsServices, rsRemarks, rsSegment, rsFees, True)

                            clsRunCom = Nothing
                        End If
                    End With
                End If
            End If
        Catch e As Exception
            If System.Configuration.ConfigurationManager.AppSettings("ErrorMailEnabled").ToUpper = "TRUE" Then
                Dim ErrorSubject As String = System.Configuration.ConfigurationManager.AppSettings("ErrorSubject") & System.Convert.ToString(Date.Now)
                Mail.Send(ErrorHelper.RenderDebugMessage("CheckInSave", e.Message, "", xmlBookingSegment, xmlPassengers, "", xmlRemarks, xmlMapping, xmlFee, "", "", ""), ErrorSubject, System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), Nothing)
            End If
            bResult = False
        Finally
            Helper.Check.ClearRecordset(rsBaggageTag)
            Helper.Check.ClearRecordset(rsSeatAssignment)
            Helper.Check.ClearRecordset(rsSegment)
            Helper.Check.ClearRecordset(rsPassengers)
            Helper.Check.ClearRecordset(rsRemarks)
            Helper.Check.ClearRecordset(rsServices)
            Helper.Check.ClearRecordset(rsFees)
        End Try

        Return bResult
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/SaveBookingHeader"),
WebMethod(Description:="SaveBookingHeader",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function SaveBookingHeader(ByVal xmlBookingHeader As String) As Boolean

        Dim bResult As Boolean
        Dim rsBookingHeader As ADODB.Recordset = Session("rsHeader")

        Try
            If (AgentHeader Is Nothing) Then
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then

                    Dim rsMappping As ADODB.Recordset = Session("rsMappings")

                    Dim r As New Helper.RecordSet
                    Dim DefaultCreateById As String

                    Dim BookingHeader As New BookingHeader
                    Dim BookingHeaderUpdate As New BookingHeaderUpdate

                    xmlBookingHeader = xmlBookingHeader.Replace("BookingHeader", "BookingHeaderUpdate")
                    BookingHeaderUpdate = Serialization.Deserialize(xmlBookingHeader, BookingHeaderUpdate)

                    With r
                        If Not Session("CreateById") Is Nothing AndAlso Not Session("CreateById") = String.Empty Then
                            DefaultCreateById = Session("CreateById")
                            r.DefaultCreateById = New Guid(DefaultCreateById)
                        Else
                            If IsGuid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")) Then
                                Session("CreateById") = System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")
                            End If
                            DefaultCreateById = Session("CreateById")
                            r.DefaultCreateById = New Guid(DefaultCreateById)
                        End If

                        If .UpdateBookingHeader(rsBookingHeader, BookingHeaderUpdate) = True Then
                            Dim clsRunCom As New clsRunComplus
                            clsRunCom.SaveBookingHeader(rsBookingHeader)
                            GetBooking(BookingHeaderUpdate.booking_id.ToString())
                            clsRunCom = Nothing
                            bResult = True
                        End If
                    End With
                End If
            End If
        Catch e As Exception
            If System.Configuration.ConfigurationManager.AppSettings("ErrorMailEnabled").ToUpper = "TRUE" Then
                Dim ErrorSubject As String = System.Configuration.ConfigurationManager.AppSettings("ErrorSubject") & System.Convert.ToString(Date.Now)
                Mail.Send(ErrorHelper.RenderDebugMessage("SaveBookingHeader", e.Message, xmlBookingHeader, "", "", "", "", "", "", "", "", ""), ErrorSubject, System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), Nothing)
            End If
            bResult = False
        Finally
        End Try

        Return bResult
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetCountrys"),
    WebMethod(Description:="Return Countrys",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetCountry(ByVal strLanguage As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetCountry(strLanguage)

                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "Countrys", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetLanguages"),
WebMethod(Description:="Return Booking Languages",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetLanguages(ByVal strLanguage As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus
                        rsA = clsRunCom.GetLanguages(strLanguage)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "Languages", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetCurrencies"),
WebMethod(Description:="Return Currencies",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetCurrencies(ByVal strLanguage As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetCurrencies(strLanguage)

                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "Currencies", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetRemarkTypes"),
    WebMethod(Description:="Return GetRemarkTypes",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetRemarkTypes(ByVal Language As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetRemarkTypes(Language)

                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "RemarkTypes", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/PaymentSave"),
    WebMethod(Description:="Return PaymentSave ",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function PaymentSave(ByVal BookingId As String, ByVal XmlMapping As String, ByVal XmlFees As String, ByVal XmlPayments As String) As Boolean
        Using objOut As New DataSet
            Dim bResult As Boolean
            Dim Allocations As Allocations = Nothing
            Dim FabricateRecordset As New FabricateRecordset

            Dim rsAllocation As ADODB.Recordset = FabricateRecordset.FabricatePaymentAllocationRecordset
            Dim rsPayments As ADODB.Recordset = Nothing
            Dim rsHeader As ADODB.Recordset = Nothing
            Dim rsRemarks As ADODB.Recordset = Nothing
            Dim rsMappings As ADODB.Recordset = Nothing
            Dim rsTaxes As ADODB.Recordset = Nothing
            Dim rsQuotes As ADODB.Recordset = Nothing

            Try
                If (AgentHeader Is Nothing) Then
                    bResult = False
                Else

                    Dim AgencyCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport
                    Dim AgencyCurrencyRcd As String = AgentHeader.AgencyCurrencyRcd

                    If (AuthenticatePassport(AgencyCode, strPassport)) Then

                        Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
                        Dim rsSegments As ADODB.Recordset = Session("rsSegments")
                        Dim rsFees As ADODB.Recordset = Session("rsFees")

                        Dim r As New Helper.RecordSet
                        Dim clsRunCom As New clsRunComplus

                        Dim Status As Boolean = clsRunCom.GetEmpty(AgentHeader.AgencyCode, AgentHeader.AgencyCurrencyRcd, , , , , rsPayments)
                        Dim PaymentStatus As Boolean

                        Dim PaymentsUpdate As New PaymentsUpdate
                        Dim Mappings As New Mappings
                        Dim Fees As New Fees

                        Dim DefaultCreateById As String
                        Dim iInfantCount As Integer

                        XmlPayments = XmlPayments.Replace("Payment", "PaymentUpdate")

                        Mappings = Serialization.Deserialize(XmlMapping, Mappings)
                        PaymentsUpdate = Serialization.Deserialize(XmlPayments, PaymentsUpdate)
                        Fees = Serialization.Deserialize(XmlFees, Fees)
                        If Not PaymentsUpdate Is Nothing AndAlso Not PaymentsUpdate(0).receive_payment_amount <= 0 Then

                            If Not Session("CreateById") Is Nothing AndAlso Not Session("CreateById") = String.Empty Then
                                DefaultCreateById = Session("CreateById")
                                r.DefaultCreateById = New Guid(DefaultCreateById)
                            Else
                                If IsGuid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")) Then
                                    Session("CreateById") = System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")
                                End If
                                DefaultCreateById = Session("CreateById")
                                r.DefaultCreateById = New Guid(DefaultCreateById)
                            End If

                            Allocations = GetPaymentAllocations(Mappings, Fees, True, Nothing)
                            r.Fill(rsAllocation, Allocations, False)
                            r.Fill(rsPayments, PaymentsUpdate)
                            rsPassengers.Filter = "passenger_type_rcd = 'INF'"
                            iInfantCount = rsPassengers.RecordCount
                            rsPassengers.Filter = 0

                            If InfantExcessLimit(rsSegments, iInfantCount) = False Then
                                PaymentStatus = clsRunCom.PaymentSave(rsPayments, rsAllocation, "")

                                If PaymentStatus Then
                                    rsHeader = New ADODB.Recordset
                                    rsRemarks = New ADODB.Recordset
                                    rsMappings = New ADODB.Recordset
                                    rsTaxes = New ADODB.Recordset
                                    rsQuotes = New ADODB.Recordset

                                    Call clsRunCom.GetBooking(BookingId,
                                                              rsHeader,
                                                              rsSegments,
                                                              rsPassengers,
                                                              rsRemarks,
                                                              rsPayments,
                                                              rsMappings, ,
                                                              rsTaxes,
                                                              rsFees,
                                                              rsQuotes)

                                    Session("rsFees") = rsFees

                                    FabricateRecordset = Nothing
                                    bResult = True
                                Else
                                    bResult = False
                                End If
                            Else
                                bResult = False
                            End If
                        Else
                            bResult = True
                        End If
                    End If
                End If
            Catch e As Exception
                Dim Serialization As New Helper.Serialization
                If System.Configuration.ConfigurationManager.AppSettings("ErrorMailEnabled").ToUpper = "TRUE" Then
                    Dim ErrorSubject As String = System.Configuration.ConfigurationManager.AppSettings("ErrorSubject") & System.Convert.ToString(Date.Now)
                    Mail.Send(ErrorHelper.RenderDebugMessage("PaymentSave", e.Message, "", "", "", XmlPayments, "", XmlMapping, XmlFees, "", "", Serialization.Serialize(Allocations)), ErrorSubject, System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), Nothing)
                End If
                bResult = False
            Finally
                Helper.Check.ClearRecordset(rsAllocation)
                Helper.Check.ClearRecordset(rsPayments)
                Helper.Check.ClearRecordset(rsHeader)
                Helper.Check.ClearRecordset(rsRemarks)
                Helper.Check.ClearRecordset(rsMappings)
                Helper.Check.ClearRecordset(rsTaxes)
                Helper.Check.ClearRecordset(rsQuotes)
            End Try

            Return bResult
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/PaymentSaveAdjust"),
    WebMethod(Description:="Return Adjusted PaymentSave",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function PaymentSaveAdjust(ByVal BookingId As String,
                                      ByVal XmlMapping As String,
                                      ByVal XmlFees As String,
                                      ByVal XmlPayments As String,
                                      ByVal XmlPaymentFees As String) As Boolean

        Dim bResult As Boolean
        Dim Allocations As Allocations = Nothing

        Dim FabricateRecordset As New FabricateRecordset

        Dim rsAllocation As ADODB.Recordset = FabricateRecordset.FabricatePaymentAllocationRecordset
        Dim rsHeader As ADODB.Recordset = Nothing
        Dim rsRemarks As ADODB.Recordset = Nothing
        Dim rsMappings As ADODB.Recordset = Nothing
        Dim rsTaxes As ADODB.Recordset = Nothing
        Dim rsQuotes As ADODB.Recordset = Nothing
        Dim rsPayments As ADODB.Recordset = Nothing

        Try
            If (AgentHeader Is Nothing) Then
                bResult = False
            Else

                Dim AgencyCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport
                Dim AgencyCurrencyRcd As String = AgentHeader.AgencyCurrencyRcd

                If (AuthenticatePassport(AgencyCode, strPassport)) Then
                    Dim r As New Helper.RecordSet

                    Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
                    Dim rsSegments As ADODB.Recordset = Session("rsSegments")
                    Dim rsFees As ADODB.Recordset = Session("rsFees")

                    Dim clsRunCom As New clsRunComplus

                    rsPayments = New ADODB.Recordset
                    Dim Status As Boolean = clsRunCom.GetEmpty(AgentHeader.AgencyCode, AgentHeader.AgencyCurrencyRcd, , , , , rsPayments)

                    Dim PaymentStatus As Boolean

                    Dim PaymentsUpdate As New PaymentsUpdate
                    Dim Mappings As New Mappings
                    Dim Fees As New Fees
                    Dim TotalFees As New Fees

                    Dim DefaultCreateById As String
                    Dim iInfantCount As Integer

                    XmlPayments = XmlPayments.Replace("Payment", "PaymentUpdate")

                    Mappings = Serialization.Deserialize(XmlMapping, Mappings)
                    PaymentsUpdate = Serialization.Deserialize(XmlPayments, PaymentsUpdate)
                    Fees = Serialization.Deserialize(XmlFees, Fees)
                    If Not PaymentsUpdate Is Nothing AndAlso Not PaymentsUpdate(0).receive_payment_amount <= 0 Then

                        If Not Session("CreateById") Is Nothing AndAlso Not Session("CreateById") = String.Empty Then
                            DefaultCreateById = Session("CreateById")
                            r.DefaultCreateById = New Guid(DefaultCreateById)
                        Else
                            If IsGuid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")) Then
                                Session("CreateById") = System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")
                            End If
                            DefaultCreateById = Session("CreateById")
                            r.DefaultCreateById = New Guid(DefaultCreateById)
                        End If

                        'Find Payment Allocation.
                        If XmlPaymentFees.Length > 0 Then
                            TotalFees = Serialization.Deserialize(XmlPaymentFees, TotalFees)
                        End If

                        Allocations = GetPaymentAllocations(Mappings, Fees, True, TotalFees)

                        r.Fill(rsAllocation, Allocations, False)
                        r.Fill(rsPayments, PaymentsUpdate)

                        rsPassengers.Filter = "passenger_type_rcd = 'INF'"
                        iInfantCount = rsPassengers.RecordCount
                        rsPassengers.Filter = 0
                        If InfantExcessLimit(rsSegments, iInfantCount) = False Then
                            PaymentStatus = clsRunCom.PaymentSave(rsPayments, rsAllocation, "")

                            If PaymentStatus Then
                                rsHeader = New ADODB.Recordset
                                rsRemarks = New ADODB.Recordset
                                rsMappings = New ADODB.Recordset
                                rsTaxes = New ADODB.Recordset
                                rsQuotes = New ADODB.Recordset

                                Call clsRunCom.GetBooking(BookingId,
                                                          rsHeader,
                                                          rsSegments,
                                                          rsPassengers,
                                                          rsRemarks,
                                                          rsPayments,
                                                          rsMappings, ,
                                                          rsTaxes,
                                                          rsFees,
                                                          rsQuotes)

                                Session("rsFees") = rsFees

                                FabricateRecordset = Nothing
                                bResult = True
                            Else
                                bResult = False
                            End If
                        Else
                            bResult = False
                        End If

                    Else
                        bResult = True
                    End If
                End If
            End If
        Catch e As Exception
            Dim Serialization As New Helper.Serialization
            If System.Configuration.ConfigurationManager.AppSettings("ErrorMailEnabled").ToUpper = "TRUE" Then
                Dim ErrorSubject As String = System.Configuration.ConfigurationManager.AppSettings("ErrorSubject") & System.Convert.ToString(Date.Now)
                Mail.Send(ErrorHelper.RenderDebugMessage("PaymentSave", e.Message, "", "", "", XmlPayments, "", XmlMapping, XmlFees, "", "", Serialization.Serialize(Allocations)), ErrorSubject, System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), Nothing)
            End If
            bResult = False
        Finally
            Helper.Check.ClearRecordset(rsAllocation)
            Helper.Check.ClearRecordset(rsHeader)
            Helper.Check.ClearRecordset(rsRemarks)
            Helper.Check.ClearRecordset(rsMappings)
            Helper.Check.ClearRecordset(rsTaxes)
            Helper.Check.ClearRecordset(rsQuotes)
            Helper.Check.ClearRecordset(rsPayments)
        End Try

        Return bResult
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/SaveBooking"),
WebMethod(Description:="Save Booking",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function SaveBooking(ByVal BookingId As String,
                                ByVal XmlHeader As String,
                                ByVal XmlSegments As String,
                                ByVal XmlPassengers As String,
                                ByVal XmlPayments As String,
                                ByVal XmlRemarks As String,
                                ByVal XmlMapping As String,
                                ByVal XmlFees As String,
                                ByVal XmlTaxes As String,
                                ByVal XmlQuotes As String,
                                ByVal XMLServices As String,
                                ByVal CreateTickets As Boolean,
                                ByVal CancelSegmentId As String,
                                ByVal strLanguage As String) As DataSet

        Using ds As New DataSet
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport
                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim BookingStatus As Short = -1
                        Dim Serialization As New Helper.Serialization
                        Dim FabricateRecordset As New FabricateRecordset
                        Dim clsRunCom As New clsRunComplus

                        Dim rsHeader As ADODB.Recordset = Session("rsHeader")
                        Dim rsSegments As ADODB.Recordset = Session("rsSegments")
                        Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
                        Dim rsRemarks As ADODB.Recordset = Session("rsRemarks")
                        Dim rsPayments As ADODB.Recordset = Session("rsPayments")
                        Dim rsMappings As ADODB.Recordset = Session("rsMappings")
                        Dim rsServices As ADODB.Recordset = Session("rsServices")
                        Dim rsTaxes As ADODB.Recordset = Session("rsTaxes")
                        Dim rsFees As ADODB.Recordset = Session("rsFees")
                        Dim rsQuotes As ADODB.Recordset = Session("rsQuotes")


                        Dim bCreateTickets As Boolean = CreateTickets

                        Dim r As New Helper.RecordSet
                        Dim DefaultCreateById As String

                        Dim FlagNew As Boolean = False

                        Dim BookingHeaderUpdate As New BookingHeaderUpdate
                        Dim Itinerary As New Itinerary
                        Dim Mappings As New Mappings
                        Dim Passengers As New Passengers
                        Dim PaymentsUpdate As New PaymentsUpdate
                        Dim Remarks As New Remarks
                        Dim MappingsUpdate As New MappingsUpdate
                        Dim Fees As New Fees
                        Dim Taxes As New Taxes
                        Dim Quotes As New Quotes
                        Dim Services As New Services
                        Dim Segments As New Itinerary

                        XmlHeader = XmlHeader.Replace("BookingHeader", "BookingHeaderUpdate")
                        BookingHeaderUpdate = Serialization.Deserialize(XmlHeader, BookingHeaderUpdate)

                        Passengers = Serialization.Deserialize(XmlPassengers, Passengers)
                        Mappings = Serialization.Deserialize(XmlMapping, Mappings)

                        XmlMapping = XmlMapping.Replace("Mapping", "MappingUpdate")
                        MappingsUpdate = Serialization.Deserialize(XmlMapping, MappingsUpdate)

                        Fees = Serialization.Deserialize(XmlFees, Fees)
                        Services = Serialization.Deserialize(XMLServices, Services)

                        Remarks = Serialization.Deserialize(XmlRemarks, Remarks)
                        Segments = Serialization.Deserialize(XmlSegments, Segments)

                        Dim BookingState As Booking = Session("BookingState")
                        With r

                            If Not Session("CreateById") Is Nothing AndAlso Not Session("CreateById") = String.Empty Then
                                DefaultCreateById = Session("CreateById")
                                r.DefaultCreateById = New Guid(DefaultCreateById)
                            Else
                                If IsGuid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")) Then
                                    Session("CreateById") = System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")
                                End If
                                DefaultCreateById = Session("CreateById")
                                r.DefaultCreateById = New Guid(DefaultCreateById)
                            End If

                            .FillHeader(rsHeader, BookingHeaderUpdate)
                            .FillPassengers(rsPassengers, Passengers)
                            .FillMapping(rsMappings, MappingsUpdate)
                            .FillRemarks(rsRemarks, Remarks)

                            .ClearFees(rsFees, Fees)
                            .FillFees(rsFees, Fees)


                            .ClearServices(rsServices, Services)
                            .FillServices(rsServices, Services)

                            If .FeeChange(rsFees, Fees) Then
                                .FillCreateDate(rsFees)
                                .FillAccountBy(rsFees)
                            End If
                            If .SegmentChange(rsSegments, Segments) Then
                                .FillCreateDate(rsSegments)
                            End If
                        End With

                        Dim objTestLog As StringBuilder = Nothing
                        If IsNothing(System.Configuration.ConfigurationManager.AppSettings("SystemLogEnabled")) = False Then
                            If System.Configuration.ConfigurationManager.AppSettings("SystemLogEnabled").ToUpper = "TRUE" Then
                                'For Debuging
                                objTestLog = New StringBuilder
                                objTestLog.Append("Before save booking" & Environment.NewLine)
                                objTestLog.Append("----------------XML String----------------------" & Environment.NewLine)
                                objTestLog.Append(XmlHeader & Environment.NewLine)
                                objTestLog.Append(XmlPassengers & Environment.NewLine)
                                objTestLog.Append(XmlMapping & Environment.NewLine)
                                'For Debuging
                            End If
                        End If
                        If InfantExcessLimit(rsSegments, BookingHeaderUpdate.number_of_infants) = False Then
                            BookingStatus = clsRunCom.SaveBooking(rsHeader,
                                                                  rsSegments,
                                                                  rsPassengers,
                                                                  rsRemarks, ,
                                                                  rsMappings,
                                                                  rsServices,
                                                                  rsTaxes,
                                                                  rsFees,
                                                                  bCreateTickets,
                                                                  False,
                                                                  False,
                                                                  True,
                                                                  True,
                                                                  True)
                            If BookingStatus = -1 Then
                                CreateErrorDataset(ds, "201", "SAVEFAILED")
                            ElseIf BookingStatus = 1 Then
                                CreateErrorDataset(ds, "202", "SESSIONTIMEOUT")
                            ElseIf BookingStatus = 2 Then
                                CreateErrorDataset(ds, "203", "DUPLICATESEAT")
                            ElseIf BookingStatus = 3 Then
                                CreateErrorDataset(ds, "204", "DATABASEACCESS")
                            ElseIf BookingStatus = 4 Then
                                CreateErrorDataset(ds, "205", "BOOKINGINUSE")
                            ElseIf BookingStatus = 5 Then
                                CreateErrorDataset(ds, "206", "BOOKINGREADERROR")
                            Else
                                Call clsRunCom.UpdatePassengerNames(rsPassengers,
                                                                    rsMappings,
                                                                    rsHeader,
                                                                    rsFees,
                                                                    rsSegments,
                                                                    AgentHeader.AgencyCode,
                                                                    AgentHeader.AgencyCurrencyRcd,
                                                                    BookingId,
                                                                    True)

                                If CreateTickets Then
                                    Dim b As Boolean = clsRunCom.TicketCreate(strCode,
                                                                              DefaultCreateById.Replace("{", "").Replace("}", ""), ,
                                                                              "{" & BookingId & "}",
                                                                              False)
                                End If

                                'Update Approval
                                Dim strIBE = "B2C,B2E"
                                If BookingHeaderUpdate.booking_source_rcd.ToUpper().IndexOf(strIBE) > 0 Then
                                    Dim bUpdateApproval As String = System.Configuration.ConfigurationManager.AppSettings("UpdateApproval")
                                    If Not String.IsNullOrEmpty(bUpdateApproval) Then
                                        If bUpdateApproval.ToString.ToLower = "true" Then
                                            clsRunCom.UpdateApproval(BookingId, 1)
                                        End If
                                    End If
                                End If


                                'Clear recordset before call get itinerary.
                                ClearBookingRecordset(rsHeader,
                                                      rsSegments,
                                                      rsPassengers,
                                                      rsRemarks,
                                                      rsPayments,
                                                      rsMappings,
                                                      rsServices,
                                                      rsTaxes,
                                                      rsFees,
                                                      rsQuotes)

                                Call clsRunCom.ItiniraryRead(BookingId,
                                                             String.Empty,
                                                             BookingHeaderUpdate.agency_code,
                                                             rsHeader,
                                                             rsSegments,
                                                             rsPassengers,
                                                             rsRemarks,
                                                             rsPayments,
                                                             rsMappings,
                                                             rsServices,
                                                             rsTaxes,
                                                             rsFees,
                                                             rsQuotes,
                                                             strLanguage)

                                'For Debuging
                                CopyRecordsetsToDataset(rsHeader,
                                                        rsSegments,
                                                        rsPassengers,
                                                        rsRemarks,
                                                        rsPayments,
                                                        rsMappings,
                                                        rsTaxes,
                                                        rsQuotes,
                                                        rsFees,
                                                        rsServices,
                                                        ds)

                                If IsNothing(System.Configuration.ConfigurationManager.AppSettings("SystemLogEnabled")) = False Then
                                    If System.Configuration.ConfigurationManager.AppSettings("SystemLogEnabled").ToUpper = "TRUE" Then
                                        'For Debuging
                                        objTestLog.Append("----------------Return String result----------------------" & Environment.NewLine)
                                        objTestLog.Append(ds.GetXml & Environment.NewLine)
                                        Dim path As String = HttpContext.Current.Server.MapPath("~") + "\Log\" + String.Format("{0:yyyyMMdd}", DateTime.Now) + ".log"
                                        SaveLog(objTestLog.ToString, path)
                                        objTestLog = Nothing
                                    End If
                                End If
                                'For Debuging

                                'Release Unmanage resource
                                ClearBookingRecordset(rsHeader,
                                                    rsSegments,
                                                    rsPassengers,
                                                    rsRemarks,
                                                    rsPayments,
                                                    rsMappings,
                                                    rsServices,
                                                    rsTaxes,
                                                    rsFees,
                                                    rsQuotes)

                                'Remove session
                                Session.Remove("rsHeader")
                                Session.Remove("rsSegments")
                                Session.Remove("rsPassenger")
                                Session.Remove("rsRemarks")
                                Session.Remove("rsPayments")
                                Session.Remove("rsMappings")
                                Session.Remove("rsServices")
                                Session.Remove("rsTaxes")
                                Session.Remove("rsFees")
                                Session.Remove("rsQuotes")

                                BookingState = New Booking
                                Serialization.FillBooking(ds, BookingState)
                                Session("BookingState") = BookingState
                                clsRunCom = Nothing
                            End If
                        Else
                            CreateErrorDataset(ds, "200", "Infant over limit")
                        End If

                    End If
                End If
            Catch e As Exception
                Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(e,
                                                                                                                      CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                     XmlHeader,
                                                                                                                                                     XmlSegments,
                                                                                                                                                     XmlPassengers,
                                                                                                                                                     XmlPayments,
                                                                                                                                                     XmlRemarks,
                                                                                                                                                     XmlMapping,
                                                                                                                                                     XmlFees,
                                                                                                                                                     String.Empty,
                                                                                                                                                     XmlTaxes,
                                                                                                                                                     XMLServices,
                                                                                                                                                     CreateTickets,
                                                                                                                                                     String.Empty,
                                                                                                                                                     String.Empty,
                                                                                                                                                     String.Empty,
                                                                                                                                                     String.Empty,
                                                                                                                                                     strLanguage,
                                                                                                                                                     String.Empty,
                                                                                                                                                     String.Empty))
                Return Nothing
            End Try

            Return ds
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/SplitBooking"),
    WebMethod(Description:="Split Booking",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function SplitBooking(ByVal XmlPassengers As String,
                                 ByVal strBookingIdOld As String,
                                 ByVal strReceivedFrom As String,
                                 ByVal strUserId As String,
                                 ByVal strAgencyCode As String,
                                 ByVal strUserCode As String,
                                 ByVal bCopyRemark As Boolean) As String

        Dim clsRunCom As New clsRunComplus

        Dim r As New Helper.RecordSet
        Dim rsPassengers As New ADODB.Recordset

        Dim Passengers As New Passengers

        Dim bookingId As String = String.Empty

        Dim Status As Boolean = clsRunCom.GetEmpty(AgentHeader.AgencyCode, AgentHeader.AgencyCurrencyRcd, , , rsPassengers)

        Passengers = Serialization.Deserialize(XmlPassengers, Passengers)
        With r
            .Fill(rsPassengers, Passengers)
        End With

        bookingId = clsRunCom.SplitBooking(rsPassengers,
                                            strBookingIdOld,
                                            strReceivedFrom,
                                            strUserId,
                                            strAgencyCode,
                                            strUserCode,
                                            bCopyRemark)
        Helper.Check.ClearRecordset(rsPassengers)

        r = Nothing
        clsRunCom = Nothing

        Return bookingId
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/AddFlight"),
 WebMethod(Description:="Return Airport Destinations",
 EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function AddFlight(ByVal xmlFlights As String,
                              ByVal xmlPassengers As String,
                              ByVal BookingId As String,
                              ByVal Boardpoint As String,
                              ByVal Offpoint As String,
                              ByVal AgencyCode As String,
                              ByVal dtFlight As Date,
                              ByVal FlightId As String,
                              ByRef FareId As String,
                              ByRef BoardingClass As String,
                              ByRef BookingClass As String,
                              ByVal Adults As Short,
                              ByVal Childs As Short,
                              ByVal Infants As Short,
                              ByVal Others As Short,
                              ByVal OtherPassengerType As String,
                              ByVal CreateById As String,
                              ByVal strIpAddress As String,
                              ByVal strLanguageCode As String,
                              ByVal strCurrencyCode As String,
                              ByVal bNoVat As Boolean) As DataSet

        Using ds As New DataSet

            Dim rsHeaderClone As ADODB.Recordset = Nothing
            Dim rsSegmentsClone As ADODB.Recordset = Nothing
            Dim rsPassengersClone As ADODB.Recordset = Nothing
            Dim rsRemarksClone As ADODB.Recordset = Nothing
            Dim rsPaymentsClone As ADODB.Recordset = Nothing
            Dim rsMappingsClone As ADODB.Recordset = Nothing
            Dim rsServicesClone As ADODB.Recordset = Nothing
            Dim rsTaxesClone As ADODB.Recordset = Nothing
            Dim rsFeesClone As ADODB.Recordset = Nothing
            Dim rsQuotesClone As ADODB.Recordset = Nothing
            Dim rsFlights As ADODB.Recordset = Nothing

            Try
                If (AgentHeader Is Nothing) Then

                Else
                    If (AuthenticatePassport(AgentHeader.AgencyCode, AgentHeader.AgencyPassport)) Then

                        Dim DefaultCreateById As String = CreateByIdGuid(CreateById).ToString
                        Session("CreateById") = DefaultCreateById

                        Dim FabricateRecordset As New FabricateRecordset
                        rsFlights = FabricateRecordset.FabricateSegmentRecordset

                        Dim Passengers As Passengers
                        If xmlPassengers.Length > 0 Then
                            Passengers = New Passengers
                            Serialization.Deserialize(xmlPassengers, Passengers)
                        End If


                        'Dim rsA As ADODB.Recordset
                        Dim clsRunCom As New clsRunComplus

                        Dim rsHeader As ADODB.Recordset = Session("rsHeader")
                        Dim rsSegments As ADODB.Recordset = Session("rsSegments")
                        Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
                        Dim rsRemarks As ADODB.Recordset = Session("rsRemarks")
                        Dim rsPayments As ADODB.Recordset = Session("rsPayments")
                        Dim rsMappings As ADODB.Recordset = Session("rsMappings")
                        Dim rsServices As ADODB.Recordset = Session("rsServices")
                        Dim rsTaxes As ADODB.Recordset = Session("rsTaxes")
                        Dim rsFees As ADODB.Recordset = Session("rsFees")
                        Dim rsQuotes As ADODB.Recordset = Session("rsQuotes")

                        Dim strAirline As String = ""
                        Dim strFlight As String = ""

                        Dim RecordSet As New Helper.RecordSet
                        With RecordSet
                            .DefaultCreateById = New Guid(DefaultCreateById)
                            .Fill(rsFlights, Serialization.Deserialize(xmlFlights, New Flights), False)
                        End With

                        'Check Infant limitation
                        If InfantExcessLimit(rsFlights, Infants) = False Then

                            'Clear recordset
                            ClearBookingRecordset(rsHeader,
                                                  rsSegments,
                                                  rsPassengers,
                                                  rsRemarks,
                                                  rsPayments,
                                                  rsMappings,
                                                  rsServices,
                                                  rsTaxes,
                                                  rsFees,
                                                  rsQuotes)

                            Dim Status As Boolean = clsRunCom.GetEmpty(AgencyCode,
                                                                       strCurrencyCode,
                                                                       rsHeader,
                                                                       rsSegments,
                                                                       rsPassengers,
                                                                       rsRemarks,
                                                                       rsPayments,
                                                                       rsMappings,
                                                                       rsServices,
                                                                       rsTaxes,
                                                                       rsFees,
                                                                       rsFlights,
                                                                       rsQuotes,
                                                                       BookingId,
                                                                       Adults,
                                                                       Childs,
                                                                       Infants,
                                                                       Others,
                                                                       OtherPassengerType,
                                                                       DefaultCreateById,
                                                                       strIpAddress,
                                                                       strLanguageCode,
                                                                       bNoVat)

                            rsHeaderClone = New ADODB.Recordset
                            rsSegmentsClone = New ADODB.Recordset
                            rsPassengersClone = New ADODB.Recordset
                            rsRemarksClone = New ADODB.Recordset
                            rsPaymentsClone = New ADODB.Recordset
                            rsMappingsClone = New ADODB.Recordset
                            rsServicesClone = New ADODB.Recordset
                            rsTaxesClone = New ADODB.Recordset
                            rsFeesClone = New ADODB.Recordset
                            rsQuotesClone = New ADODB.Recordset

                            Dim fr As New FabricateRecordset

                            'Assign create by update by user.
                            Dim r As New Helper.RecordSet()

                            If IsNothing(rsHeader) = False Then
                                r.FillCreateDate(rsHeader)
                            End If

                            If IsNothing(rsSegments) = False Then
                                r.FillCreateDate(rsSegments)
                            End If

                            If IsNothing(rsPassengers) = False Then
                                r.FillCreateDate(rsPassengers)
                            End If

                            If IsNothing(rsRemarks) = False Then
                                r.FillCreateDate(rsRemarks)
                            End If

                            If IsNothing(rsPayments) = False Then
                                r.FillCreateDate(rsPayments)
                            End If

                            If IsNothing(rsMappings) = False Then
                                r.FillCreateDate(rsMappings)
                            End If

                            If IsNothing(rsServices) = False Then
                                r.FillCreateDate(rsServices)
                            End If

                            If IsNothing(rsTaxes) = False Then
                                r.FillCreateDate(rsTaxes)
                            End If

                            If IsNothing(rsFees) = False Then
                                r.FillCreateDate(rsFees)
                            End If

                            fr.CopyAdoRecordset(rsHeader, rsHeaderClone)
                            Session("rsHeader") = rsHeader

                            fr.CopyAdoRecordset(rsSegments, rsSegmentsClone)
                            Session("rsSegments") = rsSegments

                            fr.CopyAdoRecordset(rsPassengers, rsPassengersClone)
                            Session("rsPassenger") = rsPassengers

                            fr.CopyAdoRecordset(rsRemarks, rsRemarksClone)
                            Session("rsRemarks") = rsRemarks

                            fr.CopyAdoRecordset(rsPayments, rsPaymentsClone)
                            Session("rsPayments") = rsPayments

                            fr.CopyAdoRecordset(rsMappings, rsMappingsClone)
                            Session("rsMappings") = rsMappings

                            fr.CopyAdoRecordset(rsServices, rsServicesClone)
                            Session("rsServices") = rsServices

                            fr.CopyAdoRecordset(rsFees, rsFeesClone)
                            Session("rsFees") = rsFees

                            fr.CopyAdoRecordset(rsTaxes, rsTaxesClone)
                            Session("rsTaxes") = rsTaxes

                            fr.CopyAdoRecordset(rsQuotes, rsQuotesClone)
                            Session("rsQuotes") = rsQuotes

                            CopyRecordsetsToDataset(rsHeaderClone,
                                                    rsSegmentsClone,
                                                    rsPassengersClone,
                                                    rsRemarksClone,
                                                    rsPaymentsClone,
                                                    rsMappingsClone,
                                                    rsTaxesClone,
                                                    rsQuotesClone,
                                                    rsFeesClone,
                                                    rsServicesClone,
                                                    ds)

                            Dim Booking As New Booking
                            Serialization.FillBooking(ds, Booking)
                            Session("BookingState") = Booking
                            clsRunCom = Nothing
                            Passengers = Nothing

                        Else
                            CreateErrorDataset(ds, "200", "Infant over limit")
                        End If
                    End If
                End If
            Catch e As Exception
                Throw e
            Finally
                'Clear recordset
                Helper.Check.ClearRecordset(rsFlights)
                ClearBookingRecordset(rsHeaderClone,
                                      rsSegmentsClone,
                                      rsPassengersClone,
                                      rsRemarksClone,
                                      rsPaymentsClone,
                                      rsMappingsClone,
                                      rsServicesClone,
                                      rsTaxesClone,
                                      rsFeesClone,
                                      rsQuotesClone)
            End Try
            Return ds
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ChangeSegmentAddFlight"),
WebMethod(Description:="Change Segment",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ChangeSegmentAddFlight(ByVal CancelSegmentId As String,
                                           ByVal xmlHeader As String,
                                           ByVal xmlPassenger As String,
                                           ByVal xmlSegment As String,
                                           ByVal xmlMapping As String,
                                           ByVal xmlTax As String,
                                           ByVal xmlService As String,
                                           ByVal AgencyCode As String,
                                           ByVal xmlQuote As String,
                                           ByVal xmlRemarks As String,
                                           ByVal xmlFlights As String,
                                           ByVal FlightDate As Date,
                                           ByVal BookingID As String,
                                           ByVal Boardpoint As String,
                                           ByVal Offpoint As String,
                                           ByVal FlightID As String,
                                           ByVal FareId As String,
                                           ByVal Airline As String,
                                           ByVal Flight As String,
                                           ByVal BoardingClass As String,
                                           ByVal BookingClass As String,
                                           ByVal LanguageCode As String,
                                           ByVal CurrencyCode As String,
                                           ByVal ETicket As Boolean,
                                           ByVal Refundable As Boolean,
                                           ByVal GroupBooking As Boolean,
                                           ByVal Waitlist As Boolean,
                                           ByVal QuoteOnly As Boolean,
                                           ByVal NonRevenue As Boolean,
                                           ByVal SubjectToAvailability As Boolean,
                                           ByVal MarketingAirline As String,
                                           ByVal MarketingFlight As String,
                                           ByVal SegmentId As String,
                                           ByVal IdReduction As Short,
                                           ByVal xmlFees As String,
                                           ByVal strLanguage As String,
                                           ByVal bNoVat As Boolean) As DataSet
        Using ds As New DataSet

            Dim rsHeaderClone As ADODB.Recordset = Nothing
            Dim rsSegmentsClone As ADODB.Recordset = Nothing
            Dim rsPassengersClone As ADODB.Recordset = Nothing
            Dim rsRemarksClone As ADODB.Recordset = Nothing
            Dim rsPaymentsClone As ADODB.Recordset = Nothing
            Dim rsMappingsClone As ADODB.Recordset = Nothing
            Dim rsServicesClone As ADODB.Recordset = Nothing
            Dim rsTaxesClone As ADODB.Recordset = Nothing
            Dim rsFeesClone As ADODB.Recordset = Nothing
            Dim rsQuotesClone As ADODB.Recordset = Nothing
            Dim rsFlights As ADODB.Recordset = Nothing

            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport
                    Dim AgencyCurrencyRcd As String = AgentHeader.AgencyCurrencyRcd

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        Dim rsHeader As ADODB.Recordset = Session("rsHeader")
                        Dim rsSegments As ADODB.Recordset = Session("rsSegments")
                        Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
                        Dim rsRemarks As ADODB.Recordset = Session("rsRemarks")
                        Dim rsPayments As ADODB.Recordset = Session("rsPayments")
                        Dim rsMappings As ADODB.Recordset = Session("rsMappings")
                        Dim rsServices As ADODB.Recordset = Session("rsServices")
                        Dim rsTaxes As ADODB.Recordset = Session("rsTaxes")
                        Dim rsFees As ADODB.Recordset = Session("rsFees")
                        Dim rsQuotes As ADODB.Recordset = Session("rsQuotes")

                        Dim r As New Helper.RecordSet

                        Dim FabricateRecordset As New FabricateRecordset
                        Dim DefaultCreateById As String

                        Dim BookingState As Booking = Session("BookingState")
                        Dim Booking As New Booking

                        Dim BookingHeaderUpdate As New BookingHeaderUpdate
                        Dim Itinerary As New Itinerary
                        Dim Passengers As New Passengers
                        Dim Remarks As New Remarks
                        Dim MappingsUpdate As New MappingsUpdate
                        Dim ItineraryUpdate As New ItineraryUpdate
                        Dim Fees As New Fees

                        rsFlights = FabricateRecordset.FabricateSegmentRecordset
                        xmlHeader = xmlHeader.Replace("BookingHeader", "BookingHeaderUpdate")
                        BookingHeaderUpdate = Serialization.Deserialize(xmlHeader, BookingHeaderUpdate)

                        Passengers = Serialization.Deserialize(xmlPassenger, Passengers)

                        xmlMapping = xmlMapping.Replace("Mapping", "MappingUpdate")
                        MappingsUpdate = Serialization.Deserialize(xmlMapping, MappingsUpdate)

                        xmlSegment = xmlSegment.Replace("FlightSegment", "FlightSegmentUpdate")
                        ItineraryUpdate = Serialization.Deserialize(xmlSegment, ItineraryUpdate)

                        Fees = Serialization.Deserialize(xmlFees, Fees)

                        With r
                            If Not Session("CreateById") Is Nothing AndAlso Not Session("CreateById") = String.Empty Then
                                DefaultCreateById = Session("CreateById")
                                r.DefaultCreateById = New Guid(DefaultCreateById)
                            Else
                                If IsGuid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")) Then
                                    Session("CreateById") = System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")
                                End If
                                DefaultCreateById = Session("CreateById")
                                r.DefaultCreateById = New Guid(DefaultCreateById)
                            End If
                            .Fill(rsFlights, Serialization.Deserialize(xmlFlights, New Flights), False)
                            .FillItinerary(rsSegments, ItineraryUpdate)
                            .FillMapping(rsMappings, MappingsUpdate)
                            .FillPassengers(rsPassengers, Passengers)
                            .FillFees(rsFees, Fees)
                        End With

                        If InfantExcessLimit(rsFlights, BookingHeaderUpdate.number_of_infants) = False Then
                            Dim Status As Boolean = clsRunCom.AddChangeFlight(rsPassengers,
                                                                              rsSegments,
                                                                              rsMappings,
                                                                              rsTaxes,
                                                                              rsServices,
                                                                              AgencyCode,
                                                                              rsQuotes,
                                                                              rsRemarks,
                                                                              rsFlights, ,
                                                                              BookingID,
                                                                              GroupBooking,
                                                                              Waitlist,
                                                                              DefaultCreateById,
                                                                              strLanguage,
                                                                              CurrencyCode,
                                                                              bNoVat)
                            If Status = True Then
                                Status = clsRunCom.CalculateBookingChangeFees(AgencyCode,
                                                                              BookingHeaderUpdate.currency_rcd,
                                                                              BookingHeaderUpdate.booking_id.ToString,
                                                                              rsHeader,
                                                                              rsSegments,
                                                                              rsPassengers,
                                                                              rsFees,
                                                                              rsRemarks,
                                                                              rsPayments,
                                                                              rsMappings,
                                                                              rsServices,
                                                                              rsTaxes,
                                                                              strLanguage,
                                                                              bNoVat)
                            End If

                            rsHeaderClone = New ADODB.Recordset
                            rsSegmentsClone = New ADODB.Recordset
                            rsPassengersClone = New ADODB.Recordset
                            rsRemarksClone = New ADODB.Recordset
                            rsPaymentsClone = New ADODB.Recordset
                            rsMappingsClone = New ADODB.Recordset
                            rsServicesClone = New ADODB.Recordset
                            rsTaxesClone = New ADODB.Recordset
                            rsFeesClone = New ADODB.Recordset
                            rsQuotesClone = New ADODB.Recordset

                            Dim fr As New FabricateRecordset

                            fr.CopyAdoRecordset(rsHeader, rsHeaderClone)
                            Session("rsHeader") = rsHeader

                            fr.CopyAdoRecordset(rsSegments, rsSegmentsClone)
                            Session("rsSegments") = rsSegments

                            fr.CopyAdoRecordset(rsPassengers, rsPassengersClone)
                            Session("rsPassenger") = rsPassengers

                            fr.CopyAdoRecordset(rsRemarks, rsRemarksClone)
                            Session("rsRemarks") = rsRemarks

                            fr.CopyAdoRecordset(rsPayments, rsPaymentsClone)
                            Session("rsPayments") = rsPayments

                            fr.CopyAdoRecordset(rsMappings, rsMappingsClone)
                            Session("rsMappings") = rsMappings

                            fr.CopyAdoRecordset(rsServices, rsServicesClone)
                            Session("rsServices") = rsServices

                            fr.CopyAdoRecordset(rsTaxes, rsTaxesClone)
                            Session("rsTaxes") = rsTaxes

                            fr.CopyAdoRecordset(rsFees, rsFeesClone)
                            Session("rsFees") = rsFees

                            fr.CopyAdoRecordset(rsQuotes, rsQuotesClone)
                            Session("rsQuotes") = rsQuotes

                            CopyRecordsetsToDataset(rsHeaderClone,
                                                    rsSegmentsClone,
                                                    rsPassengersClone,
                                                    rsRemarksClone,
                                                    rsPaymentsClone,
                                                    rsMappingsClone,
                                                    rsTaxesClone,
                                                    rsQuotesClone,
                                                    rsFeesClone,
                                                    rsServicesClone,
                                                    ds)

                            Serialization.FillBooking(ds, Booking)
                            clsRunCom = Nothing
                        Else
                            CreateErrorDataset(ds, "200", "Infant over limit")
                        End If
                    End If
                End If

            Catch e As Exception
                Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(e, BookingID)
            Finally
                Helper.Check.ClearRecordset(rsFlights)
                ClearBookingRecordset(rsHeaderClone,
                                      rsSegmentsClone,
                                      rsPassengersClone,
                                      rsRemarksClone,
                                      rsPaymentsClone,
                                      rsMappingsClone,
                                      rsServicesClone,
                                      rsTaxesClone,
                                      rsFeesClone,
                                      rsQuotesClone)
            End Try

            Return ds
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetCompactFlightAvailability"),
    WebMethod(Description:="Return Availibility xml",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetCompactFlightAvailability(ByVal FirstSegment As Boolean,
                                            ByVal bookingId As String,
                                            ByVal Origin As String,
                                            ByVal Destination As String,
                                            ByVal DateFrom As Date,
                                            ByVal DateTo As Date,
                                            ByVal DateBooking As Date,
                                            ByVal Adult As Short,
                                            ByVal Child As Short,
                                            ByVal Infant As Short,
                                            ByVal Other As Short,
                                            ByVal OtherPassengerType As String,
                                            ByVal BoardingClass As String,
                                            ByVal BookingClass As String,
                                            ByVal DayTimeIndicator As String,
                                            ByVal AgencyCode As String,
                                            ByVal CurrencyCode As String,
                                            ByVal FlightId As String,
                                            ByVal FareId As String,
                                            ByVal MaxAmount As Double,
                                            ByVal NonStopOnly As Boolean,
                                            ByVal IncludeDeparted As Boolean,
                                            ByVal IncludeCancelled As Boolean,
                                            ByVal IncludeWaitlisted As Boolean,
                                            ByVal IncludeSoldOut As Boolean,
                                            ByVal Refundable As Boolean,
                                            ByVal GroupFares As Boolean,
                                            ByVal ItFaresOnly As Boolean,
                                            ByVal PromotionCode As String,
                                            ByVal FareType As String
                                            ) As String
        Dim objStw = New StringWriter
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Return ReadCompactFlightAvailability(FirstSegment,
                                                           bookingId,
                                                           Origin,
                                                           Destination,
                                                           DateFrom,
                                                           DateTo,
                                                           DateBooking,
                                                           Adult,
                                                           Child,
                                                           Infant,
                                                           Other,
                                                           OtherPassengerType,
                                                           BoardingClass,
                                                           BookingClass,
                                                           DayTimeIndicator,
                                                           AgencyCode,
                                                           CurrencyCode,
                                                           FlightId,
                                                           FareId,
                                                           MaxAmount,
                                                           NonStopOnly,
                                                           IncludeDeparted,
                                                           IncludeCancelled,
                                                           IncludeWaitlisted,
                                                           IncludeSoldOut,
                                                           Refundable,
                                                           GroupFares,
                                                           ItFaresOnly,
                                                           PromotionCode,
                                                           FareType)
                End If
            End If
        Catch e As Exception
        End Try

        Return String.Empty
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetFlightAvailability"),
    WebMethod(Description:="Return Airport Destinations",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetFlightAvailability(ByVal Origin As String,
                                                ByVal Destination As String,
                                                ByVal DateDepartFrom As Date,
                                                ByVal DateDepartTo As Date,
                                                ByVal DateReturnFrom As Date,
                                                ByVal DateReturnTo As Date,
                                                ByVal DateBooking As Date,
                                                ByVal Adult As Short,
                                                ByVal Child As Short,
                                                ByVal Infant As Short,
                                                ByVal Other As Short,
                                                ByVal OtherPassengerType As String,
                                                ByVal BoardingClass As String,
                                                ByVal BookingClass As String,
                                                ByVal DayTimeIndicator As String,
                                                ByVal AgencyCode As String,
                                                ByVal CurrencyCode As String,
                                                ByVal FlightId As String,
                                                ByVal FareId As String,
                                                ByVal MaxAmount As Double,
                                                ByVal NonStopOnly As Boolean,
                                                ByVal IncludeDeparted As Boolean,
                                                ByVal IncludeCancelled As Boolean,
                                                ByVal IncludeWaitlisted As Boolean,
                                                ByVal IncludeSoldOut As Boolean,
                                                ByVal Refundable As Boolean,
                                                ByVal GroupFares As Boolean,
                                                ByVal ItFaresOnly As Boolean,
                                                ByVal PromotionCode As String,
                                                ByVal FareType As String,
                                                ByVal FareLogic As Boolean,
                                                ByVal ReturnFlight As Boolean,
                                                ByVal bLowest As Boolean,
                                                ByVal bLowestClass As Boolean,
                                                ByVal bLowestGroup As Boolean,
                                                ByVal bShowClosed As Boolean,
                                                ByVal bSort As Boolean,
                                                ByVal bDelete As Boolean,
                                                ByVal bSkipFareLogin As Boolean,
                                                ByVal strLanguage As String,
                                                ByVal strIpAddress As String,
                                                ByVal bReturnRefundable As Boolean,
                                                ByVal bNoVat As Boolean,
                                                ByVal iDayRange As Integer) As String
        Try
            If (AgentHeader Is Nothing) Then
                Return String.Empty
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Return GetAvailabilityFlight(Origin,
                                                Destination,
                                                DateDepartFrom,
                                                DateDepartTo,
                                                DateReturnFrom,
                                                DateReturnTo,
                                                DateBooking,
                                                Adult,
                                                Child,
                                                Infant,
                                                Other,
                                                OtherPassengerType,
                                                BoardingClass,
                                                BookingClass,
                                                DayTimeIndicator,
                                                AgencyCode,
                                                CurrencyCode,
                                                FlightId,
                                                FareId,
                                                MaxAmount,
                                                NonStopOnly,
                                                IncludeDeparted,
                                                IncludeCancelled,
                                                IncludeWaitlisted,
                                                IncludeSoldOut,
                                                Refundable,
                                                GroupFares,
                                                ItFaresOnly,
                                                PromotionCode,
                                                FareType,
                                                ReturnFlight,
                                                False,
                                                bLowest,
                                                bLowestClass,
                                                bLowestGroup,
                                                bShowClosed,
                                                bSort,
                                                bDelete,
                                                bSkipFareLogin,
                                                strLanguage,
                                                strIpAddress,
                                                bReturnRefundable,
                                                bNoVat,
                                                iDayRange)
                Else
                    Return String.Empty
                End If
            End If
        Catch e As Exception
            Return String.Empty
        End Try
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetLowFareFinder"),
   WebMethod(Description:="Return Airport Destinations",
   EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetLowFareFinder(ByVal Origin As String,
                                     ByVal Destination As String,
                                     ByVal DateDepartFrom As Date,
                                     ByVal DateDepartTo As Date,
                                     ByVal DateReturnFrom As Date,
                                     ByVal DateReturnTo As Date,
                                     ByVal DateBooking As Date,
                                     ByVal Adult As Short,
                                     ByVal Child As Short,
                                     ByVal Infant As Short,
                                     ByVal Other As Short,
                                     ByVal OtherPassengerType As String,
                                     ByVal BoardingClass As String,
                                     ByVal BookingClass As String,
                                     ByVal DayTimeIndicator As String,
                                     ByVal AgencyCode As String,
                                     ByVal CurrencyCode As String,
                                     ByVal FlightId As String,
                                     ByVal FareId As String,
                                     ByVal MaxAmount As Double,
                                     ByVal NonStopOnly As Boolean,
                                     ByVal IncludeDeparted As Boolean,
                                     ByVal IncludeCancelled As Boolean,
                                     ByVal IncludeWaitlisted As Boolean,
                                     ByVal IncludeSoldOut As Boolean,
                                     ByVal Refundable As Boolean,
                                     ByVal GroupFares As Boolean,
                                     ByVal ItFaresOnly As Boolean,
                                     ByVal PromotionCode As String,
                                     ByVal FareType As String,
                                     ByVal FareLogic As Boolean,
                                     ByVal ReturnFlight As Boolean,
                                     ByVal bLowest As Boolean,
                                     ByVal bLowestClass As Boolean,
                                     ByVal bLowestGroup As Boolean,
                                     ByVal bShowClosed As Boolean,
                                     ByVal bSort As Boolean,
                                     ByVal bDelete As Boolean,
                                     ByVal bSkipFareLogic As Boolean,
                                     ByVal strLanguage As String,
                                     ByVal strIpAddress As String,
                                     ByVal bReturnRefundable As Boolean,
                                     ByVal bNoVat As Boolean,
                                     ByVal iDayRange As Integer) As String
        Try
            If (AgentHeader Is Nothing) Then
                Return String.Empty
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport
                If (AuthenticatePassport(strCode, strPassport)) Then

                    Return GetAvailabilityFlight(Origin,
                                                Destination,
                                                DateDepartFrom,
                                                DateDepartTo,
                                                DateReturnFrom,
                                                DateReturnTo,
                                                DateBooking,
                                                Adult,
                                                Child,
                                                Infant,
                                                Other,
                                                OtherPassengerType,
                                                BoardingClass,
                                                BookingClass,
                                                DayTimeIndicator,
                                                AgencyCode,
                                                CurrencyCode,
                                                FlightId,
                                                FareId,
                                                MaxAmount,
                                                NonStopOnly,
                                                IncludeDeparted,
                                                IncludeCancelled,
                                                IncludeWaitlisted,
                                                IncludeSoldOut,
                                                Refundable,
                                                GroupFares,
                                                ItFaresOnly,
                                                PromotionCode,
                                                FareType,
                                                ReturnFlight,
                                                True,
                                                bLowest,
                                                bLowestClass,
                                                bLowestGroup,
                                                bShowClosed,
                                                bSort,
                                                bDelete,
                                                bSkipFareLogic,
                                                strLanguage,
                                                strIpAddress,
                                                bReturnRefundable,
                                                bNoVat,
                                                iDayRange)
                Else
                    Return String.Empty
                End If
            End If

        Catch e As Exception
            Return String.Empty
        End Try
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ReturnAirportDestination"),
    WebMethod(Description:="Return Airport Destinations",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ReturnAirportDestinations(ByVal Language As String,
                                              ByVal b2cFlag As Boolean,
                                              ByVal b2bFlag As Boolean,
                                              ByVal b2eFlag As Boolean,
                                              ByVal b2sFlag As Boolean,
                                              ByVal apiFlag As Boolean) As DataSet
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then

                    Return ReadAirportDestination(Language, b2cFlag, b2bFlag, b2eFlag, b2sFlag, apiFlag)
                End If
            End If
        Catch e As Exception

        End Try

        Return Nothing
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetAirlines"),
WebMethod(Description:="Return Airlines",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetAirlines(ByVal Language As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus
                        rsA = clsRunCom.GetAirlines(Language)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "AirportOrigins", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ReturnAirportOrigins"),
    WebMethod(Description:="Return Airport Origins",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ReturnAirportOrigins(ByVal Language As String, ByVal b2cFlag As Boolean, ByVal b2bFlag As Boolean, ByVal b2eFlag As Boolean, ByVal b2sFlag As Boolean, ByVal apiFlag As Boolean) As DataSet
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then

                    Return ReadAirportOrigin(Language, b2cFlag, b2bFlag, b2eFlag, b2sFlag, apiFlag)
                End If
            End If
        Catch e As Exception

        End Try
        Return Nothing
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetActivityTypes"),
    WebMethod(Description:="Get ActivityTypes",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetActivityTypes(ByVal language As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus
                        rsA = clsRunCom.GetActivityTypes(language)
                        clsRunCom = Nothing

                        Call InputRecordsetToDataset(rsA, "ActivitySearch", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/CancelSegment"),
    WebMethod(Description:="Cancel Segment",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function SegmentCancel(ByVal BookingId As String,
                                  ByVal SegmentId As String,
                                  ByVal userID As String,
                                  ByVal agencyCode As String,
                                  ByVal waiveFee As Boolean,
                                  ByVal processTicketRefund As Boolean,
                                  ByRef includeRefundableOnly As Boolean,
                                  ByRef noShowSegmentDateTime As Date) As String
        Dim Xml As String = ""

        Dim rsSegmentsClone As ADODB.Recordset = Nothing
        Dim rsMappingsClone As ADODB.Recordset = Nothing
        Dim rsTaxesClone As ADODB.Recordset = Nothing
        Dim rsFeesClone As ADODB.Recordset = Nothing
        Dim rsQuotesClone As ADODB.Recordset = Nothing

        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then

                    Dim clsRunCom As New clsRunComplus

                    Dim rsSegments As ADODB.Recordset = Session("rsSegments")
                    Dim rsPayments As ADODB.Recordset = Session("rsPayments")
                    Dim rsMappings As ADODB.Recordset = Session("rsMappings")
                    Dim rsServices As ADODB.Recordset = Session("rsServices")
                    Dim rsTaxes As ADODB.Recordset = Session("rsTaxes")
                    Dim rsFees As ADODB.Recordset = Session("rsFees")
                    Dim rsQuotes As ADODB.Recordset = Session("rsQuotes")

                    clsRunCom.SegmentCancel(SegmentId,
                                            rsSegments,
                                            rsMappings,
                                            rsServices,
                                            rsPayments,
                                            rsTaxes,
                                            rsQuotes,
                                            userID,
                                            agencyCode,
                                            waiveFee,
                                            processTicketRefund,
                                            includeRefundableOnly,
                                            noShowSegmentDateTime)


                    rsSegmentsClone = New ADODB.Recordset
                    rsMappingsClone = New ADODB.Recordset
                    rsTaxesClone = New ADODB.Recordset
                    rsFeesClone = New ADODB.Recordset
                    rsQuotesClone = New ADODB.Recordset

                    Call clsRunCom.GetBooking(BookingId, , , , , , , , , rsFees)

                    Dim fr As New FabricateRecordset

                    fr.CopyAdoRecordset(rsSegments, rsSegmentsClone)
                    Session("rsSegments") = rsSegments

                    fr.CopyAdoRecordset(rsMappings, rsMappingsClone)
                    Session("rsMappings") = rsMappings

                    fr.CopyAdoRecordset(rsTaxes, rsTaxesClone)
                    Session("rsTaxes") = rsTaxes

                    fr.CopyAdoRecordset(rsFees, rsFeesClone)
                    Session("rsFees") = rsFees

                    fr.CopyAdoRecordset(rsQuotes, rsQuotesClone)
                    Session("rsQuotes") = rsQuotes

                    CopyRecordsetsToXml(Nothing,
                                        rsSegmentsClone,
                                        Nothing,
                                        Nothing,
                                        Nothing,
                                        rsMappingsClone,
                                        rsTaxesClone,
                                        rsQuotesClone,
                                        rsFeesClone,
                                        Xml)

                    clsRunCom = Nothing
                End If
            End If
        Catch e As Exception
        Finally
            Helper.Check.ClearRecordset(rsSegmentsClone)
            Helper.Check.ClearRecordset(rsMappingsClone)
            Helper.Check.ClearRecordset(rsTaxesClone)
            Helper.Check.ClearRecordset(rsFeesClone)
            Helper.Check.ClearRecordset(rsQuotesClone)
        End Try
        Return Xml
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ReturnItinerary"),
      WebMethod(Description:="Display complete Itinerary",
      EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ReturnItinerary(ByVal strBookingId As String, ByVal strLanguage As String, ByVal strAgency As String) As DataSet
        Using objOut As New DataSet

            Dim rsHeader As ADODB.Recordset = Nothing
            Dim rsSegment As ADODB.Recordset = Nothing
            Dim rsPassenger As ADODB.Recordset = Nothing
            Dim rsRemark As ADODB.Recordset = Nothing
            Dim rsPayment As ADODB.Recordset = Nothing
            Dim rsMapping As ADODB.Recordset = Nothing

            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim bFound As Boolean = False

                        Dim clsRunCom As New clsRunComplus
                        bFound = clsRunCom.GetItinerary(strBookingId,
                                                        strLanguage,
                                                        "",
                                                        "",
                                                        strAgency,
                                                        rsHeader,
                                                        rsSegment,
                                                        rsPassenger,
                                                        rsRemark,
                                                        rsPayment,
                                                        rsMapping)
                        clsRunCom = Nothing

                        If Not bFound Then
                            Throw New System.Exception("Can not get Itinerary from COM+")
                        Else
                            InputItineraryToDataset(rsHeader,
                                                    rsSegment,
                                                    rsPassenger,
                                                    rsRemark,
                                                    rsPayment,
                                                    rsMapping,
                                                    objOut)
                        End If
                    Else

                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsHeader)
                Helper.Check.ClearRecordset(rsSegment)
                Helper.Check.ClearRecordset(rsPassenger)
                Helper.Check.ClearRecordset(rsRemark)
                Helper.Check.ClearRecordset(rsPayment)
                Helper.Check.ClearRecordset(rsMapping)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/UserList"),
    WebMethod(Description:="Return Agency User List",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetUserList(ByVal UserLogon As String,
                                ByVal UserCode As String,
                                ByVal LastName As String,
                                ByVal FirstName As String,
                                ByVal AgencyCode As String,
                                ByVal StatusCode As String) As String
        Dim Xml As String = ""
        Dim rsUser As ADODB.Recordset = Nothing
        Try
            If (AgentHeader Is Nothing) Then
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus

                    rsUser = clsRunCom.UserList(UserLogon,
                                                UserCode,
                                                LastName,
                                                FirstName,
                                                AgencyCode,
                                                StatusCode,
                                                False)

                    Xml = RecordSetToXML(rsUser, "UsersList", "UserList")

                    clsRunCom = Nothing
                End If
            End If
        Catch e As Exception
        Finally
            Helper.Check.ClearRecordset(rsUser)
        End Try
        Return Xml
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/UserRead"),
WebMethod(Description:="Return Agency User List",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function UserRead(ByVal UserID As String) As String

        Dim Xml As String = String.Empty
        Dim rsUser As ADODB.Recordset = Nothing
        Try
            If (AgentHeader Is Nothing) Then
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus

                    rsUser = clsRunCom.UserRead(UserID)
                    Xml = RecordSetToXML(rsUser, "UsersList", "UserList")

                    clsRunCom = Nothing
                Else

                End If
            End If
        Catch e As Exception
        Finally
            Helper.Check.ClearRecordset(rsUser)
        End Try
        Return Xml
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetVoucher"),
WebMethod(Description:="Return Agency User List",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetVouchers(ByVal strRecordLocator As String,
                            ByVal strVoucherNumber As String,
                            ByVal strVoucherID As String,
                            ByVal strStatus As String,
                            ByVal strRecipient As String,
                            ByVal strFOPSubType As String,
                            ByVal strClientProfileId As String,
                            ByVal strCurrency As String,
                            ByVal strPassword As String,
                            ByVal bIncludeExpiredVoucher As Boolean,
                            ByVal bIncludeUsedVoucher As Boolean,
                            ByVal bIncludeVoidedVoucher As Boolean,
                            ByVal bIncludeRefundable As Boolean,
                            ByVal bIncludeFareOnly As Boolean,
                            ByRef bWrite As Boolean,
                            ByVal XmlMapping As String,
                            ByVal XmlFees As String) As String

        Dim Xml As String = String.Empty
        Dim rs As ADODB.Recordset = Nothing
        Dim rsVoucherAllocation As ADODB.Recordset = Nothing

        Try
            If (AgentHeader Is Nothing) Then
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus
                    Using ds As New DataSet
                        rs = clsRunCom.GetVouchers(strRecordLocator,
                                                   strVoucherNumber,
                                                   strVoucherID,
                                                   strStatus,
                                                   strRecipient,
                                                   strFOPSubType,
                                                   strClientProfileId,
                                                   strCurrency,
                                                   strPassword,
                                                   True,
                                                   bIncludeExpiredVoucher,
                                                   bIncludeUsedVoucher,
                                                   bIncludeVoidedVoucher,
                                                   bIncludeRefundable,
                                                   bIncludeFareOnly,
                                                   bWrite)

                        If IsNothing(rs) = False Then
                            If rs.RecordCount > 0 Then
                                If XmlMapping.Length > 0 And XmlFees.Length > 0 Then
                                    If rs.Fields("voucher_status_rcd").Value = "OPEN" Then

                                        'Enter only when you want to valudate voucher
                                        Dim rsMappings As ADODB.Recordset = Session("rsMappings")
                                        Dim rsFees As ADODB.Recordset = Session("rsFees")

                                        Dim Mappings As New MappingsUpdate
                                        Dim Fees As New Fees

                                        XmlMapping = XmlMapping.Replace("Mapping", "MappingUpdate")
                                        Mappings = Serialization.Deserialize(XmlMapping, Mappings)

                                        Fees = Serialization.Deserialize(XmlFees, Fees)

                                        Dim r As New Helper.RecordSet
                                        With r
                                            .FillMapping(rsMappings, Mappings)
                                            .FillFees(rsFees, Fees)
                                        End With
                                        'Call voucher validation function
                                        rsVoucherAllocation = clsRunCom.ValidateVoucher(rs, rsMappings, rsFees, String.Empty)

                                        'Fill Status NP to voucher status if it not possible to pay
                                        InputRecordsetToDataset(rs, "Voucher", ds, "Booking")
                                        If IsNothing(rsVoucherAllocation) = True Then
                                            ds.Tables(0).Rows(0).Item("voucher_status_rcd") = "NP"
                                        Else
                                            If rsVoucherAllocation.RecordCount = 0 Then
                                                ds.Tables(0).Rows(0).Item("voucher_status_rcd") = "NP"
                                            Else
                                                'fix NP
                                                rsVoucherAllocation.Filter = "dif_amount > 0"
                                                If rsVoucherAllocation.RecordCount > 0 Then
                                                    ds.Tables(0).Rows(0).Item("voucher_status_rcd") = "NP"
                                                End If

                                                rsVoucherAllocation.Filter = 0
                                            End If
                                        End If
                                    End If
                                Else
                                    InputRecordsetToDataset(rs, "Voucher", ds, "Booking")
                                End If
                                Xml = ds.GetXml()
                            End If
                        End If
                    End Using
                End If
            End If
        Catch e As Exception
            Xml = Nothing
        Finally
            Helper.Check.ClearRecordset(rs)
            Helper.Check.ClearRecordset(rsVoucherAllocation)
        End Try
        Return Xml
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/CalculateExchange"),
WebMethod(Description:="Return Agency User List",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function CalculateExchange(ByVal currencyFrom As String,
                                      ByVal currencyTo As String,
                                      ByVal amount As Double,
                                      ByVal systemCurrency As String,
                                      ByVal dateOfExchange As Date,
                                      ByVal reverse As Boolean) As Double
        Try
            If (AgentHeader Is Nothing) Then
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus
                    Return clsRunCom.CalculateExchange(currencyFrom, currencyTo, amount, systemCurrency, dateOfExchange, reverse)
                Else

                End If
            End If
        Catch e As Exception
            Return -1
        End Try
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetBookingSegmentCheckIn"),
WebMethod(Description:="Return GetBookingSegmentCheckIn",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetBookingSegmentCheckIn(ByVal bookingId As String, ByVal clientId As String, ByVal languageCode As String) As String

        Dim Xml As String = String.Empty
        Dim rs As ADODB.Recordset = Nothing
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus
                    rs = clsRunCom.GetBookingSegmentCheckIn(bookingId, clientId, languageCode)
                    Xml = Me.RecordSetToXML(rs, "Booking", "Mapping")
                End If
            End If
        Catch e As Exception
        Finally
            Helper.Check.ClearRecordset(rs)
        End Try
        Return Xml
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/BoardPassengers"),
WebMethod(Description:="Return BoardPassengers",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function BoardPassengers(ByVal strFlightID As String, ByVal strOrigin As String, ByVal strBoard As String, ByVal strUserId As String, ByVal bBoard As Boolean) As String

        Dim strResult As String = String.Empty
        Dim rs As ADODB.Recordset = Nothing
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus
                    strResult = clsRunCom.BoardPassengers(strFlightID, strOrigin, strBoard, strUserId, bBoard)
                End If
            End If
        Catch e As Exception
        Finally
            Helper.Check.ClearRecordset(rs)
        End Try
        Return strResult
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/OffLoadPassengers"),
WebMethod(Description:="Return OffLoadPassengers",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function OffLoadPassenger(ByVal strBookingId As String, ByVal strFlightId As String, ByVal strPassengerId As String, ByVal autoBaggageFlag As Boolean, ByVal strUserId As String) As String

        Dim FabricateRecordset As New FabricateRecordset
        Dim rsMapping As ADODB.Recordset = FabricateRecordset.FabricateMappingRecordset
        Dim rsSelected As ADODB.Recordset = FabricateRecordset.FabricateMappingRecordset

        Dim strResult As String = String.Empty
        Dim rs As ADODB.Recordset = Nothing
        Dim rb As ADODB.Recordset = Nothing
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus
                    clsRunCom.Read(strBookingId, , , , , , , , rsMapping)
                    If IsNothing(rsMapping) = False AndAlso rsMapping.RecordCount > 0 Then
                        rsMapping.MoveFirst()
                        While Not rsMapping.EOF
                            Dim tmpPassID = LCase(rsMapping.Fields("passenger_id").Value)
                            Dim tmpFlightID = LCase(rsMapping.Fields("flight_id").Value)
                            If (tmpPassID.Equals("{" & strPassengerId & "}") And tmpFlightID.Equals("{" & strFlightId & "}")) Then
                                With rsSelected
                                    .AddNew()
                                    .Fields("passenger_id").Value = tmpPassID
                                    .Fields("booking_segment_id").Value = rsMapping.Fields("booking_segment_id").Value
                                    .Fields("group_sequence").Value = rsMapping.Fields("group_sequence").Value
                                    .Update()
                                End With
                            End If
                            rsMapping.MoveNext()
                        End While
                        If IsNothing(rsSelected) = False AndAlso rsSelected.RecordCount > 0 Then
                            strResult = clsRunCom.OffloadPassenger(rsSelected, autoBaggageFlag, strUserId)
                        End If
                    End If
                End If
            End If
        Catch e As Exception
        Finally
            Helper.Check.ClearRecordset(rs)
        End Try
        Return strResult
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/UserSave"),
WebMethod(Description:="Return Agency User List",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function UserSave(ByVal xmlUser As String, ByVal agencyCode As String) As Boolean

        Dim bResult As Boolean
        Dim rsUser As ADODB.Recordset = Nothing
        Dim rsAgencyUserMappingGetEmpty As ADODB.Recordset = Nothing
        Dim rsUserShaddow As ADODB.Recordset = Nothing

        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus


                    Dim r As New Helper.RecordSet
                    Dim UserListUpdate As New UserList
                    UserListUpdate = Serialization.Deserialize(xmlUser, UserListUpdate)
                    If UserListUpdate.user_account_id.ToString() = "00000000-0000-0000-0000-000000000000" Then
                        rsUser = clsRunCom.UserGetEmpty()
                        rsAgencyUserMappingGetEmpty = clsRunCom.AgencyUserMappingGetEmpty()
                        UserListUpdate.user_account_id = Guid.NewGuid
                        With rsAgencyUserMappingGetEmpty
                            .AddNew()
                            .Fields("agency_code").Value = agencyCode
                            .Fields("user_account_id").Value = "{" & UserListUpdate.user_account_id.ToString & "}"
                            .Update()
                        End With
                        Call clsRunCom.AgencyUserMappingSave(rsAgencyUserMappingGetEmpty)
                        r.Fill(rsUser, UserListUpdate)
                    Else
                        Dim UsersList As New AgencyUsersList
                        rsUserShaddow = clsRunCom.UserRead(UserListUpdate.user_account_id.ToString)
                        rsUser = clsRunCom.UserRead(UserListUpdate.user_account_id.ToString)
                        Dim xmlUsersList As String = RecordSetToXML(rsUserShaddow, "UsersList", "UserList")
                        Serialization.FillAgencyUsers(xmlUsersList, UsersList)
                        r.Fill(rsUser, UserListUpdate, UsersList(0))
                    End If
                    bResult = clsRunCom.UserSave(rsUser)
                    clsRunCom = Nothing
                Else

                End If
            End If
        Catch e As Exception
        Finally
            Helper.Check.ClearRecordset(rsUser)
            Helper.Check.ClearRecordset(rsAgencyUserMappingGetEmpty)
            Helper.Check.ClearRecordset(rsUserShaddow)
        End Try

        Return bResult
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/NewUserSave"),
WebMethod(Description:="Return Agency User List",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function NewUserSave(ByVal xmlUser As String, ByVal agencyCode As String) As Boolean

        Dim bResult As Boolean
        Dim rsUser As ADODB.Recordset = Nothing
        Dim rsAgencyUserMappingGetEmpty As ADODB.Recordset = Nothing

        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus

                    Dim r As New Helper.RecordSet
                    Dim UserListUpdate As New UserList
                    UserListUpdate = Serialization.Deserialize(xmlUser, UserListUpdate)

                    rsUser = clsRunCom.UserGetEmpty()
                    rsAgencyUserMappingGetEmpty = clsRunCom.AgencyUserMappingGetEmpty()
                    UserListUpdate.user_account_id = UserListUpdate.user_account_id
                    With rsAgencyUserMappingGetEmpty
                        .AddNew()
                        .Fields("agency_code").Value = agencyCode
                        .Fields("user_account_id").Value = "{" & UserListUpdate.user_account_id.ToString & "}"
                        .Update()
                    End With

                    Call clsRunCom.AgencyUserMappingSave(rsAgencyUserMappingGetEmpty)
                    r.Fill(rsUser, UserListUpdate)
                    bResult = clsRunCom.UserSave(rsUser)
                    clsRunCom = Nothing
                Else

                End If
            End If
        Catch e As Exception
            bResult = False
        Finally
            Helper.Check.ClearRecordset(rsUser)
            Helper.Check.ClearRecordset(rsAgencyUserMappingGetEmpty)
        End Try

        Return bResult
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ClientSave"),
    WebMethod(Description:="Save Client Data",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ClientSave(ByRef XmlClient As String,
                               ByRef XmlPassenger As String,
                               ByRef XmlBookingRemark As String) As Boolean

        Dim bSuccess As Boolean

        Dim rsClient As ADODB.Recordset = Nothing
        Dim rsPassenger As ADODB.Recordset = Nothing
        Dim rsBookingRemark As ADODB.Recordset = Nothing
        Dim rsClientSegment As ADODB.Recordset = Nothing
        Dim rsEmployee As ADODB.Recordset = Nothing

        Dim rsShadowClient As ADODB.Recordset = Nothing
        Dim rsShadowPassenger As ADODB.Recordset = Nothing

        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus

                    Dim r As New Helper.RecordSet

                    Dim ClientUpdate As New Client
                    Dim PassengerUpdate As New Passengers

                    If (XmlClient.Length > 0) Then
                        ClientUpdate = Serialization.Deserialize(XmlClient, ClientUpdate)
                    End If
                    If (XmlPassenger.Length > 0) Then
                        PassengerUpdate = Serialization.Deserialize(XmlPassenger, PassengerUpdate)
                    End If

                    If ClientUpdate.client_profile_id.ToString() = "00000000-0000-0000-0000-000000000000" Then
                        clsRunCom.ClientGetEmpty(rsClient, rsPassenger)
                        ClientUpdate.client_profile_id = Guid.NewGuid

                        For Each px As Passenger In PassengerUpdate
                            px.passenger_profile_id = Guid.NewGuid
                            px.client_profile_id = ClientUpdate.client_profile_id
                        Next

                        r.Fill(rsClient, ClientUpdate)
                        r.Fill(rsPassenger, PassengerUpdate)
                    Else
                        Dim ClientList As New Client
                        Dim xmlUsersList As String = ""
                        Dim PassengerList As New Passengers
                        Dim xmlPassengerList As String = ""

                        'Load Shadow
                        clsRunCom.ClientRead(ClientUpdate.client_profile_id.ToString,
                                             rsShadowClient,
                                             rsEmployee,
                                             rsShadowPassenger,
                                             rsBookingRemark,
                                             rsClientSegment)
                        'Load Actual
                        clsRunCom.ClientRead(ClientUpdate.client_profile_id.ToString,
                                             rsClient,
                                             rsEmployee,
                                             rsPassenger,
                                             rsBookingRemark,
                                             rsClientSegment)

                        xmlUsersList = RecordSetToXML(rsShadowClient, "UpdateClient", "Client")
                        ClientList = Serialization.FillClientList(xmlUsersList, ClientList)
                        r.Fill(rsClient, ClientUpdate, ClientList)

                        If (PassengerUpdate.Count > 0) Then
                            xmlPassengerList = RecordSetToXML(rsShadowPassenger, "PassengerList", "Passenger")
                            PassengerList = Serialization.FillPassengerList(xmlPassengerList, PassengerList)
                            r.FillUpdatePassengerProfile(rsPassenger, PassengerUpdate)
                        End If
                    End If

                    bSuccess = clsRunCom.ClientSave(rsClient, rsPassenger, rsBookingRemark)
                    clsRunCom = Nothing

                End If
            End If
        Catch e As Exception
            bSuccess = False
        Finally
            Helper.Check.ClearRecordset(rsClient)
            Helper.Check.ClearRecordset(rsPassenger)
            Helper.Check.ClearRecordset(rsBookingRemark)
            Helper.Check.ClearRecordset(rsClientSegment)
            Helper.Check.ClearRecordset(rsEmployee)

            Helper.Check.ClearRecordset(rsShadowClient)
            Helper.Check.ClearRecordset(rsShadowPassenger)
        End Try

        Return bSuccess
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/AddClientPassengerList"),
    WebMethod(Description:="Save Client passsenger list",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function AddClientPassengerList(ByRef XmlPassenger As String) As Boolean

        Dim bSuccess As Boolean
        Dim rsPassenger As ADODB.Recordset = Nothing

        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus

                    Dim r As New Helper.RecordSet

                    Dim PassengerUpdate As New Passengers

                    clsRunCom.ClientGetEmpty(Nothing, rsPassenger)
                    PassengerUpdate = Serialization.Deserialize(XmlPassenger, PassengerUpdate)
                    For Each px As Passenger In PassengerUpdate
                        px.passenger_profile_id = Guid.NewGuid
                    Next

                    r.Fill(rsPassenger, PassengerUpdate)
                    bSuccess = clsRunCom.ClientSave(Nothing, rsPassenger, Nothing)
                    clsRunCom = Nothing

                End If
            End If
        Catch e As Exception
            bSuccess = False
        Finally
            Helper.Check.ClearRecordset(rsPassenger)
        End Try

        Return bSuccess
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ClientRead"),
        WebMethod(Description:="Read Client Information",
        EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ClientRead(ByVal strClientProfileId As String) As DataSet

        Dim clsRunCom As New clsRunComplus
        Dim rsClient As ADODB.Recordset = Nothing
        Dim rsPassenger As ADODB.Recordset = Nothing
        Dim rsBookingRemark As ADODB.Recordset = Nothing
        Dim rsClientSegment As ADODB.Recordset = Nothing
        Dim rsEmployee As ADODB.Recordset = Nothing

        Using ds As New DataSet
            Try
                If String.IsNullOrEmpty(strClientProfileId) = False AndAlso IsGuid(strClientProfileId) = True Then
                    clsRunCom.ClientRead(strClientProfileId,
                                        rsClient,
                                        rsEmployee,
                                        rsPassenger,
                                        rsBookingRemark,
                                        rsClientSegment)

                    CopyClientReadRecordsetsToDataset(rsClient, rsEmployee, rsPassenger, rsBookingRemark, rsClientSegment, ds)
                End If
            Catch ex As Exception
            Finally
                Helper.Check.ClearRecordset(rsClient)
                Helper.Check.ClearRecordset(rsPassenger)
                Helper.Check.ClearRecordset(rsBookingRemark)
                Helper.Check.ClearRecordset(rsClientSegment)
                Helper.Check.ClearRecordset(rsEmployee)
            End Try
            Return ds
        End Using
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/CreateClientProfile"),
    WebMethod(Description:="Create Client Profile",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function CreateClientProfile(ByRef XmlClient As String,
                                        ByRef xmlUser As String,
                                        ByRef XmlPassenger As String,
                                        ByVal AgencyCode As String) As Boolean

        Dim bSuccess As Boolean

        Dim rsUser As ADODB.Recordset = Nothing
        Dim rsClient As ADODB.Recordset = Nothing
        Dim rsPassenger As ADODB.Recordset = Nothing
        Dim rsAgencyUserMappingGetEmpty As ADODB.Recordset = Nothing

        Try
            If (AgentHeader Is Nothing) Then
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus

                    Dim r As New Helper.RecordSet

                    Dim UserListUpdate As New UserList
                    Dim UserAccountID As Guid

                    Dim ClientUpdate As New Client
                    Dim PassengerUpdate As New Passengers

                    'Save User Information
                    UserListUpdate = Serialization.Deserialize(xmlUser, UserListUpdate)
                    rsUser = clsRunCom.UserGetEmpty()
                    rsAgencyUserMappingGetEmpty = clsRunCom.AgencyUserMappingGetEmpty()
                    UserAccountID = Guid.NewGuid
                    UserListUpdate.user_account_id = UserAccountID
                    With rsAgencyUserMappingGetEmpty
                        .AddNew()
                        .Fields("agency_code").Value = AgencyCode
                        .Fields("user_account_id").Value = "{" & UserListUpdate.user_account_id.ToString & "}"
                        .Update()
                    End With
                    Call clsRunCom.AgencyUserMappingSave(rsAgencyUserMappingGetEmpty)
                    r.Fill(rsUser, UserListUpdate)

                    bSuccess = clsRunCom.UserSave(rsUser)

                    'Save Client Information
                    clsRunCom.ClientGetEmpty(rsClient, rsPassenger)

                    ClientUpdate = Serialization.Deserialize(XmlClient, ClientUpdate)
                    PassengerUpdate = Serialization.Deserialize(XmlPassenger, PassengerUpdate)

                    ClientUpdate.client_profile_id = UserAccountID
                    For Each px As Passenger In PassengerUpdate
                        px.passenger_profile_id = Guid.NewGuid
                        px.client_profile_id = UserAccountID
                    Next
                    r.Fill(rsClient, ClientUpdate)
                    r.Fill(rsPassenger, PassengerUpdate)

                    bSuccess = clsRunCom.ClientSave(rsClient, rsPassenger, Nothing)
                    clsRunCom = Nothing
                End If
            End If
        Catch e As Exception
            bSuccess = False
        Finally

            Helper.Check.ClearRecordset(rsUser)
            Helper.Check.ClearRecordset(rsClient)
            Helper.Check.ClearRecordset(rsPassenger)
            Helper.Check.ClearRecordset(rsAgencyUserMappingGetEmpty)
        End Try

        Return bSuccess
    End Function


    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ProcessLinkserCreditCard"),
WebMethod(Description:="Linkser Credit Card Payment",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ProcessLinkserCreditCard(ByVal xml As String) As String

        Dim objLinkser As New Linkser
        With objLinkser
            ProcessLinkserCreditCard = .ProcessLinkserCreditCard(xml)
        End With
        objLinkser = Nothing
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetBookingFees"),
WebMethod(Description:="Return Booking Fee",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetBookingFees(ByVal strCurrency As String,
                               ByVal dt As Date,
                               ByVal strType As String) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus
                        rsA = clsRunCom.GetBookingFees(strCurrency, dt, strCode, strType)
                        clsRunCom = Nothing

                        Call InputRecordsetToDataset(rsA, "BookingFees", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try

            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetFees"),
WebMethod(Description:="Return Fee",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetFees(ByVal strCurrency As String,
                            ByVal dt As Date,
                            ByVal strAgency As String,
                            ByVal strType As String,
                            ByVal strClass As String,
                            ByVal strFareBasis As String,
                            ByVal strOrigin As String,
                            ByVal strDestination As String,
                            ByVal strFlightNumber As String,
                            ByVal strLanguage As String,
                            ByVal bNoVat As Boolean) As String

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetFees(strCurrency, dt, strAgency, strType, strClass, strFareBasis, strOrigin, strDestination, strFlightNumber, strLanguage, bNoVat)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "BookingFees", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try

            If IsNothing(objOut) = False Then
                If objOut.Tables.Count > 0 Then
                    Return objOut.GetXml()
                Else
                    Return String.Empty
                End If
            Else
                Return String.Empty
            End If
        End Using
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetFeesDefinition"),
WebMethod(Description:="Return Fee",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetFeesDefinition(ByVal strLanguage As String) As String

        Try
            If (AgentHeader Is Nothing) Then
                Return String.Empty
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Return FeeDefinitionRead(strLanguage)
                Else
                    Return String.Empty
                End If
            End If
        Catch e As Exception
            Return String.Empty
        End Try
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetClientSessionProfile"),
WebMethod(Description:="Return Client Session Profile",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetClientSessionProfile(ByVal clientProfileId As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        If String.IsNullOrEmpty(clientProfileId) = False AndAlso IsGuid(clientProfileId) = True Then
                            Dim clsRunCom As New clsRunComplus
                            rsA = clsRunCom.GetClientSessionProfile(clientProfileId)
                            clsRunCom = Nothing
                            Call InputRecordsetToDataset(rsA, "TikAero", "Client", objOut)
                        End If
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetCorporateSessionProfile"),
WebMethod(Description:="Return Corporate Session Profile",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetCorporateSessionProfile(ByVal clientId As String, ByVal LastName As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetCorporateSessionProfile(clientId, LastName)

                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "TikAero", "Corporate", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetClientPassenger"),
WebMethod(Description:="Return Client Passenger",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetClientPassenger(ByVal strBookingId As String, ByVal strClientProfileId As String, ByVal strClientNumber As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        If String.IsNullOrEmpty(strBookingId) = False AndAlso IsGuid(strBookingId) = False Then
                            strBookingId = String.Empty
                        ElseIf String.IsNullOrEmpty(strClientProfileId) = False AndAlso IsGuid(strClientProfileId) = False Then
                            strClientProfileId = String.Empty
                        ElseIf String.IsNullOrEmpty(strClientNumber) = False AndAlso IsNumeric(strClientNumber) = False Then
                            strClientNumber = 0
                        End If

                        Dim clsRunCom As New clsRunComplus
                        rsA = clsRunCom.GetClientPassenger(strBookingId, strClientProfileId, strClientNumber)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "TikAero", "Passenger", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetTicketsIssued"),
WebMethod(Description:="Return Issued Ticket",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetTicketsIssued(ByVal dtReportFrom As DateTime,
                                     ByVal dtReportTo As DateTime,
                                     ByVal dtFlightFrom As DateTime,
                                     ByVal dtFlightTo As DateTime,
                                     ByVal strOrigin As String,
                                     ByVal strDestination As String,
                                     ByVal strAgency As String,
                                     ByVal strAirline As String,
                                     ByVal strFlight As String) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus
                        rsA = clsRunCom.GetTicketsIssued(dtReportFrom,
                                                         dtReportTo,
                                                         dtFlightFrom,
                                                         dtFlightTo,
                                                         strOrigin,
                                                         strDestination,
                                                         strAgency,
                                                         strAirline,
                                                         strFlight)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "TicketsIssued", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try

            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetTicketsUsed"),
WebMethod(Description:="Return Flown Ticket",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetTicketsUsed(ByVal dtReportFrom As DateTime,
                                     ByVal dtReportTo As DateTime,
                                     ByVal dtFlightFrom As DateTime,
                                     ByVal dtFlightTo As DateTime,
                                     ByVal strOrigin As String,
                                     ByVal strDestination As String,
                                     ByVal strAgency As String,
                                     ByVal strAirline As String,
                                     ByVal strFlight As String) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetTicketsUsed(dtReportFrom,
                                                       dtReportTo,
                                                       dtFlightFrom,
                                                       dtFlightTo,
                                                       strOrigin,
                                                       strDestination,
                                                       strAgency,
                                                       strAirline,
                                                       strFlight)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "TicketsUsed", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetTicketsRefunded"),
WebMethod(Description:="Return Refund Ticket",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetTicketsRefunded(ByVal dtReportFrom As DateTime,
                                       ByVal dtReportTo As DateTime,
                                       ByVal dtFlightFrom As DateTime,
                                       ByVal dtFlightTo As DateTime,
                                       ByVal strOrigin As String,
                                       ByVal strDestination As String,
                                       ByVal strAgency As String,
                                       ByVal strAirline As String,
                                       ByVal strFlight As String) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetTicketsRefunded(dtReportFrom,
                                                           dtReportTo,
                                                           dtFlightFrom,
                                                           dtFlightTo,
                                                           strOrigin,
                                                           strDestination,
                                                           strAgency,
                                                           strAirline,
                                                           strFlight)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "TicketsRefunded", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try

            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetTicketsExpired"),
WebMethod(Description:="Return Expired Ticket",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetTicketsExpired(ByVal dtReportFrom As DateTime,
                                      ByVal dtReportTo As DateTime,
                                      ByVal dtFlightFrom As DateTime,
                                      ByVal dtFlightTo As DateTime,
                                      ByVal strOrigin As String,
                                      ByVal strDestination As String,
                                      ByVal strAgency As String,
                                      ByVal strAirline As String,
                                      ByVal strFlight As String) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetTicketsExpired(dtReportFrom,
                                                          dtReportTo,
                                                          dtFlightFrom,
                                                          dtFlightTo,
                                                          strOrigin,
                                                          strDestination,
                                                          strAgency,
                                                          strAirline,
                                                          strFlight)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "GetTicketsExpired", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try

            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetTicketsCancelled"),
WebMethod(Description:="Return Cancelled Ticket",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetTicketsCancelled(ByVal strOrigin As String,
                                        ByVal strDestination As String,
                                        ByVal strAgency As String,
                                        ByVal strAirline As String,
                                        ByVal strFlight As String,
                                        ByVal dtReportFrom As Date,
                                        ByVal dtReportTo As Date,
                                        ByVal dtFlightFrom As Date,
                                        ByVal dtFlightTo As Date,
                                        ByVal intTicketonly As Integer,
                                        ByVal intRefundable As Integer,
                                        ByVal strProfileID As String,
                                        ByVal strTicketNumber As String,
                                        ByVal strFirstName As String,
                                        ByVal strLastName As String,
                                        ByVal strPassengerId As String,
                                        ByVal strBookingSegmentID As String) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetTicketsCancelled(strOrigin,
                                                            strDestination,
                                                            strAgency,
                                                            strAirline,
                                                            strFlight,
                                                            dtReportFrom,
                                                            dtReportTo,
                                                            dtFlightFrom,
                                                            dtFlightTo,
                                                            intTicketonly,
                                                            intRefundable,
                                                            strProfileID,
                                                            strTicketNumber,
                                                            strFirstName,
                                                            strLastName,
                                                            strPassengerId,
                                                            strBookingSegmentID)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "TicketsCancelled", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetTicketsNotFlown"),
WebMethod(Description:="Return Unflown Ticket",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetTicketsNotFlown(ByVal strOrigin As String,
                                       ByVal strDestination As String,
                                       ByVal strAgency As String,
                                       ByVal strAirline As String,
                                       ByVal strFlight As String,
                                       ByVal dtReportFrom As Date,
                                       ByVal dtReportTo As Date,
                                       ByVal dtFlightFrom As Date,
                                       ByVal dtFlightTo As Date,
                                       ByVal bUnflown As Boolean,
                                       ByVal bNoShow As Boolean) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then


                        Dim clsRunCom As New clsRunComplus
                        rsA = clsRunCom.GetTicketsNotFlown(strOrigin,
                                                           strDestination,
                                                           strAgency,
                                                           strAirline,
                                                           strFlight,
                                                           dtReportFrom,
                                                           dtReportTo,
                                                           dtFlightFrom,
                                                           dtFlightTo,
                                                           bUnflown,
                                                           bNoShow)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "TicketsNotFlown", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using

    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetPaymentApprovals"),
WebMethod(Description:="Return Payment Approval",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetPaymentApprovals(ByVal dtPaymentFrom As Date,
                                        ByVal dtPaymentTo As Date,
                                        ByVal strDocumentFirst As String,
                                        ByVal strDocumentlast As String,
                                        ByVal strApprovalCode As String,
                                        ByVal strPaymentReference As String,
                                        ByVal strBookingReference As String,
                                        ByVal strNameOnCard As String,
                                        ByVal strStatus As String,
                                        ByVal strType As String,
                                        ByVal strError As String,
                                        ByVal strSubTypes As String,
                                        ByVal lPaymentNumber As Long,
                                        ByVal dPaymentAmount As Double,
                                        ByVal bExcludeSubTypes As Boolean) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus
                        rsA = clsRunCom.GetPaymentApprovals(dtPaymentFrom,
                                                            dtPaymentTo,
                                                            strDocumentFirst,
                                                            strDocumentlast,
                                                            strApprovalCode,
                                                            strPaymentReference,
                                                            strBookingReference,
                                                            strNameOnCard,
                                                            strStatus,
                                                            strType,
                                                            strError,
                                                            strSubTypes,
                                                            lPaymentNumber,
                                                            dPaymentAmount,
                                                            bExcludeSubTypes)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "PaymentApprovals", objOut)
                    End If
                End If
            Catch e As Exception
                Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(e, "GetPaymentApprovals Input : dtPaymentFrom " & dtPaymentFrom.ToString & "<br/>" &
                                                                                                                                ", dtPaymentTo " & dtPaymentTo.ToString & "<br/>" &
                                                                                                                                ", strDocumentFirst " & strDocumentFirst & "<br/>" &
                                                                                                                                ", strDocumentlast " & strDocumentlast & "<br/>" &
                                                                                                                                ", strApprovalCode " & strApprovalCode & "<br/>" &
                                                                                                                                ", strPaymentReference " & strPaymentReference & "<br/>" &
                                                                                                                                ", strBookingReference " & strBookingReference & "<br/>" &
                                                                                                                                ", strNameOnCard " & strNameOnCard & "<br/>" &
                                                                                                                                ", strStatus " & strStatus & "<br/>" &
                                                                                                                                ", strType " & strType & "<br/>" &
                                                                                                                                ", strError " & strError & "<br/>" &
                                                                                                                                ", strSubTypes " & strSubTypes & "<br/>" &
                                                                                                                                ", lPaymentNumber " & lPaymentNumber.ToString & "<br/>" &
                                                                                                                                ", dPaymentAmount " & dPaymentAmount.ToString & "<br/>" &
                                                                                                                                ", bExcludeSubTypes " & bExcludeSubTypes.ToString & "<br/>")
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/TicketRead"),
    WebMethod(Description:="Ticket Read",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function TicketRead(ByRef strBookingId As String,
                                   ByRef strPassengerId As String,
                                   ByRef strSegmentId As String,
                                   ByRef strTicketNumber As String,
                                   ByRef xmlTaxes As String,
                                   ByRef bReadOnly As Boolean,
                                   ByRef bReturnTax As Boolean) As DataSet


        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim rsTax As ADODB.Recordset = Session("rsTaxes")
                        Dim clsRunCom As New clsRunComplus

                        Dim r As New Helper.RecordSet

                        rsA = clsRunCom.TicketRead(strBookingId,
                                                   strPassengerId,
                                                   strSegmentId,
                                                   strTicketNumber,
                                                   rsTax,
                                                   bReadOnly,
                                                   bReturnTax)

                        clsRunCom = Nothing

                        Dim dsTaxes As New DataSet
                        Call InputRecordsetToDataset(rsTax, "Taxes", dsTaxes)
                        Call InputRecordsetToDataset(rsA, "Ticket", objOut)

                        xmlTaxes = dsTaxes.GetXml
                        If IsNothing(dsTaxes) = False Then
                            dsTaxes.Dispose()
                        End If
                        dsTaxes = Nothing
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetCashbookPayments"),
        WebMethod(Description:="Get Cashbook Payments",
        EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetCashbookPayments(ByVal strAgency As String,
                                            ByVal strGroup As String,
                                            ByVal strUserId As String,
                                            ByVal dtPaymentFrom As Date,
                                            ByVal dtPaymentTo As Date,
                                            ByVal strCashbookId As String) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetCashbookPayments(strAgency,
                                                            strGroup,
                                                            strUserId,
                                                            dtPaymentFrom,
                                                            dtPaymentTo)

                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "CashbookPayments", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function


    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetCashbookPaymentsSummary"),
      WebMethod(Description:="Get Cashbook Payments Summary",
      EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetCashbookPaymentsSummary(ByVal XmlCashbookPaymentsAll As String) As DataSet

        Using ds As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport
                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim aConvertXML As New clsConvertXML
                        Dim rsInput As ADODB.Recordset = aConvertXML.DatasetToRecordset(XmlCashbookPaymentsAll)

                        Dim clsRunCom As New clsRunComplus
                        rsA = clsRunCom.GetCashbookPaymentSummary(rsInput)
                        Call InputRecordsetToDataset(rsA, "CashbookPaymentsSummary", ds)
                    End If
                End If
            Catch e As Exception
                Return Nothing
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return ds
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetCashbookCharges"),
            WebMethod(Description:="Get Cashbook Charges",
            EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetCashbookCharges(ByVal XmlCashbookCharges As String,
                                           ByVal strCashbookId As String) As DataSet

        Using ds As New DataSet
            Dim rsInput As ADODB.Recordset = Nothing
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                    Return Nothing
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport
                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim aConvertXML As New clsConvertXML
                        rsInput = aConvertXML.DatasetToRecordset(XmlCashbookCharges)

                        Dim clsRunCom As New clsRunComplus
                        rsA = clsRunCom.GetCashbookCharges(rsInput, strCashbookId)
                        Call InputRecordsetToDataset(rsA, "CashbookCharges", ds)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsInput)
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return ds
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetBookingFeeAccounted"),
                WebMethod(Description:="Get Booking Fee Accounted",
                EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetBookingFeeAccounted(ByVal strAgencyCode As String,
                                               ByVal strUserId As String,
                                               ByVal strFee As String,
                                               ByVal dtFrom As Date,
                                               ByVal dtTo As Date) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus
                        rsA = clsRunCom.GetBookingFeeAccounted(strAgencyCode,
                                                              strUserId,
                                                              strFee,
                                                              dtFrom,
                                                              dtTo)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "FeeAccounted", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetBookingFeeBooked"),
                    WebMethod(Description:="Get Booking Fee Booked",
                    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetBookingFeeBooked(ByVal strAgencyCode As String,
                                            ByVal strUserId As String,
                                            ByVal strFee As String,
                                            ByVal dtFrom As Date,
                                            ByVal dtTo As Date) As DataSet


        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetBookingFeeBooked(strAgencyCode,
                                                            strUserId,
                                                            strFee,
                                                            dtFrom,
                                                            dtTo)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "FeeBooked", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetBookingFeeVoided"),
                    WebMethod(Description:="Get Booking Fee Voided",
                    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetBookingFeeVoided(ByVal strAgencyCode As String,
                                        ByVal strUserId As String,
                                        ByVal strFee As String,
                                        ByVal dtFrom As Date,
                                        ByVal dtTo As Date) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then


                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetBookingFeeVoided(strAgencyCode,
                                                            strUserId,
                                                            strFee,
                                                            dtFrom,
                                                            dtTo)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "FeeVoided", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/CreditCardPayment"),
                    WebMethod(Description:="CreditCard Payment",
                    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function CreditCardPayment(ByRef strCCNumber As String,
                                      ByRef strTransType As String,
                                      ByRef strTransStatus As String,
                                      ByRef dtFrom As Date,
                                      ByRef dtTo As Date,
                                      ByRef strCCType As String,
                                      ByRef strAgency As String) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.CreditCardPayment(strCCNumber,
                                                          strTransType,
                                                          strTransStatus,
                                                          dtFrom,
                                                          dtTo,
                                                          strCCType,
                                                          strAgency)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "CreditCardPayment", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetServiceFees"),
    WebMethod(Description:="Return Service Fee",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetServiceFees(ByRef strOrigin As String,
                                       ByRef strDestination As String,
                                       ByRef strCurrency As String,
                                       ByRef strAgency As String,
                                       ByRef strServiceGroup As String,
                                       ByRef dtFee As Date) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetServiceFees(strOrigin, strDestination, strCurrency, strAgency, strServiceGroup, dtFee)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "ServiceFees", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetServiceFeesByGroups"),
    WebMethod(Description:="Get Service Fee with multiple service group",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetServiceFeesByGroups(ByVal strXml As String) As String

        Try
            If (AgentHeader Is Nothing) Then
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus
                    Return clsRunCom.GetServiceFees(strXml)
                End If
            End If
        Catch e As Exception
        End Try

        Return String.Empty
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetBookingsThisUser"),
    WebMethod(Description:="Get Bookings This User",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetBookingsThisUser(ByRef strAgencyCode As String,
                                            ByRef strUserId As String,
                                            ByRef strAirline As String,
                                            ByRef strFlightNumber As String,
                                            ByRef dtFlightFrom As Date,
                                            ByRef dtFlightTo As Date,
                                            ByRef strRecordLocator As String,
                                            ByRef strOrigin As String,
                                            ByRef strDestination As String,
                                            ByRef strPassengerName As String,
                                            ByRef strSeatNumber As String,
                                            ByRef strTicketNumber As String,
                                            ByRef strPhoneNumber As String,
                                            ByRef dtCreateFrom As Date,
                                            ByRef dtCreateTo As Date) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then


                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetBookingsThisUser(strAgencyCode,
                                                            strUserId,
                                                            strAirline,
                                                            strFlightNumber,
                                                            dtFlightFrom,
                                                            dtFlightTo,
                                                            strRecordLocator,
                                                            strOrigin,
                                                            strDestination,
                                                            strPassengerName,
                                                            strSeatNumber,
                                                            strTicketNumber,
                                                            strPhoneNumber,
                                                            dtCreateFrom,
                                                            dtCreateTo)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "Bookings", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetPassengerProfileSegments"),
        WebMethod(Description:="Return Passenger Profile Segments",
        EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetPassengerProfileSegments(ByRef strPassengerProfileId As String) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetPassengerProfileSegments(strPassengerProfileId)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "ProfileSegments", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetOutstanding"),
    WebMethod(Description:="Get Outstanding",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetOutstanding(ByVal strAgencyCode As String,
                                   ByVal strAirline As String,
                                   ByVal strFlightNumber As String,
                                   ByVal dtFlightFrom As Date,
                                   ByVal dtFlightTo As Date,
                                   ByVal strOrigin As String,
                                   ByVal strDestination As String,
                                   ByVal bOffices As Boolean,
                                   ByVal bAgencies As Boolean,
                                   ByVal bLastTwentyFourHours As Boolean,
                                   ByVal bTicketedOnly As Boolean,
                                   ByVal iOlderThanHours As Integer,
                                   ByVal strLanguage As String,
                                   ByVal bAccountsPayable As Boolean) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then


                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetOutstanding(strAgencyCode,
                                                       strAirline,
                                                       strFlightNumber,
                                                       dtFlightFrom,
                                                       dtFlightTo,
                                                       strOrigin,
                                                       strDestination,
                                                       bOffices,
                                                       bAgencies,
                                                       bLastTwentyFourHours,
                                                       bTicketedOnly,
                                                       iOlderThanHours,
                                                       strLanguage,
                                                       bAccountsPayable)

                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "Outstanding", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetCorporateAgencyClients"),
        WebMethod(Description:="Get Corporate Agency Clients",
        EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetCorporateAgencyClients(ByRef AgencyCode As String) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetCorporateAgencyClients(AgencyCode)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "AgencyClients", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/InitializeUserAccountID"),
WebMethod(Description:="InitializeUserAccountID",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Sub InitializeUserAccountID(ByVal UserAccountId As String)
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim UserID As String = UserAccountId

                    Session("CreateById") = UserID.ToString
                End If
            End If

        Catch e As Exception

        End Try
    End Sub

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/AgencyRegistrationInsert"),
    WebMethod(Description:="Register Agency Information",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function AgencyRegistrationInsert(ByVal strAgencyName As String,
                                             ByVal strLegalName As String,
                                             ByVal strAgencyType As String,
                                             ByVal strIATA As String,
                                             ByVal strTaxId As String,
                                             ByVal strMail As String,
                                             ByVal strFax As String,
                                             ByVal strPhone As String,
                                             ByVal strAddress1 As String,
                                             ByVal strAddress2 As String,
                                             ByVal strStreet As String,
                                             ByVal strState As String,
                                             ByVal strDistrict As String,
                                             ByVal strProvince As String,
                                             ByVal strCity As String,
                                             ByVal strZipCode As String,
                                             ByVal strPoBox As String,
                                             ByVal strWebsite As String,
                                             ByVal strContactPerson As String,
                                             ByVal strLastName As String,
                                             ByVal strFirstName As String,
                                             ByVal strTitle As String,
                                             ByVal strUserLogon As String,
                                             ByVal strPassword As String,
                                             ByVal strCountry As String,
                                             ByVal strCurrency As String,
                                             ByVal strLanguage As String,
                                             ByVal strComment As String) As Boolean

        Dim Result As Boolean = False
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then

                    Dim clsRunCom As New clsRunComplus

                    Result = clsRunCom.AgencyRegistrationInsert(strAgencyName,
                                                                strLegalName,
                                                                strAgencyType,
                                                                strIATA,
                                                                strTaxId,
                                                                strMail,
                                                                strFax,
                                                                strPhone,
                                                                strAddress1,
                                                                strAddress2,
                                                                strStreet,
                                                                strState,
                                                                strDistrict,
                                                                strProvince,
                                                                strCity,
                                                                strZipCode,
                                                                strPoBox,
                                                                strWebsite,
                                                                strContactPerson,
                                                                strLastName,
                                                                strFirstName,
                                                                strTitle,
                                                                strUserLogon,
                                                                strPassword,
                                                                strCountry,
                                                                strCurrency,
                                                                strLanguage,
                                                                strComment)

                    clsRunCom = Nothing
                End If
            End If
        Catch e As Exception
        End Try

        Return Result
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/QueueMail"),
    WebMethod(Description:="Send mail",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function QueueMail(ByVal strFromAddress As String,
                              ByVal strFromName As String,
                              ByVal strToAddress As String,
                              ByVal strToAddressCC As String,
                              ByVal strToAddressBCC As String,
                              ByVal strReplyToAddress As String,
                              ByVal strSubject As String,
                              ByVal strBody As String,
                              ByVal strDocumentType As String,
                              ByVal strAttachmentStream As String,
                              ByVal strAttachmentFileName As String,
                              ByVal strAttachmentFileType As String,
                              ByVal strAttachmentParser As String,
                              ByVal bHtmlBody As Boolean,
                              ByVal bConvertAttachmentFromHTML2PDF As Boolean,
                              ByVal bRemoveFromQueue As Boolean,
                              ByVal strUserId As String,
                              ByVal strBookingId As String,
                              ByVal strVoucherId As String,
                              ByVal strBookingSegmentID As String,
                              ByVal strPassengerId As String,
                              ByVal strClientProfileId As String,
                              ByVal strDocumentId As String,
                              ByVal strLanguageCode As String) As Boolean

        Dim Result As Boolean = False
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then

                    Dim clsRunCom As New clsRunComplus

                    Result = clsRunCom.QueueMail(strFromAddress,
                                                strFromName,
                                                strToAddress,
                                                strToAddressCC,
                                                strToAddressBCC,
                                                strReplyToAddress,
                                                strSubject,
                                                strBody,
                                                strDocumentType,
                                                strAttachmentStream,
                                                strAttachmentFileName,
                                                strAttachmentFileType,
                                                strAttachmentParser,
                                                bHtmlBody,
                                                bConvertAttachmentFromHTML2PDF,
                                                bRemoveFromQueue, ,
                                                strUserId,
                                                strBookingId,
                                                strVoucherId,
                                                strBookingSegmentID,
                                                strPassengerId,
                                                strClientProfileId,
                                                strDocumentId,
                                                strLanguageCode)

                    clsRunCom = Nothing
                End If
            End If
        Catch e As Exception
        End Try

        Return Result
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetTransaction"),
WebMethod(Description:="Return FFP Transaction",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetTransaction(ByVal strOrigin As String,
                               ByVal strDestination As String,
                               ByVal strAirline As String,
                               ByVal strFlight As String,
                               ByVal strSegmentType As String,
                               ByVal strClientProfileId As String,
                               ByVal strPassengerProfileId As String,
                               ByVal strVendor As String,
                               ByVal strCreditDebit As String,
                               ByVal dtFlightFrom As Date,
                               ByVal dtFlightTo As Date,
                               ByVal dtTransactionFrom As Date,
                               ByVal dtTransactionTo As Date,
                               ByVal dtExpiryFrom As Date,
                               ByVal dtExpiryTo As Date,
                               ByVal dtVoidFrom As Date,
                               ByVal dtVoidTo As Date,
                               ByVal iBatch As Integer,
                               ByVal bAllVoid As Boolean,
                               ByVal bAllExpired As Boolean,
                               ByVal bAuto As Boolean,
                               ByVal bManual As Boolean,
                               ByVal bAllPoint As Boolean) As String

        Dim Xml As String = String.Empty
        Dim rsTransaction As ADODB.Recordset = Nothing
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus


                    rsTransaction = clsRunCom.GetTransaction(strOrigin,
                                                            strDestination,
                                                            strAirline,
                                                            strFlight,
                                                            strSegmentType,
                                                            strClientProfileId,
                                                            strPassengerProfileId,
                                                            strVendor,
                                                            strCreditDebit,
                                                            dtFlightFrom,
                                                            dtFlightTo,
                                                            dtTransactionFrom,
                                                            dtTransactionTo,
                                                            dtExpiryFrom,
                                                            dtExpiryTo,
                                                            dtVoidFrom,
                                                            dtVoidTo,
                                                            iBatch,
                                                            bAllVoid,
                                                            bAllExpired,
                                                            bAuto,
                                                            bManual,
                                                            bAllPoint)

                    Xml = RecordSetToXML(rsTransaction, "Transactions", "Transaction")
                    clsRunCom = Nothing
                Else

                End If
            End If
        Catch e As Exception
            Xml = String.Empty
        Finally
            Helper.Check.ClearRecordset(rsTransaction)
        End Try
        Return Xml
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/PassengerRoleRead"),
    WebMethod(Description:="Read Passenger Role",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function PassengerRoleRead(ByVal strPaxRoleCode As String,
                                      ByVal strPaxRole As String,
                                      ByVal strStatus As String,
                                      ByVal bWrite As Boolean,
                                      ByVal strLanguage As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.PassengerRoleRead(strPaxRoleCode, strPaxRole, strStatus, strLanguage, bWrite)

                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "PassengerRole", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetAgencyCommissionDetails"),
WebMethod(Description:="Get Agency Commission Details",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetAgencyCommissionDetails(ByVal strAgency As String,
                                        ByVal strOrigin As String,
                                        ByVal strDestination As String,
                                        ByVal strAirline As String,
                                        ByVal strFlight As String,
                                        ByVal dtFlightFrom As DateTime,
                                        ByVal dtFlightTo As DateTime,
                                        ByVal dtSalesFrom As DateTime,
                                        ByVal dtSalesTo As DateTime,
                                        ByVal strLanguage As String,
                                        ByVal strStatus As String,
                                        ByVal strOwner As String,
                                        ByVal strCurrency As String) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetAgencyCommissionDetails(strAgency,
                                                                    strOrigin,
                                                                    strDestination,
                                                                    strAirline,
                                                                    strFlight,
                                                                    dtFlightFrom,
                                                                    dtFlightTo,
                                                                    dtSalesFrom,
                                                                    dtSalesTo,
                                                                    strLanguage,
                                                                    strStatus,
                                                                    strOwner,
                                                                    strCurrency)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "AgencyCommissionDetails", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetAgencyTicketSales"),
WebMethod(Description:="Get Agency Ticket Sales",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetAgencyTicketSales(ByVal strAgency As String,
                                ByVal strCurrency As String,
                                ByVal dtSalesFrom As DateTime,
                                ByVal dtSalesTo As DateTime) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetAgencyTicketSales(strAgency,
                                                                strCurrency,
                                                                dtSalesFrom,
                                                                dtSalesTo)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "AgencyTicketSales", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/MemberLevelRead"),
WebMethod(Description:="Read Member Level",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function MemberLevelRead(ByVal strMemberLevelCode As String,
                                ByVal strMemberLevel As String,
                                ByVal strStatus As String,
                                ByVal bWrite As Boolean) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.MemberLevelRead(strMemberLevelCode, strMemberLevel, strStatus, bWrite)

                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "PassengerRole", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/CheckUniqueMailAddress"),
        WebMethod(Description:="Check unique client email address",
        EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function CheckUniqueMailAddress(ByVal strMail As String, ByVal strClientProfileId As String) As Boolean

        Dim Result As Boolean = False
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then

                    Dim clsRunCom As New clsRunComplus

                    Result = clsRunCom.CheckUniqueMailAddress(strMail, strClientProfileId)
                    clsRunCom = Nothing
                End If
            End If
        Catch e As Exception
        End Try

        Return Result
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetAgencyAccountTransactions"),
         WebMethod(Description:="Return summary transactions for Agency Account transaction report",
         EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetAgencyAccountTransactions(ByVal agencyCode As String, ByVal dateFrom As DateTime, ByVal dateTo As DateTime) As DataSet
        Using dsOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Dim curr As String = String.Empty
            Try
                If (Not AgentHeader Is Nothing) Then
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then


                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetAgencyAccountTransactions(agencyCode, dateFrom, dateTo)

                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "Transaction", dsOut)

                        ''----------------------------------------------------
                        ''add starting balance
                        dsOut.Tables(0).Columns.Add("balance", GetType(String))
                        dsOut.Tables(0).Rows.InsertAt(dsOut.Tables(0).NewRow(), 0)
                        dsOut.Tables(0).Rows(0)("xact") = "SUM"
                        dsOut.Tables(0).Rows(0)("doc") = "Opening Balance"
                        dsOut.Tables(0).Rows(0)("currency_rcd") = curr

                    End If
                End If
            Catch e As Exception
                Throw New Exception(e.Message)
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return dsOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/SaveBookingCreditCard"),
        WebMethod(Description:="Save booking with payment credit card",
        EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function SaveBookingCreditCard(ByVal BookingId As String,
                                        ByVal XmlHeader As String,
                                        ByVal XmlSegments As String,
                                        ByVal XmlPassengers As String,
                                        ByVal XmlPayments As String,
                                        ByVal XmlRemarks As String,
                                        ByVal XmlMapping As String,
                                        ByVal XmlFees As String,
                                        ByVal XmlPaymentFees As String,
                                        ByVal XmlTaxes As String,
                                        ByVal XmlServices As String,
                                        ByVal CreateTickets As Boolean,
                                        ByVal securityToken As String,
                                        ByVal authenticationToken As String,
                                        ByVal commerceIndicator As String,
                                        ByVal strRequestSource As String,
                                        ByVal strLanguage As String) As DataSet

        Using ds As New DataSet
            Dim FabricateRecordset As New FabricateRecordset
            Dim rsCCPayments As ADODB.Recordset = Nothing
            Dim rsVoucher As ADODB.Recordset = Nothing
            Dim rsCC As ADODB.Recordset = Nothing
            Dim rsAllocation As ADODB.Recordset = FabricateRecordset.FabricatePaymentAllocationRecordset
            Dim stSaveBookingResult As Short = -1
            Dim strCCResponse As String = ""

            Try

                Dim clsRunCom As New clsRunComplus

                Dim rsHeader As ADODB.Recordset = Session("rsHeader")
                Dim rsSegments As ADODB.Recordset = Session("rsSegments")
                Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
                Dim rsRemarks As ADODB.Recordset = Session("rsRemarks")
                Dim rsPayments As ADODB.Recordset = Session("rsPayments")
                Dim rsMappings As ADODB.Recordset = Session("rsMappings")
                Dim rsServices As ADODB.Recordset = Session("rsServices")
                Dim rsTaxes As ADODB.Recordset = Session("rsTaxes")
                Dim rsFees As ADODB.Recordset = Session("rsFees")
                Dim rsQuotes As ADODB.Recordset = Session("rsQuotes")



                Dim BookingHeaderUpdate As New BookingHeaderUpdate
                Dim Passengers As New Passengers
                Dim Mappings As New Mappings
                Dim Segments As New Itinerary
                Dim PaymentsUpdate As New PaymentsUpdate
                Dim Fees As New Fees
                Dim TotalFees As New Fees
                Dim Remarks As New Remarks
                Dim MappingsUpdate As New MappingsUpdate
                Dim Taxes As New Taxes
                Dim Quotes As New Quotes
                Dim Services As New Services

                Dim Allocations As Allocations
                Dim strRecordLocator As String = String.Empty
                Dim iBookingNumber As Integer

                Dim OutStandingBalance As Decimal

                clsRunCom.GetEmpty(AgentHeader.AgencyCode, AgentHeader.AgencyCurrencyRcd, , , , , rsCCPayments)
                If Not rsCCPayments Is Nothing Then

                    XmlHeader = XmlHeader.Replace("BookingHeader", "BookingHeaderUpdate")
                    BookingHeaderUpdate = Serialization.Deserialize(XmlHeader, BookingHeaderUpdate)

                    XmlPayments = XmlPayments.Replace("Payment", "PaymentUpdate")
                    PaymentsUpdate = Serialization.Deserialize(XmlPayments, PaymentsUpdate)

                    Passengers = Serialization.Deserialize(XmlPassengers, Passengers)
                    Segments = Serialization.Deserialize(XmlSegments, Segments)
                    Mappings = Serialization.Deserialize(XmlMapping, Mappings)
                    Fees = Serialization.Deserialize(XmlFees, Fees)

                    XmlMapping = XmlMapping.Replace("Mapping", "MappingUpdate")
                    MappingsUpdate = Serialization.Deserialize(XmlMapping, MappingsUpdate)

                    Services = Serialization.Deserialize(XmlServices, Services)
                    Remarks = Serialization.Deserialize(XmlRemarks, Remarks)

                    Dim r As New Helper.RecordSet
                    ' Fill fee
                    r.ClearFees(rsFees, Fees)
                    r.FillFees(rsFees, Fees)
                    r.FillMapping(rsMappings, MappingsUpdate)

                    OutStandingBalance = Helper.Util.CalOutStandingBalance(rsFees, rsMappings)

                    'Find Payment Allocation.
                    If XmlPaymentFees.Length > 0 Then
                        TotalFees = Serialization.Deserialize(XmlPaymentFees, TotalFees)
                        If TotalFees.Count > 0 Then
                            OutStandingBalance = OutStandingBalance + TotalFees(0).fee_amount_incl
                        End If
                    End If

                    'Validate OutStandingBalance
                    If OutStandingBalance > 0 Then
                        Allocations = GetPaymentAllocations(Mappings, Fees, True, TotalFees)
                        r.Fill(rsAllocation, Allocations, False)
                    End If

                    With r
                        If Not Session("CreateById") Is Nothing AndAlso Not Session("CreateById") = String.Empty Then
                            .DefaultCreateById = New Guid(Session("CreateById").ToString())
                        Else
                            If IsGuid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")) Then
                                Session("CreateById") = System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")
                            End If
                            r.DefaultCreateById = New Guid(Session("CreateById").ToString())
                        End If

                        .Fill(rsCCPayments, PaymentsUpdate)
                        .FillHeader(rsHeader, BookingHeaderUpdate)
                        .FillPassengers(rsPassengers, Passengers)
                        .FillMapping(rsMappings, MappingsUpdate)
                        .FillRemarks(rsRemarks, Remarks)

                        .ClearServices(rsServices, Services)
                        .FillServices(rsServices, Services)

                        If .FeeChange(rsFees, Fees) Then
                            .FillCreateDate(rsFees)
                            .FillAccountBy(rsFees)
                        End If
                        If .SegmentChange(rsSegments, Segments) Then
                            .FillCreateDate(rsSegments)
                        End If
                    End With
                    If InfantExcessLimit(rsSegments, BookingHeaderUpdate.number_of_infants) = False Then
                        If ValidateSave(rsHeader, rsSegments, rsPassengers, rsMappings) = True Then

                            'Get Record locator
                            If rsHeader.Fields("record_locator").Value Is System.DBNull.Value Then

                                clsRunCom.GetRecordLocator(strRecordLocator, iBookingNumber)
                                rsHeader.Fields("booking_number").Value = iBookingNumber
                                rsHeader.Fields("record_locator").Value = strRecordLocator
                            ElseIf Len(rsHeader.Fields("record_locator").Value) = 0 Then

                                clsRunCom.GetRecordLocator(strRecordLocator, iBookingNumber)
                                rsHeader.Fields("booking_number").Value = iBookingNumber
                                rsHeader.Fields("record_locator").Value = strRecordLocator
                            Else
                                strRecordLocator = rsHeader.Fields("record_locator").Value
                            End If

                            'Validate seat assignment and session lock que.
                            If clsRunCom.VerifyFlightInventorySession(rsSegments) = False Then
                                CreateErrorDataset(ds, "202", "SESSIONTIMEOUT")
                            ElseIf clsRunCom.VerifySeatAssignment(rsMappings, rsSegments) = True Then
                                CreateErrorDataset(ds, "203", "DUPLICATESEAT")
                            Else

                                'Validate OutStandingBalance
                                If OutStandingBalance > 0 Then
                                    rsCC = clsRunCom.ValidCreditCard(rsCCPayments,
                                                                    rsSegments,
                                                                    rsAllocation,
                                                                    rsVoucher,
                                                                    rsMappings,
                                                                    securityToken,
                                                                    authenticationToken,
                                                                    commerceIndicator,
                                                                    strRecordLocator,
                                                                    rsFees,
                                                                    rsHeader,
                                                                    rsTaxes,
                                                                    strRequestSource,
                                                                    True)
                                End If

                                Dim isSave As Boolean = False
                                If (Not rsCC Is Nothing) Then
                                    If rsCC.RecordCount > 0 Then
                                        rsCC.MoveFirst()
                                        strCCResponse = rsCC("ResponseCode").Value
                                        If strCCResponse = "APPROVED" Then
                                            isSave = True
                                        Else
                                            Call InputRecordsetToDataset(rsCC, "Payments", ds)

                                            'Assign Recordlocator to error message if have one.
                                            If String.IsNullOrEmpty(strRecordLocator) = False Then
                                                If IsNothing(ds) = False AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                                                    Dim dcRecordlocator As New DataColumn("record_locator")
                                                    Dim dcBookingNumber As New DataColumn("booking_number")

                                                    'For Record Locator
                                                    ds.Tables(0).Columns.Add(dcRecordlocator)
                                                    ds.Tables(0).Rows(0).Item("record_locator") = strRecordLocator
                                                    'For Booking number
                                                    ds.Tables(0).Columns.Add(dcBookingNumber)
                                                    ds.Tables(0).Rows(0).Item("booking_number") = iBookingNumber

                                                    dcRecordlocator.Dispose()
                                                    dcBookingNumber.Dispose()

                                                    dcRecordlocator = Nothing
                                                    dcBookingNumber = Nothing

                                                End If
                                            End If
                                        End If
                                    Else
                                        If System.Configuration.ConfigurationManager.AppSettings("ErrorMailEnabled").ToUpper = "TRUE" Then
                                            Dim ErrorSubject As String = System.Configuration.ConfigurationManager.AppSettings("ErrorSubject") & System.Convert.ToString(Date.Now)
                                            Mail.Send(ErrorHelper.RenderDebugMessage("SaveBookingCreditCard", "CC recordset has no record", XmlHeader, XmlSegments, XmlPassengers, XmlPayments, XmlRemarks, XmlMapping, XmlFees, XmlTaxes, String.Empty, String.Empty), ErrorSubject, System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), Nothing)
                                        End If
                                    End If
                                ElseIf OutStandingBalance = 0 Then
                                    isSave = True
                                Else
                                    If System.Configuration.ConfigurationManager.AppSettings("ErrorMailEnabled").ToUpper = "TRUE" Then
                                        Dim ErrorSubject As String = System.Configuration.ConfigurationManager.AppSettings("ErrorSubject") & System.Convert.ToString(Date.Now)
                                        Mail.Send(ErrorHelper.RenderDebugMessage("SaveBookingCreditCard", "CC recordset is nothing", XmlHeader, XmlSegments, XmlPassengers, XmlPayments, XmlRemarks, XmlMapping, XmlFees, XmlTaxes, String.Empty, String.Empty), ErrorSubject, System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), Nothing)
                                    End If
                                End If

                                If isSave Then
                                    'Perform Save booking
                                    stSaveBookingResult = clsRunCom.SaveBooking(rsHeader,
                                                                                 rsSegments,
                                                                                 rsPassengers,
                                                                                 rsRemarks, ,
                                                                                 rsMappings,
                                                                                 rsServices,
                                                                                 rsTaxes,
                                                                                 rsFees,
                                                                                 CreateTickets,
                                                                                 False,
                                                                                 False,
                                                                                 True,
                                                                                 False,
                                                                                 False)
                                    If stSaveBookingResult = -1 Then
                                        'Sent Error log Email
                                        Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                             XmlHeader,
                                                                                                                                                                             XmlSegments,
                                                                                                                                                                             XmlPassengers,
                                                                                                                                                                             XmlPayments,
                                                                                                                                                                             XmlRemarks,
                                                                                                                                                                             XmlMapping,
                                                                                                                                                                             XmlFees,
                                                                                                                                                                             XmlPaymentFees,
                                                                                                                                                                             XmlTaxes,
                                                                                                                                                                             XmlServices,
                                                                                                                                                                             CreateTickets,
                                                                                                                                                                             securityToken,
                                                                                                                                                                             authenticationToken,
                                                                                                                                                                             commerceIndicator,
                                                                                                                                                                             strRequestSource,
                                                                                                                                                                             strLanguage,
                                                                                                                                                                             strCCResponse,
                                                                                                                                                                             stSaveBookingResult.ToString()),
                                                                                                                                                    "SAVEFAILED",
                                                                                                                                                    "",
                                                                                                                                                    "SaveBookingCreditCard",
                                                                                                                                                    "TikAero.WebService",
                                                                                                                                                    "")
                                        CreateErrorDataset(ds, "201", "SAVEFAILED")
                                    ElseIf stSaveBookingResult = 1 Then

                                        'Sent Error log Email
                                        Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                             XmlHeader,
                                                                                                                                                                             XmlSegments,
                                                                                                                                                                             XmlPassengers,
                                                                                                                                                                             XmlPayments,
                                                                                                                                                                             XmlRemarks,
                                                                                                                                                                             XmlMapping,
                                                                                                                                                                             XmlFees,
                                                                                                                                                                             XmlPaymentFees,
                                                                                                                                                                             XmlTaxes,
                                                                                                                                                                             XmlServices,
                                                                                                                                                                             CreateTickets,
                                                                                                                                                                             securityToken,
                                                                                                                                                                             authenticationToken,
                                                                                                                                                                             commerceIndicator,
                                                                                                                                                                             strRequestSource,
                                                                                                                                                                             strLanguage,
                                                                                                                                                                             strCCResponse,
                                                                                                                                                                             stSaveBookingResult.ToString()),
                                                                                                                                                    "SESSIONTIMEOUT",
                                                                                                                                                    "",
                                                                                                                                                    "SaveBookingCreditCard",
                                                                                                                                                    "TikAero.WebService",
                                                                                                                                                    "")
                                        CreateErrorDataset(ds, "202", "SESSIONTIMEOUT")
                                    ElseIf stSaveBookingResult = 2 Then

                                        'Sent Error log Email
                                        Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                             XmlHeader,
                                                                                                                                                                             XmlSegments,
                                                                                                                                                                             XmlPassengers,
                                                                                                                                                                             XmlPayments,
                                                                                                                                                                             XmlRemarks,
                                                                                                                                                                             XmlMapping,
                                                                                                                                                                             XmlFees,
                                                                                                                                                                             XmlPaymentFees,
                                                                                                                                                                             XmlTaxes,
                                                                                                                                                                             XmlServices,
                                                                                                                                                                             CreateTickets,
                                                                                                                                                                             securityToken,
                                                                                                                                                                             authenticationToken,
                                                                                                                                                                             commerceIndicator,
                                                                                                                                                                             strRequestSource,
                                                                                                                                                                             strLanguage,
                                                                                                                                                                             strCCResponse,
                                                                                                                                                                             stSaveBookingResult.ToString()),
                                                                                                                                                    "DUPLICATESEAT",
                                                                                                                                                    "",
                                                                                                                                                    "SaveBookingCreditCard",
                                                                                                                                                    "TikAero.WebService",
                                                                                                                                                    "")
                                        CreateErrorDataset(ds, "203", "DUPLICATESEAT")
                                    ElseIf stSaveBookingResult = 3 Then
                                        'Sent Error log Email
                                        Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                             XmlHeader,
                                                                                                                                                                             XmlSegments,
                                                                                                                                                                             XmlPassengers,
                                                                                                                                                                             XmlPayments,
                                                                                                                                                                             XmlRemarks,
                                                                                                                                                                             XmlMapping,
                                                                                                                                                                             XmlFees,
                                                                                                                                                                             XmlPaymentFees,
                                                                                                                                                                             XmlTaxes,
                                                                                                                                                                             XmlServices,
                                                                                                                                                                             CreateTickets,
                                                                                                                                                                             securityToken,
                                                                                                                                                                             authenticationToken,
                                                                                                                                                                             commerceIndicator,
                                                                                                                                                                             strRequestSource,
                                                                                                                                                                             strLanguage,
                                                                                                                                                                             strCCResponse,
                                                                                                                                                                             stSaveBookingResult.ToString()),
                                                                                                                                                    "DATABASEACCESS",
                                                                                                                                                    "",
                                                                                                                                                    "SaveBookingCreditCard",
                                                                                                                                                    "TikAero.WebService",
                                                                                                                                                    "")
                                        CreateErrorDataset(ds, "204", "DATABASEACCESS")
                                    ElseIf stSaveBookingResult = 4 Then

                                        'Sent Error log Email
                                        Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                             XmlHeader,
                                                                                                                                                                             XmlSegments,
                                                                                                                                                                             XmlPassengers,
                                                                                                                                                                             XmlPayments,
                                                                                                                                                                             XmlRemarks,
                                                                                                                                                                             XmlMapping,
                                                                                                                                                                             XmlFees,
                                                                                                                                                                             XmlPaymentFees,
                                                                                                                                                                             XmlTaxes,
                                                                                                                                                                             XmlServices,
                                                                                                                                                                             CreateTickets,
                                                                                                                                                                             securityToken,
                                                                                                                                                                             authenticationToken,
                                                                                                                                                                             commerceIndicator,
                                                                                                                                                                             strRequestSource,
                                                                                                                                                                             strLanguage,
                                                                                                                                                                             strCCResponse,
                                                                                                                                                                             stSaveBookingResult.ToString()),
                                                                                                                                                    "BOOKINGINUSE",
                                                                                                                                                    "",
                                                                                                                                                    "SaveBookingCreditCard",
                                                                                                                                                    "TikAero.WebService",
                                                                                                                                                    "")
                                        CreateErrorDataset(ds, "205", "BOOKINGINUSE")
                                    ElseIf stSaveBookingResult = 5 Then

                                        'Sent Error log Email
                                        Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                             XmlHeader,
                                                                                                                                                                             XmlSegments,
                                                                                                                                                                             XmlPassengers,
                                                                                                                                                                             XmlPayments,
                                                                                                                                                                             XmlRemarks,
                                                                                                                                                                             XmlMapping,
                                                                                                                                                                             XmlFees,
                                                                                                                                                                             XmlPaymentFees,
                                                                                                                                                                             XmlTaxes,
                                                                                                                                                                             XmlServices,
                                                                                                                                                                             CreateTickets,
                                                                                                                                                                             securityToken,
                                                                                                                                                                             authenticationToken,
                                                                                                                                                                             commerceIndicator,
                                                                                                                                                                             strRequestSource,
                                                                                                                                                                             strLanguage,
                                                                                                                                                                             strCCResponse,
                                                                                                                                                                             stSaveBookingResult.ToString()),
                                                                                                                                                    "BOOKINGREADERROR",
                                                                                                                                                    "",
                                                                                                                                                    "SaveBookingCreditCard",
                                                                                                                                                    "TikAero.WebService",
                                                                                                                                                    "")
                                        CreateErrorDataset(ds, "206", "BOOKINGREADERROR")
                                    Else
                                        Call clsRunCom.UpdatePassengerNames(rsPassengers,
                                                                            rsMappings,
                                                                            rsHeader,
                                                                            rsFees,
                                                                            rsSegments,
                                                                            AgentHeader.AgencyCode,
                                                                            AgentHeader.AgencyCurrencyRcd,
                                                                            BookingId,
                                                                            True)

                                        If CreateTickets Then
                                            Dim b As Boolean = clsRunCom.TicketCreate(AgentHeader.AgencyCode,
                                                                                      Session("CreateById").ToString().Replace("{", "").Replace("}", ""), ,
                                                                                      "{" & BookingId & "}", False)
                                        End If

                                        'Update Approval
                                        Dim strIBE = "B2C,B2E"
                                        If BookingHeaderUpdate.booking_source_rcd.ToUpper().IndexOf(strIBE) > 0 Then
                                            Dim bUpdateApproval As String = System.Configuration.ConfigurationManager.AppSettings("UpdateApproval")
                                            If Not String.IsNullOrEmpty(bUpdateApproval) Then
                                                If bUpdateApproval.ToString.ToLower = "true" Then
                                                    clsRunCom.UpdateApproval(BookingId, 1)
                                                End If
                                            End If
                                        End If

                                        'Clear recordset before call get itinerary.
                                        ClearBookingRecordset(rsHeader,
                                                              rsSegments,
                                                              rsPassengers,
                                                              rsRemarks,
                                                              rsPayments,
                                                              rsMappings,
                                                              rsServices,
                                                              rsTaxes,
                                                              rsFees,
                                                              rsQuotes)

                                        Call clsRunCom.ItiniraryRead(BookingId,
                                                                     String.Empty,
                                                                     BookingHeaderUpdate.agency_code,
                                                                     rsHeader,
                                                                     rsSegments,
                                                                     rsPassengers,
                                                                     rsRemarks,
                                                                     rsPayments,
                                                                     rsMappings,
                                                                     rsServices,
                                                                     rsTaxes,
                                                                     rsFees,
                                                                     rsQuotes,
                                                                     strLanguage)


                                        CopyRecordsetsToDataset(rsHeader,
                                                                rsSegments,
                                                                rsPassengers,
                                                                rsRemarks,
                                                                rsPayments,
                                                                rsMappings,
                                                                rsTaxes,
                                                                rsQuotes,
                                                                rsFees,
                                                                rsServices,
                                                                ds)

                                        'Release Unmanage resource
                                        ClearBookingRecordset(rsHeader,
                                                              rsSegments,
                                                              rsPassengers,
                                                              rsRemarks,
                                                              rsPayments,
                                                              rsMappings,
                                                              rsServices,
                                                              rsTaxes,
                                                              rsFees,
                                                              rsQuotes)

                                        'Remove booking session
                                        Session.Remove("rsHeader")
                                        Session.Remove("rsSegments")
                                        Session.Remove("rsPassenger")
                                        Session.Remove("rsRemarks")
                                        Session.Remove("rsPayments")
                                        Session.Remove("rsMappings")
                                        Session.Remove("rsServices")
                                        Session.Remove("rsTaxes")
                                        Session.Remove("rsFees")
                                        Session.Remove("rsQuotes")

                                        Dim BookingState = New Booking
                                        Serialization.FillBooking(ds, BookingState)
                                        Session("BookingState") = BookingState
                                    End If
                                End If
                            End If

                        Else
                            If System.Configuration.ConfigurationManager.AppSettings("ErrorMailEnabled").ToUpper = "TRUE" Then
                                Dim ErrorSubject As String = System.Configuration.ConfigurationManager.AppSettings("ErrorSubject") & System.Convert.ToString(Date.Now)
                                Mail.Send(ErrorHelper.RenderDebugMessage("SaveBookingCreditCard", "Error Missing Data", XmlHeader, XmlSegments, XmlPassengers, XmlPayments, XmlRemarks, XmlMapping, XmlFees, XmlTaxes, String.Empty, String.Empty), ErrorSubject, System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), Nothing)
                            End If
                        End If
                    Else
                        CreateErrorDataset(ds, "200", "Infant over limit")
                    End If
                End If
            Catch e As Exception
                Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(e,
                                                                                                                      CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                     XmlHeader,
                                                                                                                                                     XmlSegments,
                                                                                                                                                     XmlPassengers,
                                                                                                                                                     XmlPayments,
                                                                                                                                                     XmlRemarks,
                                                                                                                                                     XmlMapping,
                                                                                                                                                     XmlFees,
                                                                                                                                                     XmlPaymentFees,
                                                                                                                                                     XmlTaxes,
                                                                                                                                                     XmlServices,
                                                                                                                                                     CreateTickets,
                                                                                                                                                     securityToken,
                                                                                                                                                     authenticationToken,
                                                                                                                                                     commerceIndicator,
                                                                                                                                                     strRequestSource,
                                                                                                                                                     strLanguage,
                                                                                                                                                     strCCResponse,
                                                                                                                                                     stSaveBookingResult.ToString()))


            Finally

                Helper.Check.ClearRecordset(rsCCPayments)
                Helper.Check.ClearRecordset(rsVoucher)
                Helper.Check.ClearRecordset(rsAllocation)
                Helper.Check.ClearRecordset(rsCC)
            End Try
            Return ds
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/SaveBookingPayment"),
            WebMethod(Description:="Save booking with payment",
            EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function SaveBookingPayment(ByVal BookingId As String,
                                            ByVal XmlHeader As String,
                                            ByVal XmlSegments As String,
                                            ByVal XmlPassengers As String,
                                            ByVal XmlPayments As String,
                                            ByVal XmlRemarks As String,
                                            ByVal XmlMapping As String,
                                            ByVal XmlFees As String,
                                            ByVal XmlPaymentFees As String,
                                            ByVal XmlTaxes As String,
                                            ByVal XMLServices As String,
                                            ByVal CreateTickets As Boolean,
                                            ByVal strLanguage As String) As DataSet

        Using ds As New DataSet
            Dim FabricateRecordset As New FabricateRecordset
            Dim rsAllocation As ADODB.Recordset = FabricateRecordset.FabricatePaymentAllocationRecordset
            Dim rsActualPayments As ADODB.Recordset = Nothing
            Dim rsVoucher As ADODB.Recordset = Nothing
            Dim stSaveBookingResult As Short = -1
            Dim strVoucherSessionId As String = Guid.NewGuid.ToString
            Try

                Dim clsRunCom As New clsRunComplus

                Dim rsHeader As ADODB.Recordset = Session("rsHeader")
                Dim rsSegments As ADODB.Recordset = Session("rsSegments")
                Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
                Dim rsRemarks As ADODB.Recordset = Session("rsRemarks")
                Dim rsPayments As ADODB.Recordset = Session("rsPayments")
                Dim rsMappings As ADODB.Recordset = Session("rsMappings")
                Dim rsServices As ADODB.Recordset = Session("rsServices")
                Dim rsTaxes As ADODB.Recordset = Session("rsTaxes")
                Dim rsFees As ADODB.Recordset = Session("rsFees")
                Dim rsQuotes As ADODB.Recordset = Session("rsQuotes")

                Dim BookingHeaderUpdate As New BookingHeaderUpdate
                Dim Passengers As New Passengers
                Dim Mappings As New Mappings
                Dim Segments As New Itinerary
                Dim Fees As New Fees
                Dim TotalFees As New Fees
                Dim PaymentsUpdate As New PaymentsUpdate
                Dim Remarks As New Remarks
                Dim MappingsUpdate As New MappingsUpdate
                Dim Taxes As New Taxes
                Dim Quotes As New Quotes
                Dim Services As New Services

                Dim Allocations As Allocations

                Dim OutStandingBalance As Decimal

                clsRunCom.GetEmpty(AgentHeader.AgencyCode, AgentHeader.AgencyCurrencyRcd, , , , , rsActualPayments)
                If Not rsActualPayments Is Nothing Then

                    XmlHeader = XmlHeader.Replace("BookingHeader", "BookingHeaderUpdate")
                    BookingHeaderUpdate = Serialization.Deserialize(XmlHeader, BookingHeaderUpdate)

                    XmlPayments = XmlPayments.Replace("Payment", "PaymentUpdate")
                    PaymentsUpdate = Serialization.Deserialize(XmlPayments, PaymentsUpdate)

                    Passengers = Serialization.Deserialize(XmlPassengers, Passengers)
                    Segments = Serialization.Deserialize(XmlSegments, Segments)
                    Mappings = Serialization.Deserialize(XmlMapping, Mappings)
                    Fees = Serialization.Deserialize(XmlFees, Fees)

                    XmlMapping = XmlMapping.Replace("Mapping", "MappingUpdate")
                    MappingsUpdate = Serialization.Deserialize(XmlMapping, MappingsUpdate)
                    Services = Serialization.Deserialize(XMLServices, Services)
                    Remarks = Serialization.Deserialize(XmlRemarks, Remarks)

                    'Find Payment Allocation.
                    If XmlPaymentFees.Length > 0 Then
                        TotalFees = Serialization.Deserialize(XmlPaymentFees, TotalFees)
                    End If

                    Dim r As New Helper.RecordSet
                    r.ClearFees(rsFees, Fees)
                    r.FillFees(rsFees, Fees)
                    r.FillMapping(rsMappings, MappingsUpdate)

                    OutStandingBalance = Helper.Util.CalOutStandingBalance(rsFees, rsMappings)

                    'Validate OutStandingBalance
                    If OutStandingBalance > 0 Then
                        Allocations = GetPaymentAllocations(Mappings, Fees, True, TotalFees)
                        r.Fill(rsAllocation, Allocations, False)
                    End If

                    'Dim Test = Serialize(Allocations)

                    With r
                        If Not Session("CreateById") Is Nothing AndAlso Not Session("CreateById") = String.Empty Then
                            .DefaultCreateById = New Guid(Session("CreateById").ToString())
                        Else
                            If IsGuid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")) Then
                                Session("CreateById") = System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")
                            End If
                            r.DefaultCreateById = New Guid(Session("CreateById").ToString())
                        End If

                        .Fill(rsActualPayments, PaymentsUpdate)

                        .FillHeader(rsHeader, BookingHeaderUpdate)
                        .FillPassengers(rsPassengers, Passengers)
                        .FillRemarks(rsRemarks, Remarks)

                        .ClearServices(rsServices, Services)
                        .FillServices(rsServices, Services)

                        If .FeeChange(rsFees, Fees) Then
                            .FillCreateDate(rsFees)
                            .FillAccountBy(rsFees)
                        End If
                        If .SegmentChange(rsSegments, Segments) Then
                            .FillCreateDate(rsSegments)
                        End If
                    End With
                    If InfantExcessLimit(rsSegments, BookingHeaderUpdate.number_of_infants) = False Then
                        If ValidateSave(rsHeader, rsSegments, rsPassengers, rsMappings) = True Then
                            'Validate duplicate seat and booking session.
                            If clsRunCom.VerifyFlightInventorySession(rsSegments) = False Then
                                CreateErrorDataset(ds, "202", "SESSIONTIMEOUT")
                            ElseIf clsRunCom.VerifySeatAssignment(rsMappings, rsSegments) = True Then
                                CreateErrorDataset(ds, "203", "DUPLICATESEAT")
                            ElseIf clsRunCom.ValidAmount(rsActualPayments, rsMappings, rsFees, BookingHeaderUpdate.agency_code, BookingHeaderUpdate.ip_address, r.DefaultCreateById.ToString, strVoucherSessionId) = False Then
                                CreateErrorDataset(ds, "207", "NOT ENOUGH AMOUNT TO MAKE PAYMENT")
                            Else
                                Dim bSave As Boolean
                                'Validate OutStandingBalance
                                If OutStandingBalance > 0 Then
                                    bSave = clsRunCom.PaymentSave(rsActualPayments, rsAllocation, "")
                                Else
                                    bSave = True
                                End If

                                If bSave Then
                                    'Perform Save booking
                                    stSaveBookingResult = clsRunCom.SaveBooking(rsHeader, rsSegments, rsPassengers, rsRemarks, , rsMappings, rsServices, rsTaxes, rsFees, CreateTickets, False, False, True, False, False)

                                    If stSaveBookingResult <> 0 Then
                                        'Clear voucher session
                                        clsRunCom.DeleteVoucherSession(strVoucherSessionId, rsActualPayments)
                                    End If

                                    If stSaveBookingResult = -1 Then
                                        'Sent Error log Email
                                        Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                             XmlHeader,
                                                                                                                                                                             XmlSegments,
                                                                                                                                                                             XmlPassengers,
                                                                                                                                                                             XmlPayments,
                                                                                                                                                                             XmlRemarks,
                                                                                                                                                                             XmlMapping,
                                                                                                                                                                             XmlFees,
                                                                                                                                                                             XmlPaymentFees,
                                                                                                                                                                             XmlTaxes,
                                                                                                                                                                             XMLServices,
                                                                                                                                                                             CreateTickets,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             strLanguage,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             stSaveBookingResult.ToString()),
                                                                                                                                                    "SAVEFAILED",
                                                                                                                                                    "",
                                                                                                                                                    "SaveBookingPayment",
                                                                                                                                                    "TikAero.WebService",
                                                                                                                                                    "")
                                        CreateErrorDataset(ds, "201", "SAVEFAILED")
                                    ElseIf stSaveBookingResult = 1 Then
                                        'Sent Error log Email
                                        Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                             XmlHeader,
                                                                                                                                                                             XmlSegments,
                                                                                                                                                                             XmlPassengers,
                                                                                                                                                                             XmlPayments,
                                                                                                                                                                             XmlRemarks,
                                                                                                                                                                             XmlMapping,
                                                                                                                                                                             XmlFees,
                                                                                                                                                                             XmlPaymentFees,
                                                                                                                                                                             XmlTaxes,
                                                                                                                                                                             XMLServices,
                                                                                                                                                                             CreateTickets,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             strLanguage,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             stSaveBookingResult.ToString()),
                                                                                                                                                    "SESSIONTIMEOUT",
                                                                                                                                                    "",
                                                                                                                                                    "SaveBookingPayment",
                                                                                                                                                    "TikAero.WebService",
                                                                                                                                                    "")
                                        CreateErrorDataset(ds, "202", "SESSIONTIMEOUT")
                                    ElseIf stSaveBookingResult = 2 Then
                                        'Sent Error log Email
                                        Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                             XmlHeader,
                                                                                                                                                                             XmlSegments,
                                                                                                                                                                             XmlPassengers,
                                                                                                                                                                             XmlPayments,
                                                                                                                                                                             XmlRemarks,
                                                                                                                                                                             XmlMapping,
                                                                                                                                                                             XmlFees,
                                                                                                                                                                             XmlPaymentFees,
                                                                                                                                                                             XmlTaxes,
                                                                                                                                                                             XMLServices,
                                                                                                                                                                             CreateTickets,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             strLanguage,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             stSaveBookingResult.ToString()),
                                                                                                                                                    "DUPLICATESEAT",
                                                                                                                                                    "",
                                                                                                                                                    "SaveBookingPayment",
                                                                                                                                                    "TikAero.WebService",
                                                                                                                                                    "")
                                        CreateErrorDataset(ds, "203", "DUPLICATESEAT")
                                    ElseIf stSaveBookingResult = 3 Then
                                        'Sent Error log Email
                                        Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                             XmlHeader,
                                                                                                                                                                             XmlSegments,
                                                                                                                                                                             XmlPassengers,
                                                                                                                                                                             XmlPayments,
                                                                                                                                                                             XmlRemarks,
                                                                                                                                                                             XmlMapping,
                                                                                                                                                                             XmlFees,
                                                                                                                                                                             XmlPaymentFees,
                                                                                                                                                                             XmlTaxes,
                                                                                                                                                                             XMLServices,
                                                                                                                                                                             CreateTickets,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             strLanguage,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             stSaveBookingResult.ToString()),
                                                                                                                                                    "DATABASEACCESS",
                                                                                                                                                    "",
                                                                                                                                                    "SaveBookingPayment",
                                                                                                                                                    "TikAero.WebService",
                                                                                                                                                    "")
                                        CreateErrorDataset(ds, "204", "DATABASEACCESS")
                                    ElseIf stSaveBookingResult = 4 Then
                                        'Sent Error log Email
                                        Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                             XmlHeader,
                                                                                                                                                                             XmlSegments,
                                                                                                                                                                             XmlPassengers,
                                                                                                                                                                             XmlPayments,
                                                                                                                                                                             XmlRemarks,
                                                                                                                                                                             XmlMapping,
                                                                                                                                                                             XmlFees,
                                                                                                                                                                             XmlPaymentFees,
                                                                                                                                                                             XmlTaxes,
                                                                                                                                                                             XMLServices,
                                                                                                                                                                             CreateTickets,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             strLanguage,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             stSaveBookingResult.ToString()),
                                                                                                                                                    "BOOKINGINUSE",
                                                                                                                                                    "",
                                                                                                                                                    "SaveBookingPayment",
                                                                                                                                                    "TikAero.WebService",
                                                                                                                                                    "")
                                        CreateErrorDataset(ds, "205", "BOOKINGINUSE")
                                    ElseIf stSaveBookingResult = 5 Then
                                        'Sent Error log Email
                                        Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                             XmlHeader,
                                                                                                                                                                             XmlSegments,
                                                                                                                                                                             XmlPassengers,
                                                                                                                                                                             XmlPayments,
                                                                                                                                                                             XmlRemarks,
                                                                                                                                                                             XmlMapping,
                                                                                                                                                                             XmlFees,
                                                                                                                                                                             XmlPaymentFees,
                                                                                                                                                                             XmlTaxes,
                                                                                                                                                                             XMLServices,
                                                                                                                                                                             CreateTickets,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             strLanguage,
                                                                                                                                                                             String.Empty,
                                                                                                                                                                             stSaveBookingResult.ToString()),
                                                                                                                                                    "BOOKINGREADERROR",
                                                                                                                                                    "",
                                                                                                                                                    "SaveBookingPayment",
                                                                                                                                                    "TikAero.WebService",
                                                                                                                                                    "")
                                        CreateErrorDataset(ds, "206", "BOOKINGREADERROR")
                                    Else
                                        Call clsRunCom.UpdatePassengerNames(rsPassengers,
                                                                            rsMappings,
                                                                            rsHeader,
                                                                            rsFees,
                                                                            rsSegments,
                                                                            AgentHeader.AgencyCode,
                                                                            AgentHeader.AgencyCurrencyRcd,
                                                                            BookingId,
                                                                            True)

                                        If CreateTickets Then
                                            Dim b As Boolean = clsRunCom.TicketCreate(AgentHeader.AgencyCode,
                                                                                      Session("CreateById").ToString().Replace("{", "").Replace("}", ""), ,
                                                                                      "{" & BookingId & "}", False)
                                        End If

                                        'Update Approval
                                        Dim strIBE = "B2C,B2E"
                                        If BookingHeaderUpdate.booking_source_rcd.ToUpper().IndexOf(strIBE) > 0 Then
                                            Dim bUpdateApproval As String = System.Configuration.ConfigurationManager.AppSettings("UpdateApproval")
                                            If Not String.IsNullOrEmpty(bUpdateApproval) Then
                                                If bUpdateApproval.ToString.ToLower = "true" Then
                                                    clsRunCom.UpdateApproval(BookingId, 1)
                                                End If
                                            End If
                                        End If

                                        'Clear recordset before call get itinerary.
                                        ClearBookingRecordset(rsHeader,
                                                              rsSegments,
                                                              rsPassengers,
                                                              rsRemarks,
                                                              rsPayments,
                                                              rsMappings,
                                                              rsServices,
                                                              rsTaxes,
                                                              rsFees,
                                                              rsQuotes)

                                        Call clsRunCom.ItiniraryRead(BookingId,
                                                                     String.Empty,
                                                                     BookingHeaderUpdate.agency_code,
                                                                     rsHeader,
                                                                     rsSegments,
                                                                     rsPassengers,
                                                                     rsRemarks,
                                                                     rsPayments,
                                                                     rsMappings,
                                                                     rsServices,
                                                                     rsTaxes,
                                                                     rsFees,
                                                                     rsQuotes,
                                                                     strLanguage)

                                        'Convert recordset to dataset
                                        CopyRecordsetsToDataset(rsHeader,
                                                                rsSegments,
                                                                rsPassengers,
                                                                rsRemarks,
                                                                rsPayments,
                                                                rsMappings,
                                                                rsTaxes,
                                                                rsQuotes,
                                                                rsFees,
                                                                rsServices,
                                                                ds)

                                        'Close recordser object
                                        ClearBookingRecordset(rsHeader,
                                                              rsSegments,
                                                              rsPassengers,
                                                              rsRemarks,
                                                              rsPayments,
                                                              rsMappings,
                                                              rsServices,
                                                              rsTaxes,
                                                              rsFees,
                                                              rsQuotes)

                                        'Remove booking session
                                        Session.Remove("rsHeader")
                                        Session.Remove("rsSegments")
                                        Session.Remove("rsPassenger")
                                        Session.Remove("rsRemarks")
                                        Session.Remove("rsPayments")
                                        Session.Remove("rsMappings")
                                        Session.Remove("rsServices")
                                        Session.Remove("rsTaxes")
                                        Session.Remove("rsFees")
                                        Session.Remove("rsQuotes")


                                        Dim BookingState = New Booking
                                        Serialization.FillBooking(ds, BookingState)
                                        Session("BookingState") = BookingState
                                    End If
                                Else
                                    'Clear voucher session
                                    clsRunCom.DeleteVoucherSession(strVoucherSessionId, rsActualPayments)
                                End If
                            End If
                        Else
                            If System.Configuration.ConfigurationManager.AppSettings("ErrorMailEnabled").ToUpper = "TRUE" Then
                                Dim ErrorSubject As String = System.Configuration.ConfigurationManager.AppSettings("ErrorSubject") & System.Convert.ToString(Date.Now)
                                Mail.Send(ErrorHelper.RenderDebugMessage("SaveBookingPayment", "Error Missing Data", XmlHeader, XmlSegments, XmlPassengers, XmlPayments, XmlRemarks, XmlMapping, XmlFees, XmlTaxes, String.Empty, String.Empty), ErrorSubject, System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), Nothing)
                            End If
                        End If
                    Else
                        CreateErrorDataset(ds, "200", "Infant over limit")
                    End If
                End If
            Catch e As Exception
                Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(e,
                                                                                                                      CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                     XmlHeader,
                                                                                                                                                     XmlSegments,
                                                                                                                                                     XmlPassengers,
                                                                                                                                                     XmlPayments,
                                                                                                                                                     XmlRemarks,
                                                                                                                                                     XmlMapping,
                                                                                                                                                     XmlFees,
                                                                                                                                                     XmlPaymentFees,
                                                                                                                                                     XmlTaxes,
                                                                                                                                                     XMLServices,
                                                                                                                                                     CreateTickets,
                                                                                                                                                     String.Empty,
                                                                                                                                                     String.Empty,
                                                                                                                                                     String.Empty,
                                                                                                                                                     String.Empty,
                                                                                                                                                     strLanguage,
                                                                                                                                                     String.Empty,
                                                                                                                                                     stSaveBookingResult))
            Finally
                Helper.Check.ClearRecordset(rsActualPayments)
                Helper.Check.ClearRecordset(rsVoucher)
                Helper.Check.ClearRecordset(rsAllocation)
            End Try

            Return ds
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/SavePayment"),
            WebMethod(Description:="Save booking with payment",
            EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function SavePayment(ByVal BookingId As String,
                                    ByVal XmlHeader As String,
                                    ByVal XmlSegments As String,
                                    ByVal XmlPassengers As String,
                                    ByVal XmlPayments As String,
                                    ByVal XmlMapping As String,
                                    ByVal XmlFees As String,
                                    ByVal XmlPaymentFees As String,
                                    ByVal CreateTickets As Boolean) As String

        Using ds As New DataSet

            Dim FabricateRecordset As New FabricateRecordset
            Dim rsActualPayments As ADODB.Recordset = Nothing
            Dim rsVoucher As ADODB.Recordset = Nothing
            Dim rsAllocation As ADODB.Recordset = FabricateRecordset.FabricatePaymentAllocationRecordset

            Try

                Dim clsRunCom As New clsRunComplus

                Dim rsHeader As ADODB.Recordset = Session("rsHeader")
                Dim rsSegments As ADODB.Recordset = Session("rsSegments")
                Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
                Dim rsRemarks As ADODB.Recordset = Session("rsRemarks")
                Dim rsPayments As ADODB.Recordset = Session("rsPayments")
                Dim rsMappings As ADODB.Recordset = Session("rsMappings")
                Dim rsServices As ADODB.Recordset = Session("rsServices")
                Dim rsTaxes As ADODB.Recordset = Session("rsTaxes")
                Dim rsFees As ADODB.Recordset = Session("rsFees")
                Dim rsQuotes As ADODB.Recordset = Session("rsQuotes")

                Dim BookingHeaderUpdate As New BookingHeaderUpdate
                Dim Passengers As New Passengers
                Dim Mappings As New Mappings
                Dim Segments As New Itinerary
                Dim Fees As New Fees
                Dim TotalFees As New Fees
                Dim PaymentsUpdate As New PaymentsUpdate
                Dim MappingsUpdate As New MappingsUpdate
                Dim Taxes As New Taxes
                Dim Quotes As New Quotes

                Dim Allocations As Allocations

                'get agency from header
                Dim XmlDoc As New XmlDocument
                Dim node As XmlNode
                Dim strAgency_code As String = String.Empty

                XmlDoc.LoadXml(XmlHeader)
                node = XmlDoc.SelectSingleNode("BookingHeader")

                If node IsNot Nothing Then
                    strAgency_code = node.Item("agency_code").InnerText
                    AgentHeader.AgencyCode = strAgency_code
                End If

                clsRunCom.GetEmpty(AgentHeader.AgencyCode, AgentHeader.AgencyCurrencyRcd, , , , , rsActualPayments)
                If Not rsActualPayments Is Nothing Then

                    XmlHeader = XmlHeader.Replace("BookingHeader", "BookingHeaderUpdate")
                    BookingHeaderUpdate = Serialization.Deserialize(XmlHeader, BookingHeaderUpdate)

                    XmlPayments = XmlPayments.Replace("Payment", "PaymentUpdate")
                    PaymentsUpdate = Serialization.Deserialize(XmlPayments, PaymentsUpdate)

                    Passengers = Serialization.Deserialize(XmlPassengers, Passengers)
                    Segments = Serialization.Deserialize(XmlSegments, Segments)
                    Mappings = Serialization.Deserialize(XmlMapping, Mappings)
                    Fees = Serialization.Deserialize(XmlFees, Fees)

                    XmlMapping = XmlMapping.Replace("Mapping", "MappingUpdate")
                    MappingsUpdate = Serialization.Deserialize(XmlMapping, MappingsUpdate)

                    'Find Payment Allocation.
                    If XmlPaymentFees.Length > 0 Then
                        TotalFees = Serialization.Deserialize(XmlPaymentFees, TotalFees)
                    End If

                    Allocations = GetPaymentAllocations(Mappings, Fees, True, TotalFees)

                    For Each f As Fee In Fees
                        TotalFees.Add(f)
                    Next

                    Dim r As New Helper.RecordSet
                    With r
                        If Not Session("CreateById") Is Nothing AndAlso Not Session("CreateById") = String.Empty Then
                            .DefaultCreateById = New Guid(Session("CreateById").ToString())
                        Else
                            If IsGuid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")) Then
                                Session("CreateById") = System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")
                            End If
                            r.DefaultCreateById = New Guid(Session("CreateById").ToString())
                        End If

                        .Fill(rsAllocation, Allocations, False)
                        .Fill(rsActualPayments, PaymentsUpdate)

                        .FillHeader(rsHeader, BookingHeaderUpdate)
                        .FillPassengers(rsPassengers, Passengers)
                        .FillMapping(rsMappings, MappingsUpdate)

                    End With

                    If ValidateSave(rsHeader, rsSegments, rsPassengers, rsMappings) = True Then
                        If clsRunCom.PaymentSave(rsActualPayments, rsAllocation, "") = True Then
                            'Perform Save booking
                            If CreateTickets Then
                                Dim b As Boolean = clsRunCom.TicketCreate(AgentHeader.AgencyCode,
                                                                          Session("CreateById").ToString().Replace("{", "").Replace("}", ""), ,
                                                                          "{" & BookingId & "}", False)
                                Call clsRunCom.GetBooking(BookingId,
                                                          rsHeader,
                                                          rsSegments,
                                                          rsPassengers,
                                                          rsRemarks,
                                                          rsPayments,
                                                          rsMappings,
                                                          rsServices,
                                                          rsTaxes,
                                                          rsFees,
                                                          rsQuotes)

                                Dim rsHeaderClone As New ADODB.Recordset
                                Dim rsSegmentsClone As New ADODB.Recordset
                                Dim rsPassengersClone As New ADODB.Recordset
                                Dim rsRemarksClone As New ADODB.Recordset
                                Dim rsPaymentsClone As New ADODB.Recordset
                                Dim rsMappingsClone As New ADODB.Recordset
                                Dim rsServicesClone As New ADODB.Recordset
                                Dim rsTaxesClone As New ADODB.Recordset
                                Dim rsFeesClone As New ADODB.Recordset
                                Dim rsQuotesClone As New ADODB.Recordset

                                Dim fr As New FabricateRecordset

                                fr.CopyAdoRecordset(rsHeader, rsHeaderClone)
                                Session("rsHeader") = rsHeader

                                fr.CopyAdoRecordset(rsSegments, rsSegmentsClone)
                                Session("rsSegments") = rsSegments

                                fr.CopyAdoRecordset(rsPassengers, rsPassengersClone)
                                Session("rsPassenger") = rsPassengers

                                fr.CopyAdoRecordset(rsRemarks, rsRemarksClone)
                                Session("rsRemarks") = rsRemarks

                                fr.CopyAdoRecordset(rsPayments, rsPaymentsClone)
                                Session("rsPayments") = rsPayments

                                fr.CopyAdoRecordset(rsMappings, rsMappingsClone)
                                Session("rsMappings") = rsMappings

                                fr.CopyAdoRecordset(rsServices, rsServicesClone)
                                Session("rsServices") = rsServices

                                fr.CopyAdoRecordset(rsTaxes, rsTaxesClone)
                                Session("rsTaxes") = rsTaxes

                                fr.CopyAdoRecordset(rsFees, rsFeesClone)
                                Session("rsFees") = rsFees

                                fr.CopyAdoRecordset(rsQuotes, rsQuotesClone)
                                Session("rsQuotes") = rsQuotes

                                CopyRecordsetsToDataset(rsHeaderClone,
                                                        rsSegmentsClone,
                                                        rsPassengersClone,
                                                        rsRemarksClone,
                                                        rsPaymentsClone,
                                                        rsMappingsClone,
                                                        rsTaxesClone,
                                                        rsQuotesClone,
                                                        rsFeesClone,
                                                        rsServicesClone,
                                                        ds)

                                'Release Unmanage resource
                                ClearBookingRecordset(rsHeaderClone,
                                                      rsSegmentsClone,
                                                      rsPassengersClone,
                                                      rsRemarksClone,
                                                      rsPaymentsClone,
                                                      rsMappingsClone,
                                                      rsServicesClone,
                                                      rsTaxesClone,
                                                      rsFeesClone,
                                                      rsQuotesClone)

                                Dim BookingState = New Booking
                                Serialization.FillBooking(ds, BookingState)
                                Session("BookingState") = BookingState
                            End If
                        End If
                    Else
                        If System.Configuration.ConfigurationManager.AppSettings("ErrorMailEnabled").ToUpper = "TRUE" Then
                            Dim ErrorSubject As String = System.Configuration.ConfigurationManager.AppSettings("ErrorSubject") & System.Convert.ToString(Date.Now)
                            Mail.Send(ErrorHelper.RenderDebugMessage("SaveBookingPayment", "Error Missing Data", XmlHeader, XmlSegments, XmlPassengers, XmlPayments, String.Empty, XmlMapping, XmlFees, String.Empty, String.Empty, String.Empty), ErrorSubject, System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), Nothing)
                        End If
                    End If
                End If
            Catch e As Exception
                'Sent Error log Email
                Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(e, CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                     XmlHeader,
                                                                                                                                                     XmlSegments,
                                                                                                                                                     XmlPassengers,
                                                                                                                                                     XmlPayments,
                                                                                                                                                     String.Empty,
                                                                                                                                                     XmlMapping,
                                                                                                                                                     XmlFees,
                                                                                                                                                     XmlPaymentFees,
                                                                                                                                                     String.Empty,
                                                                                                                                                     String.Empty,
                                                                                                                                                     CreateTickets,
                                                                                                                                                     String.Empty,
                                                                                                                                                     String.Empty,
                                                                                                                                                     String.Empty,
                                                                                                                                                     String.Empty,
                                                                                                                                                     String.Empty,
                                                                                                                                                     String.Empty,
                                                                                                                                                     String.Empty))
            Finally

                Helper.Check.ClearRecordset(rsActualPayments)
                Helper.Check.ClearRecordset(rsVoucher)
                Helper.Check.ClearRecordset(rsAllocation)
            End Try

            If IsNothing(ds) = False And ds.Tables.Count > 0 Then
                Return ds.GetXml()
            Else
                Return String.Empty
            End If
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetAgencyAccountTopUp"),
                    WebMethod(Description:="Return Agency Account Top Up report",
                    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetAgencyAccountTopUp(ByVal strAgencyCode As String, ByVal strCurrency As String, ByVal dtFrom As DateTime, ByVal dtTo As DateTime) As DataSet
        Using dsOut As New DataSet
            Dim dateFrom As DateTime = dtFrom
            Dim dateTo As DateTime = dtTo
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (Not AgentHeader Is Nothing) Then
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus
                        rsA = clsRunCom.GetAgencyAccountTopUp(strAgencyCode, strCurrency, dateFrom, dateTo)
                        clsRunCom = Nothing

                        Call InputRecordsetToDataset(rsA, "AgencyAccountTopUp", dsOut)
                    End If
                End If
            Catch e As Exception
                Throw New Exception(e.Message)
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return dsOut
        End Using
    End Function

    ' Start Yai add B2A
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetAgencyAccountBalance"),
                        WebMethod(Description:="Return Agency Account Balance List",
                        EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetAgencyAccountBalance(ByVal strAgencyCode As String, ByVal strAgencyName As String, ByVal strCurrency As String, ByVal strConsolidatorAgency As String) As DataSet
        Using dsAgencyAccountBalance As New DataSet
            Dim clsRunCom As New clsRunComplus
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (Not AgentHeader Is Nothing) Then
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        rsA = clsRunCom.GetAgencyAccountBalance(strAgencyCode, strAgencyName, strCurrency, strConsolidatorAgency)
                        Call InputRecordsetToDataset(rsA, "AgencyAccountBalance", dsAgencyAccountBalance)
                    End If
                End If
            Catch e As Exception
                Throw New Exception(e.Message)
            Finally
                clsRunCom = Nothing
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return dsAgencyAccountBalance
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/AdjustSubAgencyAccountBalance"),
                        WebMethod(Description:="Adjust Sub Agency Account Balance",
                        EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function AdjustSubAgencyAccountBalance(ByVal strXmlChildAccountBalance As String, ByVal strXmlParentAccountBalance As String) As Boolean
        Dim bChildResult As Boolean = False
        Dim bParentResult As Boolean = False
        Dim clsRunCom As New clsRunComplus

        Dim aHelperRecortSet As New Helper.RecordSet
        Dim aChildAccountBlances As New AccountBalances
        Dim aParentAccountBlances As New AccountBalances

        Dim rsChild As ADODB.Recordset = Nothing
        Dim rsParent As ADODB.Recordset = Nothing

        Try
            If (Not AgentHeader Is Nothing) Then
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then


                    aChildAccountBlances = Serialization.Deserialize(strXmlChildAccountBalance, aChildAccountBlances)
                    aParentAccountBlances = Serialization.Deserialize(strXmlParentAccountBalance, aParentAccountBlances)

                    rsChild = clsRunCom.GetEmptyAccountBalance()
                    rsParent = clsRunCom.GetEmptyAccountBalance()

                    aHelperRecortSet.FillAccountBalance(rsChild, aChildAccountBlances)
                    aHelperRecortSet.FillAccountBalance(rsParent, aParentAccountBlances)

                    bChildResult = clsRunCom.AgencyAccountSave(rsChild)
                    If (bChildResult) Then
                        bParentResult = clsRunCom.AgencyAccountSave(rsParent)
                    End If
                    clsRunCom = Nothing
                End If
            End If
        Catch e As Exception
            bParentResult = False
        Finally
            clsRunCom = Nothing

            aHelperRecortSet = Nothing
            aChildAccountBlances = Nothing
            aParentAccountBlances = Nothing

            Helper.Check.ClearRecordset(rsChild)
            Helper.Check.ClearRecordset(rsParent)
        End Try
        Return bParentResult
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/AddNewAgency"),
                        WebMethod(Description:="Add new agency and default user",
                        EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function AddNewAgency(ByVal strXmlAgency As String,
                                    ByVal strAgencyCode As String,
                                    ByVal strDefaultUserLogon As String,
                                    ByVal strDefaultPassword As String,
                                    ByVal strDefaultLastname As String,
                                    ByVal strDefaultFirstname As String,
                                    ByVal strCreateUser As String,
                                    ByVal iChangeBooking As Short,
                                    ByVal iChangeSegment As Short,
                                    ByVal iDeleteSegment As Short,
                                    ByVal iTicketIssue As Short) As Boolean

        Dim clsRunCom As New clsRunComplus
        Dim bResultAgency As Boolean = False
        Dim bResultUser As Boolean = False

        Dim aHelperRecortSet As New Helper.RecordSet
        Dim aAgencyAccounts As New AgencyAccounts

        Dim rsAgency As ADODB.Recordset = Nothing
        Dim rsA As ADODB.Recordset = Nothing

        Try
            If (Not AgentHeader Is Nothing) Then
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    ' Check Exist Agency
                    rsA = clsRunCom.GetAgencyCode(strAgencyCode)

                    ' If Exist Agency, Not add
                    If rsA.RecordCount > 0 Then
                        Helper.Check.ClearRecordset(rsAgency)
                        Helper.Check.ClearRecordset(rsA)
                        Throw New System.Exception("Exist Agency '" + strAgencyCode + "'")
                    Else
                        ' Prepare Agency Recordset
                        aAgencyAccounts = Serialization.Deserialize(strXmlAgency, aAgencyAccounts)
                        rsAgency = clsRunCom.GetEmptyAgency()
                        aHelperRecortSet.FillAgency(rsAgency, aAgencyAccounts)

                        ' Add New Agency
                        bResultAgency = clsRunCom.AgencySave(rsAgency)

                        ' Add Agency Default User
                        If (bResultAgency) Then
                            bResultUser = clsRunCom.AgencyDefaultUserSave(rsAgency, "TA", strDefaultUserLogon, strDefaultPassword, strDefaultLastname, strDefaultFirstname, String.Empty, strCreateUser, iChangeBooking, iChangeSegment, iDeleteSegment, iTicketIssue)
                        End If
                        clsRunCom = Nothing
                    End If
                End If
            End If
        Catch e As Exception
            bResultUser = False
        Finally
            clsRunCom = Nothing
            aHelperRecortSet = Nothing
            aAgencyAccounts = Nothing

            Helper.Check.ClearRecordset(rsAgency)
            Helper.Check.ClearRecordset(rsA)
        End Try
        Return bResultUser
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/AgencyRead"),
                        WebMethod(Description:="Read Agency",
                        EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function AgencyRead(ByVal strAgencyCode As String, ByVal bKeepSession As Boolean) As DataSet
        Using dsAgency As New DataSet
            Dim rsAgency As ADODB.Recordset = Nothing
            Dim rsAgencyClone As ADODB.Recordset = Nothing
            Dim clsRunCom As New clsRunComplus
            Try
                If (Not AgentHeader Is Nothing) Then
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then


                        If (bKeepSession) Then
                            Dim fr As New FabricateRecordset

                            rsAgencyClone = New ADODB.Recordset
                            rsAgency = clsRunCom.AgencyRead(strAgencyCode)

                            fr.CopyAdoRecordset(rsAgency, rsAgencyClone)
                            'Session("rsAgency") = rsAgency
                            Call InputRecordsetToDataset(rsAgencyClone, "AgencyAccountBalance", dsAgency)
                        Else
                            rsAgency = clsRunCom.AgencyRead(strAgencyCode)
                            Call InputRecordsetToDataset(rsAgency, "AgencyAccountBalance", dsAgency)
                        End If
                    End If
                End If
            Catch e As Exception
            Finally
                clsRunCom = Nothing
                Helper.Check.ClearRecordset(rsAgency)
                Helper.Check.ClearRecordset(rsAgencyClone)
            End Try
            Return dsAgency
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ExchangeRateRead"),
                        WebMethod(Description:="ExchangeRateRead",
                        EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ExchangeRateRead(ByVal strOriginCurrencyCode As String, ByVal strDestCurrencyCode As String) As Double
        'Dim dsCurrency As New DataSet
        Dim clsRunCom As New clsRunComplus
        Dim dCurrencyCode As Double = 0D

        Try
            If (Not AgentHeader Is Nothing) Then
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim fr As New FabricateRecordset
                    dCurrencyCode = clsRunCom.ExchangeRateRead(strOriginCurrencyCode, strDestCurrencyCode)

                End If
            End If
        Catch e As Exception
            Throw New Exception(e.Message)
        Finally
            clsRunCom = Nothing
        End Try
        Return dCurrencyCode
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/UpdateAgency"),
                        WebMethod(Description:="Update Agency",
                        EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function UpdateAgency(ByVal strXmlAgency As String) As Boolean
        Dim clsRunCom As New clsRunComplus
        Dim bResult As Boolean = False

        Dim aHelperRecortSet As New Helper.RecordSet
        Dim aAgencyAccounts As New AgencyAccounts
        Try
            If (Not AgentHeader Is Nothing) Then
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim rsAgency As ADODB.Recordset '= Session("rsAgency")

                    aAgencyAccounts = Serialization.Deserialize(strXmlAgency, aAgencyAccounts)
                    rsAgency = clsRunCom.AgencyRead(aAgencyAccounts(0).agency_code)

                    aHelperRecortSet.FillAgencyUpdate(rsAgency, aAgencyAccounts)

                    ' Update Agency
                    bResult = clsRunCom.AgencySave(rsAgency)
                End If
            End If
        Catch e As Exception
            Throw New Exception(e.Message)
        Finally
            clsRunCom = Nothing

            aHelperRecortSet = Nothing
            aAgencyAccounts = Nothing
        End Try
        Return bResult
    End Function
    ' End Yai add B2A

    ' Start Yai Add Account Topup
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/AgencyAccountAdd"),
    WebMethod(Description:="Add Agency Topup Request", EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function AgencyAccountAdd(ByVal strAgencyCode As String,
                                     ByVal strCurrency As String,
                                     ByVal strUserId As String,
                                     ByVal strComment As String,
                                     ByVal dAmount As Double,
                                     ByVal strExternalReference As String,
                                     ByVal strInternalReference As String,
                                     ByVal strTransactionReference As String,
                                     ByVal bExternalTopup As Boolean) As Boolean
        Dim bResult As Boolean = False
        Dim aclsRunComplus As New clsRunComplus()
        Try
            If (Not AgentHeader Is Nothing) Then
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport
                If (AuthenticatePassport(strCode, strPassport)) Then
                    bResult = aclsRunComplus.AgencyAccountAdd(strAgencyCode,
                                                              strCurrency,
                                                              strUserId,
                                                              strComment,
                                                              dAmount,
                                                              strExternalReference,
                                                              strInternalReference,
                                                              strTransactionReference,
                                                              bExternalTopup)
                End If
            End If
        Catch ex As Exception
            bResult = False
        Finally
            aclsRunComplus = Nothing
        End Try
        Return bResult
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/AgencyAccountVoid"),
    WebMethod(Description:="Void Agency Topup", EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function AgencyAccountVoid(ByVal strAgencyAccountId As String, ByVal strUserId As String) As Boolean
        Dim bResult As Boolean = False
        Dim aclsRunComplus As New clsRunComplus()
        Try
            If (Not AgentHeader Is Nothing) Then
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport
                If (AuthenticatePassport(strCode, strPassport)) Then
                    bResult = aclsRunComplus.AgencyAccountVoid(strAgencyAccountId, strUserId)
                End If
            End If
        Catch ex As Exception
            bResult = False
        Finally
            aclsRunComplus = Nothing
        End Try
        Return bResult
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ExternalPaymentListAgencyTopUp"),
    WebMethod(Description:="List pending agency topups",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ExternalPaymentListAgencyTopUp(ByVal strAgencyCode As String) As DataSet
        Using objOut As New DataSet
            Dim rsResult As ADODB.Recordset = Nothing
            Try
                If (Not AgentHeader Is Nothing) Then
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus
                        rsResult = clsRunCom.ExternalPaymentListAgencyTopUp(strAgencyCode)

                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsResult, "AgencyAccountDetails", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsResult)
            End Try
            Return objOut
        End Using

    End Function
    ' End Yai Add Account Topup

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/UpdateApproval"),
                        WebMethod(Description:="UpdateApproval",
                        EnableSession:=True), SoapHeader("AgentHeader")>
    Public Sub UpdateApproval(ByVal strBookingId As String, ByVal iApproval As Integer)
        Dim clsRunCom As New clsRunComplus

        Try
            If (Not AgentHeader Is Nothing) Then
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    clsRunCom.UpdateApproval(strBookingId, iApproval)
                End If
            End If
        Catch e As Exception
            Throw New Exception(e.Message)
        Finally
            clsRunCom = Nothing
        End Try
    End Sub
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/SingleFlightQuoteSummary"),
                            WebMethod(Description:="Get Summary of quote",
                            EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function SingleFlightQuoteSummary(ByVal strFlightXml As String,
                                             ByVal strPassengerXml As String,
                                             ByVal strAgencyCode As String,
                                             ByVal strLanguage As String,
                                             ByVal strCurrencyCode As String,
                                             ByVal bNoVat As Boolean) As String
        Dim clsRunCom As New clsRunComplus

        If (Not AgentHeader Is Nothing) Then
            Dim strCode As String = AgentHeader.AgencyCode
            Dim strPassport As String = AgentHeader.AgencyPassport


            If (AuthenticatePassport(strCode, strPassport)) Then
                Return ReadFlightQuoteSummary(strFlightXml, strPassengerXml, strAgencyCode, strLanguage, strCurrencyCode, bNoVat)
            End If
        End If

        Return String.Empty

    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetRecordLocator"),
           WebMethod(Description:="Get Booking Reference",
           EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetRecordLocator(ByRef strRecordLocator As String, ByRef iBookingNumber As Integer) As Boolean

        Dim Result As Boolean = False
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then

                    Dim clsRunCom As New clsRunComplus

                    Result = clsRunCom.GetRecordLocator(strRecordLocator, iBookingNumber)
                    clsRunCom = Nothing
                End If
            End If
        Catch e As Exception
        End Try

        Return Result
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/SegmentFee"),
    WebMethod(Description:="Segment Fee",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function SegmentFee(ByVal strAgencyCode As String,
                                ByVal strCurrency As String,
                                ByVal xmlSegmentFee As String,
                                ByVal strLanguage As String,
                                ByVal bNoVat As Boolean) As String

        Dim objOut As DataSet = Nothing
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus
                    objOut = clsRunCom.SegmentFee(strAgencyCode, strCurrency, xmlSegmentFee, strLanguage, False, bNoVat)
                End If
            End If
        Catch e As Exception
        End Try

        If IsNothing(objOut) = False Then
            Return objOut.GetXml()
        Else
            Return String.Empty
        End If
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/SpecialServiceFee"),
    WebMethod(Description:="Special Service Fee",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function SpecialServiceFee(ByVal strAgencyCode As String,
                                    ByVal strCurrency As String,
                                    ByVal xmlSegmentFee As String,
                                    ByVal strLanguage As String,
                                    ByVal bNoVat As Boolean) As String

        Dim objOut As DataSet = Nothing
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus
                    objOut = clsRunCom.SegmentFee(strAgencyCode, strCurrency, xmlSegmentFee, strLanguage, True, bNoVat)
                End If
            End If
        Catch e As Exception
        End Try

        If IsNothing(objOut) = False Then
            Return objOut.GetXml()
        Else
            Return String.Empty
        End If
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/SpecialServiceRead"),
    WebMethod(Description:="Return Special service definition",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function SpecialServiceRead(ByVal strSpecialServiceCode As String,
                                       ByVal strSpecialServiceGroupCode As String,
                                       ByVal strSpecialService As String,
                                       ByVal strStatus As String,
                                       ByVal bTextAllowed As Boolean,
                                       ByVal bTextRequired As Boolean,
                                       ByVal bInventoryControl As Boolean,
                                       ByVal bServiceOnRequest As Boolean,
                                       ByVal bManifest As Boolean,
                                       ByVal bWrite As Boolean,
                                       ByVal strLanguage As String) As String
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.SpecialServiceRead(strSpecialServiceCode,
                                                            strSpecialServiceGroupCode,
                                                            strSpecialService,
                                                            strStatus,
                                                            bTextAllowed,
                                                            bTextRequired,
                                                            bServiceOnRequest,
                                                            bManifest,
                                                            bWrite,
                                                            strLanguage)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "Service", objOut)
                        If rsA.State = ADODB.ObjectStateEnum.adStateOpen Then
                            rsA.Close()
                        End If
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut.GetXml
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/InsertPaymentApproval"),
                            WebMethod(Description:="Insert Payment Approval",
                            EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function InsertPaymentApproval(ByVal strPaymentApprovalID As String,
                                                ByVal strCardRcd As String,
                                                ByVal strCardNumber As String,
                                                ByVal strNameOnCard As String,
                                                ByVal iExpiryMonth As Integer,
                                                ByVal iExpiryYear As Integer,
                                                ByVal iIssueMonth As Integer,
                                                ByVal iIssueYear As Integer,
                                                ByVal iIssueNumber As Integer,
                                                ByVal strPaymentStateCode As String,
                                                ByVal strBookingPaymentId As String,
                                                ByVal strCurrencyRcd As String,
                                                ByVal dPaymentAmount As Double,
                                                ByVal strIpAddress As String) As Boolean
        Dim clsRunCom As New clsRunComplus
        Dim bResult As Boolean = False
        Try
            If (Not AgentHeader Is Nothing) Then
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then

                    bResult = clsRunCom.InsertPaymentApproval(strPaymentApprovalID,
                                                                strCardRcd,
                                                                strCardNumber,
                                                                strNameOnCard,
                                                                iExpiryMonth,
                                                                iExpiryYear,
                                                                iIssueMonth,
                                                                iIssueYear,
                                                                iIssueNumber,
                                                                strPaymentStateCode,
                                                                strBookingPaymentId,
                                                                strCurrencyRcd,
                                                                dPaymentAmount,
                                                                strIpAddress)
                End If
            End If
        Catch e As Exception
            Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(e, "InsertPaymentApproval Input : strPaymentApprovalID " & strPaymentApprovalID & "<br/>" &
                                                                                                                            ", strCardRcd " & strCardRcd & "<br/>" &
                                                                                                                            ", strCardNumber " & strCardNumber & "<br/>" &
                                                                                                                            ", strNameOnCard " & strNameOnCard & "<br/>" &
                                                                                                                            ", iExpiryMonth " & iExpiryYear.ToString & "<br/>" &
                                                                                                                            ", iExpiryYear " & iExpiryYear.ToString & "<br/>" &
                                                                                                                            ", iIssueMonth " & iIssueMonth.ToString & "<br/>" &
                                                                                                                            ", iIssueYear " & iIssueYear.ToString & "<br/>" &
                                                                                                                            ", iIssueNumber " & iIssueNumber.ToString & "<br/>" &
                                                                                                                            ", strPaymentStateCode " & strPaymentStateCode & "<br/>" &
                                                                                                                            ", strBookingPaymentId " & strBookingPaymentId & "<br/>" &
                                                                                                                            ", strCurrencyRcd " & strCurrencyRcd & "<br/>" &
                                                                                                                            ", dPaymentAmount " & dPaymentAmount.ToString & "<br/>" &
                                                                                                                            ", strIpAddress " & strIpAddress & "<br/>")
            Throw New Exception(e.Message)
        Finally
            clsRunCom = Nothing
        End Try
        Return bResult
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/UpdatePaymentApproval"),
                            WebMethod(Description:="Update Payment Approval",
                            EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function UpdatePaymentApproval(ByVal strApprovalCode As String,
                                    ByVal strPaymentReference As String,
                                    ByVal strTransactionReference As String,
                                    ByVal strTransactionDescription As String,
                                    ByVal strAvsCode As String,
                                    ByVal strAvsResponse As String,
                                    ByVal strCvvCode As String,
                                    ByVal strCvvResponse As String,
                                    ByVal strErrorCode As String,
                                    ByVal strErrorResponse As String,
                                    ByVal strPaymentStateCode As String,
                                    ByVal strResponseCode As String,
                                    ByVal strReturnCode As String,
                                    ByVal strResponseText As String,
                                    ByVal strPaymentApprovalId As String,
                                    ByVal strRequestStreamText As String,
                                    ByVal strReplyStreamText As String,
                                    ByVal strCardNumber As String,
                                    ByVal strCardType As String) As Boolean
        Dim clsRunCom As New clsRunComplus
        Dim bResult As Boolean = False
        Try
            If (Not AgentHeader Is Nothing) Then
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then

                    bResult = clsRunCom.UpdatePaymentApproval(strApprovalCode,
                                                                strPaymentReference,
                                                                strTransactionReference,
                                                                strTransactionDescription,
                                                                strAvsCode,
                                                                strAvsResponse,
                                                                strCvvCode,
                                                                strCvvResponse,
                                                                strErrorCode,
                                                                strErrorResponse,
                                                                strPaymentStateCode,
                                                                strResponseCode,
                                                                strReturnCode,
                                                                strResponseText,
                                                                strPaymentApprovalId,
                                                                strRequestStreamText,
                                                                strReplyStreamText,
                                                                strCardNumber,
                                                                strCardType)
                End If
            End If
        Catch e As Exception
            Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(e, "UpdatePaymentApproval Input : strApprovalCode " & strApprovalCode & "<br/>" &
                                                                                                                    ", strPaymentReference " & strPaymentReference & "<br/>" &
                                                                                                                    ", strTransactionReference " & strTransactionReference & "<br/>" &
                                                                                                                    ", strTransactionDescription " & strTransactionDescription & "<br/>" &
                                                                                                                    ", strAvsCode " & strAvsCode & "<br/>" &
                                                                                                                    ", strAvsResponse " & strAvsResponse & "<br/>" &
                                                                                                                    ", strCvvCode " & strCvvCode & "<br/>" &
                                                                                                                    ", strCvvResponse " & strCvvResponse & "<br/>" &
                                                                                                                    ", strErrorCode " & strErrorCode & "<br/>" &
                                                                                                                    ", strErrorResponse " & strErrorResponse & "<br/>" &
                                                                                                                    ", strPaymentStateCode " & strPaymentStateCode & "<br/>" &
                                                                                                                    ", strResponseCode " & strResponseCode & "<br/>" &
                                                                                                                    ", strReturnCode " & strReturnCode & "<br/>" &
                                                                                                                    ", strResponseText " & strResponseText & "<br/>" &
                                                                                                                    ", strPaymentApprovalId " & strPaymentApprovalId & "<br/>" &
                                                                                                                    ", strRequestStreamText " & strRequestStreamText & "<br/>" &
                                                                                                                    ", strReplyStreamText " & strReplyStreamText & "<br/>" &
                                                                                                                    ", strCardNumber " & strCardNumber & "<br/>" &
                                                                                                                    ", strCardType " & strCardType & "<br/>")
            Throw New Exception(e.Message)
        Finally
            clsRunCom = Nothing
        End Try
        Return bResult
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/TourOperators"),
   WebMethod(Description:="Return TourOperators",
   EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetTourOperators(ByVal strLanguage As String) As String
        Dim objOut As String = ""
        Dim rsA As ADODB.Recordset = Nothing
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then

                    Dim clsRunCom As New clsRunComplus
                    rsA = clsRunCom.GetTourOperators(strLanguage)
                    clsRunCom = Nothing

                    objOut = RecordSetToXML(rsA, "TourOperators", "TourOperator")
                End If
            End If
        Catch e As Exception
        Finally
            Helper.Check.ClearRecordset(rsA)
        End Try
        Return objOut
    End Function


    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetVendorTourOperator"),
   WebMethod(Description:="Return GetVendorTourOperator",
   EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetVendorTourOperator(ByVal strVendorRcd As String) As String
        Dim objOut As String = ""
        Dim rsA As ADODB.Recordset = Nothing
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus
                    rsA = clsRunCom.GetVendorTourOperator(strVendorRcd)
                    clsRunCom = Nothing

                    objOut = RecordSetToXML(rsA, "VendorTourOperators", "VendorTourOperator")
                End If
            End If
        Catch e As Exception
        Finally
            Helper.Check.ClearRecordset(rsA)
        End Try
        Return objOut
    End Function


    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetTourOperatorCodeMappingRead"),
   WebMethod(Description:="Return GetTourOperatorCodeMappingRead",
   EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetTourOperatorCodeMappingRead(ByVal strTourOperatorId As String, ByVal bInclude As Boolean) As String
        Dim objOut As String = ""
        Dim rsA As ADODB.Recordset = Nothing
        Try
            If (AgentHeader Is Nothing) Then
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus
                    rsA = clsRunCom.GetTourOperatorCodeMappingRead(strTourOperatorId, bInclude)
                    clsRunCom = Nothing

                    objOut = RecordSetToXML(rsA, "TourOperators", "TourOperator")
                End If
            End If
        Catch e As Exception
        Finally
            Helper.Check.ClearRecordset(rsA)
        End Try
        Return objOut
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetSessionlessFlightAvailability"),
    WebMethod(Description:="Return Airport Destinations",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetSessionlessFlightAvailability(ByVal Origin As String,
                                                            ByVal Destination As String,
                                                            ByVal DateDepartFrom As Date,
                                                            ByVal DateDepartTo As Date,
                                                            ByVal DateReturnFrom As Date,
                                                            ByVal DateReturnTo As Date,
                                                            ByVal DateBooking As Date,
                                                            ByVal Adult As Short,
                                                            ByVal Child As Short,
                                                            ByVal Infant As Short,
                                                            ByVal Other As Short,
                                                            ByVal OtherPassengerType As String,
                                                            ByVal BoardingClass As String,
                                                            ByVal BookingClass As String,
                                                            ByVal DayTimeIndicator As String,
                                                            ByVal AgencyCode As String,
                                                            ByVal CurrencyCode As String,
                                                            ByVal FlightId As String,
                                                            ByVal FareId As String,
                                                            ByVal MaxAmount As Double,
                                                            ByVal NonStopOnly As Boolean,
                                                            ByVal IncludeDeparted As Boolean,
                                                            ByVal IncludeCancelled As Boolean,
                                                            ByVal IncludeWaitlisted As Boolean,
                                                            ByVal IncludeSoldOut As Boolean,
                                                            ByVal Refundable As Boolean,
                                                            ByVal GroupFares As Boolean,
                                                            ByVal ItFaresOnly As Boolean,
                                                            ByVal PromotionCode As String,
                                                            ByVal FareType As String,
                                                            ByVal FareLogic As Boolean,
                                                            ByVal ReturnFlight As Boolean,
                                                            ByVal bLowest As Boolean,
                                                            ByVal bLowestClass As Boolean,
                                                            ByVal bLowestGroup As Boolean,
                                                            ByVal bShowClosed As Boolean,
                                                            ByVal bSort As Boolean,
                                                            ByVal bDelete As Boolean,
                                                            ByVal bSkipFareLogic As Boolean,
                                                            ByVal strLanguage As String,
                                                            ByVal strIpAddress As String,
                                                            ByVal strToken As String,
                                                            ByVal bReturnRefundable As Boolean,
                                                            ByVal bNoVat As Boolean,
                                                            ByVal iDayRange As Integer) As String
        Dim ds As New DataSet

        Try
            If String.IsNullOrEmpty(strToken) Then
                Return String.Empty
            Else

                If (IsTokenValid(strToken) = True) Then
                    Return GetAvailabilityFlight(Origin,
                                                Destination,
                                                DateDepartFrom,
                                                DateDepartTo,
                                                DateReturnFrom,
                                                DateReturnTo,
                                                DateBooking,
                                                Adult,
                                                Child,
                                                Infant,
                                                Other,
                                                OtherPassengerType,
                                                BoardingClass,
                                                BookingClass,
                                                DayTimeIndicator,
                                                AgencyCode,
                                                CurrencyCode,
                                                FlightId,
                                                FareId,
                                                MaxAmount,
                                                NonStopOnly,
                                                IncludeDeparted,
                                                IncludeCancelled,
                                                IncludeWaitlisted,
                                                IncludeSoldOut,
                                                Refundable,
                                                GroupFares,
                                                ItFaresOnly,
                                                PromotionCode,
                                                FareType,
                                                ReturnFlight,
                                                False,
                                                bLowest,
                                                bLowestClass,
                                                bLowestGroup,
                                                bShowClosed,
                                                bSort,
                                                bDelete,
                                                bSkipFareLogic,
                                                strLanguage,
                                                strIpAddress,
                                                bReturnRefundable,
                                                bNoVat,
                                                iDayRange)
                Else
                    Return String.Empty
                End If
            End If

        Catch e As Exception
            Return String.Empty
        End Try
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetSessionlessLowFareFinder"),
   WebMethod(Description:="Return Airport Destinations",
   EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetSessionlessLowFareFinder(ByVal Origin As String,
                                                 ByVal Destination As String,
                                                 ByVal DateDepartFrom As Date,
                                                 ByVal DateDepartTo As Date,
                                                 ByVal DateReturnFrom As Date,
                                                 ByVal DateReturnTo As Date,
                                                 ByVal DateBooking As Date,
                                                 ByVal Adult As Short,
                                                 ByVal Child As Short,
                                                 ByVal Infant As Short,
                                                 ByVal Other As Short,
                                                 ByVal OtherPassengerType As String,
                                                 ByVal BoardingClass As String,
                                                 ByVal BookingClass As String,
                                                 ByVal DayTimeIndicator As String,
                                                 ByVal AgencyCode As String,
                                                 ByVal CurrencyCode As String,
                                                 ByVal FlightId As String,
                                                 ByVal FareId As String,
                                                 ByVal MaxAmount As Double,
                                                 ByVal NonStopOnly As Boolean,
                                                 ByVal IncludeDeparted As Boolean,
                                                 ByVal IncludeCancelled As Boolean,
                                                 ByVal IncludeWaitlisted As Boolean,
                                                 ByVal IncludeSoldOut As Boolean,
                                                 ByVal Refundable As Boolean,
                                                 ByVal GroupFares As Boolean,
                                                 ByVal ItFaresOnly As Boolean,
                                                 ByVal PromotionCode As String,
                                                 ByVal FareType As String,
                                                 ByVal FareLogic As Boolean,
                                                 ByVal ReturnFlight As Boolean,
                                                 ByVal bLowest As Boolean,
                                                 ByVal bLowestClass As Boolean,
                                                 ByVal bLowestGroup As Boolean,
                                                 ByVal bShowClosed As Boolean,
                                                 ByVal bSort As Boolean,
                                                 ByVal bDelete As Boolean,
                                                 ByVal bSkipFareLogin As Boolean,
                                                 ByVal strLanguage As String,
                                                 ByVal strIpAddress As String,
                                                 ByVal strToken As String,
                                                 ByVal bReturnRefundable As Boolean,
                                                 ByVal bNoVat As Boolean,
                                                 ByVal iDayRange As Integer) As String
        Dim ds As New DataSet

        Try
            If (String.IsNullOrEmpty(strToken)) Then
                Return String.Empty
            Else

                If IsTokenValid(strToken) = True Then

                    Return GetAvailabilityFlight(Origin,
                                            Destination,
                                            DateDepartFrom,
                                            DateDepartTo,
                                            DateReturnFrom,
                                            DateReturnTo,
                                            DateBooking,
                                            Adult,
                                            Child,
                                            Infant,
                                            Other,
                                            OtherPassengerType,
                                            BoardingClass,
                                            BookingClass,
                                            DayTimeIndicator,
                                            AgencyCode,
                                            CurrencyCode,
                                            FlightId,
                                            FareId,
                                            MaxAmount,
                                            NonStopOnly,
                                            IncludeDeparted,
                                            IncludeCancelled,
                                            IncludeWaitlisted,
                                            IncludeSoldOut,
                                            Refundable,
                                            GroupFares,
                                            ItFaresOnly,
                                            PromotionCode,
                                            FareType,
                                            ReturnFlight,
                                            True,
                                            bLowest,
                                            bLowestClass,
                                            bLowestGroup,
                                            bShowClosed,
                                            bSort,
                                            bDelete,
                                            bSkipFareLogin,
                                            strLanguage,
                                            strIpAddress,
                                            bReturnRefundable,
                                            bNoVat,
                                            iDayRange)
                Else
                    Return String.Empty
                End If
            End If

        Catch e As Exception
            Return String.Empty
        End Try
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetSessionlessAirportOrigins"),
    WebMethod(Description:="Return Airport Origins",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetSessionlessAirportOrigins(ByVal Language As String,
                                                 ByVal b2cFlag As Boolean,
                                                 ByVal b2bFlag As Boolean,
                                                 ByVal b2eFlag As Boolean,
                                                 ByVal b2sFlag As Boolean,
                                                 ByVal apiFlag As Boolean,
                                                 ByVal strToken As String) As DataSet

        Try
            If (String.IsNullOrEmpty(strToken)) Then
                Return Nothing
            Else
                If IsTokenValid(strToken) = True Then
                    Return ReadAirportOrigin(Language, b2cFlag, b2bFlag, b2eFlag, b2sFlag, apiFlag)
                End If
            End If
        Catch e As Exception

        End Try
        Return Nothing
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetSessionlessAirportDestination"),
   WebMethod(Description:="Return Airport Destinations",
   EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetSessionlessAirportDestinations(ByVal Language As String,
                                                      ByVal b2cFlag As Boolean,
                                                      ByVal b2bFlag As Boolean,
                                                      ByVal b2eFlag As Boolean,
                                                      ByVal b2sFlag As Boolean,
                                                      ByVal apiFlag As Boolean,
                                                      ByVal strToken As String) As DataSet
        Try
            If (String.IsNullOrEmpty(strToken)) Then
                Return Nothing
            Else
                If IsTokenValid(strToken) = True Then
                    Return ReadAirportDestination(Language, b2cFlag, b2bFlag, b2eFlag, b2sFlag, apiFlag)
                End If
            End If
        Catch e As Exception

        End Try

        Return Nothing
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetSessionlessCurrencies"),
WebMethod(Description:="Return Currencies",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetSessionlessCurrencies(ByVal strLanguage As String, ByVal strToken As String) As DataSet
        Dim objOut As New DataSet
        Dim rsA As ADODB.Recordset = Nothing
        Try
            If (String.IsNullOrEmpty(strToken)) Then
                Return Nothing
            Else
                If IsTokenValid(strToken) = True Then

                    Dim clsRunCom As New clsRunComplus

                    rsA = clsRunCom.GetCurrencies(strLanguage)

                    clsRunCom = Nothing
                    Call InputRecordsetToDataset(rsA, "Currencies", objOut)
                End If
            End If
        Catch e As Exception
        Finally
            Helper.Check.ClearRecordset(rsA)
        End Try
        Return objOut
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetSessionlessCountry"),
WebMethod(Description:="Return Country",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetSessionlessCountry(ByVal strLanguage As String, ByVal strToken As String) As DataSet
        Dim objOut As New DataSet
        Dim rsA As ADODB.Recordset = Nothing
        Try
            If (String.IsNullOrEmpty(strToken)) Then
                Return Nothing
            Else
                If IsTokenValid(strToken) = True Then

                    Dim clsRunCom As New clsRunComplus

                    rsA = clsRunCom.GetCountry(strLanguage)

                    clsRunCom = Nothing
                    Call InputRecordsetToDataset(rsA, "Countrys", objOut)
                End If
            End If
        Catch e As Exception
        Finally
            Helper.Check.ClearRecordset(rsA)
        End Try
        Return objOut
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ReleaseSessionlessFlightInventorySession"),
WebMethod(Description:="Get Client Bookings",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ReleaseSessionlessFlightInventorySession(ByVal BookingId As String, ByVal strToken As String) As Boolean
        Try
            If (String.IsNullOrEmpty(strToken)) Then
                Return Nothing
            Else
                If IsTokenValid(strToken) = True Then
                    Dim clsRunCom As New clsRunComplus
                    Return clsRunCom.ReleaseFlightInventorySession(BookingId)
                End If
            End If
        Catch e As Exception
            Return False
        End Try
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/SessionlessSingleFlightQuoteSummary"),
                           WebMethod(Description:="Get Summary of quote",
                           EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function SessionlessSingleFlightQuoteSummary(ByVal strFlightXml As String,
                                                        ByVal strPassengerXml As String,
                                                        ByVal strAgencyCode As String,
                                                        ByVal strToken As String,
                                                        ByVal strLanguage As String,
                                                        ByVal strCurrencyCode As String,
                                                        ByVal bNoVat As Boolean) As String
        Dim clsRunCom As New clsRunComplus

        If (String.IsNullOrEmpty(strToken)) Then

        Else
            If IsTokenValid(strToken) = True Then
                Return ReadFlightQuoteSummary(strFlightXml, strPassengerXml, strAgencyCode, strLanguage, strCurrencyCode, bNoVat)
            End If
        End If

        Return String.Empty

    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetSessionlessCompactFlightAvailability"),
    WebMethod(Description:="Return Availibility xml",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetSessionlessCompactFlightAvailability(ByVal FirstSegment As Boolean,
                                                            ByVal bookingId As String,
                                                            ByVal Origin As String,
                                                            ByVal Destination As String,
                                                            ByVal DateFrom As Date,
                                                            ByVal DateTo As Date,
                                                            ByVal DateBooking As Date,
                                                            ByVal Adult As Short,
                                                            ByVal Child As Short,
                                                            ByVal Infant As Short,
                                                            ByVal Other As Short,
                                                            ByVal OtherPassengerType As String,
                                                            ByVal BoardingClass As String,
                                                            ByVal BookingClass As String,
                                                            ByVal DayTimeIndicator As String,
                                                            ByVal AgencyCode As String,
                                                            ByVal CurrencyCode As String,
                                                            ByVal FlightId As String,
                                                            ByVal FareId As String,
                                                            ByVal MaxAmount As Double,
                                                            ByVal NonStopOnly As Boolean,
                                                            ByVal IncludeDeparted As Boolean,
                                                            ByVal IncludeCancelled As Boolean,
                                                            ByVal IncludeWaitlisted As Boolean,
                                                            ByVal IncludeSoldOut As Boolean,
                                                            ByVal Refundable As Boolean,
                                                            ByVal GroupFares As Boolean,
                                                            ByVal ItFaresOnly As Boolean,
                                                            ByVal PromotionCode As String,
                                                            ByVal FareType As String,
                                                            ByVal strToken As String) As String
        Dim objStw = New StringWriter
        Try
            If (String.IsNullOrEmpty(strToken)) Then

            Else
                If IsTokenValid(strToken) = True Then
                    Return ReadCompactFlightAvailability(FirstSegment,
                                                           bookingId,
                                                           Origin,
                                                           Destination,
                                                           DateFrom,
                                                           DateTo,
                                                           DateBooking,
                                                           Adult,
                                                           Child,
                                                           Infant,
                                                           Other,
                                                           OtherPassengerType,
                                                           BoardingClass,
                                                           BookingClass,
                                                           DayTimeIndicator,
                                                           AgencyCode,
                                                           CurrencyCode,
                                                           FlightId,
                                                           FareId,
                                                           MaxAmount,
                                                           NonStopOnly,
                                                           IncludeDeparted,
                                                           IncludeCancelled,
                                                           IncludeWaitlisted,
                                                           IncludeSoldOut,
                                                           Refundable,
                                                           GroupFares,
                                                           ItFaresOnly,
                                                           PromotionCode,
                                                           FareType)
                End If
            End If
        Catch e As Exception
        End Try

        Return String.Empty
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetSessionlessBinRangeSearch"),
    WebMethod(Description:="Return  Activities",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetSessionlessBinRangeSearch(ByVal strCardType As String, ByVal strStatusCode As String, ByVal strToken As String) As DataSet
        Dim objOut As New DataSet
        Dim rsA As ADODB.Recordset = Nothing
        Try
            If (String.IsNullOrEmpty(strToken)) Then
                Return Nothing
            Else

                If IsTokenValid(strToken) = True Then

                    Dim clsRunCom As New clsRunComplus

                    rsA = clsRunCom.GetBinRangeSearch(strCardType, strStatusCode)
                    clsRunCom = Nothing
                    Call InputRecordsetToDataset(rsA, "BinRangeSearch", objOut)
                End If
            End If
        Catch e As Exception
        Finally
            Helper.Check.ClearRecordset(rsA)
        End Try
        Return objOut
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetSessionlessFeesDefinition"),
WebMethod(Description:="Return Fee",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetSessionlessFeesDefinition(ByVal strLanguage As String, ByVal strToken As String) As String

        Try
            If (String.IsNullOrEmpty(strToken)) Then
                Return String.Empty
            Else
                If IsTokenValid(strToken) = True Then
                    Return FeeDefinitionRead(strLanguage)
                Else
                    Return String.Empty
                End If
            End If
        Catch e As Exception
            Return String.Empty
        End Try
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/SessionlessExternalPaymentAddPayment"),
   WebMethod(Description:="Add payment to booking",
   EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function SessionlessExternalPaymentAddPayment(ByVal strBookingId As String,
                                                          ByVal strAgencyCode As String,
                                                          ByVal strFormOfPayment As String,
                                                          ByVal strCurrencyCode As String,
                                                          ByVal dAmount As Decimal,
                                                          ByVal strFormOfPaymentSubtype As String,
                                                          ByVal strUserId As String,
                                                          ByVal strDocumentNumber As String,
                                                          ByVal strApprovalCode As String,
                                                          ByVal strRemark As String,
                                                          ByVal strLanguage As String,
                                                          ByVal dtPayment As DateTime,
                                                          ByVal bReturnItinerary As Boolean,
                                                          ByVal strToken As String) As String
        Try
            If (String.IsNullOrEmpty(strToken)) Then
                Return Nothing
            Else

                If IsTokenValid(strToken) = True Then

                    Return AddPayment(strBookingId,
                                        strAgencyCode,
                                        strFormOfPayment,
                                        strCurrencyCode,
                                        dAmount,
                                        strFormOfPaymentSubtype,
                                        strUserId,
                                        strDocumentNumber,
                                        strApprovalCode,
                                        strRemark,
                                        strLanguage,
                                        dtPayment,
                                        bReturnItinerary)
                End If
            End If
        Catch e As Exception
            Return String.Empty
        End Try
        Return String.Empty
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetBaggageFeeOptions"),
 WebMethod(Description:="Get Baggage Fee",
 EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetBaggageFeeOptions(ByVal xmlMapping As String,
                                         ByVal strSegmentId As String,
                                         ByVal strPassengerId As String,
                                         ByVal strAgencyCode As String,
                                         ByVal strLanguage As String,
                                         ByVal lMaxUnits As Long,
                                         ByVal xmlFees As String,
                                         ByVal bApplySegmentFee As Boolean,
                                         ByVal bNoVat As Boolean) As String

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Dim rsFees As ADODB.Recordset = Nothing
            Try
                Dim rsMappings As ADODB.Recordset = Session("rsMappings")

                objOut.DataSetName = "BaggageFees"
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then


                        Dim clsRunCom As New clsRunComplus
                        Dim MappingsUpdate As New MappingsUpdate
                        Dim Fees As New Fees
                        Dim r As New Helper.RecordSet

                        xmlMapping = xmlMapping.Replace("Mapping", "MappingUpdate")
                        MappingsUpdate = Helper.Serialization.Deserialize(xmlMapping, MappingsUpdate)
                        r.FillMapping(rsMappings, MappingsUpdate)

                        If String.IsNullOrEmpty(xmlFees) = False Then
                            rsFees = Session("rsFees")
                            Fees = Helper.Serialization.Deserialize(xmlFees, Fees)
                            r.FillFees(rsFees, Fees)
                        End If

                        rsA = clsRunCom.GetBaggageFeeOptions(rsMappings, strSegmentId, strPassengerId, strAgencyCode, strLanguage, lMaxUnits, rsFees, bApplySegmentFee, bNoVat)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "Fee", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try

            Return objOut.GetXml()
        End Using
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ExternalPaymentAddPayment"),
 WebMethod(Description:="Add payment to booking",
 EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ExternalPaymentAddPayment(ByVal strBookingId As String,
                                              ByVal strAgencyCode As String,
                                              ByVal strFormOfPayment As String,
                                              ByVal strCurrencyCode As String,
                                              ByVal dAmount As Decimal,
                                              ByVal strFormOfPaymentSubtype As String,
                                              ByVal strUserId As String,
                                              ByVal strDocumentNumber As String,
                                              ByVal strApprovalCode As String,
                                              ByVal strRemark As String,
                                              ByVal strLanguage As String,
                                              ByVal dtPayment As DateTime,
                                              ByVal bReturnItinerary As Boolean) As String

        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Return AddPayment(strBookingId,
                                        strAgencyCode,
                                        strFormOfPayment,
                                        strCurrencyCode,
                                        dAmount,
                                        strFormOfPaymentSubtype,
                                        strUserId,
                                        strDocumentNumber,
                                        strApprovalCode,
                                        strRemark,
                                        strLanguage,
                                        dtPayment,
                                        bReturnItinerary)
                End If
            End If
        Catch e As Exception
            Return String.Empty
        End Try

        Return String.Empty
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/SaveBookingMultipleFormOfPayment"),
            WebMethod(Description:="Save booking with payment",
            EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function SaveBookingMultipleFormOfPayment(ByVal BookingId As String,
                                                    ByVal XmlHeader As String,
                                                    ByVal XmlSegments As String,
                                                    ByVal XmlPassengers As String,
                                                    ByVal XmlPayments As String,
                                                    ByVal XmlRemarks As String,
                                                    ByVal XmlMapping As String,
                                                    ByVal XmlFees As String,
                                                    ByVal XmlPaymentFees As String,
                                                    ByVal XmlTaxes As String,
                                                    ByVal XMLServices As String,
                                                    ByVal CreateTickets As Boolean,
                                                    ByVal securityToken As String,
                                                    ByVal authenticationToken As String,
                                                    ByVal commerceIndicator As String,
                                                    ByVal strRequestSource As String,
                                                    ByVal strLanguage As String) As DataSet

        Using ds As New DataSet()
            Dim FabricateRecordset As New FabricateRecordset

            Dim rsVoucherPayments As ADODB.Recordset = Nothing
            Dim rsCCPayments As ADODB.Recordset = Nothing
            Dim rsVoucher As ADODB.Recordset = Nothing
            Dim rsCC As ADODB.Recordset = Nothing
            Dim rsAllocation As ADODB.Recordset = FabricateRecordset.FabricatePaymentAllocationRecordset
            Dim stSaveBookingResult As Short = -1
            Dim strCCResponse As String = String.Empty
            Dim strVoucherSessionId As String = Guid.NewGuid.ToString

            Try
                Dim clsRunCom As New clsRunComplus

                Dim rsHeader As ADODB.Recordset = Session("rsHeader")
                Dim rsSegments As ADODB.Recordset = Session("rsSegments")
                Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
                Dim rsRemarks As ADODB.Recordset = Session("rsRemarks")
                Dim rsPayments As ADODB.Recordset = Session("rsPayments")
                Dim rsMappings As ADODB.Recordset = Session("rsMappings")
                Dim rsServices As ADODB.Recordset = Session("rsServices")
                Dim rsTaxes As ADODB.Recordset = Session("rsTaxes")
                Dim rsFees As ADODB.Recordset = Session("rsFees")
                Dim rsQuotes As ADODB.Recordset = Session("rsQuotes")


                Dim BookingHeaderUpdate As New BookingHeaderUpdate
                Dim Passengers As New Passengers
                Dim Mappings As New Mappings
                Dim Segments As New Itinerary
                Dim Fees As New Fees
                Dim TotalFees As New Fees
                Dim Remarks As New Remarks
                Dim MappingsUpdate As New MappingsUpdate
                Dim Taxes As New Taxes
                Dim Quotes As New Quotes
                Dim Services As New Services
                Dim Allocations As Allocations

                Dim PaymentsUpdate As New PaymentsUpdate
                Dim PaymentsVoucherUpdate As New PaymentsUpdate
                Dim PaymentsCCUpdate As New PaymentsUpdate

                Dim r As New Helper.RecordSet

                Dim strRecordLocator As String = String.Empty
                Dim iBookingNumber As Integer
                Dim bCCvalidationPass As Boolean

                clsRunCom.GetEmpty(AgentHeader.AgencyCode, AgentHeader.AgencyCurrencyRcd, , , , , rsVoucherPayments)
                If Not rsVoucherPayments Is Nothing Then

                    XmlHeader = XmlHeader.Replace("BookingHeader", "BookingHeaderUpdate")
                    BookingHeaderUpdate = Serialization.Deserialize(XmlHeader, BookingHeaderUpdate)

                    XmlPayments = XmlPayments.Replace("Payment", "PaymentUpdate")
                    PaymentsUpdate = Serialization.Deserialize(XmlPayments, PaymentsUpdate)

                    'Split payment object
                    For i As Integer = 0 To PaymentsUpdate.Count - 1
                        If PaymentsUpdate(i).form_of_payment_rcd <> "CC" Then
                            'Fill Voucher payment
                            PaymentsVoucherUpdate.Add(PaymentsUpdate(i))
                        ElseIf PaymentsUpdate(i).form_of_payment_rcd = "CC" Then
                            'Fill CC payment.
                            PaymentsCCUpdate.Add(PaymentsUpdate(i))
                        End If
                    Next

                    r.Fill(rsVoucherPayments, PaymentsVoucherUpdate)


                    Passengers = Serialization.Deserialize(XmlPassengers, Passengers)
                    Segments = Serialization.Deserialize(XmlSegments, Segments)
                    Mappings = Serialization.Deserialize(XmlMapping, Mappings)
                    Fees = Serialization.Deserialize(XmlFees, Fees)

                    XmlMapping = XmlMapping.Replace("Mapping", "MappingUpdate")
                    MappingsUpdate = Serialization.Deserialize(XmlMapping, MappingsUpdate)
                    Services = Serialization.Deserialize(XMLServices, Services)
                    Remarks = Serialization.Deserialize(XmlRemarks, Remarks)

                    'Find Payment Allocation.
                    If XmlPaymentFees.Length > 0 Then
                        TotalFees = Serialization.Deserialize(XmlPaymentFees, TotalFees)
                    End If


                    Allocations = GetPaymentAllocations(PaymentsUpdate, Mappings, Fees, TotalFees)
                    Dim Test = Serialize(Allocations)
                    With r
                        If Not Session("CreateById") Is Nothing AndAlso Not Session("CreateById") = String.Empty Then
                            .DefaultCreateById = New Guid(Session("CreateById").ToString())
                        Else
                            If IsGuid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")) Then
                                Session("CreateById") = System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")
                            End If
                            r.DefaultCreateById = New Guid(Session("CreateById").ToString())
                        End If

                        .Fill(rsAllocation, Allocations, False)

                        .FillHeader(rsHeader, BookingHeaderUpdate)
                        .FillPassengers(rsPassengers, Passengers)
                        .FillMapping(rsMappings, MappingsUpdate)
                        .FillRemarks(rsRemarks, Remarks)

                        .ClearFees(rsFees, Fees)
                        .FillFees(rsFees, Fees)

                        .ClearServices(rsServices, Services)
                        .FillServices(rsServices, Services)

                        If .FeeChange(rsFees, Fees) Then
                            .FillCreateDate(rsFees)
                            .FillAccountBy(rsFees)
                        End If
                        If .SegmentChange(rsSegments, Segments) Then
                            .FillCreateDate(rsSegments)
                        End If
                    End With

                    If InfantExcessLimit(rsSegments, BookingHeaderUpdate.number_of_infants) = False Then
                        If ValidateSave(rsHeader, rsSegments, rsPassengers, rsMappings) = True Then
                            'Validate duplicate seat and booking session.
                            If clsRunCom.VerifyFlightInventorySession(rsSegments) = False Then
                                CreateErrorDataset(ds, "202", "SESSIONTIMEOUT")
                            ElseIf clsRunCom.VerifySeatAssignment(rsMappings, rsSegments) = True Then
                                CreateErrorDataset(ds, "203", "DUPLICATESEAT")
                            ElseIf clsRunCom.ValidAmount(rsVoucherPayments, rsMappings, rsFees, BookingHeaderUpdate.agency_code, BookingHeaderUpdate.ip_address, r.DefaultCreateById.ToString, strVoucherSessionId) = False Then
                                CreateErrorDataset(ds, "207", "NOT ENOUGH AMOUNT TO MAKE PAYMENT")
                            Else
                                If PaymentsCCUpdate.Count > 0 Then

                                    'Get Record locator
                                    clsRunCom.GetEmpty(AgentHeader.AgencyCode, AgentHeader.AgencyCurrencyRcd, , , , , rsCCPayments)
                                    r.Fill(rsCCPayments, PaymentsCCUpdate)

                                    If rsHeader.Fields("record_locator").Value Is System.DBNull.Value Then

                                        clsRunCom.GetRecordLocator(strRecordLocator, iBookingNumber)
                                        rsHeader.Fields("booking_number").Value = iBookingNumber
                                        rsHeader.Fields("record_locator").Value = strRecordLocator
                                    ElseIf Len(rsHeader.Fields("record_locator").Value) = 0 Then

                                        clsRunCom.GetRecordLocator(strRecordLocator, iBookingNumber)
                                        rsHeader.Fields("booking_number").Value = iBookingNumber
                                        rsHeader.Fields("record_locator").Value = strRecordLocator
                                    Else
                                        strRecordLocator = rsHeader.Fields("record_locator").Value
                                    End If

                                    rsCC = clsRunCom.ValidCreditCard(rsCCPayments,
                                                                    rsSegments,
                                                                    rsAllocation,
                                                                    rsVoucher,
                                                                    rsMappings,
                                                                    securityToken,
                                                                    authenticationToken,
                                                                    commerceIndicator,
                                                                    strRecordLocator,
                                                                    rsFees,
                                                                    rsHeader,
                                                                    rsTaxes,
                                                                    strRequestSource,
                                                                    True)

                                    If (Not rsCC Is Nothing) Then
                                        If rsCC.RecordCount > 0 Then
                                            strCCResponse = rsCC("ResponseCode").Value
                                            If strCCResponse = "APPROVED" Then
                                                bCCvalidationPass = True
                                            End If
                                        Else
                                            bCCvalidationPass = False
                                        End If
                                    End If
                                Else
                                    bCCvalidationPass = True
                                End If

                                If bCCvalidationPass = True Then
                                    If clsRunCom.PaymentSave(rsVoucherPayments, rsAllocation, "") = True Then
                                        'Perform Save booking
                                        stSaveBookingResult = clsRunCom.SaveBooking(rsHeader, rsSegments, rsPassengers, rsRemarks, , rsMappings, rsServices, rsTaxes, rsFees, CreateTickets, False, False, True, False, False)

                                        If stSaveBookingResult <> 0 Then
                                            'Clear voucher session
                                            clsRunCom.DeleteVoucherSession(strVoucherSessionId, rsVoucherPayments)
                                        End If

                                        If stSaveBookingResult = -1 Then
                                            'Sent Error log Email
                                            Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                                 XmlHeader,
                                                                                                                                                                                 XmlSegments,
                                                                                                                                                                                 XmlPassengers,
                                                                                                                                                                                 XmlPayments,
                                                                                                                                                                                 XmlRemarks,
                                                                                                                                                                                 XmlMapping,
                                                                                                                                                                                 XmlFees,
                                                                                                                                                                                 XmlPaymentFees,
                                                                                                                                                                                 XmlTaxes,
                                                                                                                                                                                 XMLServices,
                                                                                                                                                                                 CreateTickets,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 strLanguage,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 stSaveBookingResult.ToString()),
                                                                                                                                                        "SAVEFAILED",
                                                                                                                                                        "",
                                                                                                                                                        "SaveBookingMultipleFormOfPayment",
                                                                                                                                                        "TikAero.WebService",
                                                                                                                                                        "")
                                            CreateErrorDataset(ds, "201", "SAVEFAILED")
                                        ElseIf stSaveBookingResult = 1 Then
                                            'Sent Error log Email
                                            Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                                 XmlHeader,
                                                                                                                                                                                 XmlSegments,
                                                                                                                                                                                 XmlPassengers,
                                                                                                                                                                                 XmlPayments,
                                                                                                                                                                                 XmlRemarks,
                                                                                                                                                                                 XmlMapping,
                                                                                                                                                                                 XmlFees,
                                                                                                                                                                                 XmlPaymentFees,
                                                                                                                                                                                 XmlTaxes,
                                                                                                                                                                                 XMLServices,
                                                                                                                                                                                 CreateTickets,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 strLanguage,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 stSaveBookingResult.ToString()),
                                                                                                                                                        "SESSIONTIMEOUT",
                                                                                                                                                        "",
                                                                                                                                                        "SaveBookingMultipleFormOfPayment",
                                                                                                                                                        "TikAero.WebService",
                                                                                                                                                        "")
                                            CreateErrorDataset(ds, "202", "SESSIONTIMEOUT")
                                        ElseIf stSaveBookingResult = 2 Then
                                            'Sent Error log Email
                                            Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                                 XmlHeader,
                                                                                                                                                                                 XmlSegments,
                                                                                                                                                                                 XmlPassengers,
                                                                                                                                                                                 XmlPayments,
                                                                                                                                                                                 XmlRemarks,
                                                                                                                                                                                 XmlMapping,
                                                                                                                                                                                 XmlFees,
                                                                                                                                                                                 XmlPaymentFees,
                                                                                                                                                                                 XmlTaxes,
                                                                                                                                                                                 XMLServices,
                                                                                                                                                                                 CreateTickets,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 strLanguage,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 stSaveBookingResult.ToString()),
                                                                                                                                                        "DUPLICATESEAT",
                                                                                                                                                        "",
                                                                                                                                                        "SaveBookingMultipleFormOfPayment",
                                                                                                                                                        "TikAero.WebService",
                                                                                                                                                        "")
                                            CreateErrorDataset(ds, "203", "DUPLICATESEAT")
                                        ElseIf stSaveBookingResult = 3 Then
                                            'Sent Error log Email
                                            Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                                 XmlHeader,
                                                                                                                                                                                 XmlSegments,
                                                                                                                                                                                 XmlPassengers,
                                                                                                                                                                                 XmlPayments,
                                                                                                                                                                                 XmlRemarks,
                                                                                                                                                                                 XmlMapping,
                                                                                                                                                                                 XmlFees,
                                                                                                                                                                                 XmlPaymentFees,
                                                                                                                                                                                 XmlTaxes,
                                                                                                                                                                                 XMLServices,
                                                                                                                                                                                 CreateTickets,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 strLanguage,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 stSaveBookingResult.ToString()),
                                                                                                                                                        "DATABASEACCESS",
                                                                                                                                                        "",
                                                                                                                                                        "SaveBookingMultipleFormOfPayment",
                                                                                                                                                        "TikAero.WebService",
                                                                                                                                                        "")
                                            CreateErrorDataset(ds, "204", "DATABASEACCESS")
                                        ElseIf stSaveBookingResult = 4 Then
                                            'Sent Error log Email
                                            Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                                 XmlHeader,
                                                                                                                                                                                 XmlSegments,
                                                                                                                                                                                 XmlPassengers,
                                                                                                                                                                                 XmlPayments,
                                                                                                                                                                                 XmlRemarks,
                                                                                                                                                                                 XmlMapping,
                                                                                                                                                                                 XmlFees,
                                                                                                                                                                                 XmlPaymentFees,
                                                                                                                                                                                 XmlTaxes,
                                                                                                                                                                                 XMLServices,
                                                                                                                                                                                 CreateTickets,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 strLanguage,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 stSaveBookingResult.ToString()),
                                                                                                                                                        "BOOKINGINUSE",
                                                                                                                                                        "",
                                                                                                                                                        "SaveBookingMultipleFormOfPayment",
                                                                                                                                                        "TikAero.WebService",
                                                                                                                                                        "")
                                            CreateErrorDataset(ds, "205", "BOOKINGINUSE")
                                        ElseIf stSaveBookingResult = 5 Then
                                            'Sent Error log Email
                                            Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                                                 XmlHeader,
                                                                                                                                                                                 XmlSegments,
                                                                                                                                                                                 XmlPassengers,
                                                                                                                                                                                 XmlPayments,
                                                                                                                                                                                 XmlRemarks,
                                                                                                                                                                                 XmlMapping,
                                                                                                                                                                                 XmlFees,
                                                                                                                                                                                 XmlPaymentFees,
                                                                                                                                                                                 XmlTaxes,
                                                                                                                                                                                 XMLServices,
                                                                                                                                                                                 CreateTickets,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 strLanguage,
                                                                                                                                                                                 String.Empty,
                                                                                                                                                                                 stSaveBookingResult.ToString()),
                                                                                                                                                        "BOOKINGREADERROR",
                                                                                                                                                        "",
                                                                                                                                                        "SaveBookingMultipleFormOfPayment",
                                                                                                                                                        "TikAero.WebService",
                                                                                                                                                        "")
                                            CreateErrorDataset(ds, "206", "BOOKINGREADERROR")
                                        Else
                                            Call clsRunCom.UpdatePassengerNames(rsPassengers,
                                                                                rsMappings,
                                                                                rsHeader,
                                                                                rsFees,
                                                                                rsSegments,
                                                                                AgentHeader.AgencyCode,
                                                                                AgentHeader.AgencyCurrencyRcd,
                                                                                BookingId,
                                                                                True)

                                            If CreateTickets Then
                                                Dim b As Boolean = clsRunCom.TicketCreate(AgentHeader.AgencyCode,
                                                                                          Session("CreateById").ToString().Replace("{", "").Replace("}", ""), ,
                                                                                          "{" & BookingId & "}", False)
                                            End If

                                            'Update Approval
                                            Dim strIBE = "B2C,B2E"
                                            If BookingHeaderUpdate.booking_source_rcd.ToUpper().IndexOf(strIBE) > 0 Then
                                                Dim bUpdateApproval As String = System.Configuration.ConfigurationManager.AppSettings("UpdateApproval")
                                                If Not String.IsNullOrEmpty(bUpdateApproval) Then
                                                    If bUpdateApproval.ToString.ToLower = "true" Then
                                                        clsRunCom.UpdateApproval(BookingId, 1)
                                                    End If
                                                End If
                                            End If

                                            'Clear recordset before call get itinerary.
                                            ClearBookingRecordset(rsHeader,
                                                                  rsSegments,
                                                                  rsPassengers,
                                                                  rsRemarks,
                                                                  rsPayments,
                                                                  rsMappings,
                                                                  rsServices,
                                                                  rsTaxes,
                                                                  rsFees,
                                                                  rsQuotes)

                                            Call clsRunCom.ItiniraryRead(BookingId,
                                                                         String.Empty,
                                                                         BookingHeaderUpdate.agency_code,
                                                                         rsHeader,
                                                                         rsSegments,
                                                                         rsPassengers,
                                                                         rsRemarks,
                                                                         rsPayments,
                                                                         rsMappings,
                                                                         rsServices,
                                                                         rsTaxes,
                                                                         rsFees,
                                                                         rsQuotes,
                                                                         strLanguage)

                                            CopyRecordsetsToDataset(rsHeader,
                                                                    rsSegments,
                                                                    rsPassengers,
                                                                    rsRemarks,
                                                                    rsPayments,
                                                                    rsMappings,
                                                                    rsTaxes,
                                                                    rsQuotes,
                                                                    rsFees,
                                                                    rsServices,
                                                                    ds)

                                            'Clear recordset before call get itinerary.
                                            ClearBookingRecordset(rsHeader,
                                                                  rsSegments,
                                                                  rsPassengers,
                                                                  rsRemarks,
                                                                  rsPayments,
                                                                  rsMappings,
                                                                  rsServices,
                                                                  rsTaxes,
                                                                  rsFees,
                                                                  rsQuotes)

                                            'Remove booking session
                                            Session.Remove("rsHeader")
                                            Session.Remove("rsSegments")
                                            Session.Remove("rsPassenger")
                                            Session.Remove("rsRemarks")
                                            Session.Remove("rsPayments")
                                            Session.Remove("rsMappings")
                                            Session.Remove("rsServices")
                                            Session.Remove("rsTaxes")
                                            Session.Remove("rsFees")
                                            Session.Remove("rsQuotes")


                                            Dim BookingState = New Booking
                                            Serialization.FillBooking(ds, BookingState)
                                            Session("BookingState") = BookingState
                                        End If
                                    End If
                                Else
                                    If (Not rsCC Is Nothing) Then
                                        Call InputRecordsetToDataset(rsCC, "Payments", ds)
                                        'Clear voucher session
                                        clsRunCom.DeleteVoucherSession(strVoucherSessionId, rsVoucherPayments)
                                        'Assign Recordlocator to error message if have one.
                                        If strRecordLocator.Length > 0 Then
                                            If IsNothing(ds) = False AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                                                Dim dcRecordlocator As New DataColumn("record_locator")
                                                ds.Tables(0).Columns.Add(dcRecordlocator)
                                                ds.Tables(0).Rows(0).Item("record_locator") = strRecordLocator
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        Else
                            If System.Configuration.ConfigurationManager.AppSettings("ErrorMailEnabled").ToUpper = "TRUE" Then
                                Dim ErrorSubject As String = System.Configuration.ConfigurationManager.AppSettings("ErrorSubject") & System.Convert.ToString(Date.Now)
                                Mail.Send(ErrorHelper.RenderDebugMessage("SaveBookingPayment", "Error Missing Data", XmlHeader, XmlSegments, XmlPassengers, XmlPayments, XmlRemarks, XmlMapping, XmlFees, XmlTaxes, String.Empty, String.Empty), ErrorSubject, System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), Nothing)
                            End If
                        End If
                    Else
                        CreateErrorDataset(ds, "200", "Infant over limit")
                    End If
                End If
            Catch e As Exception
                Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(e,
                                                                                                                      CreateBookingSaveInputErrorLog(BookingId,
                                                                                                                                                     XmlHeader,
                                                                                                                                                     XmlSegments,
                                                                                                                                                     XmlPassengers,
                                                                                                                                                     XmlPayments,
                                                                                                                                                     XmlRemarks,
                                                                                                                                                     XmlMapping,
                                                                                                                                                     XmlFees,
                                                                                                                                                     XmlPaymentFees,
                                                                                                                                                     XmlTaxes,
                                                                                                                                                     XMLServices,
                                                                                                                                                     CreateTickets,
                                                                                                                                                     securityToken,
                                                                                                                                                     authenticationToken,
                                                                                                                                                     commerceIndicator,
                                                                                                                                                     strRequestSource,
                                                                                                                                                     strLanguage,
                                                                                                                                                     strCCResponse,
                                                                                                                                                     stSaveBookingResult.ToString()))
            Finally
                Helper.Check.ClearRecordset(rsVoucherPayments)
                Helper.Check.ClearRecordset(rsCCPayments)
                Helper.Check.ClearRecordset(rsVoucher)
                Helper.Check.ClearRecordset(rsAllocation)
                Helper.Check.ClearRecordset(rsCC)
            End Try
            Return ds
        End Using
    End Function

    'add GetAvailabilety for check SSR inventory availability
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetAvailabilety"),
 WebMethod(Description:="Check SSR inventory availability",
 EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetAvailabilety(ByVal strFlightID As String,
                                ByVal strOriginRcd As String,
                                ByVal strDestinationRcd As String,
                                ByVal strSpecialServiceRcd As String,
                                ByVal strBoardingClassRcd As String
                                ) As String

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing

            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport


                    Dim clsRunCom As New clsRunComplus

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        rsA = clsRunCom.GetAvailabilety(strFlightID, strOriginRcd, strDestinationRcd, strSpecialServiceRcd, strBoardingClassRcd)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "SpecialService", objOut)
                    End If
                End If
            Catch e As Exception
                Helper.Check.ClearRecordset(rsA)
                Return String.Empty
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try

            If IsNothing(objOut) = False AndAlso objOut.Tables.Count > 0 AndAlso objOut.Tables(0).Rows.Count Then
                Return objOut.GetXml
            Else
                Return String.Empty
            End If
        End Using

    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/SpecialServiceInventoryRead"),
WebMethod(Description:="get SSR inventory",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function SpecialServiceInventoryRead(ByVal strFlightID As String, ByVal bWrite As Boolean) As String
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.SpecialServiceInventoryFlightGet(strFlightID, bWrite)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "Service", objOut)
                        If rsA.State = ADODB.ObjectStateEnum.adStateOpen Then
                            rsA.Close()
                        End If
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut.GetXml
        End Using
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/AddFee"),
   WebMethod(Description:="Calculate Fee",
   EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function AddFee(ByVal AgencyCode As String,
                            ByVal strCurrency As String,
                            ByVal strBookingId As String,
                            ByVal strFee As String,
                            ByVal XmlHeader As String,
                            ByVal XmlSegments As String,
                            ByVal XmlPassengers As String,
                            ByVal XmlFees As String,
                            ByVal XmlRemarks As String,
                            ByVal XmlPayments As String,
                            ByVal XmlMapping As String,
                            ByVal XMLServices As String,
                            ByVal XmlTaxes As String,
                            ByVal strLanguage As String,
                            ByVal bNoVat As Boolean) As String

        Using ds As New DataSet
            Dim rsFeesClone As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport
                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim fr As New FabricateRecordset
                        Dim Serialization As New Helper.Serialization
                        Dim clsRunCom As New clsRunComplus
                        Dim rsHeader As ADODB.Recordset = Session("rsHeader")
                        Dim rsSegments As ADODB.Recordset = Session("rsSegments")
                        Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
                        Dim rsRemarks As ADODB.Recordset = Session("rsRemarks")
                        Dim rsPayments As ADODB.Recordset = Session("rsPayments")
                        Dim rsMappings As ADODB.Recordset = Session("rsMappings")
                        Dim rsTaxes As ADODB.Recordset = Session("rsTaxes")
                        Dim rsQuotes As ADODB.Recordset = Session("rsQuotes")
                        Dim rsServices As ADODB.Recordset = Session("rsServices")
                        Dim rsFees As ADODB.Recordset = Session("rsFees")

                        Dim r As New Helper.RecordSet

                        Dim BookingHeaderUpdate As New BookingHeaderUpdate

                        Dim Itinerary As New Itinerary
                        Dim Mappings As New Mappings
                        Dim Passengers As New Passengers
                        Dim PaymentsUpdate As New PaymentsUpdate
                        Dim Remarks As New Remarks
                        Dim MappingsUpdate As New MappingsUpdate
                        Dim Fees As New Fees
                        Dim Taxes As New Taxes
                        Dim Services As New Services

                        rsFeesClone = New ADODB.Recordset
                        Dim Status As Boolean = True

                        If Status = True Then
                            XmlHeader = XmlHeader.Replace("BookingHeader", "BookingHeaderUpdate")
                            BookingHeaderUpdate = Helper.Serialization.Deserialize(XmlHeader, BookingHeaderUpdate)
                            r.FillHeader(rsHeader, BookingHeaderUpdate)

                            XmlMapping = XmlMapping.Replace("Mapping", "MappingUpdate")
                            MappingsUpdate = Helper.Serialization.Deserialize(XmlMapping, MappingsUpdate)
                            r.FillMapping(rsMappings, MappingsUpdate)

                            Fees = Helper.Serialization.Deserialize(XmlFees, Fees)
                            r.ClearFees(rsFees, Fees)
                            r.FillFees(rsFees, Fees)

                            clsRunCom.AddFee(AgencyCode, strCurrency, strBookingId, strFee, rsHeader, rsSegments, rsPassengers, rsFees, rsRemarks, rsPayments, rsMappings, rsServices, rsTaxes, strLanguage, bNoVat)
                            rsMappings.Filter = 0
                            fr.CopyAdoRecordset(rsFees, rsFeesClone)

                            Call InputRecordsetToDataset(rsFeesClone, "Fees", "Fee", ds)
                        End If
                    End If
                End If
            Catch e As Exception
                If System.Configuration.ConfigurationManager.AppSettings("ErrorMailEnabled").ToUpper = "TRUE" Then
                    Dim ErrorSubject As String = System.Configuration.ConfigurationManager.AppSettings("ErrorSubject") & System.Convert.ToString(Date.Now)
                    Mail.Send(ErrorHelper.RenderDebugMessage("CalculateNewFees", e.Message, XmlHeader, XmlSegments, XmlPassengers, XmlPayments, XmlRemarks, XmlMapping, XmlFees, XmlTaxes, String.Empty, String.Empty), ErrorSubject, System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), Nothing)
                End If
            Finally
                Helper.Check.ClearRecordset(rsFeesClone)
            End Try
            If IsNothing(ds) = False Then
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Return ds.GetXml
                Else
                    Return String.Empty
                End If
            Else
                Return String.Empty
            End If
        End Using
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetPassengerRole"),
        WebMethod(Description:="Read Passenger Role",
        EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetPassengerRole(ByVal strLanguage As String) As String
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetPassengerRole(strLanguage)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "PassengerRole", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try

            If IsNothing(objOut) = False Then
                Return objOut.GetXml
            Else
                Return String.Empty
            End If
        End Using
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetSpecialServices"),
        WebMethod(Description:="Read Passenger Role",
        EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetSpecialServices(ByVal strLanguage As String) As String
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetSpecialServices(strLanguage)
                        clsRunCom = Nothing

                        Call InputRecordsetToDataset(rsA, "Service", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try

            If IsNothing(objOut) = False Then
                Return objOut.GetXml
            Else
                Return String.Empty
            End If
        End Using
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ProcessRefundVoucher"),
        WebMethod(Description:="Refund Voucher",
        EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ProcessRefundVoucher(ByVal strBookingPaymentId As String,
                                         ByVal strBookingId As String,
                                         ByVal strUserId As String,
                                         ByVal strFormOfPayment As String,
                                         ByVal strAgencyCode As String,
                                         ByVal strSaleCurrency As String,
                                         ByVal strPaymentCurrency As String,
                                         ByVal dSaleAmount As Decimal,
                                         ByVal dPaymentAmount As Decimal,
                                         ByVal strVoucherId As String,
                                         ByVal strRefundVoucher As String,
                                         ByVal strClientId As String,
                                         ByVal strNameOnVoucher As String,
                                         ByVal strDebitAgencyCode As String,
                                         ByVal strOriginBookingPaymentId As String,
                                         ByVal strDocumentNumber As String,
                                         ByVal dtPayment As DateTime) As String

        Dim clsRunCom As New clsRunComplus
        Try
            If (AgentHeader Is Nothing) Then
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport
                If (AuthenticatePassport(strCode, strPassport)) Then

                    Return clsRunCom.ProcessRefund(strBookingPaymentId,
                                                    strBookingId,
                                                    strUserId,
                                                    strFormOfPayment,
                                                    strAgencyCode,
                                                    strSaleCurrency,
                                                    strPaymentCurrency,
                                                    dSaleAmount,
                                                    dPaymentAmount, , ,
                                                    strVoucherId,
                                                    strRefundVoucher,
                                                    strClientId,
                                                    strNameOnVoucher,
                                                    strDebitAgencyCode,
                                                    strOriginBookingPaymentId,
                                                    strDocumentNumber,
                                                    dtPayment)
                End If
            End If

            clsRunCom = Nothing
            Return String.Empty

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ViewBookingChange"),
WebMethod(Description:="Return Booking Information",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ViewBookingChange(ByVal BookingId As String, ByVal strLanguage As String, ByVal strAgencyCode As String) As String
        Using ds As New DataSet
            Try
                If (AgentHeader Is Nothing) Then

                Else

                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport
                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus
                        Dim rsHeader As ADODB.Recordset = Nothing
                        Dim rsSegments As ADODB.Recordset = Nothing
                        Dim rsPassengers As ADODB.Recordset = Nothing
                        Dim rsRemarks As ADODB.Recordset = Nothing
                        Dim rsPayments As ADODB.Recordset = Nothing
                        Dim rsMappings As ADODB.Recordset = Nothing
                        Dim rsServices As ADODB.Recordset = Nothing
                        Dim rsTaxes As ADODB.Recordset = Nothing
                        Dim rsFees As ADODB.Recordset = Nothing
                        Dim rsQuotes As ADODB.Recordset = Nothing
                        Dim bBookingLock As Boolean = False

                        'Clear Current Booking Recordset session
                        ClearBookingRecordset(rsHeader, rsSegments, rsPassengers, rsRemarks, rsPayments, rsMappings, rsServices, rsTaxes, rsFees, rsQuotes)
                        'Remove Session
                        Session.Remove("rsHeader")
                        Session.Remove("rsSegments")
                        Session.Remove("rsPassenger")
                        Session.Remove("rsRemarks")
                        Session.Remove("rsPayments")
                        Session.Remove("rsMappings")
                        Session.Remove("rsServices")
                        Session.Remove("rsTaxes")
                        Session.Remove("rsFees")
                        Session.Remove("rsQuotes")

                        Call clsRunCom.ItiniraryRead(BookingId,
                                                     String.Empty,
                                                     strAgencyCode,
                                                     rsHeader,
                                                     rsSegments,
                                                     rsPassengers,
                                                     rsRemarks,
                                                     rsPayments,
                                                     rsMappings,
                                                     rsServices,
                                                     rsTaxes,
                                                     rsFees,
                                                     rsQuotes,
                                                     strLanguage)

                        'Convert to Recordset.
                        CopyRecordsetsToDataset(rsHeader, rsSegments, rsPassengers, rsRemarks, rsPayments, rsMappings, rsTaxes, rsQuotes, rsFees, rsServices, ds)

                        'Add Field booking lock date time.
                        If String.IsNullOrEmpty(BookingId) = False Then
                            If IsNothing(ds) = False AndAlso ds.Tables.Count > 0 AndAlso ds.Tables("BookingHeader").Rows.Count = 1 Then
                                bBookingLock = clsRunCom.BookingVerifyForChange(New Guid(BookingId))
                                'Add column
                                ds.Tables("BookingHeader").Columns.Add(New DataColumn("booking_lock_flag", System.Type.GetType("System.Byte")))
                                ds.Tables("BookingHeader").Rows(0).Item("booking_lock_flag") = Convert.ToByte(bBookingLock)
                            End If
                        End If

                    End If
                End If
            Catch e As Exception
            Finally
            End Try
            Return ds.GetXml()
        End Using
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/InitiateBookingChange"),
WebMethod(Description:="Start store booking info into user session",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function InitiateBookingChange(ByVal BookingId As String) As Boolean

        Try
            If (AgentHeader Is Nothing) Then
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport
                If (AuthenticatePassport(strCode, strPassport)) Then

                    Dim clsRunCom As New clsRunComplus
                    Dim rsHeader As ADODB.Recordset = Session("rsHeader")
                    Dim rsSegments As ADODB.Recordset = Session("rsSegments")
                    Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
                    Dim rsRemarks As ADODB.Recordset = Session("rsRemarks")
                    Dim rsPayments As ADODB.Recordset = Session("rsPayments")
                    Dim rsMappings As ADODB.Recordset = Session("rsMappings")
                    Dim rsServices As ADODB.Recordset = Session("rsServices")
                    Dim rsTaxes As ADODB.Recordset = Session("rsTaxes")
                    Dim rsFees As ADODB.Recordset = Session("rsFees")
                    Dim rsQuotes As ADODB.Recordset = Session("rsQuotes")

                    'Clear Recordset before create new.
                    ClearBookingRecordset(rsHeader, rsSegments, rsPassengers, rsRemarks, rsPayments, rsMappings, rsServices, rsTaxes, rsFees, rsQuotes)

                    'Remove Session
                    Session.Remove("rsHeader")
                    Session.Remove("rsSegments")
                    Session.Remove("rsPassenger")
                    Session.Remove("rsRemarks")
                    Session.Remove("rsPayments")
                    Session.Remove("rsMappings")
                    Session.Remove("rsServices")
                    Session.Remove("rsTaxes")
                    Session.Remove("rsFees")
                    Session.Remove("rsQuotes")

                    rsHeader = New ADODB.Recordset
                    rsSegments = New ADODB.Recordset
                    rsPassengers = New ADODB.Recordset
                    rsRemarks = New ADODB.Recordset
                    rsPayments = New ADODB.Recordset
                    rsMappings = New ADODB.Recordset
                    rsServices = New ADODB.Recordset
                    rsTaxes = New ADODB.Recordset
                    rsFees = New ADODB.Recordset
                    rsQuotes = New ADODB.Recordset

                    Call clsRunCom.GetBooking(BookingId,
                                              rsHeader,
                                              rsSegments,
                                              rsPassengers,
                                              rsRemarks,
                                              rsPayments,
                                              rsMappings,
                                              rsServices,
                                              rsTaxes,
                                              rsFees,
                                              rsQuotes)

                    If (rsHeader IsNot Nothing AndAlso rsHeader.RecordCount > 0) Then
                        'Put recordset state into session.
                        Session("rsHeader") = rsHeader
                        Session("rsSegments") = rsSegments
                        Session("rsPassenger") = rsPassengers
                        Session("rsRemarks") = rsRemarks
                        Session("rsPayments") = rsPayments
                        Session("rsMappings") = rsMappings
                        Session("rsServices") = rsServices
                        Session("rsTaxes") = rsTaxes
                        Session("rsFees") = rsFees
                        Session("rsQuotes") = rsQuotes
                        Return True
                    Else
                        Return False
                    End If

                End If
            End If
        Catch e As Exception
        Finally

        End Try
        Return False
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/InitiateBookingChangeXML"),
WebMethod(Description:="Start store booking info into user session and return xml",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function InitiateBookingChangeXML(ByVal BookingId As String) As String
        Using objOut As New DataSet
            Dim rsHeader As ADODB.Recordset = Nothing
            Dim rsSegments As ADODB.Recordset = Nothing
            Dim rsPassengers As ADODB.Recordset = Nothing
            Dim rsRemarks As ADODB.Recordset = Nothing
            Dim rsPayments As ADODB.Recordset = Nothing
            Dim rsMappings As ADODB.Recordset = Nothing
            Dim rsServices As ADODB.Recordset = Nothing
            Dim rsTaxes As ADODB.Recordset = Nothing
            Dim rsFees As ADODB.Recordset = Nothing
            Dim rsQuotes As ADODB.Recordset = Nothing
            Dim stb As StringBuilder = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport
                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus
                        rsHeader = Session("rsHeader")
                        rsSegments = Session("rsSegments")
                        rsPassengers = Session("rsPassenger")
                        rsRemarks = Session("rsRemarks")
                        rsPayments = Session("rsPayments")
                        rsMappings = Session("rsMappings")
                        rsServices = Session("rsServices")
                        rsTaxes = Session("rsTaxes")
                        rsFees = Session("rsFees")
                        rsQuotes = Session("rsQuotes")

                        'Clear Recordset before create new.
                        ClearBookingRecordset(rsHeader, rsSegments, rsPassengers, rsRemarks, rsPayments, rsMappings, rsServices, rsTaxes, rsFees, rsQuotes)

                        'Remove Session
                        Session.Remove("rsHeader")
                        Session.Remove("rsSegments")
                        Session.Remove("rsPassenger")
                        Session.Remove("rsRemarks")
                        Session.Remove("rsPayments")
                        Session.Remove("rsMappings")
                        Session.Remove("rsServices")
                        Session.Remove("rsTaxes")
                        Session.Remove("rsFees")
                        Session.Remove("rsQuotes")

                        rsHeader = New ADODB.Recordset
                        rsSegments = New ADODB.Recordset
                        rsPassengers = New ADODB.Recordset
                        rsRemarks = New ADODB.Recordset
                        rsPayments = New ADODB.Recordset
                        rsMappings = New ADODB.Recordset
                        rsServices = New ADODB.Recordset
                        rsTaxes = New ADODB.Recordset
                        rsFees = New ADODB.Recordset
                        rsQuotes = New ADODB.Recordset

                        Call clsRunCom.GetBooking(BookingId,
                                                  rsHeader,
                                                  rsSegments,
                                                  rsPassengers,
                                                  rsRemarks,
                                                  rsPayments,
                                                  rsMappings,
                                                  rsServices,
                                                  rsTaxes,
                                                  rsFees,
                                                  rsQuotes)

                        If (rsHeader IsNot Nothing AndAlso rsHeader.RecordCount > 0) Then
                            'Put recordset state into session.
                            Session("rsHeader") = rsHeader
                            Session("rsSegments") = rsSegments
                            Session("rsPassenger") = rsPassengers
                            Session("rsRemarks") = rsRemarks
                            Session("rsPayments") = rsPayments
                            Session("rsMappings") = rsMappings
                            Session("rsServices") = rsServices
                            Session("rsTaxes") = rsTaxes
                            Session("rsFees") = rsFees
                            Session("rsQuotes") = rsQuotes

                            'Create Availability XML.

                            CopyRecordsetsToDataset(rsHeader,
                                                        rsSegments,
                                                        rsPassengers,
                                                        rsRemarks,
                                                        rsPayments,
                                                        rsMappings,
                                                        rsTaxes,
                                                        rsQuotes,
                                                        rsFees,
                                                        rsServices,
                                                        objOut)

                            rsHeader.Filter = 0
                            rsSegments.Filter = 0
                            rsPassengers.Filter = 0
                            rsRemarks.Filter = 0
                            rsPayments.Filter = 0
                            rsMappings.Filter = 0
                            rsServices.Filter = 0
                            rsTaxes.Filter = 0
                            rsFees.Filter = 0
                            rsQuotes.Filter = 0

                        End If

                    End If
                End If
            Catch e As Exception
                Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(e, BookingId)
            Finally

            End Try

            Return objOut.GetXml()

        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetActiveBookings"),
WebMethod(Description:="Get Client Bookings",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetActiveBookings(ByVal strClientProfileId As String) As String
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetActiveBookings(strClientProfileId)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "ClientBookings", "ClientBooking", objOut)
                    End If
                End If
            Catch e As Exception

            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut.GetXml()
        End Using
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetFlownBookings"),
WebMethod(Description:="Get Client Bookings",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetFlownBookings(ByVal strClientProfileId As String) As String
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then

                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim clsRunCom As New clsRunComplus

                        rsA = clsRunCom.GetFlownBookings(strClientProfileId)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "ClientBookings", "ClientBooking", objOut)
                    End If
                End If
            Catch e As Exception

            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut.GetXml
        End Using
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetBookingHistorysList"),
WebMethod(Description:="Get Client Bookings",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetBookingHistorysList(ByVal strClientProfileId As String) As String
        Dim rsFlown As ADODB.Recordset = Nothing
        Dim rsActive As ADODB.Recordset = Nothing
        Dim stb As New StringBuilder
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus

                    rsActive = clsRunCom.GetActiveBookings(strClientProfileId)
                    rsFlown = clsRunCom.GetFlownBookings(strClientProfileId)
                    clsRunCom = Nothing

                    'Create Availability XML.
                    Using stw As New StringWriter(stb)
                        Using xtw As XmlWriter = XmlWriter.Create(stw)
                            xtw.WriteStartElement("ClientBookings")

                            'Start Outward******************************************************
                            xtw.WriteStartElement("BookingActive")
                            If IsNothing(rsActive) = False AndAlso rsActive.RecordCount > 0 Then
                                CreateBookingHistoryXml(xtw, rsActive)
                            End If
                            xtw.WriteEndElement() 'BookingActive

                            'Start Return******************************************************
                            xtw.WriteStartElement("BookingFlown")
                            If IsNothing(rsFlown) = False AndAlso rsFlown.RecordCount > 0 Then
                                CreateBookingHistoryXml(xtw, rsFlown)
                            End If
                            xtw.WriteEndElement() 'BookingFlown

                            xtw.WriteEndElement() 'ClientBookings
                        End Using

                    End Using
                End If
            End If
        Catch e As Exception

        Finally
            Helper.Check.ClearRecordset(rsActive)
            Helper.Check.ClearRecordset(rsFlown)
        End Try
        If IsNothing(stb) = False Then
            If stb.Length > 0 Then
                Return Helper.DataTransform.CompressString(stb.ToString)
            Else
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/UpdatePassengerCheckinDetails"),
WebMethod(Description:="Update passenger checkin detail",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function UpdatePassengerCheckinDetails(ByVal passengers As Passengers) As Integer
        Dim iCount As Integer = 0
        Dim gUserId As Guid
        Try
            If (AgentHeader Is Nothing) Then
            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus

                    'Find user update information.
                    If Not Session("CreateById") Is Nothing AndAlso Not Session("CreateById") = String.Empty Then
                        gUserId = New Guid(Session("CreateById").ToString())
                    Else
                        If IsGuid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")) Then
                            Session("CreateById") = System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")
                        End If
                        gUserId = New Guid(Session("CreateById").ToString())
                    End If

                    If (IsNothing(passengers) = False AndAlso passengers.Count > 0) Then
                        For i As Integer = 0 To passengers.Count - 1
                            If (clsRunCom.UpdatePassengerCheckinDetails(passengers(i).passenger_id.ToString(),
                                                                        gUserId,
                                                                        passengers(i).gender_type_rcd,
                                                                        passengers(i).date_of_birth,
                                                                        passengers(i).nationality_rcd,
                                                                        passengers(i).passport_issue_country_rcd,
                                                                        passengers(i).passport_expiry_date,
                                                                        passengers(i).passport_number,
                                                                        passengers(i).document_type_rcd) = True) Then
                                iCount = iCount + 1
                            End If
                        Next
                    End If
                    clsRunCom = Nothing
                End If
            End If
        Catch e As Exception
            Dim path As String = HttpContext.Current.Server.MapPath("~") + "\Log\" + String.Format("{0:yyyyMMdd}", DateTime.Now) + ".log"
            SaveLog("Function Name : UpdatePassengerCheckinDetails : " + Environment.NewLine + e.Message, path)
        End Try
        Return iCount
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetBookingClasses"),
WebMethod(Description:="Return Booking Classes",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetBookingClasses(ByVal strBoardingclass As String, ByVal strLanguage As String) As DataSet

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport

                    If (AuthenticatePassport(strCode, strPassport)) Then

                        Dim clsRunCom As New clsRunComplus
                        rsA = clsRunCom.GetBookingClasses(strBoardingclass, strLanguage)
                        clsRunCom = Nothing
                        Call InputRecordsetToDataset(rsA, "BookingClasses", objOut)
                    End If
                End If
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetFlightsFLIFO"),
WebMethod(Description:="Return Flight Schedule",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetFlightsFLIFO(ByVal OriginCode As String,
                                        ByVal DestinationCode As String,
                                        ByVal AirlineCode As String,
                                        ByVal FlightNumber As String,
                                        ByVal dtFlightFrom As Date,
                                        ByVal dtFlightTo As Date,
                                        ByVal strLanguage As String,
                                        ByVal strToken As String) As String

        Using objOut As New DataSet
            Dim strXML As String = ""
            Try
                If (String.IsNullOrEmpty(strToken)) Then
                    Return ""
                Else
                    If IsTokenValid(strToken) = True Then
                        Dim clsRunCom As New clsRunComplus
                        strXML = clsRunCom.GetFlightsFLIFO(OriginCode, DestinationCode, AirlineCode, FlightNumber, dtFlightFrom, dtFlightTo, strLanguage)
                        clsRunCom = Nothing
                    End If
                End If
            Catch e As Exception
                Throw e
            End Try

            Return strXML
        End Using
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/LowFareFinderAllow"),
WebMethod(Description:="Check if low fare finder is allowed to search",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function LowFareFinderAllow(ByVal AgencyCode As String, ByVal strToken As String) As Boolean
        Dim bResult As Boolean
        Try
            If (String.IsNullOrEmpty(strToken)) Then
                Return Nothing
            Else
                If IsTokenValid(strToken) = True Then

                    Dim clsRunCom As New clsRunComplus

                    bResult = clsRunCom.LowFareFinderAllow(AgencyCode)

                    clsRunCom = Nothing
                End If
            End If
        Catch e As Exception
        End Try

        Return bResult
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/BoardingClassRead"),
    WebMethod(Description:="Return Boarding class",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function BoardingClassRead(ByVal strBoardingClassCode As String,
                                      ByVal strBoardingClass As String,
                                      ByVal strSortSeq As String,
                                      ByVal strStatus As String,
                                      ByVal bWrite As Boolean,
                                      ByVal strToken As String) As String
        Try
            If (String.IsNullOrEmpty(strToken)) Then
                Throw New Exception("Invalid token")
            Else
                If IsTokenValid(strToken) = True Then
                    Using objOut As New DataSet
                        Dim rsA As ADODB.Recordset = Nothing

                        Try
                            Dim clsRunCom As New clsRunComplus
                            rsA = clsRunCom.BoardingClassRead(strBoardingClassCode, strBoardingClass, strSortSeq, strStatus, bWrite)
                            clsRunCom = Nothing

                            objOut.DataSetName = "BoardingClasses"
                            Call InputRecordsetToDataset(rsA, "BoardingClass", objOut)
                        Catch e As Exception
                            Throw e
                        Finally
                            Helper.Check.ClearRecordset(rsA)
                        End Try

                        Return objOut.GetXml()
                    End Using
                End If
            End If
        Catch e As Exception

        End Try
        Return Nothing
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/RemarkAdd"),
    WebMethod(Description:="Add remark to booking.",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function RemarkAdd(ByVal strRemarkType As String,
                              ByVal strBookingRemarkId As String,
                              ByVal strBookingId As String,
                              ByVal strClientProfileId As String,
                              ByVal strNickname As String,
                              ByVal strRemarkText As String,
                              ByVal strAgencyCode As String,
                              ByVal strAddedBy As String,
                              ByVal strUserId As String,
                              ByVal bProtected As Boolean,
                              ByVal bWarning As Boolean,
                              ByVal bProcessMessage As Boolean,
                              ByVal bSystemRemark As Boolean,
                              ByVal timelimit As Date,
                              ByVal timelimitUTC As Date) As String

        Dim result As String = String.Empty
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus

                    result = clsRunCom.RemarkAdd(strRemarkType,
                                                        strBookingRemarkId,
                                                        strBookingId,
                                                        strClientProfileId,
                                                        strNickname,
                                                        strRemarkText,
                                                        strAgencyCode,
                                                        strAddedBy,
                                                        strUserId,
                                                        bProtected,
                                                        bWarning,
                                                        bProcessMessage,
                                                        bSystemRemark,
                                                        timelimit,
                                                        timelimitUTC)
                    clsRunCom = Nothing
                End If
            End If
        Catch e As Exception
            Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(e, strRemarkType & "," &
                                                        strBookingRemarkId & "," &
                                                        strBookingId & "," &
                                                        strClientProfileId & "," &
                                                        strNickname & "," &
                                                        strRemarkText & "," &
                                                        strAgencyCode & "," &
                                                        strAddedBy & "," &
                                                        strUserId & "," &
                                                        bProtected & "," &
                                                        bWarning & "," &
                                                        bProcessMessage & "," &
                                                        bSystemRemark & "," &
                                                        timelimit & "," &
                                                        timelimitUTC)
        End Try
        Return result

    End Function
#End Region

#Region "Old Engine availity"
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetAvailability"),
    WebMethod(Description:="Return Airport Destinations",
    EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetAvailability(ByVal FirstSegment As Boolean,
                                            ByRef bookingId As String,
                                            ByRef Origin As String,
                                            ByRef Destination As String,
                                            ByRef DateFrom As Date,
                                            ByRef DateTo As Date,
                                            ByRef DateBooking As Date,
                                            ByRef Adult As Short,
                                            ByRef Child As Short,
                                            ByRef Infant As Short,
                                            ByRef Other As Short,
                                            ByRef OtherPassengerType As String,
                                            ByRef BoardingClass As String,
                                            ByRef BookingClass As String,
                                            ByRef DayTimeIndicator As String,
                                            ByRef AgencyCode As String,
                                            ByRef CurrencyCode As String,
                                            ByRef FlightId As String,
                                            ByRef FareId As String,
                                            ByRef MaxAmount As Double,
                                            ByRef NonStopOnly As Boolean,
                                            ByRef IncludeDeparted As Boolean,
                                            ByRef IncludeCancelled As Boolean,
                                            ByRef IncludeWaitlisted As Boolean,
                                            ByRef IncludeSoldOut As Boolean,
                                            ByRef Refundable As Boolean,
                                            ByRef GroupFares As Boolean,
                                            ByRef ItFaresOnly As Boolean,
                                            ByRef PromotionCode As String,
                                            ByRef FareType As String
                                            ) As DataSet
        Dim ds As New DataSet

        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    If FirstSegment Then
                        Session("BookingState") = Nothing
                    End If
                    Dim rsA As ADODB.Recordset = Nothing
                    Dim clsRunCom As New clsRunComplus

                    If bookingId = String.Empty Then
                        Dim Serialization As New Helper.Serialization
                        Dim BookingState As New Booking
                        Dim rsHeader As ADODB.Recordset = Nothing
                        Call clsRunCom.GetEmpty(AgentHeader.AgencyCode, AgentHeader.AgencyCurrencyRcd, rsHeader)
                        If rsHeader.RecordCount > 0 Then
                            bookingId = rsHeader("booking_id").Value.ToString
                        Else
                            bookingId = Guid.NewGuid().ToString
                        End If
                        Session("rsHeader") = rsHeader
                    End If
                    'Debug Code
                    'Mail.Send("FareShopperStartGetAvailability: " & Date.Now.Hour & ":" & Date.Now.Minute & ":" & Date.Now.Second & ":" & Date.Now.Millisecond, "FareShopperStart 3 WebService 1", "Local." & System.Configuration.ConfigurationSettings.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationSettings.AppSettings("ErrorSenderEmail"), Nothing)
                    Dim DebugString As String
                    DebugString = " bookingId: " & bookingId & vbCrLf &
                                    " FirstSegment " & FirstSegment.ToString & vbCrLf &
                                    " Refundable" & Refundable & vbCrLf

                    rsA = clsRunCom.GetAvailability(Origin,
                                                    Destination,
                                                    DateFrom,
                                                    DateTo,
, ,
                                                    DateBooking,
                                                    Adult,
                                                    Child,
                                                    Infant,
                                                    Other,
                                                    OtherPassengerType,
                                                    BoardingClass,
                                                    BookingClass,
                                                    DayTimeIndicator,
                                                    AgencyCode,
                                                    CurrencyCode,
                                                    FlightId,
                                                    FareId,
                                                    MaxAmount,
                                                    NonStopOnly,
                                                    IncludeDeparted,
                                                    IncludeCancelled,
                                                    IncludeWaitlisted,
                                                    IncludeSoldOut,
                                                    Refundable,
                                                    GroupFares,
                                                    ItFaresOnly,
                                                    Nothing,
                                                    PromotionCode,
                                                    FareType)

                    'Debug Code
                    'Mail.Send("AVL: " & Date.Now.Hour & ":" & Date.Now.Minute & ":" & Date.Now.Second & ":" & DebugString, "AVL", "Local." & System.Configuration.ConfigurationSettings.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationSettings.AppSettings("ErrorSenderEmail"), Nothing)

                    'Debug Code
                    'Mail.Send("FareShopperParsingStartCopyRecodset: " & Date.Now.Hour & ":" & Date.Now.Minute & ":" & Date.Now.Second & ":" & Date.Now.Millisecond & " Found:" & rsA.RecordCount.ToString, "FareShopperStart 3 WebService 2", "Local." & System.Configuration.ConfigurationSettings.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationSettings.AppSettings("ErrorSenderEmail"), Nothing)


                    Dim rsC As ADODB.Recordset = Nothing
                    Dim r As New FabricateRecordset
                    r.SerializeAdoRecordsets(rsA, rsC)

                    'Debug Code
                    'Mail.Send("FareShopperParsingStartParsingRecodset: " & Date.Now.Hour & ":" & Date.Now.Minute & ":" & Date.Now.Second & ":" & Date.Now.Millisecond, "FareShopperStart 3 WebService 3", "Local." & System.Configuration.ConfigurationSettings.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationSettings.AppSettings("ErrorSenderEmail"), Nothing)


                    If FirstSegment Then
                        Session("AvailabilitOutBound") = rsA
                    Else
                        Session("AvailabilitInBound") = rsA
                    End If

                    clsRunCom = Nothing
                    Call InputRecordsetToDataset(rsC, "Booking", "AvailabilityFlight", ds)
                End If
            End If
        Catch e As Exception

        End Try
        'Debug Code
        'Mail.Send("FareShopperEndGetAvailability: " & Date.Now.Hour & ":" & Date.Now.Minute & ":" & Date.Now.Second & ":" & Date.Now.Millisecond, "FareShopperStart 3 WebService 4", "Local." & System.Configuration.ConfigurationSettings.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationSettings.AppSettings("ErrorSenderEmail"), Nothing)
        Return ds
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetAvailabilityWithFareLogic"),
WebMethod(Description:="Return Airport Destinations",
EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetAvailabilityWithFareLogic(ByRef FlightOutboundXml As String,
                                                ByRef FlightReturnXml As String,
                                                ByRef Origin As String,
                                                ByRef Destination As String,
                                                ByVal OutboundDate As Date,
                                                ByRef ReturnDate As Date,
                                                ByVal iAdult As Integer,
                                                ByVal iChild As Integer,
                                                ByVal iInfant As Integer,
                                                ByVal iOther As Integer) As Boolean
        Dim ds1 As New DataSet
        Dim ds2 As New DataSet

        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus
                    Dim Serialization As New Helper.Serialization
                    Dim FabricateRecordset As New FabricateRecordset

                    Dim FlightSegmentOutward As New AvailabilityFlights
                    Dim FlightSegmentReturn As New AvailabilityFlights
                    Dim ReturnFlight As Boolean = False
                    Dim bReturn As Boolean = False

                    Dim rsOutbound As ADODB.Recordset = Session("AvailabilitOutBound")

                    Dim rsReturn As ADODB.Recordset = IIf(FlightReturnXml = String.Empty, Nothing, Session("AvailabilitINBound"))
                    If FlightReturnXml.Length > 0 Then
                        bReturn = True
                    End If

                    clsRunCom.FilterAvailability(rsOutbound, iAdult, iChild, iInfant, iOther, rsReturn, , Origin, Destination, OutboundDate, ReturnDate, , bReturn, False, False, False, True, True, True)

                    Call InputRecordsetToDataset(rsOutbound, "Booking", "AvailabilityFlight", ds1)
                    FlightOutboundXml = ds1.GetXml
                    Call InputRecordsetToDataset(rsReturn, "Booking", "AvailabilityFlight", ds2)
                    FlightReturnXml = ds2.GetXml
                End If
            End If
        Catch e As Exception
            Return False
        End Try
    End Function
#End Region
#Region "Helper"
    Private Function GetAvailabilityFlight(ByVal Origin As String,
                                            ByVal Destination As String,
                                            ByVal DateDepartFrom As Date,
                                            ByVal DateDepartTo As Date,
                                            ByVal DateReturnFrom As Date,
                                            ByVal DateReturnTo As Date,
                                            ByVal DateBooking As Date,
                                            ByVal Adult As Short,
                                            ByVal Child As Short,
                                            ByVal Infant As Short,
                                            ByVal Other As Short,
                                            ByVal OtherPassengerType As String,
                                            ByVal BoardingClass As String,
                                            ByVal BookingClass As String,
                                            ByVal DayTimeIndicator As String,
                                            ByVal AgencyCode As String,
                                            ByVal CurrencyCode As String,
                                            ByVal FlightId As String,
                                            ByVal FareId As String,
                                            ByVal MaxAmount As Double,
                                            ByVal NonStopOnly As Boolean,
                                            ByVal IncludeDeparted As Boolean,
                                            ByVal IncludeCancelled As Boolean,
                                            ByVal IncludeWaitlisted As Boolean,
                                            ByVal IncludeSoldOut As Boolean,
                                            ByVal Refundable As Boolean,
                                            ByVal GroupFares As Boolean,
                                            ByVal ItFaresOnly As Boolean,
                                            ByVal PromotionCode As String,
                                            ByVal FareType As String,
                                            ByVal ReturnFlight As Boolean,
                                            ByVal bLowFareFinder As Boolean,
                                            ByVal bLowest As Boolean,
                                            ByVal bLowestClass As Boolean,
                                            ByVal bLowestGroup As Boolean,
                                            ByVal bShowClosed As Boolean,
                                            ByVal bSort As Boolean,
                                            ByVal bDelete As Boolean,
                                            ByVal bSkipFilterFareLogic As Boolean,
                                            ByVal strLanguage As String,
                                            ByVal strIpAddress As String,
                                            ByVal bReturnRefundable As Boolean,
                                            ByVal bNoVat As Boolean,
                                            ByVal iDayRange As Integer) As String

        Dim rsOutward As ADODB.Recordset = Nothing
        Dim rsReturn As ADODB.Recordset = Nothing
        Dim stb As StringBuilder = Nothing

        Try
            Session("BookingState") = Nothing

            Dim clsRunCom As New clsRunComplus

            'Get Outward Flight
            rsOutward = clsRunCom.GetAvailability(Origin,
                                                  Destination,
                                                  DateDepartFrom,
                                                  DateDepartTo,
                                                  DateReturnFrom,
                                                  DateReturnTo,
                                                  DateBooking,
                                                  Adult,
                                                  Child,
                                                  Infant,
                                                  Other,
                                                  OtherPassengerType,
                                                  BoardingClass,
                                                  BookingClass,
                                                  DayTimeIndicator,
                                                  AgencyCode,
                                                  CurrencyCode,
                                                  FlightId,
                                                  FareId,
                                                  MaxAmount,
                                                  NonStopOnly,
                                                  IncludeDeparted,
                                                  IncludeCancelled,
                                                  IncludeWaitlisted,
                                                  IncludeSoldOut,
                                                  Refundable,
                                                  GroupFares,
                                                  ItFaresOnly,
                                                  rsReturn,
                                                  PromotionCode,
                                                  FareType,
                                                  strLanguage,
                                                  strIpAddress,
                                                  False,
                                                  bNoVat)

            If iDayRange <> -1 Then
                If DateDepartFrom.Equals(DateDepartTo) = False Then
                    DateDepartFrom = DateDepartFrom.AddDays(iDayRange)
                End If

                If ReturnFlight = True And DateReturnFrom.Equals(DateReturnTo) = False Then
                    DateReturnFrom = DateReturnFrom.AddDays(iDayRange)
                End If
            End If

            'Applied fare logic.
            clsRunCom.FilterAvailability(rsOutward,
                                         Adult,
                                         Child,
                                         Infant,
                                         Other,
                                         rsReturn, ,
                                         Origin,
                                         Destination,
                                         DateDepartFrom,
                                         DateReturnFrom, ,
                                         ReturnFlight,
                                         bLowest,
                                         bLowestClass,
                                         bLowestGroup,
                                         bShowClosed,
                                         bSort,
                                         bDelete,
                                         bSkipFilterFareLogic,
                                         bReturnRefundable)

            'Create Availability XML.
            stb = New StringBuilder
            Using stw As New System.IO.StringWriter(stb)
                Dim writerSettings As New System.Xml.XmlWriterSettings()
                writerSettings.OmitXmlDeclaration = False

                Using xtw As System.Xml.XmlWriter = System.Xml.XmlWriter.Create(stw, writerSettings)
                    'Get Array of Field.
                    'Get Array of data.
                    Dim arrOutBound As Array = Nothing
                    Dim arrReturn As Array = Nothing

                    xtw.WriteStartElement("Availability")
                    If True Then
                        Dim srrFieldList As System.Collections.Specialized.StringDictionary = GetAvailabilityColumn(rsOutward)
                        If srrFieldList IsNot Nothing Then
                            'Start Outward******************************************************
                            xtw.WriteStartElement("AvailabilityOutbound")
                            If (rsOutward Is Nothing) = False AndAlso rsOutward.RecordCount > 0 Then
                                'Get Field Array
                                arrOutBound = DirectCast(rsOutward.GetRows(), Array)
                                CreateAvailabilityXml(xtw, srrFieldList, arrOutBound)
                            End If
                            xtw.WriteEndElement()
                            'AvailabilityOutbound

                            'Start Return******************************************************
                            xtw.WriteStartElement("AvailabilityReturn")
                            If (rsReturn Is Nothing) = False AndAlso rsReturn.RecordCount > 0 Then
                                arrReturn = DirectCast(rsReturn.GetRows(), Array)
                                CreateAvailabilityXml(xtw, srrFieldList, arrReturn)
                            End If
                            'AvailabilityReturn
                            xtw.WriteEndElement()
                        End If
                    End If
                    'Availability
                    xtw.WriteEndElement()
                End Using
            End Using
        Catch e As Exception
            Avantik.Web.Service.Helpers.Logger.Instance(Avantik.Web.Service.Helpers.Logger.LogType.Mail).WriteLog(e, "Origin : " & Origin & "<br />" &
                                                                                                                    "Destination : " & Destination & "<br />" &
                                                                                                                    "DateDepartFrom : " & DateDepartFrom & "<br />" &
                                                                                                                    "DateDepartTo : " & DateDepartTo & "<br />" &
                                                                                                                    "DateReturnFrom : " & DateReturnFrom & "<br />" &
                                                                                                                    "DateReturnTo : " & DateReturnTo & "<br />" &
                                                                                                                    "DateBooking : " & DateBooking & "<br />" &
                                                                                                                    "Adult : " & Adult & "<br />" &
                                                                                                                    "Child : " & Child & "<br />" &
                                                                                                                    "Infant : " & Infant & "<br />" &
                                                                                                                    "Other : " & Other & "<br />" &
                                                                                                                    "OtherPassengerType : " & OtherPassengerType & "<br />" &
                                                                                                                    "BoardingClass : " & BoardingClass & "<br />" &
                                                                                                                    "BookingClass : " & BookingClass & "<br />" &
                                                                                                                    "DayTimeIndicator : " & DayTimeIndicator & "<br />" &
                                                                                                                    "AgencyCode : " & AgencyCode & "<br />" &
                                                                                                                    "CurrencyCode : " & CurrencyCode & "<br />" &
                                                                                                                    "FlightId : " & FlightId & "<br />" &
                                                                                                                    "FareId : " & FareId & "<br />" &
                                                                                                                    "MaxAmount : " & MaxAmount & "<br />" &
                                                                                                                    "NonStopOnly : " & NonStopOnly & "<br />" &
                                                                                                                    "IncludeDeparted : " & IncludeDeparted & "<br />" &
                                                                                                                    "IncludeCancelled : " & IncludeCancelled & "<br />" &
                                                                                                                    "IncludeWaitlisted : " & IncludeWaitlisted & "<br />" &
                                                                                                                    "IncludeSoldOut : " & IncludeSoldOut & "<br />" &
                                                                                                                    "Refundable : " & Refundable & "<br />" &
                                                                                                                    "GroupFares : " & GroupFares & "<br />" &
                                                                                                                    "ItFaresOnly : " & ItFaresOnly & "<br />" &
                                                                                                                    "PromotionCode : " & PromotionCode & "<br />" &
                                                                                                                    "FareType : " & FareType & "<br />" &
                                                                                                                    "ReturnFlight : " & ReturnFlight & "<br />" &
                                                                                                                    "bLowFareFinder : " & bLowFareFinder & "<br />" &
                                                                                                                    "bLowest : " & bLowest & "<br />" &
                                                                                                                    "bLowestClass : " & bLowestClass & "<br />" &
                                                                                                                    "bLowestGroup : " & bLowestGroup & "<br />" &
                                                                                                                    "bShowClosed : " & bShowClosed & "<br />" &
                                                                                                                    "bSort : " & bSort & "<br />" &
                                                                                                                    "bDelete : " & bDelete & "<br />" &
                                                                                                                    "bSkipFilterFareLogic : " & bSkipFilterFareLogic & "<br />" &
                                                                                                                    "strLanguage : " & strLanguage & "<br />" &
                                                                                                                    "strIpAddress : " & strIpAddress & "<br />" &
                                                                                                                    "bReturnRefundable : " & bReturnRefundable & "<br />" &
                                                                                                                    "bNoVat : " & bNoVat & "<br />" &
                                                                                                                    "iDayRange : " & iDayRange & "<br />")

        Finally
            Helper.Check.ClearRecordset(rsOutward)
            Helper.Check.ClearRecordset(rsReturn)
        End Try

        If IsNothing(stb) = False Then
            If stb.Length > 0 Then
                Return Helper.DataTransform.CompressString(stb.ToString)
            Else
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function
#Region "Availability helper"

    Private Function GetAvailabilityColumn(rs As ADODB.Recordset) As System.Collections.Specialized.StringDictionary
        Dim std As System.Collections.Specialized.StringDictionary = Nothing
        If rs IsNot Nothing AndAlso rs.Fields.Count > 0 Then
            std = New System.Collections.Specialized.StringDictionary()

            For i As Integer = 0 To rs.Fields.Count - 1
                Select Case rs.Fields(i).Name
                    Case "airline_rcd"
                        std.Add(i.ToString(), "airline_rcd")
                        Exit Select
                    Case "airline_name"
                        std.Add(i.ToString(), "airline_name")
                        Exit Select
                    Case "flight_number"
                        std.Add(i.ToString(), "flight_number")
                        Exit Select
                    Case "operating_airline_rcd"
                        std.Add(i.ToString(), "operating_airline_rcd")
                        Exit Select
                    Case "operating_airline_name"
                        std.Add(i.ToString(), "operating_airline_name")
                        Exit Select
                    Case "operating_flight_number"
                        std.Add(i.ToString(), "operating_flight_number")
                        Exit Select
                    Case "booking_class_rcd"
                        std.Add(i.ToString(), "booking_class_rcd")
                        Exit Select
                    Case "boarding_class_rcd"
                        std.Add(i.ToString(), "boarding_class_rcd")
                        Exit Select
                    Case "flight_id"
                        std.Add(i.ToString(), "flight_id")
                        Exit Select
                    Case "origin_rcd"
                        std.Add(i.ToString(), "origin_rcd")
                        Exit Select
                    Case "destination_rcd"
                        std.Add(i.ToString(), "destination_rcd")
                        Exit Select
                    Case "origin_name"
                        std.Add(i.ToString(), "origin_name")
                        Exit Select
                    Case "destination_name"
                        std.Add(i.ToString(), "destination_name")
                        Exit Select
                    Case "departure_date"
                        std.Add(i.ToString(), "departure_date")
                        Exit Select
                    Case "arrival_date"
                        std.Add(i.ToString(), "arrival_date")
                        Exit Select
                    Case "planned_departure_time"
                        std.Add(i.ToString(), "planned_departure_time")
                        Exit Select
                    Case "planned_arrival_time"
                        std.Add(i.ToString(), "planned_arrival_time")
                        Exit Select
                    Case "fare_id"
                        std.Add(i.ToString(), "fare_id")
                        Exit Select
                    Case "fare_code"
                        std.Add(i.ToString(), "fare_code")
                        Exit Select
                    Case "transit_points"
                        std.Add(i.ToString(), "transit_points")
                        Exit Select
                    Case "transit_points_name"
                        std.Add(i.ToString(), "transit_points_name")
                        Exit Select
                    Case "transit_flight_id"
                        std.Add(i.ToString(), "transit_flight_id")
                        Exit Select
                    Case "transit_booking_class_rcd"
                        std.Add(i.ToString(), "transit_booking_class_rcd")
                        Exit Select
                    Case "transit_boarding_class_rcd"
                        std.Add(i.ToString(), "transit_boarding_class_rcd")
                        Exit Select
                    Case "transit_airport_rcd"
                        std.Add(i.ToString(), "transit_airport_rcd")
                        Exit Select
                    Case "transit_departure_date"
                        std.Add(i.ToString(), "transit_departure_date")
                        Exit Select
                    Case "transit_arrival_date"
                        std.Add(i.ToString(), "transit_arrival_date")
                        Exit Select
                    Case "transit_planned_departure_time"
                        std.Add(i.ToString(), "transit_planned_departure_time")
                        Exit Select
                    Case "transit_planned_arrival_time"
                        std.Add(i.ToString(), "transit_planned_arrival_time")
                        Exit Select
                    Case "transit_fare_id"
                        std.Add(i.ToString(), "transit_fare_id")
                        Exit Select
                    Case "transit_name"
                        std.Add(i.ToString(), "transit_name")
                        Exit Select
                    Case "nesting_string"
                        std.Add(i.ToString(), "nesting_string")
                        Exit Select
                    Case "full_flight_flag"
                        std.Add(i.ToString(), "full_flight_flag")
                        Exit Select
                    Case "class_open_flag"
                        std.Add(i.ToString(), "class_open_flag")
                        Exit Select
                    Case "close_web_sales"
                        std.Add(i.ToString(), "close_web_sales")
                        Exit Select
                    Case "waitlist_open_flag"
                        std.Add(i.ToString(), "waitlist_open_flag")
                        Exit Select
                    Case "adult_fare"
                        std.Add(i.ToString(), "adult_fare")
                        Exit Select
                    Case "child_fare"
                        std.Add(i.ToString(), "child_fare")
                        Exit Select
                    Case "infant_fare"
                        std.Add(i.ToString(), "infant_fare")
                        Exit Select
                    Case "other_fare"
                        std.Add(i.ToString(), "other_fare")
                        Exit Select
                    Case "total_adult_fare"
                        std.Add(i.ToString(), "total_adult_fare")
                        Exit Select
                    Case "total_child_fare"
                        std.Add(i.ToString(), "total_child_fare")
                        Exit Select
                    Case "total_infant_fare"
                        std.Add(i.ToString(), "total_infant_fare")
                        Exit Select
                    Case "total_other_fare"
                        std.Add(i.ToString(), "total_other_fare")
                        Exit Select
                    Case "fare_column"
                        std.Add(i.ToString(), "fare_column")
                        Exit Select
                    Case "flight_comment"
                        std.Add(i.ToString(), "flight_comment")
                        Exit Select
                    Case "transit_flight_comment"
                        std.Add(i.ToString(), "transit_flight_comment")
                        Exit Select
                    Case "filter_logic_flag"
                        std.Add(i.ToString(), "filter_logic_flag")
                        Exit Select
                    Case "restriction_text"
                        std.Add(i.ToString(), "restriction_text")
                        Exit Select
                    Case "endorsement_text"
                        std.Add(i.ToString(), "endorsement_text")
                        Exit Select
                    Case "fare_type_rcd"
                        std.Add(i.ToString(), "fare_type_rcd")
                        Exit Select
                    Case "redemption_points"
                        std.Add(i.ToString(), "redemption_points")
                        Exit Select
                    Case "transit_redemption_points"
                        std.Add(i.ToString(), "transit_redemption_points")
                        Exit Select
                    Case "flight_duration"
                        std.Add(i.ToString(), "flight_duration")
                        Exit Select
                    Case "promotion_code"
                        std.Add(i.ToString(), "promotion_code")
                        Exit Select
                    Case "nested_book_available"
                        std.Add(i.ToString(), "nested_book_available")
                        Exit Select
                    Case "transit_airline_rcd"
                        std.Add(i.ToString(), "transit_airline_rcd")
                        Exit Select
                    Case "transit_flight_number"
                        std.Add(i.ToString(), "transit_flight_number")
                        Exit Select
                    Case "transit_flight_status_rcd"
                        std.Add(i.ToString(), "transit_flight_status_rcd")
                        Exit Select
                    Case "transit_flight_duration"
                        std.Add(i.ToString(), "transit_flight_duration")
                        Exit Select
                    Case "transit_aircraft_type_rcd"
                        std.Add(i.ToString(), "transit_aircraft_type_rcd")
                        Exit Select
                    Case "transit_nested_book_available"
                        std.Add(i.ToString(), "transit_nested_book_available")
                        Exit Select
                    Case "transit_waitlist_open_flag"
                        std.Add(i.ToString(), "transit_waitlist_open_flag")
                        Exit Select
                    Case "transit_adult_fare"
                        std.Add(i.ToString(), "transit_adult_fare")
                        Exit Select
                    Case "transit_class_open_flag"
                        std.Add(i.ToString(), "transit_class_open_flag")
                        Exit Select
                    Case "flight_information_1"
                        std.Add(i.ToString(), "flight_information_1")
                        Exit Select
                    Case "corporate_fare_flag"
                        std.Add(i.ToString(), "corporate_fare_flag")
                        Exit Select
                    Case "refundable_flag"
                        std.Add(i.ToString(), "refundable_flag")
                        Exit Select
                    Case "transit_flight_duration"
                        std.Add(i.ToString(), "transit_flight_duration")
                        Exit Select
                    Case "aircraft_type_rcd"
                        std.Add(i.ToString(), "aircraft_type_rcd")
                        Exit Select
                End Select
            Next

            Return std
        End If
        Return Nothing
    End Function

    Private Sub CreateAvailabilityXml(xtw As System.Xml.XmlWriter, stdFieldList As System.Collections.Specialized.StringDictionary, arrField As Array)
        If stdFieldList IsNot Nothing AndAlso stdFieldList.Count > 0 AndAlso arrField IsNot Nothing AndAlso stdFieldList.Count > 0 Then
            'Construct the availability xml and get the lowest fare.
            Dim iNumberOfField As Integer = arrField.GetUpperBound(0)
            Dim iNumberOfRows As Integer = arrField.GetUpperBound(1)
            Dim strFieldName As String = String.Empty

            Try
                For i As Integer = 0 To iNumberOfRows
                    xtw.WriteStartElement("AvailabilityFlight")
                    If True Then
                        For j As Integer = 0 To iNumberOfField
                            strFieldName = stdFieldList(j.ToString())
                            If String.IsNullOrEmpty(strFieldName) = False Then
                                xtw.WriteStartElement(strFieldName)
                                If (TypeOf arrField.GetValue(j, i) Is DBNull) = False Then
                                    xtw.WriteValue(arrField.GetValue(j, i))
                                End If
                                xtw.WriteEndElement()

                            End If
                        Next
                    End If
                    xtw.WriteEndElement()
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End If
    End Sub

    '=======================================================
    'Service provided by Telerik (www.telerik.com)
    'Conversion powered by NRefactory.
    'Twitter: @telerik
    'Facebook: facebook.com/telerik
    '=======================================================

#End Region


    Private Sub CreateAvailabilityXml(ByRef xtw As XmlWriter, ByVal rs As ADODB.Recordset, ByVal bFareLogin As Boolean, ByVal bLowFareFinder As Boolean)

        If IsNothing(rs) = False AndAlso rs.RecordCount > 0 Then
            'Construct the availability xml and get the lowest fare.
            rs.MoveFirst()
            While Not rs.EOF
                xtw.WriteStartElement("AvailabilityFlight")
                If bLowFareFinder = False Then
                    GetAvailabilityField(xtw, rs)
                Else
                    GetLowFareFinderField(xtw, rs)
                End If
                xtw.WriteEndElement() 'AvailabilityFlight
                rs.MoveNext()
            End While
        End If
    End Sub
    Private Sub GetAvailabilityField(ByRef xtw As XmlWriter, ByRef rs As ADODB.Recordset)

        xtw.WriteStartElement("airline_rcd")
        If rs.Fields("airline_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("airline_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("airline_name")
        If rs.Fields("airline_name").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("airline_name").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("flight_number")
        If rs.Fields("flight_number").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("flight_number").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("booking_class_rcd")
        If rs.Fields("booking_class_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("booking_class_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("boarding_class_rcd")
        If rs.Fields("boarding_class_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("boarding_class_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("flight_id")
        If rs.Fields("flight_id").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("flight_id").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("origin_rcd")
        If rs.Fields("origin_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("origin_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("destination_rcd")
        If rs.Fields("destination_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("destination_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("origin_name")
        If rs.Fields("origin_name").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("origin_name").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("destination_name")
        If rs.Fields("destination_name").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("destination_name").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("departure_date")
        If rs.Fields("departure_date").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("departure_date").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("arrival_date")
        If rs.Fields("arrival_date").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("arrival_date").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("planned_departure_time")
        If rs.Fields("planned_departure_time").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("planned_departure_time").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("planned_arrival_time")
        If rs.Fields("planned_arrival_time").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("planned_arrival_time").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("fare_id")
        If rs.Fields("fare_id").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("fare_id").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("fare_code")
        If rs.Fields("fare_code").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("fare_code").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_points")
        If rs.Fields("transit_points").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_points").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_points_name")
        If rs.Fields("transit_points_name").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_points_name").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_flight_id")
        If rs.Fields("transit_flight_id").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_flight_id").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_booking_class_rcd")
        If rs.Fields("transit_booking_class_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_booking_class_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_boarding_class_rcd")
        If rs.Fields("transit_boarding_class_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_boarding_class_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_airport_rcd")
        If rs.Fields("transit_airport_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_airport_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_departure_date")
        If rs.Fields("transit_departure_date").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_departure_date").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_arrival_date")
        If rs.Fields("transit_arrival_date").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_arrival_date").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_planned_departure_time")
        If rs.Fields("transit_planned_departure_time").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_planned_departure_time").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_planned_arrival_time")
        If rs.Fields("transit_planned_arrival_time").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_planned_arrival_time").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_fare_id")
        If rs.Fields("transit_fare_id").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_fare_id").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_name")
        If rs.Fields("transit_name").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_name").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("nesting_string")
        If rs.Fields("nesting_string").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("nesting_string").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("full_flight_flag")
        If rs.Fields("full_flight_flag").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("full_flight_flag").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("class_open_flag")
        If rs.Fields("class_open_flag").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("class_open_flag").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("close_web_sales")
        If rs.Fields("close_web_sales").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("close_web_sales").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("waitlist_open_flag")
        If rs.Fields("waitlist_open_flag").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("waitlist_open_flag").Value)
        End If
        xtw.WriteEndElement()


        xtw.WriteStartElement("adult_fare")
        If rs.Fields("adult_fare").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("adult_fare").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("child_fare")
        If rs.Fields("child_fare").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("child_fare").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("infant_fare")
        If rs.Fields("infant_fare").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("infant_fare").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("total_adult_fare")
        If rs.Fields("total_adult_fare").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("total_adult_fare").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("total_child_fare")
        If rs.Fields("total_child_fare").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("total_child_fare").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("total_infant_fare")
        If rs.Fields("total_infant_fare").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("total_infant_fare").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("fare_column")
        If rs.Fields("fare_column").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("fare_column").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("flight_comment")
        If rs.Fields("flight_comment").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("flight_comment").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_flight_comment")
        If rs.Fields("transit_flight_comment").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_flight_comment").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("filter_logic_flag")
        If rs.Fields("filter_logic_flag").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("filter_logic_flag").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("restriction_text")
        If rs.Fields("restriction_text").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("restriction_text").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("endorsement_text")
        If rs.Fields("endorsement_text").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("endorsement_text").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("fare_type_rcd")
        If rs.Fields("fare_type_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("fare_type_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("redemption_points")
        If rs.Fields("redemption_points").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("redemption_points").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_redemption_points")
        If rs.Fields("transit_redemption_points").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_redemption_points").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("flight_duration")
        If rs.Fields("flight_duration").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("flight_duration").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("promotion_code")
        If rs.Fields("promotion_code").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("promotion_code").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("nested_book_available")
        If rs.Fields("nested_book_available").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("nested_book_available").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_airline_rcd")
        If rs.Fields("transit_airline_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_airline_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_flight_number")
        If rs.Fields("transit_flight_number").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_flight_number").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_flight_status_rcd")
        If rs.Fields("transit_flight_status_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_flight_status_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_flight_duration")
        If rs.Fields("transit_flight_duration").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_flight_duration").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_aircraft_type_rcd")
        If rs.Fields("transit_aircraft_type_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_aircraft_type_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_nested_book_available")
        If rs.Fields("transit_nested_book_available").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_nested_book_available").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_waitlist_open_flag")
        If rs.Fields("transit_waitlist_open_flag").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_waitlist_open_flag").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_adult_fare")
        If rs.Fields("transit_adult_fare").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_adult_fare").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_class_open_flag")
        If rs.Fields("transit_class_open_flag").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_class_open_flag").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("flight_information_1")
        If rs.Fields("flight_information_1").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("flight_information_1").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("corporate_fare_flag")
        If rs.Fields("corporate_fare_flag").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("corporate_fare_flag").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("refundable_flag")
        If rs.Fields("refundable_flag").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("refundable_flag").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_flight_duration")
        If rs.Fields("transit_flight_duration").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_flight_duration").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("aircraft_type_rcd")
        If rs.Fields("aircraft_type_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("aircraft_type_rcd").Value)
        End If
        xtw.WriteEndElement()

    End Sub
    Private Sub GetLowFareFinderField(ByRef xtw As XmlWriter, ByRef rs As ADODB.Recordset)

        xtw.WriteStartElement("airline_rcd")
        If rs.Fields("airline_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("airline_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("flight_number")
        If rs.Fields("flight_number").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("flight_number").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("booking_class_rcd")
        If rs.Fields("booking_class_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("booking_class_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("boarding_class_rcd")
        If rs.Fields("boarding_class_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("boarding_class_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("flight_id")
        If rs.Fields("flight_id").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("flight_id").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("origin_rcd")
        If rs.Fields("origin_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("origin_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("destination_rcd")
        If rs.Fields("destination_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("destination_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("origin_name")
        If rs.Fields("origin_name").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("origin_name").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("destination_name")
        If rs.Fields("destination_name").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("destination_name").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("departure_date")
        If rs.Fields("departure_date").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("departure_date").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("full_flight_flag")
        If rs.Fields("full_flight_flag").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("full_flight_flag").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("class_open_flag")
        If rs.Fields("class_open_flag").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("class_open_flag").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("close_web_sales")
        If rs.Fields("close_web_sales").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("close_web_sales").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("adult_fare")
        If rs.Fields("adult_fare").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("adult_fare").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("child_fare")
        If rs.Fields("child_fare").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("child_fare").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("infant_fare")
        If rs.Fields("infant_fare").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("infant_fare").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("total_adult_fare")
        If rs.Fields("total_adult_fare").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("total_adult_fare").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("total_child_fare")
        If rs.Fields("total_child_fare").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("total_child_fare").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("total_infant_fare")
        If rs.Fields("total_infant_fare").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("total_infant_fare").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_flight_id")
        If rs.Fields("transit_flight_id").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_flight_id").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("planned_departure_time")
        If rs.Fields("planned_departure_time").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("planned_departure_time").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("planned_arrival_time")
        If rs.Fields("planned_arrival_time").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("planned_arrival_time").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_departure_date")
        If rs.Fields("transit_departure_date").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_departure_date").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_airport_rcd")
        If rs.Fields("transit_airport_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_airport_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_fare_id")
        If rs.Fields("transit_fare_id").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_fare_id").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("fare_id")
        If rs.Fields("fare_id").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("fare_id").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("full_flight_flag")
        If rs.Fields("full_flight_flag").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("full_flight_flag").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("class_open_flag")
        If rs.Fields("class_open_flag").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("class_open_flag").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("close_web_sales")
        If rs.Fields("close_web_sales").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("close_web_sales").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("waitlist_open_flag")
        If rs.Fields("waitlist_open_flag").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("waitlist_open_flag").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("arrival_date")
        If rs.Fields("arrival_date").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("arrival_date").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_airline_rcd")
        If rs.Fields("transit_airline_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_airline_rcd").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_name")
        If rs.Fields("transit_name").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_name").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_flight_number")
        If rs.Fields("transit_flight_number").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_flight_number").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_arrival_date")
        If rs.Fields("transit_arrival_date").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_arrival_date").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_planned_departure_time")
        If rs.Fields("transit_planned_departure_time").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_planned_departure_time").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_planned_arrival_time")
        If rs.Fields("transit_planned_arrival_time").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_planned_arrival_time").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_points")
        If rs.Fields("transit_points").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_points").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_points_name")
        If rs.Fields("transit_points_name").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_points_name").Value)
        End If
        xtw.WriteEndElement()

        xtw.WriteStartElement("transit_boarding_class_rcd")
        If rs.Fields("transit_boarding_class_rcd").Value Is System.DBNull.Value Then
            xtw.WriteValue(String.Empty)
        Else
            xtw.WriteValue(rs.Fields("transit_boarding_class_rcd").Value)
        End If
        xtw.WriteEndElement()
    End Sub
    Private Function ReadAirportOrigin(ByVal Language As String, ByVal b2cFlag As Boolean, ByVal b2bFlag As Boolean, ByVal b2eFlag As Boolean, ByVal b2sFlag As Boolean, ByVal apiFlag As Boolean) As DataSet
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                Dim clsRunCom As New clsRunComplus
                rsA = clsRunCom.GetOrigins(Language, b2cFlag, b2bFlag, b2eFlag, b2sFlag, apiFlag)
                clsRunCom = Nothing
                Call InputRecordsetToDataset(rsA, "AirportOrigins", objOut)
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function
    Private Function ReadAirportDestination(ByVal Language As String, ByVal b2cFlag As Boolean, ByVal b2bFlag As Boolean, ByVal b2eFlag As Boolean, ByVal b2sFlag As Boolean, ByVal apiFlag As Boolean) As DataSet
        Dim rsA As ADODB.Recordset = Nothing
        Using objOut As New DataSet
            Try

                Dim clsRunCom As New clsRunComplus
                rsA = clsRunCom.GetDestination(Language, b2cFlag, b2bFlag, b2eFlag, b2sFlag, apiFlag)
                clsRunCom = Nothing
                Call InputRecordsetToDataset(rsA, "AirportOrigins", objOut)
            Catch e As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try
            Return objOut
        End Using
    End Function
    Private Function IsTokenValid(ByVal strToken As String) As Boolean

        If System.Configuration.ConfigurationManager.AppSettings("AffixEncrypt") <> Nothing And System.Configuration.ConfigurationManager.AppSettings("PrefixEncrypt") <> Nothing Then
            Dim objCrypto As New clsCrypto
            Dim strDecryptValue = objCrypto.DecryptString(strToken, System.Configuration.ConfigurationManager.AppSettings("PrefixEncrypt"))
            Dim strArr = strDecryptValue.ToString().Split("|")
            If strArr.length = 2 Then
                If strArr(0).ToString().Equals(System.Configuration.ConfigurationManager.AppSettings("AffixEncrypt")) Then
                    Dim endTime As DateTime = DateTime.Now
                    Dim span As TimeSpan = endTime - Convert.ToDateTime(strArr(1))
                    If span.TotalMinutes <= 10 Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
    Private Function ReadFlightQuoteSummary(ByVal strFlightXml As String,
                                            ByVal strPassengerXml As String,
                                            ByVal strAgencyCode As String,
                                            ByVal strLanguage As String,
                                            ByVal strCurrencyCode As String,
                                            ByVal bNoVat As Boolean) As String
        Dim clsRunCom As New clsRunComplus

        Dim FabricateRecordset As New FabricateRecordset
        Dim rsFlight As ADODB.Recordset = FabricateRecordset.FabricateSegmentRecordset
        Dim rsPassenger As ADODB.Recordset = FabricateRecordset.FabricatePassengerRecordset
        Dim rsMapping As ADODB.Recordset = Nothing
        Dim rsTax As ADODB.Recordset = Nothing
        Dim rsQuotes As ADODB.Recordset = Nothing
        Dim rsFee As ADODB.Recordset = Nothing

        Using ds As New DataSet
            Try
                Dim objFlights As New Flights
                Dim objPassengers As New Passengers

                objFlights = Helper.Serialization.Deserialize(strFlightXml, objFlights)
                objPassengers = Helper.Serialization.Deserialize(strPassengerXml, objPassengers)
                Dim r As New Helper.RecordSet
                r.Fill(rsFlight, objFlights)
                r.Fill(rsPassenger, objPassengers)

                If clsRunCom.FareQuote(rsPassenger, strAgencyCode, rsTax, rsMapping, rsQuotes, rsFlight, , , , , , , , , , strCurrencyCode, , , , , , , strLanguage, bNoVat) = True Then
                    rsFlight.MoveFirst()
                    CopyRecordsetsToDataset(Nothing, rsFlight, rsPassenger, Nothing, Nothing, rsMapping, rsTax, rsQuotes, Nothing, Nothing, ds)
                End If

            Catch e As Exception
                Throw New Exception(e.Message)
            Finally
                clsRunCom = Nothing
                Helper.Check.ClearRecordset(rsFlight)
                Helper.Check.ClearRecordset(rsPassenger)
                Helper.Check.ClearRecordset(rsMapping)
                Helper.Check.ClearRecordset(rsTax)
                Helper.Check.ClearRecordset(rsQuotes)
                Helper.Check.ClearRecordset(rsFee)
            End Try

            If IsNothing(ds) = False Then
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Return ds.GetXml
                Else
                    Return String.Empty
                End If
            Else
                Return String.Empty
            End If
        End Using
    End Function
    Private Function FeeDefinitionRead(ByVal strLanguage As String) As String

        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Try
                If strLanguage.Length > 0 Then

                    Dim clsRunCom As New clsRunComplus

                    rsA = clsRunCom.GetFeesDefinition(strLanguage)
                    clsRunCom = Nothing
                    Call InputRecordsetToDataset(rsA, "BookingFees", objOut)

                End If
            Catch ex As Exception
            Finally
                Helper.Check.ClearRecordset(rsA)
            End Try

            If IsNothing(objOut) = False Then
                If objOut.Tables.Count > 0 AndAlso objOut.Tables(0).Rows.Count Then
                    Return objOut.GetXml
                Else
                    Return String.Empty
                End If
            Else
                Return String.Empty
            End If
        End Using
    End Function
    Public Function ReadCompactFlightAvailability(ByVal FirstSegment As Boolean,
                                                    ByVal bookingId As String,
                                                    ByVal Origin As String,
                                                    ByVal Destination As String,
                                                    ByVal DateFrom As Date,
                                                    ByVal DateTo As Date,
                                                    ByVal DateBooking As Date,
                                                    ByVal Adult As Short,
                                                    ByVal Child As Short,
                                                    ByVal Infant As Short,
                                                    ByVal Other As Short,
                                                    ByVal OtherPassengerType As String,
                                                    ByVal BoardingClass As String,
                                                    ByVal BookingClass As String,
                                                    ByVal DayTimeIndicator As String,
                                                    ByVal AgencyCode As String,
                                                    ByVal CurrencyCode As String,
                                                    ByVal FlightId As String,
                                                    ByVal FareId As String,
                                                    ByVal MaxAmount As Double,
                                                    ByVal NonStopOnly As Boolean,
                                                    ByVal IncludeDeparted As Boolean,
                                                    ByVal IncludeCancelled As Boolean,
                                                    ByVal IncludeWaitlisted As Boolean,
                                                    ByVal IncludeSoldOut As Boolean,
                                                    ByVal Refundable As Boolean,
                                                    ByVal GroupFares As Boolean,
                                                    ByVal ItFaresOnly As Boolean,
                                                    ByVal PromotionCode As String,
                                                    ByVal FareType As String) As String
        Dim objStw = New StringWriter
        Dim rsA As ADODB.Recordset = Nothing
        Dim rsC As ADODB.Recordset = Nothing

        Try
            If FirstSegment Then
                Session("BookingState") = Nothing
            End If

            Dim clsRunCom As New clsRunComplus

            If bookingId = String.Empty Then
                Dim rsHeader As ADODB.Recordset = Nothing
                Call clsRunCom.GetEmpty(AgentHeader.AgencyCode, AgentHeader.AgencyCurrencyRcd, rsHeader)
                If rsHeader.RecordCount > 0 Then
                    bookingId = rsHeader("booking_id").Value.ToString
                Else
                    bookingId = Guid.NewGuid().ToString
                End If
                Session("rsHeader") = rsHeader
            End If

            rsA = clsRunCom.GetAvailability(Origin,
                                            Destination,
                                            DateFrom,
                                            DateTo,
, ,
                                            DateBooking,
                                            Adult,
                                            Child,
                                            Infant,
                                            Other,
                                            OtherPassengerType,
                                            BoardingClass,
                                            BookingClass,
                                            DayTimeIndicator,
                                            AgencyCode,
                                            CurrencyCode,
                                            FlightId,
                                            FareId,
                                            MaxAmount,
                                            NonStopOnly,
                                            IncludeDeparted,
                                            IncludeCancelled,
                                            IncludeWaitlisted,
                                            IncludeSoldOut,
                                            Refundable,
                                            GroupFares,
                                            ItFaresOnly,
                                            Nothing,
                                            PromotionCode,
                                            FareType)


            Dim r As New FabricateRecordset
            r.SerializeAdoRecordsets(rsA, rsC)

            clsRunCom = Nothing
            Call CompactAvailibityRecordsetToXml(rsC, objStw)

        Catch e As Exception
        Finally
            Helper.Check.ClearRecordset(rsA)
            Helper.Check.ClearRecordset(rsC)
        End Try
        Return objStw.ToString
    End Function
    Private Function AddPayment(ByVal strBookingId As String,
                                ByVal strAgencyCode As String,
                                ByVal strFormOfPayment As String,
                                ByVal strCurrencyCode As String,
                                ByVal dAmount As Decimal,
                                ByVal strFormOfPaymentSubtype As String,
                                ByVal strUserId As String,
                                ByVal strDocumentNumber As String,
                                ByVal strApprovalCode As String,
                                ByVal strRemark As String,
                                ByVal strLanguage As String,
                                ByVal dtPayment As DateTime,
                                ByVal bReturnItinerary As Boolean) As String

        Dim strOut As String = String.Empty
        Try
            Dim clsRunCom As New clsRunComplus

            If IsGuid(strBookingId) = False Then
                Dim rsHeader As New ADODB.Recordset
                clsRunCom.Read("", strBookingId, , rsHeader)
                If IsNothing(rsHeader) = False AndAlso rsHeader.RecordCount > 0 Then
                    strBookingId = rsHeader.Fields("booking_id").Value
                End If
            End If

            strOut = clsRunCom.ExternalPaymantAddPayment(strBookingId,
                                                         strAgencyCode,
                                                         strFormOfPayment,
                                                         strCurrencyCode,
                                                         dAmount,
                                                         strFormOfPaymentSubtype,
                                                         strUserId,
                                                         strDocumentNumber,
                                                         strApprovalCode,
                                                         strRemark,
                                                         strLanguage,
                                                         dtPayment,
                                                         bReturnItinerary)
        Catch e As Exception
            strOut = String.Empty
        End Try

        Return strOut
    End Function
    Private Function ReadFormOfPaymentSubTypes(ByVal strType As String, ByVal strLanguage As String) As DataSet
        Dim rsA As ADODB.Recordset = Nothing
        Dim objOut As New DataSet
        Try
            If (AgentHeader Is Nothing) Then

            Else
                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then

                    Dim clsRunCom As New clsRunComplus

                    rsA = clsRunCom.GetFormOfPaymentSubTypes(strType, strLanguage)

                    clsRunCom = Nothing
                    Call InputRecordsetToDataset(rsA, "GetFormOfPayments", objOut)
                End If
            End If
        Catch e As Exception

        Finally
            Helper.Check.ClearRecordset(rsA)
        End Try

        objOut.DataSetName = "FormOfPayments"
        Return objOut
    End Function

    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/ReadFormOfPayment"),
   WebMethod(Description:="Read FormOfPlayment",
   EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function ReadFormOfPayment(ByVal strType As String) As DataSet
        Dim rsA As ADODB.Recordset = Nothing
        Dim objOut As New DataSet
        Try
            If (AgentHeader Is Nothing) Then

            Else

                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus

                    rsA = clsRunCom.ReadFormOfPayment(strType)

                    clsRunCom = Nothing
                    Call InputRecordsetToDataset(rsA, "ReadFormOfPayment", objOut)
                End If
            End If
        Catch e As Exception

        Finally
            Helper.Check.ClearRecordset(rsA)
        End Try

        objOut.DataSetName = "FormOfPayments"
        Return objOut
    End Function
    <SoapDocumentMethod(Action:="http://www.tiksystems.com/tikAeroWebService/GetAirportTimezone"),
   WebMethod(Description:="Get Airtport Timezone Name",
   EnableSession:=True), SoapHeader("AgentHeader")>
    Public Function GetAirportTimezone(ByVal strOdOrigin As String) As String
        Dim rsA As ADODB.Recordset = Nothing
        Dim rsB As ADODB.Recordset = Nothing
        Dim objOut As New DataSet
        Dim ariportZone As String = ""
        Try
            If (AgentHeader Is Nothing) Then

            Else

                Dim strCode As String = AgentHeader.AgencyCode
                Dim strPassport As String = AgentHeader.AgencyPassport

                If (AuthenticatePassport(strCode, strPassport)) Then
                    Dim clsRunCom As New clsRunComplus

                    rsA = clsRunCom.GetAirportCode(strOdOrigin)
                    If (rsA.RecordCount > 0) Then
                        ariportZone = rsA.Fields("timezone_name").Value
                    End If
                    clsRunCom = Nothing
                    Call InputRecordsetToDataset(rsA, "GetAirportTimezone", objOut)
                End If
            End If
        Catch e As Exception

        Finally
            Helper.Check.ClearRecordset(rsA)
        End Try
        objOut.DataSetName = "GetAirportTimezone"
        Return ariportZone
    End Function
    Private Sub ClearBookingRecordset(ByRef rsHeader As ADODB.Recordset,
                                      ByRef rsSegments As ADODB.Recordset,
                                      ByRef rsPassengers As ADODB.Recordset,
                                      ByRef rsRemarks As ADODB.Recordset,
                                      ByRef rsPayments As ADODB.Recordset,
                                      ByRef rsMappings As ADODB.Recordset,
                                      ByRef rsServices As ADODB.Recordset,
                                      ByRef rsTaxes As ADODB.Recordset,
                                      ByRef rsFees As ADODB.Recordset,
                                      ByRef rsQuotes As ADODB.Recordset)

        Helper.Check.ClearRecordset(rsHeader)
        Helper.Check.ClearRecordset(rsSegments)
        Helper.Check.ClearRecordset(rsPassengers)
        Helper.Check.ClearRecordset(rsRemarks)
        Helper.Check.ClearRecordset(rsPayments)
        Helper.Check.ClearRecordset(rsMappings)
        Helper.Check.ClearRecordset(rsServices)
        Helper.Check.ClearRecordset(rsTaxes)
        Helper.Check.ClearRecordset(rsFees)
        Helper.Check.ClearRecordset(rsQuotes)
    End Sub

    Private Sub SaveLog(ByVal Message As String, ByVal Path As String)
        'For debuging
        Dim stw As StreamWriter = Nothing
        If Message.Length > 0 Then
            Try
                stw = New StreamWriter(Path, True)
                stw.WriteLine("--------------- " + String.Format("{0:dd/MM/yyyy hh:mm:ss}", DateTime.Now) + " ---------------")
                stw.WriteLine(Message)
                stw.Close()
                stw = Nothing
            Catch ex As Exception
                If IsNothing(stw) = False Then
                    stw.Close()
                End If
                stw = Nothing
            End Try
        End If
    End Sub
    Private Function GetUserId() As Guid
        Dim DefaultCreateById As String
        If Not Session("CreateById") Is Nothing AndAlso Not Session("CreateById") = String.Empty Then
            DefaultCreateById = Session("CreateById")
            Return New Guid(DefaultCreateById)
        Else
            If IsGuid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")) Then
                Session("CreateById") = System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")
            End If
            DefaultCreateById = Session("CreateById")
            Return New Guid(DefaultCreateById)
        End If
    End Function
    Private Sub BuildRs(ByVal rs As ADODB.Recordset)
        Dim a(rs.Fields.Count - 1) As String
        For i As Integer = 0 To rs.Fields.Count - 1
            a(i) = ".Append(""" & rs.Fields(i).Name & """, " & rs.Fields(i).Type.ToString & " , " & rs.Fields(i).DefinedSize & " , adFldIsNullable + adFldMayBeNull)"
        Next
        Dim s As String = Join(a, vbCrLf)
    End Sub
    Private Sub BuildDoc(ByVal rs As ADODB.Recordset)
        Dim a(rs.Fields.Count - 1) As String
        For i As Integer = 0 To rs.Fields.Count - 1
            a(i) = rs.Fields(i).Name & vbTab & vbTab & rs.Fields(i).Value
        Next
        Dim s As String = Join(a, vbCrLf)
    End Sub
    Private Function CopyCashbookChargesRecordsetsToDataset(ByVal rsCashbookCharges As ADODB.Recordset,
                                            ByRef ds As DataSet) As Boolean
        Dim da As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter
        ds.DataSetName = "CashbookCharges"
        Try
            If Not rsCashbookCharges Is Nothing Then
                If rsCashbookCharges.EOF = False Then
                    da.Fill(ds, rsCashbookCharges, 0)
                    ds.Tables(ds.Tables.Count - 1).TableName = "CashbookCharges"
                Else
                End If
            End If
        Catch e As Exception

        End Try
    End Function
    Private Function InputRecordsetToDataset(ByVal rsA As ADODB.Recordset, ByVal strTableName As String,
                                               ByRef ds As DataSet, Optional ByVal NamesSpace As String = "") As Boolean
        Dim da As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter
        Try
            If Not rsA Is Nothing Then
                If Not NamesSpace.Length = 0 Then
                    ds.DataSetName = NamesSpace
                End If
                da.Fill(ds, rsA, 0)
                ds.Tables(0).TableName = strTableName
            End If
        Catch e As Exception

        End Try
    End Function

    Private Function RecordSetToXML(ByVal rs As ADODB.Recordset, ByVal NamesSpace As String, ByVal strTableName As String) As String
        Dim da As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter
        Dim ds As New DataSet
        Dim XmlString As String = String.Empty
        ds.DataSetName = NamesSpace
        Try
            If Not rs Is Nothing Then
                da.Fill(ds, rs, 0)
                ds.Tables(0).TableName = strTableName
                XmlString = ds.GetXml
            End If
        Catch e As Exception

        End Try
        Return XmlString
    End Function

    Private Function Serialize(ByVal o As Object) As String
        If Not o Is Nothing Then
            Dim s As XmlSerializer = New XmlSerializer(o.GetType())
            Dim w As StringWriter = New StringWriter
            s.Serialize(w, o)
            Return w.ToString
        Else
            Return ""
        End If
    End Function

    Public Function InputRecordsetToDataset(ByVal rsA As ADODB.Recordset, ByVal NamesSpace As String, ByVal strTableName As String,
                                            ByRef ds As DataSet) As Boolean
        Dim da As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter
        ds.DataSetName = NamesSpace
        Try
            If Not rsA Is Nothing Then
                da.Fill(ds, rsA, ds.Tables.Count - 1)
                ds.Tables(ds.Tables.Count - 1).TableName = strTableName
            End If
        Catch e As Exception

        End Try
    End Function
    Private Sub CompactAvailibityRecordsetToXml(ByVal rsA As ADODB.Recordset, ByRef objStw As StringWriter)
        Dim xml As New XmlTextWriter(objStw)
        Try
            If Not rsA Is Nothing Then
                xml.WriteStartElement("Booking")
                'Start Booking-------------------------------------------------
                rsA.MoveFirst()
                While Not rsA.EOF

                    xml.WriteStartElement("AvailabilityFlight")
                    'Start AvailabilityFlight-------------------------------------------------

                    xml.WriteStartElement("airline_rcd")
                    xml.WriteValue(rsA.Fields("airline_rcd").Value)
                    xml.WriteEndElement()

                    xml.WriteStartElement("flight_number")
                    xml.WriteValue(rsA.Fields("flight_number").Value)
                    xml.WriteEndElement()

                    xml.WriteStartElement("flight_id")
                    xml.WriteValue(rsA.Fields("flight_id").Value)
                    xml.WriteEndElement()

                    xml.WriteStartElement("fare_id")
                    xml.WriteValue(rsA.Fields("fare_id").Value)
                    xml.WriteEndElement()

                    xml.WriteStartElement("origin_rcd")
                    xml.WriteValue(rsA.Fields("origin_rcd").Value)
                    xml.WriteEndElement()

                    xml.WriteStartElement("destination_rcd")
                    xml.WriteValue(rsA.Fields("destination_rcd").Value)
                    xml.WriteEndElement()

                    xml.WriteStartElement("transit_flight_id")
                    If rsA.Fields("transit_flight_id").Value Is DBNull.Value Then
                        xml.WriteValue(String.Empty)
                    Else
                        xml.WriteValue(rsA.Fields("transit_flight_id").Value)
                    End If
                    xml.WriteEndElement()

                    xml.WriteStartElement("transit_fare_id")
                    If rsA.Fields("transit_fare_id").Value Is DBNull.Value Then
                        xml.WriteValue(String.Empty)
                    Else
                        xml.WriteValue(rsA.Fields("transit_fare_id").Value)
                    End If
                    xml.WriteEndElement()

                    xml.WriteStartElement("transit_departure_date")
                    If rsA.Fields("transit_departure_date").Value Is DBNull.Value Then
                        xml.WriteValue(String.Empty)
                    Else
                        xml.WriteValue(rsA.Fields("transit_departure_date").Value)
                    End If
                    xml.WriteEndElement()

                    xml.WriteStartElement("transit_airport_rcd")
                    If rsA.Fields("transit_airport_rcd").Value Is DBNull.Value Then
                        xml.WriteValue(String.Empty)
                    Else
                        xml.WriteValue(rsA.Fields("transit_airport_rcd").Value)
                    End If
                    xml.WriteEndElement()

                    xml.WriteStartElement("transit_planned_departure_time")
                    If rsA.Fields("transit_planned_departure_time").Value Is DBNull.Value Then
                        xml.WriteValue(String.Empty)
                    Else
                        xml.WriteValue(rsA.Fields("transit_planned_departure_time").Value)
                    End If
                    xml.WriteEndElement()

                    xml.WriteStartElement("transit_planned_arrival_time")
                    If rsA.Fields("transit_planned_arrival_time").Value Is DBNull.Value Then
                        xml.WriteValue(String.Empty)
                    Else
                        xml.WriteValue(rsA.Fields("transit_planned_arrival_time").Value)
                    End If
                    xml.WriteEndElement()

                    xml.WriteStartElement("departure_date")
                    xml.WriteValue(rsA.Fields("departure_date").Value)
                    xml.WriteEndElement()

                    xml.WriteStartElement("planned_departure_time")
                    xml.WriteValue(rsA.Fields("planned_departure_time").Value)
                    xml.WriteEndElement()

                    xml.WriteStartElement("planned_arrival_time")
                    xml.WriteValue(rsA.Fields("planned_arrival_time").Value)
                    xml.WriteEndElement()

                    xml.WriteStartElement("adult_fare")
                    xml.WriteValue(rsA.Fields("adult_fare").Value)
                    xml.WriteEndElement()

                    xml.WriteStartElement("total_adult_fare")
                    xml.WriteValue(rsA.Fields("total_adult_fare").Value)
                    xml.WriteEndElement()

                    xml.WriteStartElement("full_flight_flag")
                    xml.WriteValue(rsA.Fields("full_flight_flag").Value)
                    xml.WriteEndElement()

                    xml.WriteStartElement("class_open_flag")
                    xml.WriteValue(rsA.Fields("class_open_flag").Value)
                    xml.WriteEndElement()

                    xml.WriteStartElement("close_web_sales")
                    xml.WriteValue(rsA.Fields("close_web_sales").Value)
                    xml.WriteEndElement()

                    xml.WriteStartElement("booking_class_rcd")
                    xml.WriteValue(rsA.Fields("booking_class_rcd").Value)
                    xml.WriteEndElement()

                    xml.WriteStartElement("boarding_class_rcd")
                    xml.WriteValue(rsA.Fields("boarding_class_rcd").Value)
                    xml.WriteEndElement()

                    'End AvailabilityFlight-------------------------------------------------
                    xml.WriteEndElement()
                    rsA.MoveNext()
                End While
                'End Booking---------------------------------------------------
                xml.WriteEndElement()
            End If
        Catch e As Exception
        Finally
            xml.Close()
        End Try
    End Sub

    Private Function CopyClientReadRecordsetsToDataset(ByVal rsClient As ADODB.Recordset,
                                                        ByVal rsEmployee As ADODB.Recordset,
                                                        ByVal rsPassenger As ADODB.Recordset,
                                                        ByVal rsBookingRemark As ADODB.Recordset,
                                                        ByVal rsClientSegment As ADODB.Recordset,
                                                        ByRef ds As DataSet) As Boolean

        Dim da As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter
        ds.DataSetName = "ClientRead"
        Try
            If Not rsClient Is Nothing Then
                If rsClient.EOF = False Then
                    da.Fill(ds, rsClient, ds.Tables.Count - 1)
                    ds.Tables(ds.Tables.Count - 1).TableName = "Client"
                Else
                End If
            End If
            If Not rsEmployee Is Nothing Then
                If rsEmployee.EOF = False Then
                    da.Fill(ds, rsEmployee, 0)
                    ds.Tables(ds.Tables.Count - 1).TableName = "Employee"
                Else
                End If
            End If
            If Not rsPassenger Is Nothing Then
                If rsPassenger.EOF = False Then
                    da.Fill(ds, rsPassenger, 0)
                    ds.Tables(ds.Tables.Count - 1).TableName = "Passenger"
                Else
                End If
            End If
            If Not rsBookingRemark Is Nothing Then
                If rsBookingRemark.EOF = False Then
                    da.Fill(ds, rsBookingRemark, 0)
                    ds.Tables(ds.Tables.Count - 1).TableName = "Remark"
                Else
                End If
            End If
            If Not rsClientSegment Is Nothing Then
                If rsClientSegment.EOF = False Then
                    da.Fill(ds, rsClientSegment, 0)
                    ds.Tables(ds.Tables.Count - 1).TableName = "ClientSegment"
                Else
                End If
            End If
        Catch e As Exception

        End Try
    End Function

    Private Function CopyRecordsetsToDataset(ByVal rsHeader As ADODB.Recordset,
                                            ByVal rsSegment As ADODB.Recordset,
                                            ByVal rsPassenger As ADODB.Recordset,
                                            ByVal rsRemark As ADODB.Recordset,
                                            ByVal rsPayment As ADODB.Recordset,
                                            ByVal rsMapping As ADODB.Recordset,
                                            ByVal rsTax As ADODB.Recordset,
                                            ByVal rsQuote As ADODB.Recordset,
                                            ByVal rsFee As ADODB.Recordset,
                                            ByVal rsServices As ADODB.Recordset,
                                            ByRef ds As DataSet) As Boolean

        Dim dt As DataTable
        ds.DataSetName = "Booking"
        Try
            If Not rsHeader Is Nothing Then
                rsHeader.Filter = 0
                If rsHeader.EOF = False Then
                    dt = RecordsetToDatatable(rsHeader, "BookingHeader")
                    ds.Tables.Add(dt)
                    dt.Dispose()
                Else
                End If
            End If
            If Not rsSegment Is Nothing Then
                rsSegment.Filter = 0
                If rsSegment.EOF = False Then
                    dt = RecordsetToDatatable(rsSegment, "FlightSegment")
                    ds.Tables.Add(dt)
                    dt.Dispose()
                Else
                End If
            End If
            If Not rsPassenger Is Nothing Then
                rsPassenger.Filter = 0
                If rsPassenger.EOF = False Then
                    dt = RecordsetToDatatable(rsPassenger, "Passenger")
                    ds.Tables.Add(dt)
                    dt.Dispose()
                Else
                End If
            End If
            If Not rsRemark Is Nothing Then
                rsRemark.Filter = 0
                If rsRemark.EOF = False Then
                    dt = RecordsetToDatatable(rsRemark, "Remark")
                    ds.Tables.Add(dt)
                    dt.Dispose()
                Else
                End If
            End If
            If Not rsPayment Is Nothing Then
                rsPayment.Filter = 0
                If rsPayment.EOF = False Then
                    dt = RecordsetToDatatable(rsPayment, "Payment")
                    ds.Tables.Add(dt)
                    dt.Dispose()
                Else
                End If
            End If
            If Not rsMapping Is Nothing Then
                rsMapping.Filter = 0
                If rsMapping.EOF = False Then
                    dt = RecordsetToDatatable(rsMapping, "Mapping")
                    ds.Tables.Add(dt)
                    dt.Dispose()
                Else
                End If
            End If
            If Not rsTax Is Nothing Then
                rsTax.Filter = 0
                If rsTax.EOF = False Then
                    dt = RecordsetToDatatable(rsTax, "Tax")
                    ds.Tables.Add(dt)
                    dt.Dispose()
                Else
                End If
            End If
            If Not rsQuote Is Nothing Then
                rsQuote.Filter = 0
                If rsQuote.EOF = False Then
                    dt = RecordsetToDatatable(rsQuote, "Quote")
                    ds.Tables.Add(dt)
                    dt.Dispose()
                Else
                End If
            End If
            If Not rsFee Is Nothing Then
                rsFee.Filter = 0
                If rsFee.EOF = False Then
                    dt = RecordsetToDatatable(rsFee, "Fee")
                    ds.Tables.Add(dt)
                    dt.Dispose()
                Else
                End If
            End If
            If Not rsServices Is Nothing Then
                rsServices.Filter = 0
                If rsServices.EOF = False Then
                    dt = RecordsetToDatatable(rsServices, "Service")
                    ds.Tables.Add(dt)
                    dt.Dispose()
                Else
                End If
            End If
        Catch e As Exception

        End Try
    End Function

    Private Function RecordsetToDatatable(ByVal rs As ADODB.Recordset, ByVal tableName As String) As DataTable

        Dim dt As New DataTable(tableName)

        Try
            For Each fld As ADODB.Field In rs.Fields

                Dim dc_column As New DataColumn("column")

                Select Case fld.Type
                    Case ADODB.DataTypeEnum.adNumeric
                        dc_column.DataType = System.Type.GetType("System.Double")
                        Exit Select
                    Case ADODB.DataTypeEnum.adDouble
                        dc_column.DataType = System.Type.GetType("System.Double")
                        Exit Select
                    Case ADODB.DataTypeEnum.adCurrency
                        dc_column.DataType = System.Type.GetType("System.Decimal")
                        Exit Select
                    Case ADODB.DataTypeEnum.adDecimal
                        dc_column.DataType = System.Type.GetType("System.Decimal")
                        Exit Select
                    Case ADODB.DataTypeEnum.adChar
                        dc_column.DataType = System.Type.GetType("System.Char")
                        Exit Select
                    Case ADODB.DataTypeEnum.adWChar
                        dc_column.DataType = System.Type.GetType("System.String")
                        Exit Select
                    Case ADODB.DataTypeEnum.adVarChar
                        dc_column.DataType = System.Type.GetType("System.String")
                        Exit Select
                    Case ADODB.DataTypeEnum.adLongVarChar
                        dc_column.DataType = System.Type.GetType("System.String")
                        Exit Select
                    Case ADODB.DataTypeEnum.adVarWChar
                        dc_column.DataType = System.Type.GetType("System.String")
                        Exit Select
                    Case ADODB.DataTypeEnum.adBigInt
                        dc_column.DataType = System.Type.GetType("System.Int64")
                        Exit Select
                    Case ADODB.DataTypeEnum.adInteger
                        dc_column.DataType = System.Type.GetType("System.Int32")
                        Exit Select
                    Case ADODB.DataTypeEnum.adSmallInt
                        dc_column.DataType = System.Type.GetType("System.Int16")
                        Exit Select
                    Case ADODB.DataTypeEnum.adUnsignedTinyInt
                        dc_column.DataType = System.Type.GetType("System.Byte")
                        Exit Select
                    Case ADODB.DataTypeEnum.adTinyInt
                        dc_column.DataType = System.Type.GetType("System.SByte")
                        Exit Select
                    Case ADODB.DataTypeEnum.adDBTimeStamp
                        dc_column.DataType = System.Type.GetType("System.DateTime")
                        Exit Select
                    Case ADODB.DataTypeEnum.adDBDate
                        dc_column.DataType = System.Type.GetType("System.DateTime")
                        Exit Select
                    Case ADODB.DataTypeEnum.adDate
                        dc_column.DataType = System.Type.GetType("System.DateTime")
                        Exit Select
                    Case ADODB.DataTypeEnum.adGUID
                        dc_column.DataType = System.Type.GetType("System.Guid")
                        Exit Select
                    Case ADODB.DataTypeEnum.adBoolean
                        dc_column.DataType = System.Type.GetType("System.Boolean")
                        Exit Select
                    Case ADODB.DataTypeEnum.adSingle
                        dc_column.DataType = System.Type.GetType("System.Single")
                        Exit Select
                    Case ADODB.DataTypeEnum.adLongVarWChar
                        dc_column.DataType = System.Type.GetType("System.String")
                        Exit Select
                    Case Else
                        System.Diagnostics.Debug.Assert(False)
                        Exit Select
                End Select

                dt.Columns.Add(New DataColumn(fld.Name, dc_column.DataType))

                If dc_column IsNot Nothing Then
                    dc_column.Dispose()
                    dc_column = Nothing
                End If
            Next

            Dim vals As Object() = New Object(rs.Fields.Count - 1) {}

            rs.MoveFirst()
            While Not rs.EOF
                Dim nFldCnt As Integer = 0

                For Each fld As ADODB.Field In rs.Fields
                    vals(nFldCnt) = fld.Value
                    nFldCnt += 1
                Next

                dt.Rows.Add(vals)

                rs.MoveNext()
            End While

        Catch exp As Exception
            Console.WriteLine(exp)
        End Try
        Return dt

    End Function

    Private Function GetTimestamp(ByVal FromDateTime As DateTime) As Integer
        Dim Startdate As DateTime = #1/1/1970#
        Dim Spanne As TimeSpan

        Spanne = FromDateTime.Subtract(Startdate)
        Return CType(Math.Abs(Spanne.TotalSeconds()), Integer)
    End Function


    Private Function CopyRecordsetsToXml(ByVal rsHeader As ADODB.Recordset,
                                           ByVal rsSegment As ADODB.Recordset,
                                           ByVal rsPassenger As ADODB.Recordset,
                                           ByVal rsRemark As ADODB.Recordset,
                                           ByVal rsPayment As ADODB.Recordset,
                                           ByVal rsMapping As ADODB.Recordset,
                                           ByVal rsTax As ADODB.Recordset,
                                           ByVal rsQuote As ADODB.Recordset,
                                           ByVal rsFee As ADODB.Recordset,
                                           ByRef xml As String) As Boolean
        Dim ds As New DataSet
        Dim da As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter
        ds.DataSetName = "Booking"
        Try
            If Not rsHeader Is Nothing Then
                If rsHeader.EOF = False Then
                    da.Fill(ds, rsHeader, 0)
                    ds.Tables(0).TableName = "BookingHeader"
                End If
            End If
            If Not rsSegment Is Nothing Then
                If rsSegment.EOF = False Then
                    da.Fill(ds, rsSegment, 0)
                    ds.Tables(ds.Tables.Count - 1).TableName = "FlightSegment"
                End If
            End If
            If Not rsPassenger Is Nothing Then
                If rsPassenger.EOF = False Then
                    da.Fill(ds, rsPassenger, 0)
                    ds.Tables(ds.Tables.Count - 1).TableName = "Passenger"
                End If
            End If
            If Not rsRemark Is Nothing Then
                If rsRemark.EOF = False Then
                    da.Fill(ds, rsRemark, 0)
                    ds.Tables(ds.Tables.Count - 1).TableName = "Remark"
                End If
            End If
            If Not rsPayment Is Nothing Then
                If rsPayment.EOF = False Then
                    da.Fill(ds, rsPayment, 0)
                    ds.Tables(ds.Tables.Count - 1).TableName = "Payment"
                End If
            End If
            If Not rsMapping Is Nothing Then
                If rsMapping.EOF = False Then
                    da.Fill(ds, rsMapping, 0)
                    ds.Tables(ds.Tables.Count - 1).TableName = "Mapping"
                End If
            End If
            If Not rsTax Is Nothing Then
                If rsTax.EOF = False Then
                    da.Fill(ds, rsTax, 0)
                    ds.Tables(ds.Tables.Count - 1).TableName = "Tax"
                End If
            End If
            If Not rsQuote Is Nothing Then
                If rsQuote.EOF = False Then
                    da.Fill(ds, rsQuote, 0)
                    ds.Tables(ds.Tables.Count - 1).TableName = "Quote"
                End If
            End If

            If Not rsFee Is Nothing Then
                If rsFee.EOF = False Then
                    da.Fill(ds, rsFee, 0)
                    ds.Tables(ds.Tables.Count - 1).TableName = "Fee"
                Else
                End If
            End If
            xml = ds.GetXml
            Return True
        Catch e As Exception
            Return False
        End Try
    End Function

    Private Function InputItineraryToDataset(ByVal rsHeader As ADODB.Recordset,
                                                ByVal rsSegment As ADODB.Recordset,
                                                ByVal rsPassenger As ADODB.Recordset,
                                                ByVal rsRemark As ADODB.Recordset,
                                                ByVal rsPayment As ADODB.Recordset,
                                                ByVal rsMapping As ADODB.Recordset,
                                                ByRef ds As DataSet) As Boolean
        Dim daItinerary As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter

        Try
            If rsHeader.EOF = False Then
                If Not rsHeader Is Nothing Then
                    'If rsHeader.EOF = False Then
                    daItinerary.Fill(ds, rsHeader, 0)
                    ds.Tables(0).TableName = "Header"
                    'End If
                End If
                If Not rsSegment Is Nothing Then
                    If rsSegment.EOF = False Then
                        daItinerary.Fill(ds, rsSegment, 0)
                        ds.Tables(ds.Tables.Count - 1).TableName = "Segment"
                    End If
                End If
                If Not rsPassenger Is Nothing Then
                    If rsPassenger.EOF = False Then
                        daItinerary.Fill(ds, rsPassenger, 0)
                        ds.Tables(ds.Tables.Count - 1).TableName = "Passenger"
                    End If
                End If
                If Not rsRemark Is Nothing Then
                    If rsRemark.EOF = False Then
                        daItinerary.Fill(ds, rsRemark, 0)
                        ds.Tables(ds.Tables.Count - 1).TableName = "Remark"
                    End If
                End If
                If Not rsPayment Is Nothing Then
                    If rsPayment.EOF = False Then
                        daItinerary.Fill(ds, rsPayment, 0)
                        ds.Tables(ds.Tables.Count - 1).TableName = "Payment"
                    End If
                End If
                If Not rsMapping Is Nothing Then
                    If rsMapping.EOF = False Then
                        daItinerary.Fill(ds, rsMapping, 0)
                        ds.Tables(ds.Tables.Count - 1).TableName = "Mapping"
                    End If
                End If
            End If
        Catch e As Exception

        End Try
    End Function

    Private Shared GuidRegex As New Regex("^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled)
    Friend Shared Function IsGuid(ByVal candidate As String) As Boolean
        Dim isValid As Boolean = False
        If Not (candidate Is Nothing) Then
            If GuidRegex.IsMatch(candidate) AndAlso Not candidate = "00000000-0000-0000-0000-000000000000" Then
                isValid = True
            End If
        End If
        Return isValid
    End Function

    Friend Shared Function CreateByIdGuid(ByVal candidate As String) As Guid
        If Not (candidate Is Nothing) Then
            If GuidRegex.IsMatch(candidate) AndAlso Not candidate = "00000000-0000-0000-0000-000000000000" Then
                Return New Guid(candidate)
            ElseIf GuidRegex.IsMatch(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy")) AndAlso Not candidate = "00000000-0000-0000-0000-000000000000" Then
                Return New Guid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy"))
            Else
                Return New Guid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy"))
            End If
        End If
    End Function

    Private Function BuildRsBookingSegments(ByVal rsMapping As ADODB.Recordset, ByVal strPassengerID As String) As ADODB.Recordset
        Dim i As Integer
        Dim rsBookingSegmentsIDs As New ADODB.Recordset
        rsMapping.Filter = "passenger_id = '{" & strPassengerID & "}'"
        With rsBookingSegmentsIDs
            '.Open()
            .Fields.Append("booking_segment_id", ADODB.DataTypeEnum.adVarChar, 38)
            .Open()
            For i = 0 To rsMapping.RecordCount - 1
                .AddNew(0, rsMapping("booking_segment_id").Value)
                .Update()
                rsMapping.MoveNext()
            Next
        End With
        Return rsBookingSegmentsIDs
    End Function

    Private Function GetPaymentAllocations(ByVal payments As PaymentsUpdate,
                                           ByVal mappings As Mappings,
                                           ByVal fees As Fees,
                                           ByVal paymentFees As Fees) As Allocations

        Dim Allocations As New Allocations

        Dim Paid As Decimal
        Dim Charge As Decimal
        Dim Outstanding As Decimal
        Dim dclTotalPayment As Decimal

        For i As Integer = 0 To payments.Count - 1
            If payments(i).payment_amount > payments(i).allocated_amount Then
                dclTotalPayment = payments(i).payment_amount

                If (dclTotalPayment > 0) Then
                    'Allocate mapping
                    For j As Integer = 0 To mappings.Count - 1
                        With mappings(j)
                            If Not .exclude_pricing_flag = 0 Then
                                Charge = .payment_amount
                            ElseIf Not .refund_date_time = Date.MinValue Then
                                Charge = .refund_charge
                            Else
                                Dim ExchangedPaid As Double = 0
                                For Each ExchangedMapping As Mapping In mappings
                                    If Not ExchangedMapping.exchanged_date_time = Date.MinValue AndAlso .booking_segment_id.ToString = ExchangedMapping.exchanged_segment_id.ToString AndAlso .passenger_id.ToString = ExchangedMapping.passenger_id.ToString Then
                                        ExchangedPaid = ExchangedMapping.payment_amount
                                        Exit For
                                    End If
                                Next
                                Charge = .net_total - ExchangedPaid

                                Paid = .payment_amount
                                Outstanding = (Charge - Paid) - mappings(j).AllocatedAmount
                            End If

                            If Not Outstanding = 0 Then
                                If dclTotalPayment = 0 Then
                                    Exit For
                                Else
                                    If .AllocatedAmount <> Outstanding Then
                                        Dim Allocation As New Allocation
                                        With Allocation
                                            .passenger_id = mappings(j).passenger_id
                                            .booking_segment_id = mappings(j).booking_segment_id
                                            .user_id = GetUserId()
                                            .currency_rcd = mappings(j).currency_rcd
                                            .booking_payment_id = payments(i).booking_payment_id
                                            .voucher_id = payments(i).voucher_payment_id
                                            If dclTotalPayment >= Outstanding Then

                                                .sales_amount = Outstanding
                                                mappings(j).AllocatedAmount = mappings(j).AllocatedAmount + Outstanding
                                                dclTotalPayment = dclTotalPayment - Outstanding
                                            Else

                                                .sales_amount = dclTotalPayment
                                                mappings(j).AllocatedAmount = mappings(j).AllocatedAmount + dclTotalPayment
                                                dclTotalPayment = 0
                                            End If

                                        End With
                                        Allocations.Add(Allocation)
                                    End If
                                End If
                            End If
                        End With
                    Next

                    'Allocate Payment fee
                    For j As Integer = 0 To paymentFees.Count - 1
                        With paymentFees(j)

                            If .void_date_time = Date.MinValue Then
                                Charge = .fee_amount_incl
                            Else
                                Charge = 0
                            End If
                            Paid = .payment_amount
                            Outstanding = (Charge - Paid) - paymentFees(j).AllocatedAmount

                            If Not Outstanding = 0 Then
                                If dclTotalPayment = 0 Then
                                    Exit For
                                Else
                                    If .AllocatedAmount <> Outstanding Then
                                        Dim Allocation As New Allocation
                                        With Allocation
                                            .booking_fee_id = paymentFees(j).booking_fee_id
                                            .fee_id = paymentFees(j).fee_id
                                            .user_id = GetUserId()
                                            .currency_rcd = paymentFees(j).currency_rcd
                                            .booking_payment_id = payments(i).booking_payment_id
                                            .voucher_id = payments(i).voucher_payment_id
                                            If dclTotalPayment >= Outstanding Then
                                                .sales_amount = Outstanding
                                                paymentFees(j).AllocatedAmount = paymentFees(j).AllocatedAmount + Outstanding
                                                dclTotalPayment = dclTotalPayment - Outstanding
                                            Else
                                                .sales_amount = dclTotalPayment
                                                paymentFees(j).AllocatedAmount = paymentFees(j).AllocatedAmount + dclTotalPayment
                                                dclTotalPayment = 0
                                            End If

                                        End With
                                        Allocations.Add(Allocation)
                                    End If
                                End If

                            End If
                        End With
                    Next

                    'Allocate other fee
                    For j As Integer = 0 To fees.Count - 1
                        With fees(j)
                            If .void_date_time = Date.MinValue Then
                                Charge = .fee_amount_incl
                            Else
                                Charge = 0
                            End If
                            Paid = .payment_amount
                            Outstanding = (Charge - Paid) - fees(j).AllocatedAmount

                            If Not Outstanding = 0 Then
                                If dclTotalPayment = 0 Then
                                    Exit For
                                Else
                                    If .AllocatedAmount <> Outstanding Then
                                        Dim Allocation As New Allocation
                                        With Allocation
                                            .booking_fee_id = fees(j).booking_fee_id
                                            .user_id = GetUserId()
                                            .currency_rcd = fees(j).currency_rcd
                                            .booking_payment_id = payments(i).booking_payment_id
                                            .voucher_id = payments(i).voucher_payment_id
                                            If dclTotalPayment >= Outstanding Then
                                                .sales_amount = Outstanding
                                                fees(j).AllocatedAmount = fees(j).AllocatedAmount + Outstanding
                                                dclTotalPayment = dclTotalPayment - Outstanding
                                            Else
                                                .sales_amount = dclTotalPayment
                                                fees(j).AllocatedAmount = fees(j).AllocatedAmount + dclTotalPayment
                                                dclTotalPayment = 0
                                            End If

                                        End With
                                        Allocations.Add(Allocation)
                                    End If
                                End If
                            End If
                        End With
                    Next
                End If
            End If
        Next

        Return Allocations
    End Function
    Private Function GetPaymentAllocations(ByVal mappings As Mappings, ByVal fees As Fees, ByVal bAddFeeId As Boolean, ByVal paymentFees As Fees) As Allocations
        Dim Paid As Double
        Dim Charge As Double
        Dim Outstanding As Double
        Dim Allocations As New Allocations

        If mappings Is Nothing OrElse mappings.Count = 0 Then
        Else
            For Each Mapping As Mapping In mappings
                With Mapping

                    If Not .exclude_pricing_flag = 0 Then
                        Charge = .payment_amount
                    ElseIf Not .refund_date_time = Date.MinValue Then
                        Charge = .refund_charge
                    Else
                        Dim ExchangedPaid As Double = 0
                        For Each ExchangedMapping As Mapping In mappings
                            If Not ExchangedMapping.exchanged_date_time = Date.MinValue AndAlso Mapping.booking_segment_id.ToString = ExchangedMapping.exchanged_segment_id.ToString AndAlso Mapping.passenger_id.ToString = ExchangedMapping.passenger_id.ToString Then
                                ExchangedPaid = ExchangedMapping.payment_amount
                                Exit For
                            End If
                        Next
                        Charge = .net_total - ExchangedPaid
                    End If
                    Paid = .payment_amount
                    Outstanding = Charge - Paid
                End With
                If Not Outstanding = 0 Then
                    Dim Allocation As New Allocation
                    With Allocation
                        .passenger_id = Mapping.passenger_id
                        .booking_segment_id = Mapping.booking_segment_id
                        .user_id = GetUserId()
                        .currency_rcd = Mapping.currency_rcd
                        .sales_amount = Outstanding
                    End With
                    Allocations.Add(Allocation)
                End If
            Next
        End If

        'Payment Fee
        If paymentFees Is Nothing OrElse paymentFees.Count = 0 Then
        Else
            For Each Fee As Fee In paymentFees
                With Fee
                    If .void_date_time = Date.MinValue Then
                        Charge = .fee_amount_incl
                    Else
                        Charge = 0
                    End If
                    Paid = .payment_amount
                    Outstanding = Charge - Paid
                End With
                If Not Outstanding = 0 Then
                    Dim Allocation As New Allocation
                    With Allocation
                        .booking_fee_id = Fee.booking_fee_id
                        If bAddFeeId = True Then
                            .fee_id = Fee.fee_id
                        Else
                            If Fee.booking_fee_id.Equals(Guid.Empty) Then
                                .fee_id = Fee.fee_id
                            End If
                        End If
                        .passenger_id = Fee.passenger_id
                        .booking_segment_id = Fee.booking_segment_id
                        .user_id = GetUserId()
                        .currency_rcd = Fee.currency_rcd
                        .sales_amount = Outstanding
                    End With
                    Allocations.Add(Allocation)
                End If
            Next
        End If

        'Other Fee
        If fees Is Nothing OrElse fees.Count = 0 Then
        Else
            For Each Fee As Fee In fees
                With Fee
                    If .void_date_time = Date.MinValue Then
                        Charge = .fee_amount_incl
                    Else
                        Charge = 0
                    End If
                    Paid = .payment_amount
                    Outstanding = Charge - Paid
                End With
                If Not Outstanding = 0 Then
                    Dim Allocation As New Allocation
                    With Allocation
                        .booking_fee_id = Fee.booking_fee_id
                        .passenger_id = Fee.passenger_id
                        .booking_segment_id = Fee.booking_segment_id
                        .user_id = GetUserId()
                        .currency_rcd = Fee.currency_rcd
                        .sales_amount = Outstanding
                    End With
                    Allocations.Add(Allocation)
                End If
            Next
        End If
        Return Allocations
    End Function

    Private Function ValidateSave(ByVal rsBookingHeaderUpdate As ADODB.Recordset, ByVal rsItinerary As ADODB.Recordset, ByVal rsPassenger As ADODB.Recordset, ByVal rsMapping As ADODB.Recordset)
        If IsNothing(rsBookingHeaderUpdate) = True Then
            Return False
        ElseIf IsNothing(rsItinerary) = True Then
            Return False
        ElseIf IsNothing(rsPassenger) = True Then
            Return False
        ElseIf IsNothing(rsMapping) = True Then
            Return False
        Else
            If rsItinerary.RecordCount = 0 Then
                Return False
            ElseIf rsPassenger.RecordCount = 0 Then
                Return False
            ElseIf rsMapping.RecordCount = 0 Then
                Return False
            ElseIf rsBookingHeaderUpdate.RecordCount = 0 Then
                Return False
            Else
                If rsBookingHeaderUpdate.Fields("group_name").Value.ToString().Trim() = String.Empty Then
                    'Check for empty lastname
                    With rsPassenger
                        .MoveFirst()
                        While Not .EOF
                            If .Fields("lastname").Value Is System.DBNull.Value Then
                                .MoveFirst()
                                .Filter = 0
                                Return False
                            ElseIf Len(.Fields("lastname").Value) = 0 Then
                                .MoveFirst()
                                .Filter = 0
                                Return False
                            End If
                            .MoveNext()
                        End While
                        .MoveFirst()
                        .Filter = 0
                    End With

                    With rsMapping
                        .MoveFirst()
                        While Not .EOF
                            If .Fields("lastname").Value Is System.DBNull.Value Then
                                .MoveFirst()
                                .Filter = 0
                                Return False
                            ElseIf Len(.Fields("lastname").Value) = 0 Then
                                .MoveFirst()
                                .Filter = 0
                                Return False
                            End If
                            .MoveNext()
                        End While
                        .MoveFirst()
                        .Filter = 0
                    End With
                End If
            End If
        End If

        Return True
    End Function
    Private Function CalculateFees(ByVal AgencyCode As String,
                                    ByVal strCurrency As String,
                                    ByVal strBookingId As String,
                                    ByVal XmlHeader As String,
                                    ByVal XmlSegments As String,
                                    ByVal XmlPassengers As String,
                                    ByVal XmlFees As String,
                                    ByVal XmlRemarks As String,
                                    ByVal XmlPayments As String,
                                    ByVal XmlMapping As String,
                                    ByVal XMLServices As String,
                                    ByVal XmlTaxes As String,
                                    ByVal strCalculateType As String,
                                    ByVal strLanguage As String,
                                    ByVal bNoVat As Boolean) As String
        Using ds As New DataSet
            Dim rsFeesClone As New ADODB.Recordset
            Try
                If (AgentHeader Is Nothing) Then
                Else
                    Dim strCode As String = AgentHeader.AgencyCode
                    Dim strPassport As String = AgentHeader.AgencyPassport
                    If (AuthenticatePassport(strCode, strPassport)) Then
                        Dim fr As New FabricateRecordset

                        Dim clsRunCom As New clsRunComplus

                        Dim rsHeader As ADODB.Recordset = Session("rsHeader")
                        Dim rsSegments As ADODB.Recordset = Session("rsSegments")
                        Dim rsPassengers As ADODB.Recordset = Session("rsPassenger")
                        Dim rsRemarks As ADODB.Recordset = Session("rsRemarks")
                        Dim rsPayments As ADODB.Recordset = Session("rsPayments")
                        Dim rsMappings As ADODB.Recordset = Session("rsMappings")
                        Dim rsTaxes As ADODB.Recordset = Session("rsTaxes")
                        Dim rsQuotes As ADODB.Recordset = Session("rsQuotes")
                        Dim rsServices As ADODB.Recordset = Session("rsServices")
                        Dim rsFees As ADODB.Recordset = Session("rsFees")

                        Dim r As New Helper.RecordSet

                        Dim BookingHeaderUpdate As New BookingHeaderUpdate

                        Dim Itinerary As New Itinerary
                        Dim Mappings As New Mappings
                        Dim Passengers As New Passengers
                        Dim PaymentsUpdate As New PaymentsUpdate
                        Dim Remarks As New Remarks
                        Dim MappingsUpdate As New MappingsUpdate
                        Dim Fees As New Fees
                        Dim Taxes As New Taxes
                        Dim Services As New Services

                        XmlHeader = XmlHeader.Replace("BookingHeader", "BookingHeaderUpdate")
                        BookingHeaderUpdate = Serialization.Deserialize(XmlHeader, BookingHeaderUpdate)
                        r.FillHeader(rsHeader, BookingHeaderUpdate)

                        XmlMapping = XmlMapping.Replace("Mapping", "MappingUpdate")
                        MappingsUpdate = Serialization.Deserialize(XmlMapping, MappingsUpdate)
                        r.FillMapping(rsMappings, MappingsUpdate)

                        Services = Serialization.Deserialize(XMLServices, Services)
                        r.ClearServices(rsServices, Services)
                        r.FillServices(rsServices, Services)

                        Fees = Serialization.Deserialize(XmlFees, Fees)

                        r.ClearFees(rsFees, Fees)

                        If strCalculateType.ToUpper <> "SSR" Then
                            Passengers = Serialization.Deserialize(XmlPassengers, Passengers)
                            r.FillPassengers(rsPassengers, Passengers)
                            r.FillFees(rsFees, Fees)
                        End If

                        If strCalculateType = "CREATEBOOKING" Then
                            clsRunCom.CalculateBookingCreateFees(AgencyCode,
                                                                 strCurrency,
                                                                 strBookingId,
                                                                 rsHeader,
                                                                 rsSegments,
                                                                 rsPassengers,
                                                                 rsFees,
                                                                 rsRemarks,
                                                                 rsPayments,
                                                                 rsMappings,
                                                                 rsServices,
                                                                 rsTaxes,
                                                                 strLanguage,
                                                                 bNoVat)
                            fr.CopyAdoRecordset(rsFees, rsFeesClone)
                        ElseIf strCalculateType = "UPDATEBOOKING" Then
                            clsRunCom.CalculateBookingChangeFees(AgencyCode,
                                                                 strCurrency,
                                                                 strBookingId,
                                                                 rsHeader,
                                                                 rsSegments,
                                                                 rsPassengers,
                                                                 rsFees,
                                                                 rsRemarks,
                                                                 rsPayments,
                                                                 rsMappings,
                                                                 rsServices,
                                                                 rsTaxes,
                                                                 strLanguage,
                                                                 bNoVat)
                            fr.CopyAdoRecordset(rsFees, rsFeesClone)
                        ElseIf strCalculateType = "NAMECHANGE" Then
                            clsRunCom.CalculateNameChangeFees(AgencyCode,
                                                              strCurrency,
                                                              strBookingId,
                                                              rsHeader,
                                                              rsSegments,
                                                              rsPassengers,
                                                              rsFees,
                                                              rsRemarks,
                                                              rsPayments,
                                                              rsMappings,
                                                              rsServices,
                                                              rsTaxes,
                                                              strLanguage,
                                                              bNoVat)
                            fr.CopyAdoRecordset(rsFees, rsFeesClone)
                        ElseIf strCalculateType = "SEATASSIGN" Then
                            clsRunCom.CalculateSeatAssignmentFees(AgencyCode,
                                                                  strCurrency,
                                                                  strBookingId,
                                                                  rsHeader,
                                                                  rsSegments,
                                                                  rsPassengers,
                                                                  rsFees,
                                                                  rsRemarks,
                                                                  rsPayments,
                                                                  rsMappings,
                                                                  rsServices,
                                                                  rsTaxes,
                                                                  strLanguage,
                                                                  bNoVat)
                            fr.CopyAdoRecordset(rsFees, rsFeesClone)
                        ElseIf strCalculateType = "SSR" Then

                            fr.CopyAdoRecordset(rsFees, rsFeesClone)
                            r.FillFees(rsFeesClone, Fees)
                            clsRunCom.CalculateSpecialServiceFees(AgencyCode,
                                                                  strCurrency,
                                                                  strBookingId,
                                                                  rsHeader,
                                                                  rsServices,
                                                                  rsFeesClone,
                                                                  rsRemarks,
                                                                  rsMappings,
                                                                  strLanguage,
                                                                  bNoVat)
                        End If

                        Session("rsMappings") = rsMappings

                        Call InputRecordsetToDataset(rsFeesClone, "Fees", "Fee", ds)
                    End If
                End If
            Catch e As Exception
                If System.Configuration.ConfigurationManager.AppSettings("ErrorMailEnabled").ToUpper = "TRUE" Then
                    Dim ErrorSubject As String = System.Configuration.ConfigurationManager.AppSettings("ErrorSubject") & System.Convert.ToString(Date.Now)
                    Mail.Send(ErrorHelper.RenderDebugMessage("CalculateNewFees", e.Message, XmlHeader, XmlSegments, XmlPassengers, XmlPayments, XmlRemarks, XmlMapping, XmlFees, XmlTaxes, String.Empty, String.Empty), ErrorSubject, System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), System.Configuration.ConfigurationManager.AppSettings("ErrorSenderEmail"), Nothing)
                End If
            Finally
                Helper.Check.ClearRecordset(rsFeesClone)
            End Try

            If IsNothing(ds) = False Then
                Return ds.GetXml
            Else
                Return String.Empty
            End If
        End Using
    End Function
    Private Sub CreateErrorDataset(ByVal ds As DataSet, ByVal strCode As String, ByVal strMessage As String)

        If (IsNothing(ds) = False) Then
            ds.DataSetName = "ErrorResponse"
            If IsNothing(ds.Tables("Error")) = True Then
                Dim dt = New DataTable()

                Dim dcErrorCode As DataColumn
                Dim dcMessage As DataColumn

                'Set Table name
                dt.TableName = "Error"

                'Initialize the columns
                dcErrorCode = New DataColumn("ErrorCode")
                dcMessage = New DataColumn("Message")

                'Add them  to your DataTable
                dt.Columns.Add(dcErrorCode)
                dt.Columns.Add(dcMessage)

                'Add the DataTable to your DataSet
                ds.Tables.Add(dt)

                dcErrorCode.Dispose()
                dcMessage.Dispose()
            End If

            Dim drError As DataRow = ds.Tables("Error").NewRow

            drError("ErrorCode") = strCode
            drError("Message") = strMessage
            ds.Tables("Error").Rows.Add(drError)
        End If
    End Sub
    Private Function InfantExcessLimit(ByVal rsFlights As ADODB.Recordset, ByVal iInfant As Integer) As Boolean

        Dim clsRunCom As New clsRunComplus
        Dim rs As ADODB.Recordset

        With rsFlights
            .Filter = "segment_status_rcd = 'NN' or segment_status_rcd = null"
            While Not rsFlights.EOF

                If IsDBNull(rsFlights.Fields("boarding_class_rcd").Value) Then
                    rsFlights.Fields("boarding_class_rcd").Value = ""
                End If
                'Read infant limitation
                rs = clsRunCom.GetFlightLegInfants(.Fields("flight_id").Value,
                                                    .Fields("origin_rcd").Value,
                                                    .Fields("destination_rcd").Value,
                                                    .Fields("boarding_class_rcd").Value)
                If IsNothing(rs) = False Then
                    If rs.RecordCount > 0 Then
                        If Not rs.Fields("infant_capacity").Value Is DBNull.Value Then
                            If Val(rs.Fields("infant_capacity").Value) > 0 And iInfant > 0 Then
                                If Val(rs.Fields("available_infant").Value) < iInfant Then

                                    Marshal.ReleaseComObject(rs)
                                    rs = Nothing

                                    Return True
                                End If
                            End If
                        End If
                    End If
                End If
                Marshal.ReleaseComObject(rs)
                rs = Nothing
                .MoveNext()
            End While
            .Filter = 0
        End With

        Return False
    End Function
    Private Sub CreateBookingHistoryXml(ByRef xtw As XmlWriter, ByVal rs As ADODB.Recordset)

        If IsNothing(rs) = False AndAlso rs.RecordCount > 0 Then
            'Construct the availability xml and get the lowest fare.
            rs.MoveFirst()
            While Not rs.EOF
                xtw.WriteStartElement("ClientBooking")

                For i As Integer = 0 To rs.Fields.Count - 1
                    xtw.WriteStartElement(rs.Fields(i).Name)
                    If rs.Fields(i).Value Is System.DBNull.Value Then
                        xtw.WriteValue(String.Empty)
                    Else
                        xtw.WriteValue(rs.Fields(i).Value)
                    End If
                    xtw.WriteEndElement()
                Next

                xtw.WriteEndElement() 'ClientBooking
                rs.MoveNext()
            End While

        End If
    End Sub
    Private Function CreateBookingSaveInputErrorLog(ByVal BookingId As String,
                                                    ByVal XmlHeader As String,
                                                    ByVal XmlSegments As String,
                                                    ByVal XmlPassengers As String,
                                                    ByVal XmlPayments As String,
                                                    ByVal XmlRemarks As String,
                                                    ByVal XmlMapping As String,
                                                    ByVal XmlFees As String,
                                                    ByVal XmlPaymentFees As String,
                                                    ByVal XmlTaxes As String,
                                                    ByVal XmlServices As String,
                                                    ByVal CreateTickets As Boolean,
                                                    ByVal securityToken As String,
                                                    ByVal authenticationToken As String,
                                                    ByVal commerceIndicator As String,
                                                    ByVal strRequestSource As String,
                                                    ByVal strLanguage As String,
                                                    ByVal strCCResponse As String,
                                                    ByVal stSaveBookingResult As String)

        Dim root As XElement

        root = XElement.Parse(XmlPayments)
        Dim paymentUpdateElement As IEnumerable(Of XElement) = root.Elements("PaymentUpdate")

        For Each payment As XElement In paymentUpdateElement
            Dim document_number As String = payment.Elements("document_number").Value
            Dim cvv_code As String = payment.Elements("cvv_code").Value
            If (document_number.Length >= 3) Then

                Dim count As Integer = document_number.Length / 2
                Dim start As Integer = (document_number.Length / 3) - 1
                Dim output As String = document_number.Remove(start, count)
                Dim str As String = New String("X", count)
                payment.SetElementValue("document_number", output.Insert(start, str))
                payment.SetElementValue("cvv_code", New String("X", cvv_code.Length))
            End If
            XmlPayments = root.ToString()
        Next

        Return "Authorize Result : " & strCCResponse &
                "<br/>Save Booking Result : " & stSaveBookingResult &
                "<br/>Booking Id Input : " & BookingId &
                "<br/>BookingHeader Input : " & XmlHeader.Replace("<", "&lt;").Replace(">", "&gt;") &
                "<br/>FlightSegment Input : " & XmlSegments.Replace("<", "&lt;").Replace(">", "&gt;") &
                "<br/>Passenger Input : " & XmlPassengers.Replace("<", "&lt;").Replace(">", "&gt;") &
                "<br/>Payment Input : " & XmlPayments.Replace("<", "&lt;").Replace(">", "&gt;") &
                "<br/>Remark Input : " & XmlRemarks.Replace("<", "&lt;").Replace(">", "&gt;") &
                "<br/>Mapping Input : " & XmlMapping.Replace("<", "&lt;").Replace(">", "&gt;") &
                "<br/>Fee Input : " & XmlFees.Replace("<", "&lt;").Replace(">", "&gt;") &
                "<br/>PaymentFee Input : " & XmlPaymentFees.Replace("<", "&lt;").Replace(">", "&gt;") &
                "<br/>Taxes Input : " & XmlTaxes.Replace("<", "&lt;").Replace(">", "&gt;") &
                "<br/>Services Input : " & XmlServices.Replace("<", "&lt;").Replace(">", "&gt;") &
                "<br/>CreateTickets Input : " & CreateTickets.ToString() &
                "<br/>SecurityToken Input : " & securityToken &
                "<br/>AuthenticationToken Input : " & authenticationToken &
                "<br/>CommerceIndicator Input : " & commerceIndicator &
                "<br/>RequestSource : " & strRequestSource &
                "<br/>Language : " & strLanguage

    End Function
#End Region

End Class

Public Class FabricateRecordset
    '---- DataTypeEnum Values ----
    Const adEmpty = 0
    Const adTinyInt = 16
    Const adSmallInt = 2
    Const adInteger = 3
    Const adBigInt = 20
    Const adUnsignedTinyInt = 17
    Const adUnsignedSmallInt = 18
    Const adUnsignedInt = 19
    Const adUnsignedBigInt = 21
    Const adSingle = 4
    Const adDouble = 5
    Const adCurrency = 6
    Const adDecimal = 14
    Const adNumeric = 131
    Const adBoolean = 11
    Const adError = 10
    Const adUserDefined = 132
    Const adVariant = 12
    Const adIDispatch = 9
    Const adIUnknown = 13
    Const adGUID = 72
    Const adDate = 7
    Const adDBDate = 133
    Const adDBTime = 134
    Const adDBTimeStamp = 135
    Const adBSTR = 8
    Const adChar = 129
    Const adVarChar = 200
    Const adLongVarChar = 201
    Const adWChar = 130
    Const adVarWChar = 202
    Const adLongVarWChar = 203
    Const adBinary = 128
    Const adVarBinary = 204
    Const adLongVarBinary = 205
    Const adChapter = 136
    Const adFileTime = 64
    Const adDBFileTime = 137
    Const adPropVariant = 138
    Const adVarNumeric = 139

    '---- FieldAttributeEnum Values ----
    Const adFldMayDefer = &H2
    Const adFldUpdatable = &H4
    Const adFldUnknownUpdatable = &H8
    Const adFldFixed = &H10
    Const adFldIsNullable = &H20
    Const adFldMayBeNull = &H40
    Const adFldLong = &H80
    Const adFldRowID = &H100
    Const adFldRowVersion = &H200
    Const adFldCacheDeferred = &H1000
    Const adFldKeyColumn = &H8000

    Const adStateOpen = 1
    Const adBookmarkFirst = 1

    Public Function FabricatePaymentAllocationRecordset() As ADODB.Recordset
        FabricatePaymentAllocationRecordset = New ADODB.Recordset
        With FabricatePaymentAllocationRecordset.Fields
            .Append("booking_payment_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("passenger_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("booking_segment_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("booking_fee_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("voucher_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("fee_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("user_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("currency_rcd", adVarChar, 10, adFldIsNullable + adFldMayBeNull)
            .Append("sales_amount", adDouble, , adFldIsNullable + adFldMayBeNull)
            .Append("payment_amount", adDouble, , adFldIsNullable + adFldMayBeNull)
            .Append("account_amount", adDouble, , adFldIsNullable + adFldMayBeNull)

            .Append("passenger_segment_service_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("od_origin_rcd", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("od_destination_rcd", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("charge_amount", adDouble, , adFldIsNullable + adFldMayBeNull)
            .Append("charge_amount_incl", adDouble, , adFldIsNullable + adFldMayBeNull)
            .Append("charge_currency_rcd", adVarChar, 10, adFldIsNullable + adFldMayBeNull)
            .Append("fee_category_rcd", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("external_reference", adVarChar, 50, adFldIsNullable + adFldMayBeNull)
            .Append("vendor_rcd", adVarChar, 50, adFldIsNullable + adFldMayBeNull)
            .Append("weight_lbs", adNumeric, , adFldIsNullable + adFldMayBeNull)
            .Append("weight_kgs", adNumeric, , adFldIsNullable + adFldMayBeNull)
            .Append("units", adNumeric, , adFldIsNullable + adFldMayBeNull)
            .Append("create_by", adGUID, , adFldIsNullable + adFldMayBeNull)

        End With
        FabricatePaymentAllocationRecordset.Open()
    End Function

    Public Function FabricateFlightAvailabilityRecordset() As ADODB.Recordset
        FabricateFlightAvailabilityRecordset = New ADODB.Recordset
        With FabricateFlightAvailabilityRecordset.Fields
            .Append("flight_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("fare_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("airline_rcd", adVarChar, 3, adFldIsNullable + adFldMayBeNull)
            .Append("flight_number", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("departure_date", adDate, , adFldIsNullable + adFldMayBeNull)
            .Append("departure_time", adInteger, , adFldIsNullable + adFldMayBeNull)
            .Append("planned_departure_date", adDate, , adFldIsNullable + adFldMayBeNull)
            .Append("planned_departure_time", adInteger, , adFldIsNullable + adFldMayBeNull)
            .Append("planned_arrival_date", adDate, , adFldIsNullable + adFldMayBeNull)
            .Append("planned_arrival_time", adInteger, , adFldIsNullable + adFldMayBeNull)
            .Append("flight_duration", adInteger, , adFldIsNullable + adFldMayBeNull)
            .Append("adult_fare", adDecimal, , adFldIsNullable + adFldMayBeNull)
            .Append("child_fare", adDecimal, , adFldIsNullable + adFldMayBeNull)
            .Append("infant_fare", adDecimal, , adFldIsNullable + adFldMayBeNull)
            .Append("origin_rcd", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("destination_rcd", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("booking_segment_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("boarding_class_rcd", adVarChar, 2, adFldIsNullable + adFldMayBeNull)
            .Append("booking_class_rcd", adVarChar, 2, adFldIsNullable + adFldMayBeNull)
            .Append("class_open_flag", adInteger, adFldIsNullable + adFldMayBeNull)
            .Append("nested_book_available", adInteger, , adFldIsNullable + adFldMayBeNull)
            .Append("flight_connection_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("od_origin_rcd", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("od_destination_rcd", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("ignore_logic_flag", adInteger, , adFldIsNullable + adFldMayBeNull)
            .Append("it_fare_flag", adInteger, , adFldIsNullable + adFldMayBeNull)
        End With
        FabricateFlightAvailabilityRecordset.Open()
    End Function

    Public Function FabricateHeaderRecordset() As ADODB.Recordset
        FabricateHeaderRecordset = New ADODB.Recordset
        With FabricateHeaderRecordset.Fields
            .Append("booking_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("currency_rcd", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("client_profile_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("booking_number", adNumeric, , adFldIsNullable + adFldMayBeNull)
            .Append("record_locator", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("number_of_adults", adInteger, , adFldIsNullable + adFldMayBeNull)
            .Append("number_of_children", adInteger, , adFldIsNullable + adFldMayBeNull)
            .Append("number_of_infants", adInteger, , adFldIsNullable + adFldMayBeNull)
            .Append("language_rcd", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("agency_code", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("contact_name", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("contact_email", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("phone_mobile", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("phone_home", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("phone_business", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("received_from", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("phone_fax", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("phone_search", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("comment", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("notify_by_email_flag", adBoolean, , adFldIsNullable + adFldMayBeNull)
            .Append("notify_by_sms_flag", adBoolean, , adFldIsNullable + adFldMayBeNull)
            .Append("group_name", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("group_booking_flag", adBoolean, , adFldIsNullable + adFldMayBeNull)
            .Append("agency_name", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("own_agency_flag", adBoolean, , adFldIsNullable + adFldMayBeNull)
            .Append("web_agency_flag", adBoolean, , adFldIsNullable + adFldMayBeNull)
            .Append("client_number", adNumeric, , adFldIsNullable + adFldMayBeNull)
            .Append("lastname", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("firstname", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("city", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("create_name", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("update_name", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
        End With
        FabricateHeaderRecordset.Open()
    End Function

    Public Function FabricateSegmentRecordset() As ADODB.Recordset
        FabricateSegmentRecordset = New ADODB.Recordset
        With FabricateSegmentRecordset.Fields
            .Append("flight_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("fare_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("booking_segment_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("origin_rcd", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("destination_rcd", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("airline_rcd", adVarChar, 3, adFldIsNullable + adFldMayBeNull)
            .Append("flight_number", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("boarding_class_rcd", adVarChar, 2, adFldIsNullable + adFldMayBeNull)
            .Append("booking_class_rcd", adVarChar, 2, adFldIsNullable + adFldMayBeNull)
            .Append("departure_date", adDate, , adFldIsNullable + adFldMayBeNull)
            .Append("fare_reduction", adInteger, , adFldIsNullable + adFldMayBeNull)
            .Append("waitlist_flag", adBoolean, , adFldIsNullable + adFldMayBeNull)
            .Append("refundable_flag", adBoolean, , adFldIsNullable + adFldMayBeNull)
            .Append("non_revenue_flag", adBoolean, , adFldIsNullable + adFldMayBeNull)
            .Append("eticket_flag", adBoolean, , adFldIsNullable + adFldMayBeNull)
            .Append("group_flag", adBoolean, , adFldIsNullable + adFldMayBeNull)
            .Append("overbook_flag", adBoolean, , adFldIsNullable + adFldMayBeNull)
            .Append("exchanged_segment_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("segment_status_rcd", adVarChar, 3, adFldIsNullable + adFldMayBeNull)
            .Append("exclude_quote_flag", adUnsignedTinyInt, 1, adFldIsNullable + adFldMayBeNull)
            .Append("advanced_purchase_flag", adBoolean, , adFldIsNullable + adFldMayBeNull)
            'change
            .Append("flight_connection_id", adGUID, , adFldIsNullable + adFldMayBeNull)
            .Append("od_origin_rcd", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("od_destination_rcd", adVarChar, 5, adFldIsNullable + adFldMayBeNull)
            .Append("create_by", adGUID, 16, adFldIsNullable + adFldMayBeNull)
            .Append("create_date_time", adDBTimeStamp, 16, adFldIsNullable + adFldMayBeNull)
            .Append("update_by", adGUID, 16, adFldIsNullable + adFldMayBeNull)
            .Append("update_date_time", adDBTimeStamp, 16, adFldIsNullable + adFldMayBeNull)
            .Append("priority_rcd", adVarChar, 20, adFldIsNullable + adFldMayBeNull)
        End With
        FabricateSegmentRecordset.Open()
    End Function

    Public Function FabricatePaymentRecordset() As ADODB.Recordset
        FabricatePaymentRecordset = New ADODB.Recordset
        With FabricatePaymentRecordset.Fields
            .Append("booking_payment_id", adGUID, 16, adFldIsNullable + adFldMayBeNull)
            .Append("booking_id", adGUID, 16, adFldIsNullable + adFldMayBeNull)
            .Append("form_of_payment_rcd", adVarChar, 20, adFldIsNullable + adFldMayBeNull)
            .Append("currency_rcd", adVarChar, 20, adFldIsNullable + adFldMayBeNull)
            .Append("agency_code", adVarChar, 20, adFldIsNullable + adFldMayBeNull)
            .Append("debit_agency_code", adVarChar, 20, adFldIsNullable + adFldMayBeNull)
            .Append("payment_amount", adNumeric, 19, adFldIsNullable + adFldMayBeNull)
            .Append("acct_payment_amount", adNumeric, 19, adFldIsNullable + adFldMayBeNull)
            .Append("payment_by", adGUID, 16, adFldIsNullable + adFldMayBeNull)
            .Append("payment_date_time", adDBTimeStamp, 16, adFldIsNullable + adFldMayBeNull)
            .Append("payment_due_date_time", adDBTimeStamp, 16, adFldIsNullable + adFldMayBeNull)
            .Append("document_number", adVarWChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("approval_code", adVarChar, 20, adFldIsNullable + adFldMayBeNull)
            .Append("expiry_month", adInteger, 4, adFldIsNullable + adFldMayBeNull)
            .Append("expiry_year", adInteger, 4, adFldIsNullable + adFldMayBeNull)
            .Append("name_on_card", adVarWChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("due_date_flag", adUnsignedTinyInt, 1, adFldIsNullable + adFldMayBeNull)
            .Append("document_amount", adNumeric, 19, adFldIsNullable + adFldMayBeNull)
            .Append("form_of_payment_subtype_rcd", adVarChar, 20, adFldIsNullable + adFldMayBeNull)
            .Append("issue_date", adDBTimeStamp, 16, adFldIsNullable + adFldMayBeNull)
            .Append("payment_remark", adLongVarChar, 2147483647, adFldIsNullable + adFldMayBeNull)
            .Append("booking_reference", adVarWChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("cvv_code", adVarChar, 20, adFldIsNullable + adFldMayBeNull)
            .Append("issue_by", adVarWChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("void_by", adGUID, 16, adFldIsNullable + adFldMayBeNull)
            .Append("void_date_time", adDBTimeStamp, 16, adFldIsNullable + adFldMayBeNull)
            .Append("create_by", adGUID, 16, adFldIsNullable + adFldMayBeNull)
            .Append("create_date_time", adDBTimeStamp, 16, adFldIsNullable + adFldMayBeNull)
            .Append("update_by", adGUID, 16, adFldIsNullable + adFldMayBeNull)
            .Append("update_date_time", adDBTimeStamp, 16, adFldIsNullable + adFldMayBeNull)
            .Append("voucher_payment_id", adGUID, 16, adFldIsNullable + adFldMayBeNull)
            .Append("passenger_id", adGUID, 16, adFldIsNullable + adFldMayBeNull)
            .Append("payment_number", adBigInt, 8, adFldIsNullable + adFldMayBeNull)
            .Append("booking_segment_id", adGUID, 16, adFldIsNullable + adFldMayBeNull)
            .Append("payment_reference", adVarWChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("transaction_reference", adVarWChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("processed_authorization_flag", adUnsignedTinyInt, 1, adFldIsNullable + adFldMayBeNull)
            .Append("processed_settlement_flag", adUnsignedTinyInt, 1, adFldIsNullable + adFldMayBeNull)
            .Append("processed_credit_flag", adUnsignedTinyInt, 1, adFldIsNullable + adFldMayBeNull)
            .Append("processed_authorization_date_time", adDBTimeStamp, 16, adFldIsNullable + adFldMayBeNull)
            .Append("processed_settlement_date_time", adDBTimeStamp, 16, adFldIsNullable + adFldMayBeNull)
            .Append("processed_credit_date_time", adDBTimeStamp, 16, adFldIsNullable + adFldMayBeNull)
            .Append("refund_payment_flag", adUnsignedTinyInt, 1, adFldIsNullable + adFldMayBeNull)
            .Append("client_profile_id", adGUID, 16, adFldIsNullable + adFldMayBeNull)
            .Append("street", adVarWChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("state", adVarWChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("city", adVarWChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("zip_code", adVarWChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("country_rcd", adVarChar, 20, adFldIsNullable + adFldMayBeNull)
            .Append("receive_payment_amount", adNumeric, 19, adFldIsNullable + adFldMayBeNull)
            .Append("receive_currency_rcd", adVarChar, 20, adFldIsNullable + adFldMayBeNull)
            .Append("create_name", adVarWChar, 121, adFldIsNullable + adFldMayBeNull)
            .Append("update_name", adVarWChar, 121, adFldIsNullable + adFldMayBeNull)
            .Append("record_locator", adVarWChar, 60, adFldIsNullable + adFldMayBeNull)
        End With
        FabricatePaymentRecordset.Open()
    End Function

    Public Function FabricateMappingRecordset() As ADODB.Recordset
        FabricateMappingRecordset = New ADODB.Recordset
        With FabricateMappingRecordset.Fields
            .Append("passenger_id", adGUID)
            .Append("booking_segment_id", adGUID)
            .Append("passenger_type_rcd", adVarChar, 60)
            .Append("fare", adCurrency, , adFldIsNullable + adFldMayBeNull)
            .Append("yq", adCurrency, , adFldIsNullable + adFldMayBeNull)
            .Append("acct_fare", adCurrency, , adFldIsNullable + adFldMayBeNull)
            .Append("acct_yq", adCurrency, , adFldIsNullable + adFldMayBeNull)
            .Append("group_sequence", adInteger, , adFldIsNullable + adFldMayBeNull)
        End With
        FabricateMappingRecordset.Open()
    End Function

    Public Function FabricatePassengerRecordset() As ADODB.Recordset
        FabricatePassengerRecordset = New ADODB.Recordset
        With FabricatePassengerRecordset.Fields
            .Append("passenger_id", adGUID)
            .Append("flight_id", adGUID, 16) 'flight_id
            .Append("origin_rcd", adVarChar, 60, adFldIsNullable + adFldMayBeNull) 'origin
            .Append("lastname", adVarChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("firstname", adVarChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("title_rcd", adVarChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("passenger_type_rcd", adVarChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("passenger_check_in_status_rcd", adVarChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("destination_rcd", adVarChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("class_rcd", adVarChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("gender_type_rcd", adVarChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("action", adVarChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("record_locator", adVarChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("create_by", adGUID, 16, adFldIsNullable + adFldMayBeNull)
            .Append("create_date_time", adDBTimeStamp, 16, adFldIsNullable + adFldMayBeNull)
            .Append("update_by", adGUID, 16, adFldIsNullable + adFldMayBeNull)
            .Append("update_date_time", adDBTimeStamp, 16, adFldIsNullable + adFldMayBeNull)
        End With
        FabricatePassengerRecordset.Open()
    End Function

    Public Function FabricateQuotesRecordset() As ADODB.Recordset
        FabricateQuotesRecordset = New ADODB.Recordset
        With FabricateQuotesRecordset.Fields
            .Append("booking_segment_id", adGUID, 16, adFldIsNullable + adFldMayBeNull)
            .Append("passenger_type_rcd", adVarChar, 20, adFldIsNullable + adFldMayBeNull)
            .Append("currency_rcd", adVarChar, 20, adFldIsNullable + adFldMayBeNull)
            .Append("charge_type", adVarChar, 20, adFldIsNullable + adFldMayBeNull)
            .Append("charge_name", adVarChar, 60, adFldIsNullable + adFldMayBeNull)
            .Append("charge_amount", adCurrency, 8, adFldIsNullable + adFldMayBeNull)
            .Append("total_amount", adCurrency, 8, adFldIsNullable + adFldMayBeNull)
            .Append("tax_amount", adCurrency, 8, adFldIsNullable + adFldMayBeNull)
            .Append("sort_sequence", adInteger, 4, adFldIsNullable + adFldMayBeNull)
        End With
        FabricateQuotesRecordset.Open()
    End Function

    Public Shared Function SerializeRecordSet(ByVal rs As ADODB.Recordset) As Byte()
        Dim s As New ADODB.Stream
        rs.Save(s, ADODB.PersistFormatEnum.adPersistADTG)
        Return s.Read(s.Size)
    End Function

    Public Sub SerializeAdoRecordsets(ByVal rsIn As ADODB.Recordset, ByRef rsOut As ADODB.Recordset)
        Dim rsByte() As Byte = SerializeRecordSet(rsIn)
        rsOut = Me.DeserializeRecordSet(rsByte)
    End Sub

    'Public Sub CopyAdoRecordset(ByVal rsIn As ADODB.Recordset, ByRef rsOut As ADODB.Recordset)
    '    Dim rsByte() As Byte = SerializeRecordSet(rsIn)
    '    rsOut = Me.DeserializeRecordSet(rsByte)
    'End Sub

    Public Shared Function DeserializeRecordSet(ByVal data As Byte()) As ADODB.Recordset
        Dim s As New ADODB.Stream
        s.Open(System.Reflection.Missing.Value, ConnectModeEnum.adModeUnknown, ADODB.StreamOpenOptionsEnum.adOpenStreamUnspecified, "", "")
        s.Type = ADODB.StreamTypeEnum.adTypeBinary
        s.Write(data)
        s.Position = 0
        Dim rs As New ADODB.Recordset
        rs.Open(s, System.Reflection.Missing.Value, CursorTypeEnum.adOpenUnspecified, LockTypeEnum.adLockUnspecified, -1)
        Return rs
    End Function

    Public Sub CopyAdoRecordset(ByVal adoOrigin As ADODB.Recordset, ByRef adoDestination As ADODB.Recordset)
        CopyADOStructure(adoOrigin, adoDestination)
        CopyADOData(adoOrigin, adoDestination)
    End Sub

    Private Sub CopyADOStructure(ByVal adoOrigin As ADODB.Recordset, ByRef adoDestination As ADODB.Recordset)
        Try
            Dim lFields As Integer
            Dim lLoop As Integer

            If adoOrigin Is Nothing Then
                adoDestination = Nothing
                Exit Sub
            End If

            If adoDestination Is Nothing Then adoDestination = New ADODB.Recordset

            lFields = adoOrigin.Fields.Count
            For lLoop = 0 To lFields - 1
                AppendField(adoDestination.Fields, adoOrigin.Fields(lLoop))
            Next lLoop
        Catch ex As Exception

        End Try
    End Sub

    Private Sub AppendField(ByVal colFields As ADODB.Fields, ByVal fldNewField As ADODB.Field)
        Try
            Dim lNewIndex As Integer

            With fldNewField

                If .Type = ADODB.DataTypeEnum.adNumeric Then
                    colFields.Append(.Name, ADODB.DataTypeEnum.adDecimal, .DefinedSize, .Attributes)
                    lNewIndex = colFields.Count - 1
                    colFields(lNewIndex).NumericScale = 2
                    colFields(lNewIndex).Precision = 10
                Else
                    colFields.Append(.Name, .Type, .DefinedSize, .Attributes)
                    lNewIndex = colFields.Count - 1
                    colFields(lNewIndex).NumericScale = .NumericScale
                    colFields(lNewIndex).Precision = .Precision
                End If

                If (colFields(lNewIndex).Attributes And adFldUnknownUpdatable) = adFldUnknownUpdatable Then
                    colFields(lNewIndex).Attributes = colFields(lNewIndex).Attributes - adFldUnknownUpdatable
                End If
                If (colFields(lNewIndex).Attributes And adFldUpdatable) <> adFldUpdatable Then
                    colFields(lNewIndex).Attributes = colFields(lNewIndex).Attributes + adFldUpdatable
                End If
            End With
        Catch ex As Exception

        End Try

    End Sub

    Private Sub CopyADOData(ByVal adoOrigin As ADODB.Recordset, ByRef adoDestination As ADODB.Recordset)
        Try
            Dim lFields As Integer
            Dim lLoop As Integer
            Dim vBookmark As Object
            Dim strFieldname As String

            If (adoOrigin Is Nothing) Or (adoDestination Is Nothing) Then Exit Sub

            If (adoDestination.State And adStateOpen) <> adStateOpen Then
                adoDestination.Open()
            End If

            If adoOrigin.RecordCount = 0 Then Exit Sub

            lFields = adoOrigin.Fields.Count
            With adoOrigin
                'there might not be a bookmark, in which we set it to the first
                ' record
                vBookmark = .Bookmark
                If (Err.Number <> 0) Then
                    vBookmark = adBookmarkFirst
                    Err.Clear()
                End If
                'resume normal error handling

                If .RecordCount > 0 Then .MoveFirst()
                Do While Not .EOF
                    adoDestination.AddNew()
                    For lLoop = 0 To lFields - 1
                        strFieldname = .Fields(lLoop).Name
                        If .Fields(lLoop).Value Is Nothing Then
                            adoDestination.Fields(strFieldname).Value = Nothing
                        ElseIf .Fields(lLoop).Type = adChapter Then
                            CopyADOData(.Fields(lLoop).Value, adoDestination.Fields(strFieldname).Value)
                        ElseIf (.Fields(lLoop).Attributes And adFldLong) = adFldLong Then
                            If .Fields(lLoop).ActualSize = 0 Then
                                adoDestination.Fields(strFieldname).Value = ""
                            Else
                                adoDestination.Fields(strFieldname).AppendChunk(.Fields(lLoop).GetChunk(.Fields(lLoop).ActualSize))
                            End If
                        Else
                            adoDestination.Fields(strFieldname).Value = .Fields(lLoop).Value
                        End If
                    Next lLoop
                    adoDestination.Update()
                    .MoveNext()
                Loop

                'there seem to be problems with bookmarks. Made mental note never to revisit
                .Bookmark = vBookmark
                'resume normal error handling

            End With
            If adoDestination.RecordCount > 0 Then adoDestination.MoveFirst()
        Catch ex As Exception
        End Try
    End Sub

    Public Sub New()

    End Sub
End Class

Public Class AgentAuthHeader : Inherits SoapHeader
    Public AgencyCode As String
    Public AgencyPassport As String
    Public AgencyCurrencyRcd As String
End Class


Public Class Mail
#Region "Mail"
    Public Shared Function Send(ByVal emailBody As String, ByVal emailSubject As String, ByVal fromEmailAddress As String, ByVal toEmailAddress As String, ByVal bbcEmailAddress() As String) As String
        Try
            Dim SmtpHost As String = System.Configuration.ConfigurationManager.AppSettings("SmtpServer")
            Dim SmtpPort As Integer = System.Configuration.ConfigurationManager.AppSettings("SmtpServerPort")
            Dim SenderEmail As String = fromEmailAddress
            Dim RecipientEmail As String = toEmailAddress
            Dim Subject As String = emailSubject
            Dim Body As String = emailBody


            'Imports System.Net.Mail
            'newversion Dim Msg As MailMessage = New MailMessage(SenderEmail, RecipientEmail)

            Dim Msg As New System.Net.Mail.MailMessage
            With Msg
                .Subject = Subject
                '.HtmlBody = Body
            End With

            'newversion Dim Smtp As Smtp = New Smtp(SmtpHost, SmtpPort)
            Dim Smtp As New System.Net.Mail.SmtpClient(SmtpHost, SmtpPort)
            'newversion SmtpConfig.VerifyAddresses = False
            With Smtp
                'newversion .SendMail(Msg)
                .Send(Msg)
            End With
            Return "OK"


        Catch mfa As System.Net.Mail.SmtpFailedRecipientsException
            Return "Smtp error occured: " & mfa.Message
        Catch se As System.Net.Mail.SmtpException
            Return "Smtp error occured: " & se.Message
        Catch e As Exception
            Return "Error occured: " & e.Message & "r\n" & e.ToString
        End Try
    End Function
#End Region
End Class


Public Class ErrorHelper
    Public Shared LastError As System.Exception

    Public Shared Function RenderDebugMessage(ByVal FunctionName As String, ByVal ErrorMessage As String, ByVal XmlHeader As String, ByVal XmlSegments As String, ByVal XmlPassengers As String, ByVal XmlPayments As String, ByVal XmlRemarks As String, ByVal XmlMapping As String, ByVal XmlFees As String, ByVal XmlTaxes As String, ByVal XmlQuotes As String, ByVal XmlAllocation As String) As String
        Dim strMessage As New StringBuilder
        strMessage.Append("<style type=""text/css"">")
        strMessage.Append("<!--")
        strMessage.Append(".basix {")
        strMessage.Append("font-family: Verdana, Arial, Helvetica, sans-serif;")
        strMessage.Append("font-size: 12px;")
        strMessage.Append("}")
        strMessage.Append(".header1 {")
        strMessage.Append("font-family: Verdana, Arial, Helvetica, sans-serif;")
        strMessage.Append("font-size: 12px;")
        strMessage.Append("font-weight: bold;")
        strMessage.Append("color: #000099;")
        strMessage.Append("}")
        strMessage.Append(".tlbbkground1 {")
        strMessage.Append("background-color: #000099;")
        strMessage.Append("}")
        strMessage.Append("-->")
        strMessage.Append("</style>")
        strMessage.Append("<table width=""85%"" border=""0"" align=""center"" cellpadding=""5"" cellspacing=""1"" class=""tlbbkground1"">")
        strMessage.Append("<tr bgcolor=""#eeeeee"">")
        strMessage.Append("<td colspan=""2"" class=""header1"">Webservice Log: " & FunctionName & "</td>")
        strMessage.Append("</tr>")
        strMessage.Append("<tr>")
        strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Time</td>")
        strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & System.DateTime.Now & " EST</td>")
        strMessage.Append("</tr>")

        strMessage.Append("<tr>")
        strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>ErrorMessage</td>")
        strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & ErrorMessage & "</td>")
        strMessage.Append("</tr>")

        strMessage.Append("<tr>")
        strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Header</td>")
        strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & XmlHeader.Replace("><", "<br>").Replace("<", "&lt;").Replace(">", "&gt;") & "</td>")
        strMessage.Append("</tr>")
        strMessage.Append("<tr>")

        strMessage.Append("<tr>")
        strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Segments</td>")
        strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & XmlSegments.Replace("><", "<br>").Replace("<", "&lt;").Replace(">", "&gt;") & "</td>")
        strMessage.Append("</tr>")
        strMessage.Append("<tr>")

        strMessage.Append("<tr>")
        strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Passengers</td>")
        strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & XmlPassengers.Replace("><", "<br>").Replace("<", "&lt;").Replace(">", "&gt;") & "</td>")
        strMessage.Append("</tr>")
        strMessage.Append("<tr>")

        strMessage.Append("<tr>")
        strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Payments</td>")
        strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & XmlPayments.Replace("><", "<br>").Replace("<", "&lt;").Replace(">", "&gt;") & "</td>")
        strMessage.Append("</tr>")
        strMessage.Append("<tr>")

        strMessage.Append("<tr>")
        strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Remarks</td>")
        strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & XmlRemarks.Replace("><", "<br>").Replace("<", "&lt;").Replace(">", "&gt;") & "</td>")
        strMessage.Append("</tr>")
        strMessage.Append("<tr>")

        strMessage.Append("<tr>")
        strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Mapping</td>")
        strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & XmlMapping.Replace("><", "<br>").Replace("<", "&lt;").Replace(">", "&gt;") & "</td>")
        strMessage.Append("</tr>")
        strMessage.Append("<tr>")

        strMessage.Append("<tr>")
        strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Fees</td>")
        strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & XmlFees.Replace("><", "<br>").Replace("<", "&lt;").Replace(">", "&gt;") & "</td>")
        strMessage.Append("</tr>")
        strMessage.Append("<tr>")

        strMessage.Append("<tr>")
        strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Taxes</td>")
        strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & XmlTaxes.Replace("><", "<br>").Replace("<", "&lt;").Replace(">", "&gt;") & "</td>")
        strMessage.Append("</tr>")
        strMessage.Append("<tr>")

        strMessage.Append("<tr>")
        strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Quotes</td>")
        strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & XmlQuotes.Replace("><", "<br>").Replace("<", "&lt;").Replace(">", "&gt;") & "</td>")
        strMessage.Append("</tr>")
        strMessage.Append("<tr>")

        strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Allocation</td>")
        strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & XmlAllocation.Replace("><", "<br>").Replace("<", "&lt;").Replace(">", "&gt;") & "</td>")
        strMessage.Append("</tr>")
        strMessage.Append("</table>")
        Return strMessage.ToString
    End Function

    Public Shared Function RenderCcDebugMessage(ByVal FunctionName As String, ByVal ErrorMessage As String, ByVal XmlHeader As String, ByVal XmlSegments As String, ByVal XmlPassengers As String, ByVal XmlPayments As String, ByVal XmlRemarks As String, ByVal XmlMapping As String, ByVal XmlFees As String, ByVal XmlTaxes As String, ByVal XmlQuotes As String, ByVal XmlAllocation As String) As String
        Dim strMessage As New StringBuilder
        strMessage.Append("<style type=""text/css"">")
        strMessage.Append("<!--")
        strMessage.Append(".basix {")
        strMessage.Append("font-family: Verdana, Arial, Helvetica, sans-serif;")
        strMessage.Append("font-size: 12px;")
        strMessage.Append("}")
        strMessage.Append(".header1 {")
        strMessage.Append("font-family: Verdana, Arial, Helvetica, sans-serif;")
        strMessage.Append("font-size: 12px;")
        strMessage.Append("font-weight: bold;")
        strMessage.Append("color: #000099;")
        strMessage.Append("}")
        strMessage.Append(".tlbbkground1 {")
        strMessage.Append("background-color: #000099;")
        strMessage.Append("}")
        strMessage.Append("-->")
        strMessage.Append("</style>")
        strMessage.Append("<table width=""85%"" border=""0"" align=""center"" cellpadding=""5"" cellspacing=""1"" class=""tlbbkground1"">")
        strMessage.Append("<tr bgcolor=""#eeeeee"">")
        strMessage.Append("<td colspan=""2"" class=""header1"">Webservice Log: " & FunctionName & "</td>")
        strMessage.Append("</tr>")
        strMessage.Append("<tr>")
        strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Time</td>")
        strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & System.DateTime.Now & " EST</td>")
        strMessage.Append("</tr>")

        strMessage.Append("<tr>")
        strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Payment RecordSet</td>")
        strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & ErrorMessage.Replace("><", "<br>").Replace("<", "&lt;").Replace(">", "&gt;") & "</td>")
        strMessage.Append("</tr>")

        strMessage.Append("<tr>")
        strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Payments</td>")
        strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & XmlPayments.Replace("><", "<br>").Replace("<", "&lt;").Replace(">", "&gt;") & "</td>")
        strMessage.Append("</tr>")
        strMessage.Append("<tr>")

        strMessage.Append("</table>")
        Return strMessage.ToString
    End Function

    Public Shared Function RenderErrorEmail(ByVal strInput As String, ByVal strMessage As String, ByVal strTrace As String, ByVal strLocation As String, ByVal strFunctionName As String) As String
        Try
            Dim stbHtml As New StringBuilder()

            stbHtml.Append("<html xmlns='http://www.w3.org/1999/xhtml'>")
            stbHtml.Append("<body>")
            stbHtml.Append("<table width='400' border='0' cellpadding='4' cellspacing='2' bgcolor='#B4CBD6' style='margin:0px auto; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#304974;'>")
            stbHtml.Append("<tr>")
            stbHtml.Append("<td width='20%' valign='top' bgcolor='#FFFFFF' style='font-weight:bold;'>Location</td>")
            stbHtml.Append("<td valign='top' bgcolor='#FFFFFF'>" & strLocation & "</td>")
            stbHtml.Append("</tr>")
            stbHtml.Append("<tr>")
            stbHtml.Append("<td width='20%' valign='top' bgcolor='#FFFFFF' style='font-weight:bold;'>Function</td>")
            stbHtml.Append("<td valign='top' bgcolor='#FFFFFF'>" & strFunctionName & "</td>")
            stbHtml.Append("</tr>")
            stbHtml.Append("<tr>")
            stbHtml.Append("<td width='20%' valign='top' bgcolor='#FFFFFF' style='font-weight:bold;'>Input</td>")
            stbHtml.Append("<td valign='top' bgcolor='#FFFFFF'>")
            stbHtml.Append(strInput)
            stbHtml.Append("</td>")
            stbHtml.Append("</tr>")
            stbHtml.Append("<tr>")
            stbHtml.Append("<td width='20%' valign='top' bgcolor='#FFFFFF' style='font-weight:bold;'>Massage</td>")
            stbHtml.Append("<td valign='top' bgcolor='#FFFFFF'>")
            stbHtml.Append(strMessage)
            stbHtml.Append("</td>")
            stbHtml.Append("</tr>")
            stbHtml.Append("<tr>")
            stbHtml.Append("<td width='20%' valign='top' bgcolor='#FFFFFF' style='font-weight:bold;'>Trace</td>")
            stbHtml.Append("<td valign='top' bgcolor='#FFFFFF'>")
            stbHtml.Append(strTrace)
            stbHtml.Append("</td>")
            stbHtml.Append("</tr>")
            stbHtml.Append("</table>")
            stbHtml.Append("</body>")
            stbHtml.Append("</html>")

            Return stbHtml.ToString()
        Catch
            Return String.Empty
        End Try
    End Function

End Class



Imports System.Xml.Serialization
Imports System.Xml
Imports System.IO
Imports TikAero.DAL.BusinessObjects
Imports System.Text.RegularExpressions
Imports System.Runtime.InteropServices
Imports System
Imports ADODB


Namespace Helper
    Public Class Serialization
        Public Shared Function Serialize(ByVal o As Object) As String
            Try
                Dim s As XmlSerializer = New XmlSerializer(o.GetType())
                Dim w As StringWriter = New StringWriter
                s.Serialize(w, o)
                Return w.ToString
            Catch ex As Exception
                Return String.Empty
            End Try
        End Function

        Public Shared Function Deserialize(ByVal xml As String, ByVal o As Object) As Object
            Dim s As XmlSerializer = New XmlSerializer(o.GetType())
            xml = xml.Replace("encoding=""utf-16""", "encoding=""utf-8""")
            Dim m As System.IO.MemoryStream = New System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(xml))
            Return s.Deserialize(m)
        End Function

        Public Shared Function SerializeRecordset(ByVal rs As ADODB.Recordset, ByVal RootName As String) As String
            Dim ds As New DataSet
            Call FillToDataset(rs, "Test", ds)
            Return Helper.Serialization.Serialize(ds)
        End Function

        Public Shared Function SerializeToDataset(ByVal o As Object) As DataSet
            Dim s As XmlSerializer = New XmlSerializer(o.GetType)
            'Dim ss As XmlSchemas
            Dim m As MemoryStream = New MemoryStream
            s.Serialize(m, o)
            Dim ds As DataSet = New DataSet
            m.Position = 0
            ds.ReadXml(m)
            m.Close()
            Return ds
        End Function

        Public Shared Sub FillToDataset(ByVal rs As ADODB.Recordset, ByVal strTableName As String, ByRef ds As DataSet)
            Dim da As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter
            If Not rs Is Nothing Then
                da.Fill(ds, rs, 0)
                ds.Tables(0).TableName = strTableName
            End If
        End Sub

        Public Shared Sub FillBooking(ByVal ds As DataSet, ByRef Booking As Booking)
            AddPassenger(ds, Booking)
            AddFlightSegmentAndMapping(ds, Booking)
            Booking.Header = New Header
            AddBookingHeader(ds, Booking)
            Booking.Payments = New Payments
            AddPayments(ds, Booking)
            Booking.Remarks = New Remarks
            AddRemarks(ds, Booking)
            Booking.Taxes = New Taxes
            AddTax(ds, Booking)
            If Booking.Fees Is Nothing Then
                Booking.Fees = New Fees
                AddFee(ds.GetXml, Booking)
            End If
        End Sub

        Public Shared Sub AddCheckInPassenger(ByVal xml As String, ByVal CheckInPassengers As CheckInPassengers)
            Dim XmlDoc As New XmlDocument
            Dim nl As XmlNodeList
            XmlDoc.LoadXml(xml)
            nl = XmlDoc.SelectNodes("ArrayOfCheckinPassenger/CheckinPassenger")
            For Each n As XmlNode In nl
                Dim CheckInPassenger As New CheckinPassenger
                CheckInPassenger = Serialization.Deserialize(n.OuterXml, CheckInPassenger)
                CheckInPassengers.Add(CheckInPassenger)
            Next
        End Sub

        Public Shared Sub AddFee(ByVal xml As String, ByVal Booking As Booking)
            Dim XmlDoc As New XmlDocument
            Dim nl As XmlNodeList
            XmlDoc.LoadXml(xml)
            nl = XmlDoc.SelectNodes("Booking/Fee")
            For Each n As XmlNode In nl
                Dim Fee As New Fee
                Fee = Serialization.Deserialize(n.OuterXml, Fee)
                Booking.Fees.Add(Fee)
            Next
        End Sub

        Public Shared Sub AddPayments(ByVal ds As DataSet, ByVal Booking As Booking)
            Dim XmlDoc As New XmlDocument
            Dim nl As XmlNodeList
            XmlDoc.LoadXml(ds.GetXml())
            nl = XmlDoc.SelectNodes("Booking/Payment")
            For Each n As XmlNode In nl
                Dim Payment As New Payment
                Payment = Serialization.Deserialize(n.OuterXml, Payment)
                Booking.Payments.Add(Payment)
            Next
        End Sub

        Public Shared Sub AddRemarks(ByVal ds As DataSet, ByVal Booking As Booking)
            Dim XmlDoc As New XmlDocument
            Dim nl As XmlNodeList
            XmlDoc.LoadXml(ds.GetXml())
            nl = XmlDoc.SelectNodes("Booking/Remark")
            For Each n As XmlNode In nl
                Dim Remark As New Remark
                Remark = Helper.Serialization.Deserialize(n.OuterXml, Remark)
                Booking.Remarks.Add(Remark)
            Next
        End Sub

        Public Shared Sub AddClientBookings(ByVal ds As DataSet, ByVal ClientBookings As ClientBookings)
            Dim XmlDoc As New XmlDocument
            Dim nl As XmlNodeList
            XmlDoc.LoadXml(ds.GetXml())
            nl = XmlDoc.SelectNodes("ClientBookings/ClientBooking")
            For Each n As XmlNode In nl
                Dim ClientBooking As New ClientBooking
                ClientBooking = Helper.Serialization.Deserialize(n.OuterXml, ClientBooking)
                ClientBookings.Add(ClientBooking)
            Next
        End Sub

        Public Shared Sub AddClient(ByVal ds As DataSet, ByVal Booking As Booking)
            Dim XmlDoc As New XmlDocument
            Dim nl As XmlNodeList
            XmlDoc.LoadXml(ds.GetXml())
            nl = XmlDoc.SelectNodes("Booking/Client")
            For Each n As XmlNode In nl
                Dim Client As New Client
                Client = Helper.Serialization.Deserialize(n.OuterXml, Client)
                Booking.Client = Client
            Next
        End Sub

        Public Shared Sub AddBookingHeader(ByVal ds As DataSet, ByVal Booking As Booking)
            Dim XmlDoc As New XmlDocument
            Dim nl As XmlNodeList
            XmlDoc.LoadXml(ds.GetXml())
            nl = XmlDoc.SelectNodes("Booking/BookingHeader")
            For Each n As XmlNode In nl
                Dim BookingHeader As New BookingHeader
                BookingHeader = Helper.Serialization.Deserialize(n.OuterXml, BookingHeader)
                Booking.Header.BookingHeader = BookingHeader
            Next
        End Sub

        Public Shared Sub AddAvailabilityFlight(ByVal Xml As String, ByVal AvailabilityFlights As AvailabilityFlights)
            Dim XmlDoc As New XmlDocument
            Dim nl As XmlNodeList
            XmlDoc.LoadXml(Xml)
            nl = XmlDoc.SelectNodes("ArrayOfAvailabilityFlight/AvailabilityFlight")
            For Each n As XmlNode In nl
                Dim AvailabilityFlight As New AvailabilityFlight
                AvailabilityFlight = Serialization.Deserialize(n.OuterXml, AvailabilityFlight)
                AvailabilityFlights.Add(AvailabilityFlight)
            Next
        End Sub

        Public Shared Sub AddBookingHeader(ByVal xml As String, ByVal Booking As Booking)
            Dim XmlDoc As New XmlDocument
            Dim nl As XmlNodeList
            XmlDoc.LoadXml(xml)
            nl = XmlDoc.SelectNodes("Booking/BookingHeader")
            For Each n As XmlNode In nl
                Dim BookingHeader As New BookingHeader
                BookingHeader = Helper.Serialization.Deserialize(n.OuterXml, BookingHeader)
                Booking.Header.BookingHeader = BookingHeader
            Next
        End Sub

        Public Shared Sub AddFlightSegmentAndMapping(ByVal ds As DataSet, ByVal Booking As Booking)
            Dim XmlDoc As New XmlDocument
            Dim nl As XmlNodeList
            XmlDoc.LoadXml(ds.GetXml())
            nl = XmlDoc.SelectNodes("Booking/FlightSegment")
            If Not nl.Count = 0 Then
                Booking.Itinerary = New Itinerary
                For Each n As XmlNode In nl
                    Dim FlightSegment As New FlightSegment
                    FlightSegment = Helper.Serialization.Deserialize(n.OuterXml, FlightSegment)
                    Booking.Itinerary.Add(FlightSegment)
                Next
            End If
            nl = XmlDoc.SelectNodes("Booking/Mapping")
            If Not nl.Count = 0 Then
                Booking.Mappings = New Mappings
                For Each n As XmlNode In nl
                    Dim Mapping As New Mapping
                    Mapping = Helper.Serialization.Deserialize(n.OuterXml, Mapping)
                    Booking.Mappings.Add(Mapping)
                Next
            End If
        End Sub

        Public Shared Sub AddPassenger(ByVal ds As DataSet, ByVal Booking As Booking)
            Dim XmlDoc As New XmlDocument
            Dim nl As XmlNodeList
            XmlDoc.LoadXml(ds.GetXml())
            nl = XmlDoc.SelectNodes("Booking/Passenger")
            If Not nl.Count = 0 Then
                Booking.Passengers = New Passengers
                For Each n As XmlNode In nl
                    Dim Passenger As New Passenger
                    Passenger = Helper.Serialization.Deserialize(n.OuterXml, Passenger)
                    Booking.Passengers.Add(Passenger)
                Next
            End If
        End Sub

        Public Shared Sub AddPassenger(ByVal xml As String, ByVal Booking As Booking)
            Dim XmlDoc As New XmlDocument
            Dim nl As XmlNodeList
            XmlDoc.LoadXml(xml)
            nl = XmlDoc.SelectNodes("Booking/Passenger")
            For Each n As XmlNode In nl
                Dim Passenger As New Passenger
                Passenger = Helper.Serialization.Deserialize(n.OuterXml, Passenger)
                Booking.Passengers.Add(Passenger)
            Next
        End Sub

        Public Shared Sub FillAgencyUsers(ByVal xml As String, ByRef agencyUsersList As AgencyUsersList)
            Dim XmlDoc As New XmlDocument
            Dim nl As XmlNodeList
            XmlDoc.LoadXml(xml)
            nl = XmlDoc.SelectNodes("UsersList/UserList")
            For Each n As XmlNode In nl
                Dim UserList As New UserList
                UserList = Serialization.Deserialize(n.OuterXml, UserList)
                agencyUsersList.Add(UserList)
            Next
        End Sub

        Public Shared Function FillClientList(ByVal xml As String, ByRef ClientList As Client)
            Dim XmlDoc As New XmlDocument
            Dim nl As XmlNodeList
            XmlDoc.LoadXml(xml)
            nl = XmlDoc.SelectNodes("UpdateClient/Client")
            For Each n As XmlNode In nl
                ClientList = Serialization.Deserialize(n.OuterXml, ClientList)
            Next
            FillClientList = ClientList
        End Function
        Public Shared Function FillPassengerList(ByVal xml As String, ByRef passengers As Passengers)
            Dim XmlDoc As New XmlDocument
            Dim nl As XmlNodeList
            XmlDoc.LoadXml(xml)
            nl = XmlDoc.SelectNodes("PassengerList/Passenger")
            For Each n As XmlNode In nl
                Dim PassengerList As New Passenger
                PassengerList = Serialization.Deserialize(n.OuterXml, PassengerList)
                passengers.Add(PassengerList)
            Next
            FillPassengerList = passengers
        End Function

        Public Shared Sub AddTax(ByVal ds As DataSet, ByVal Booking As Booking)
            Dim XmlDoc As New XmlDocument
            Dim nl As XmlNodeList
            XmlDoc.LoadXml(ds.GetXml())
            nl = XmlDoc.SelectNodes("Booking/Tax")
            For Each n As XmlNode In nl
                Dim Tax As New Tax
                Tax = Serialization.Deserialize(n.OuterXml, Tax)
                Booking.Taxes.Add(Tax)
            Next
        End Sub
    End Class

    Class Check
        Private Shared GuidRegex As New Regex("^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled)
        Friend Shared Function IsGuid(ByVal candidate As String, ByRef output As Guid) As Boolean
            Dim isValid As Boolean = False
            output = Guid.Empty
            If Not (candidate Is Nothing) Then
                If GuidRegex.IsMatch(candidate) Then
                    output = New Guid(candidate)
                    isValid = True
                End If
            End If
            Return isValid
        End Function
        Friend Shared Sub ClearRecordset(ByRef rs As ADODB.Recordset)
            If rs IsNot Nothing Then
                If rs.State = ADODB.ObjectStateEnum.adStateOpen Then
                    rs.Close()
                End If
                Marshal.FinalReleaseComObject(rs)
                rs = Nothing
            End If
        End Sub
    End Class

    Public Class Util
        Public Shared Function CalOutStandingBalance(ByVal rsFees As ADODB.Recordset, ByVal rsMapping As ADODB.Recordset) As Decimal

            Dim TotalOutStanding As Decimal = 0
            Dim dclChargeAmount As Decimal = 0
            Dim dclPaymentAmount As Decimal = 0
            Dim dclTotalTicket As Decimal = 0
            Dim dclTotalPayment As Decimal = 0

            'Find Total Ticket
            If Not rsMapping Is Nothing And rsMapping.RecordCount > 0 Then
                rsMapping.MoveFirst()
                While Not rsMapping.EOF
                    dclChargeAmount = rsMapping("net_total").Value
                    If Not rsMapping("refund_date_time").Value Is DBNull.Value Then
                        dclChargeAmount = rsMapping("refund_charge").Value
                    ElseIf Not rsMapping("exclude_pricing_flag").Value Is DBNull.Value AndAlso Convert.ToByte(rsMapping("exclude_pricing_flag").Value) <> 0 Then
                        dclChargeAmount = 0
                    End If

                    'Find ticket payment amount
                    dclTotalTicket = dclTotalTicket + dclChargeAmount
                    If rsMapping("passenger_status_rcd").Value = "XX" And (rsMapping("ticket_number").Value Is DBNull.Value OrElse rsMapping("ticket_number").Value = "") Then
                        dclPaymentAmount = 0
                    Else
                        If Not rsMapping("payment_amount").Value Is DBNull.Value Then
                            dclPaymentAmount = rsMapping("payment_amount").Value
                        Else
                            dclPaymentAmount = 0
                        End If
                    End If

                    dclTotalPayment = dclTotalPayment + dclPaymentAmount
                    rsMapping.MoveNext()
                End While
            End If

            'Find Fee
            If Not rsFees Is Nothing And rsFees.RecordCount > 0 Then
                rsFees.MoveFirst()
                While Not rsFees.EOF
                    If rsFees("void_date_time").Value Is DBNull.Value Then
                        If Not rsFees("fee_amount_incl").Value Is DBNull.Value Then
                            dclTotalTicket = dclTotalTicket + Convert.ToDecimal(rsFees("fee_amount_incl").Value)
                        End If

                        If Not rsFees("payment_amount").Value Is DBNull.Value Then
                            dclTotalPayment = dclTotalPayment + Convert.ToDecimal(rsFees("payment_amount").Value)
                        End If
                    End If
                    rsFees.MoveNext()
                End While
            End If

            TotalOutStanding = dclTotalTicket - dclTotalPayment
            Return TotalOutStanding
        End Function

    End Class

    Public Class RsUtil
        Private Sub New()
        End Sub
        Public Shared Function SerializeRs(ByVal rs As ADODB.Recordset) As Byte()
            Dim s As New ADODB.Stream()
            rs.Save(s, ADODB.PersistFormatEnum.adPersistADTG)
            Return DirectCast(s.Read(s.Size), Byte())
        End Function
        Public Shared Function DeserializeRs(ByVal data As Byte()) As ADODB.Recordset
            Dim s As New ADODB.Stream()
            s.Open(, ConnectModeEnum.adModeUnknown, ADODB.StreamOpenOptionsEnum.adOpenStreamUnspecified, "", "")
            s.Type = ADODB.StreamTypeEnum.adTypeBinary
            s.Write(data)
            s.Position = 0
            Dim rs As New ADODB.Recordset()

            rs.Open(s, , CursorTypeEnum.adOpenUnspecified, LockTypeEnum.adLockUnspecified, -1)
            Return rs
        End Function
        Public Shared Function SaveRsToSession(ByVal rsSession As ADODB.Recordset) As Object
            If IsNothing(rsSession) = False Then
                If IsNothing(System.Configuration.ConfigurationManager.AppSettings("ConvertRecordset")) = False And System.Configuration.ConfigurationManager.AppSettings("ConvertRecordset") = "1" Then
                    Return SerializeRs(rsSession)
                Else
                    Return rsSession
                End If
            Else
                Return Nothing
            End If
        End Function
        Public Shared Function GetRsFromSession(ByVal rsSession As Object) As ADODB.Recordset
            If IsNothing(rsSession) = False Then
                If IsNothing(System.Configuration.ConfigurationManager.AppSettings("ConvertRecordset")) = False And System.Configuration.ConfigurationManager.AppSettings("ConvertRecordset") = "1" Then
                    Return DeserializeRs(rsSession)
                Else
                    Return CType(rsSession, ADODB.Recordset)
                End If
            Else
                Return Nothing
            End If
            
        End Function
    End Class

End Namespace
Imports System.Security
Imports System.Security.Cryptography
Imports System.Text

Public Class clsCrypto
    Dim myKey As String
    Dim des As New TripleDESCryptoServiceProvider
    Dim hashmd5 As New MD5CryptoServiceProvider

    Public Sub New()
        'Inserire codice di configurazione della classe
        Try
            myKey = "tik8848:260;?SuperDev@com9999%Foxspy"
        Catch
        End Try
    End Sub

    Public Function clsCrypto(ByVal strUser As String, _
                              ByVal testo As String, _
                              ByVal Operazione As Boolean) As String
        If Operazione Then
            clsCrypto = Cifra(strUser, testo)
        Else
            clsCrypto = DeCifra(strUser, testo)
        End If
    End Function

    Private Function DeCifra(ByVal strUser As String, ByVal testo As String) As String
        Try
            Dim result As String = ""
            des.Key = hashmd5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strUser & "_" & myKey))
            des.Mode = CipherMode.ECB
            Dim desdencrypt As ICryptoTransform = des.CreateDecryptor()
            Dim buff() As Byte = Convert.FromBase64String(testo)
            DeCifra = ASCIIEncoding.ASCII.GetString(desdencrypt.TransformFinalBlock(buff, 0, buff.Length))
        Catch
            DeCifra = ""
        End Try
    End Function

    Private Function Cifra(ByVal strUser As String, ByVal testo As String) As String
        Try
            des.Key = hashmd5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strUser & "_" & myKey))
            des.Mode = CipherMode.ECB
            Dim desdencrypt As ICryptoTransform = des.CreateEncryptor()
            Dim MyASCIIEncoding = New ASCIIEncoding
            Dim buff() As Byte = ASCIIEncoding.ASCII.GetBytes(testo)
            Cifra = Convert.ToBase64String(desdencrypt.TransformFinalBlock(buff, 0, buff.Length))
        Catch
            Cifra = ""
        End Try
    End Function

    Public Function EncryptString(Message As String, Passphrase As String) As String
        Dim Results As Byte()
        Dim UTF8 As New System.Text.UTF8Encoding()

        ' Step 1. We hash the passphrase using MD5
        ' We use the MD5 hash generator as the result is a 128 bit byte array
        ' which is a valid length for the TripleDES encoder we use below

        Dim HashProvider As New System.Security.Cryptography.MD5CryptoServiceProvider()
        Dim TDESKey As Byte() = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase))

        ' Step 2. Create a new TripleDESCryptoServiceProvider object
        Dim TDESAlgorithm As New System.Security.Cryptography.TripleDESCryptoServiceProvider()

        ' Step 3. Setup the encoder
        TDESAlgorithm.Key = TDESKey
        TDESAlgorithm.Mode = System.Security.Cryptography.CipherMode.ECB
        TDESAlgorithm.Padding = System.Security.Cryptography.PaddingMode.PKCS7

        ' Step 4. Convert the input string to a byte[]
        Dim DataToEncrypt As Byte() = UTF8.GetBytes(Message)

        ' Step 5. Attempt to encrypt the string
        Try
            Dim Encryptor As System.Security.Cryptography.ICryptoTransform = TDESAlgorithm.CreateEncryptor()
            Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length)
        Finally
            ' Clear the TripleDes and Hashprovider services of any sensitive information
            TDESAlgorithm.Clear()
            HashProvider.Clear()
        End Try

        ' Step 6. Return the encrypted string as a base64 encoded string
        Return Convert.ToBase64String(Results)
    End Function

    Public Function DecryptString(ByVal Message As String, ByVal Passphrase As String) As String
        Dim strResult As String = String.Empty
        Dim Results As Byte()
        Dim UTF8 As New System.Text.UTF8Encoding()

        ' Step 1. We hash the passphrase using MD5
        ' We use the MD5 hash generator as the result is a 128 bit byte array
        ' which is a valid length for the TripleDES encoder we use below

        Dim HashProvider As New System.Security.Cryptography.MD5CryptoServiceProvider()
        Dim TDESKey As Byte() = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase))

        ' Step 2. Create a new TripleDESCryptoServiceProvider object
        Dim TDESAlgorithm As New System.Security.Cryptography.TripleDESCryptoServiceProvider()

        ' Step 3. Setup the decoder
        TDESAlgorithm.Key = TDESKey
        TDESAlgorithm.Mode = System.Security.Cryptography.CipherMode.ECB
        TDESAlgorithm.Padding = System.Security.Cryptography.PaddingMode.PKCS7

        Try
            ' Step 4. Convert the input string to a byte[]
            Dim DataToDecrypt As Byte() = Convert.FromBase64String(Message)

            ' Step 5. Attempt to decrypt the string
            Dim Decryptor As System.Security.Cryptography.ICryptoTransform = TDESAlgorithm.CreateDecryptor()
            Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length)
            strResult = UTF8.GetString(Results)
        Catch
            strResult = String.Empty
        Finally
            ' Clear the TripleDes and Hashprovider services of any sensitive information
            TDESAlgorithm.Clear()
            HashProvider.Clear()
        End Try

        ' Step 6. Return the decrypted string in UTF8 format
        Return strResult
    End Function

End Class
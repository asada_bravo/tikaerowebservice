Imports System
Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports System.IO
Imports System.Threading
Imports System.Xml

Public Class Linkser

#Region "Property "
    Private _TerminalID As String
    Public WriteOnly Property TerminalID() As String
        Set(ByVal value As String)
            _TerminalID = value
        End Set
    End Property

    Private _MerchantID As String
    Public WriteOnly Property MerchantID() As String
        Set(ByVal value As String)
            _MerchantID = value
        End Set
    End Property

    Private _IP As String
    Public WriteOnly Property IP() As String
        Set(ByVal value As String)
            _IP = value
        End Set
    End Property

    Private _Port As String
    Public WriteOnly Property Port() As String
        Set(ByVal value As String)
            _Port = value
        End Set
    End Property

    Private _CardNumber As String
    Public Property CardNumber() As String
        Get
            Return _CardNumber
        End Get
        Set(ByVal value As String)
            _CardNumber = value
        End Set
    End Property

    Private _ExpMonth As String
    Public Property ExpMonth() As String
        Get
            Return _ExpMonth
        End Get
        Set(ByVal value As String)
            _ExpMonth = value
        End Set
    End Property

    Private _ExpYear As String
    Public Property ExpYear() As String
        Get
            Return _ExpYear
        End Get
        Set(ByVal value As String)
            _ExpYear = value
        End Set
    End Property

    Private _Amount As Double
    Public Property Amount() As Double
        Get
            Return _Amount
        End Get
        Set(ByVal value As Double)
            _Amount = value
        End Set
    End Property

    Private _AuthCode As String
    Public Property AuthCode() As String
        Get
            Return _AuthCode
        End Get
        Set(ByVal value As String)
            _AuthCode = value
        End Set
    End Property

    Private _ResponseCode As String
    Public Property ResponseCode() As String
        Get
            Return _ResponseCode
        End Get
        Set(ByVal value As String)
            _ResponseCode = value
        End Set
    End Property

    Private _ResponseText As String
    Public Property ResponseText() As String
        Get
            Return _ResponseText
        End Get
        Set(ByVal value As String)
            _ResponseText = value
        End Set
    End Property

    Private _Approved As Boolean
    Public ReadOnly Property Approved() As Boolean
        Get
            Return _Approved
        End Get
    End Property
    Private _Transaction_id As String
    Public Property Transaction_id() As String
        Get
            Return _Transaction_id
        End Get
        Set(ByVal Value As String)
            _Transaction_id = Value
        End Set
    End Property
#End Region

    Public Function ProcessLinkserCreditCard(ByVal StrXML As String) As String

        Dim XMLDoc As XmlDocument
        Dim xn As XmlNodeList
        Dim objStr As StringBuilder

        Dim strTerminalID As String = String.Empty
        Dim strMerchantID As String = String.Empty
        Dim strIP As String = String.Empty
        Dim strPort As String = String.Empty
        Dim strCardNumber As String = String.Empty
        Dim strExpMonth As String = String.Empty
        Dim strExpYear As String = String.Empty
        Dim strCurrency As String = String.Empty
        Dim strResult As String = String.Empty
        Dim strTransType As String = String.Empty
        Dim strAuthCode As String = String.Empty
        Dim strMerchType As String = String.Empty

        Dim dblAmount As Double

        Try
            If StrXML.Length > 0 Then
                XMLDoc = New XmlDocument
                XMLDoc.LoadXml(StrXML)
                xn = XMLDoc.SelectNodes("Payment/Query")

                For Each n As XmlNode In xn

                    strTerminalID = n.Item("TerminalID").InnerText
                    strMerchantID = n.Item("MerchantID").InnerText
                    strMerchType = n.Item("MerchType").InnerText
                    strIP = n.Item("IP").InnerText
                    strPort = n.Item("Port").InnerText
                    strCardNumber = n.Item("CardNumber").InnerText
                    strExpMonth = n.Item("ExpMonth").InnerText
                    If n.Item("ExpYear").InnerText.Length >= 2 Then
                        strExpYear = Right(n.Item("ExpYear").InnerText, 2)
                    End If
                    strCurrency = n.Item("Currency").InnerText
                    dblAmount = Convert.ToDouble(n.Item("Amount").InnerText)
                    strTransType = n.Item("Transaction_type").InnerText
                    _AuthCode = n.Item("AuthCode").InnerText
                    _Transaction_id = n.Item("Trans_ID").InnerText
                Next

                strResult = ProcessCreditCard(strTerminalID, _
                                              strMerchantID, _
                                              strMerchType, _
                                              strIP, _
                                              strPort, _
                                              strCardNumber, _
                                              strExpMonth, _
                                              strExpYear, _
                                              strCurrency, _
                                              strTransType, _
                                              dblAmount)
            Else
                _AuthCode = String.Empty
                _Approved = False
                _ResponseCode = "105"
                _ResponseText = "ERROR : No XML input found !!"
            End If
        Catch ex As Exception
            _AuthCode = String.Empty
            _Approved = False
            _ResponseCode = "102"
            _ResponseText = "ERROR : " & ex.Message
        Finally
            If strResult.Length = 0 Then
                objStr = New StringBuilder
                With objStr
                    .Append("<?xml version='1.0' encoding='UTF-8'?>" & Environment.NewLine)
                    .Append("<Payment>" & Environment.NewLine)
                    .Append("   <Result>" & Environment.NewLine)
                    .Append("       <Transaction_id>" & "" & "</Transaction_id>" & Environment.NewLine)
                    .Append("       <AuthCode>" & _AuthCode & "</AuthCode>" & Environment.NewLine)
                    .Append("       <ResponseCode>" & _ResponseCode & "</ResponseCode>" & Environment.NewLine)
                    .Append("       <ResponseText>" & _ResponseText & "</ResponseText>" & Environment.NewLine)
                    .Append("       <Approved>" & Convert.ToInt16(_Approved) & "</Approved>" & Environment.NewLine)
                    .Append("   </Result>" & Environment.NewLine)
                    .Append("</Payment>")
                End With
                strResult = objStr.ToString
                objStr = Nothing
            End If

            ProcessLinkserCreditCard = strResult
            XMLDoc = Nothing
            xn = Nothing
        End Try
    End Function
    Private Function ProcessCreditCard(ByVal strTerminalID As String, _
                                       ByVal strMerchantID As String, _
                                       ByVal strMerchType As String, _
                                       ByVal strIP As String, _
                                       ByVal strPort As String, _
                                       ByVal strCardNumber As String, _
                                       ByVal strExpMonth As String, _
                                       ByVal strExpYear As String, _
                                       ByVal strCurrency As String, _
                                       ByVal strTransType As String, _
                                       ByVal dblAmount As Double) As String
        Dim objClient As TcpClient = Nothing
        Dim stream As NetworkStream = Nothing
        Dim objIP As IPEndPoint

        Dim objISO As Iso8583Formatter.Iso8583
        Dim stbResult As New StringBuilder
        Dim objXML As StringBuilder
        'Dim s As Socket

        Dim bAvailable As Boolean

        Dim iTimeOut As Integer = 30
        Dim iLength As Integer

        Dim dtStarttime As Date = DateTime.Now
        Dim bytes(512) As Byte

        Try
            If strIP.Length > 0 And strPort.Length > 0 Then
                'Start to connect to payment gateway
                objISO = New Iso8583Formatter.Iso8583
                With objISO
                    'Construct credit information used Iso8583

                    .MTI = strTransType '"0100"
                    If strTransType = "0400" Then
                        .de38_auth_num = _AuthCode
                    End If
                    .de2_pan = strCardNumber
                    .de3_pcode = "000000"
                    .de4_txn_amt = (dblAmount * 100).ToString("000000000000") '"000000010000"
                    .de6_card_amt = (dblAmount * 100).ToString("000000000000")
                    .de11_trace = _Transaction_id
                    .de12_local_txn_date_time = DateTime.Now.ToString("HHmmss")
                    .de13_local_txn_date = DateTime.Now.ToString("MMdd")
                    .de14_card_expire = strExpYear & strExpMonth '"0708"
                    .de18_merch_type = strMerchType '"5813"
                    .de22_pos_entry = "012"
                    .de25_message_reason_code = "08"

                    .de32_acq_bin = "455782"
                    .de37_ref_num = DateTime.UtcNow.Year.ToString("0000").Substring(3, 1) + DateTime.UtcNow.DayOfYear.ToString("000") + DateTime.UtcNow.Hour.ToString("00") + .de11_trace
                    .de41_terminal_id = strTerminalID '"12039999"
                    .de42_card_accept_code = strMerchantID '"999999900000000"
                    .de43_card_accept_name = "AEROCON             SANTA CRUZ       BOL"
                    .de49_txn_currency = strCurrency
                    .de51_card_currency = strCurrency
                    .de62_add_data2 = DateTime.Now.DayOfYear.ToString("000").PadLeft(6, "0")

                    .FromImfToXmf() '// Create the iso message

                End With
                If objISO.error_de_formato = False Then
                    'No error in converting data

                    objIP = New IPEndPoint(IPAddress.Parse(strIP.Trim), strPort.Trim)
                    objClient = New TcpClient
                    objClient.Connect(objIP)

                    'Connection Connected
                    'Send credit information to payment gateway
                    stream = objClient.GetStream
                    If IsNothing(stream) = False Then
                        If SendToGateway(stream, objISO.XMF) = True Then
                            'Credit information send and wait for reply
                            While bAvailable = False

                                If DateDiff(DateInterval.Second, dtStarttime, DateTime.Now) > iTimeOut Then
                                    'Time out
                                    _ResponseCode = "DECLINED"
                                    _ResponseText = "Time out!"
                                    Exit While
                                Else
                                    bAvailable = stream.DataAvailable
                                    If bAvailable = True Then
                                        'Data available 
                                        iLength = stream.Read(bytes, 0, bytes.Length)
                                        stbResult.Append(bytesToAscii(bytes, iLength))
                                        If stream.DataAvailable = True Then
                                            bAvailable = False
                                        End If
                                    End If
                                End If
                                Thread.Sleep(300)
                            End While

                            'Receive Data
                            If stbResult.Length > 0 Then
                                'Find Credit Information used Iso8583
                                With objISO
                                    .XMF = stbResult.ToString
                                    .FromXmfToImf()
                                    If .error_de_formato = False Then
                                        'No error found
                                        _AuthCode = .de38_auth_num
                                        _Transaction_id = .de11_trace
                                        _ResponseCode = .de39_resp_code
                                        If _ResponseCode = "00" Then
                                            _Approved = True
                                        Else
                                            _Approved = False
                                        End If

                                        Select Case _ResponseCode
                                            Case "00"
                                                _ResponseText = "Approved"
                                            Case "02"
                                                _ResponseText = "Refer Card Issuer"
                                            Case "03"
                                                _ResponseText = "Invalid Merchant"
                                            Case "04"
                                                _ResponseText = "Do Not Honor"
                                            Case "05"
                                                _ResponseText = "Unable To Process"
                                            Case "06"
                                                _ResponseText = "Invalid Transaction Term"
                                            Case "08"
                                                _ResponseText = "Issuer Timeout"
                                            Case "09"
                                                _ResponseText = "No Original"
                                            Case "10"
                                                _ResponseText = "Unable To Reverse"
                                            Case "12"
                                                _ResponseText = "Invalid Transaction"
                                            Case "13"
                                                _ResponseText = "Invalid Amount"
                                            Case "14"
                                                _ResponseText = "Invalid Card"
                                            Case "17"
                                                _ResponseText = "Invalid Capture Date"
                                            Case "19"
                                                _ResponseText = "System Error Reenter"
                                            Case "20"
                                                _ResponseText = "No From Account"
                                            Case "21"
                                                _ResponseText = "No To Account"
                                            Case "22"
                                                _ResponseText = "No Checking Account"
                                            Case "23"
                                                _ResponseText = "No Saving Account"
                                            Case "24"
                                                _ResponseText = "No Credit Account"
                                            Case "30"
                                                _ResponseText = "Message Format Error"
                                            Case "39"
                                                _ResponseText = "Transaction Not Allowed"
                                            Case "41"
                                                _ResponseText = "Hot Card"
                                            Case "42"
                                                _ResponseText = "Special Pickup"
                                            Case "43"
                                                _ResponseText = "Hot Card Pick Up"
                                            Case "44"
                                                _ResponseText = "Pick Up Card"
                                            Case "45"
                                                _ResponseText = "Txn Back Off"
                                            Case "46"
                                                _ResponseText = "Chip Off Declined"
                                            Case "47"
                                                _ResponseText = "Chip Unable Online"
                                            Case "48"
                                                _ResponseText = "Chip Arqc Fail"
                                            Case "51"
                                                _ResponseText = "No Funds"
                                            Case "54"
                                                _ResponseText = "Expired Card"
                                            Case "55"
                                                _ResponseText = "Incorrect P I N"
                                            Case "56"
                                                _ResponseText = "No Card Record"
                                            Case "57"
                                                _ResponseText = "Txn Not Permitted On Card"
                                            Case "58"
                                                _ResponseText = "Txn Not Permitted On Term"
                                            Case "61"
                                                _ResponseText = "Exceeds Limit"
                                            Case "62"
                                                _ResponseText = "Restricted Card"
                                            Case "63"
                                                _ResponseText = "M A C Key Error"
                                            Case "65"
                                                _ResponseText = "Exceeds Freq Limit"
                                            Case "66"
                                                _ResponseText = "Exceeds Acquirer Limit"
                                            Case "67"
                                                _ResponseText = "Retain Card"
                                            Case "68"
                                                _ResponseText = "Late Response"
                                            Case "75"
                                                _ResponseText = "Exceeds P I N Retry"
                                            Case "76"
                                                _ResponseText = "Invalid Account"
                                            Case "77"
                                                _ResponseText = "No Sharing Arrangement"
                                            Case "78"
                                                _ResponseText = "Function Not Available"
                                            Case "79"
                                                _ResponseText = "Key Validation Error"
                                            Case "80"
                                                _ResponseText = "Pin Length Error"
                                            Case "81"
                                                _ResponseText = "Invalid Pin Block"
                                            Case "82"
                                                _ResponseText = "Invalid C V V"
                                            Case "83"
                                                _ResponseText = "Counter Sync Error"
                                            Case "84"
                                                _ResponseText = "Invalid Life Cycle"
                                            Case "85"
                                                _ResponseText = "No Keys To Use"
                                            Case "86"
                                                _ResponseText = "K M E Sync Error"
                                            Case "87"
                                                _ResponseText = "P I N Key Error"
                                            Case "88"
                                                _ResponseText = "M A C Sync Error"
                                            Case "89"
                                                _ResponseText = "Security Violation"
                                            Case "91"
                                                _ResponseText = "Switch Not Available"
                                            Case "92"
                                                _ResponseText = "Invalid Issuer"
                                            Case "93"
                                                _ResponseText = "Invalid Acquirer"
                                            Case "94"
                                                _ResponseText = "Invalid Originator"
                                            Case "95"
                                                _ResponseText = "Violation Law"
                                            Case "96"
                                                _ResponseText = "System Error"
                                            Case "97"
                                                _ResponseText = "No Funds Transfer"
                                            Case "98"
                                                _ResponseText = "Duplicate Reversal"
                                            Case "99"
                                                _ResponseText = "Duplicate Transaction"
                                            Case Else
                                                _ResponseText = "Transaction Declined"
                                        End Select
                                    Else
                                        '.descripcion_error 
                                        _ResponseCode = "100"
                                        _ResponseText = "DECLINED : " & objISO.descripcion_error
                                    End If
                                End With
                            End If
                        End If
                    Else
                        _Approved = False
                        _ResponseCode = "101"
                        _ResponseText = "ERROR : Connction could not be establish"
                    End If
                Else
                    _ResponseCode = "100"
                    _ResponseText = "ERROR : " & objISO.descripcion_error
                    _Approved = False
                End If
            End If
        Catch ex As Exception
            _Approved = False
            _ResponseCode = "102"
            _ResponseText = "ERROR : " & ex.Message
        Finally

            If IsNothing(objClient) = False Then
                objClient.Close()
            End If

            If IsNothing(stream) = False Then
                stream.Close()
            End If

            objXML = New StringBuilder
            With objXML

                .Append("<?xml version='1.0' encoding='UTF-8'?>" & Environment.NewLine)
                .Append("<Payment>" & Environment.NewLine)
                .Append("   <Result>" & Environment.NewLine)
                .Append("       <Transaction_id>" & _Transaction_id & "</Transaction_id>" & Environment.NewLine)
                .Append("       <AuthCode>" & _AuthCode & "</AuthCode>" & Environment.NewLine)
                .Append("       <ResponseCode>" & _ResponseCode & "</ResponseCode>" & Environment.NewLine)
                .Append("       <ResponseText>" & _ResponseText & "</ResponseText>" & Environment.NewLine)
                .Append("       <Approved>" & Convert.ToInt16(_Approved) & "</Approved>" & Environment.NewLine)
                .Append("   </Result>" & Environment.NewLine)
                .Append("</Payment>")
            End With

            ProcessCreditCard = objXML.ToString

            stbResult = Nothing
            objXML = Nothing
            stream = Nothing
            objClient = Nothing
            objIP = Nothing
        End Try
    End Function
    Private Function SendToGateway(ByVal stream As NetworkStream, ByVal strData As String) As Boolean

        Dim bytes As [Byte]()
        Dim bCompleted As Boolean

        Try
            bytes = ASCIIToBytes(strData)
            With stream
                .Write(bytes, 0, bytes.Length)
            End With
            bCompleted = True
        Catch ex As Exception
            bCompleted = False
        Finally
            SendToGateway = bCompleted
        End Try

    End Function

    Private Function ASCIIToBytes(ByVal value As String) As Byte()
        Return System.Text.Encoding.ASCII.GetBytes(value)
    End Function
    Private Function bytesToAscii(ByVal bytes As Byte(), ByVal length As Integer) As String
        Return System.Text.Encoding.ASCII.GetString(bytes, 0, length)
    End Function
End Class

Imports System.Threading
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.Serialization

Public Class clsProcAvail

    Private objXMLResponse As New XmlDocument
    Private objXMLRequest As New XmlDocument

    Private strXMLPath As String
    Private strXSDPath As String

    Private strOrigin As String
    Private strDestination As String
    Private dteFrom As Date
    Private dteTo As Date
    Private dteBooking As Date
    Private iAdult As Integer
    Private iChild As Integer
    Private iInfant As Integer
    Private iOther As Integer
    Private strOtherPassengerType As String
    Private bAllFlights As Boolean
    Private bRefundable As Boolean
    Private strAgencyCode As String

    Public WriteOnly Property PathXMLLocation() As String
        Set(ByVal Value As String)
            strXMLPath = Value
        End Set
    End Property

    Public WriteOnly Property PathXSDLocation() As String
        Set(ByVal Value As String)
            strXSDPath = Value
        End Set
    End Property

    Public ReadOnly Property XMLResponse() As XmlDocument
        Get
            Return objXMLResponse
        End Get
    End Property

    Private Function ConvertStrToDate(ByVal strIn As String) As Date
        Dim dtDate As Date
        Try
            dtDate = Date.Parse(strIn)
            If dtDate = #12:00:00 AM# Then
                dtDate = #12/30/1899#
            End If
        Catch
            dtDate = #12/30/1899#
        End Try
        Return dtDate
    End Function

    Public WriteOnly Property XMLRequest() As XmlDocument
        Set(ByVal objXML As XmlDocument)
            objXMLRequest = objXML
            Try
                Dim root As XmlElement = objXML.DocumentElement
                'Dim xmlNode As XmlNode
                Dim nsmgr As XmlNamespaceManager = New XmlNamespaceManager(objXML.NameTable)

                nsmgr.AddNamespace("ab", "http://tempuri.org/xmlRequestAvaliability.xsd")

                With objXML.DocumentElement.FirstChild

                    strOrigin = .ChildNodes(0).InnerText & ""
                    strDestination = .ChildNodes(1).InnerText & ""
                    dteFrom = ConvertStrToDate(.ChildNodes(2).InnerText)
                    dteTo = ConvertStrToDate(.ChildNodes(3).InnerText)
                    iAdult = Integer.Parse(.ChildNodes(5).InnerText)
                    iChild = Integer.Parse(.ChildNodes(6).InnerText)
                    iInfant = Integer.Parse(.ChildNodes(7).InnerText)
                    iOther = Integer.Parse(.ChildNodes(8).InnerText)
                    strOtherPassengerType = .ChildNodes(9).InnerText & ""
                    bRefundable = Boolean.Parse(.ChildNodes(11).InnerText)
                End With
            Catch
            End Try
        End Set
    End Property

    Private Function ConvertRSIntoXMLResponse(ByVal rs As ADODB.Recordset, _
                                              Optional ByVal bClear As Boolean = True) As String

        Dim strReturn As String = ""
        Try
            Dim objDataSet As New DataSet

            objDataSet.ReadXmlSchema(strXSDPath & "xmlResponseAvailiability.xsd")
            If objXMLResponse.DocumentElement Is Nothing Then
                Dim strReader As String
                'Dim strStore As String
                Dim iStart As Integer
                Dim iEnd As Integer
                Dim xmlTmp As New XmlDocument

                xmlTmp.Load(strXMLPath & "xmlResponseAvailiability.xml")
                strReader = xmlTmp.InnerXml
                iStart = InStr(strReader, "<Request>", CompareMethod.Text)
                Do While iStart > 0
                    iEnd = InStr(strReader, "</Request>", CompareMethod.Text)
                    strReader = strReader.Remove(iStart - 1, ((iEnd + 10) - (iStart - 1)) - 1)
                    iStart = InStr(strReader, "<Request>", CompareMethod.Text)
                Loop

                Dim memTmp As System.IO.MemoryStream = New System.IO.MemoryStream
                objXMLRequest.Save(memTmp)
                memTmp.Position = 0
                Dim readerTmp As XmlTextReader = New XmlTextReader(memTmp)
                readerTmp.Namespaces = False

                Dim docOut As New XmlDocument
                docOut.Load(readerTmp)
                iStart = InStr(strReader, "<Response>", CompareMethod.Text)

                strReader = strReader.Insert(iStart - 1, docOut.DocumentElement.InnerXml)
                objXMLResponse.LoadXml(strReader)
            End If
            Dim strm As System.IO.MemoryStream = New System.IO.MemoryStream
            objXMLResponse.Save(strm)
            strm.Position = 0
            Dim tr As XmlTextReader = New XmlTextReader(strm)

            objDataSet.ReadXml(tr, XmlReadMode.Auto)

            If bClear = True Then
                With objDataSet.Tables("Response")  ' Clear all previous row
                    Dim FlightRows() As DataRow = .Select()
                    If .Rows.Count > 0 Then
                        .Rows.Clear()
                    End If
                End With
            End If

            Dim bDontShow As Boolean
            Dim strStatus As String

            Do While rs.EOF = False
                bDontShow = False
                strStatus = ""

                'If CType(rs.Fields("departure_date").Value, System.DateTime) = Now.Today And _
                If CType(rs.Fields("departure_date").Value, System.DateTime) = Now And _
                   Val(rs.Fields("planned_departure_time").Value & "") < Val(Format(Now, "HHmm")) Then
                    strStatus = "DEP"
                    bDontShow = True
                'ElseIf CType(rs.Fields("departure_date").Value, System.DateTime) < Now.Today Then
                ElseIf CType(rs.Fields("departure_date").Value, System.DateTime) < Now Then
                    strStatus = "DEP"
                    bDontShow = True
                ElseIf Val(rs.Fields("bookable_capacity").Value & "") < Val(rs.Fields("booked_seats").Value & "") Then
                    bDontShow = True
                ElseIf (rs.Fields("booking_class_rcd").Value & "") <> "" And _
                   Val(rs.Fields("class_bookable_capacity").Value & "") < (iAdult + iChild + iOther) Then
                    bDontShow = True
                ElseIf (rs.Fields("flight_status_rcd").Value & "") = "INACTIVE" Then
                    strStatus = "NOOP"
                    bDontShow = True
                ElseIf (rs.Fields("flight_status_rcd").Value & "") = "DEPARTED" Then
                    strStatus = "DEP"
                    bDontShow = True
                ElseIf (rs.Fields("flight_status_rcd").Value & "") = "CANCELLED" Then
                    strStatus = "XX"
                    bDontShow = True
                ElseIf (rs.Fields("flight_status_rcd").Value & "") = "" Then
                    strStatus = "NOOP"
                    bDontShow = True
                End If

                If bAllFlights = False And bDontShow = True Then
                Else
                    With objDataSet.Tables("Response")
                        Dim FlightRows() As DataRow = .Select()
                        Dim objRow As DataRow

                        objRow = .NewRow
                        objRow.Item("Airline") = "" & rs.Fields("airline_rcd").Value
                        objRow.Item("FlightNumber") = "" & rs.Fields("flight_number").Value
                        objRow.Item("Origin") = "" & rs.Fields("origin_rcd").Value
                        objRow.Item("Destination") = "" & rs.Fields("destination_rcd").Value
                        objRow.Item("DepartDate") = CType(rs.Fields("departure_date").Value, System.DateTime).ToString("dd MMM yyyy")
                        objRow.Item("PlanDepartTime") = "" & rs.Fields("planned_departure_time").Value
                        objRow.Item("PlanArrivalTime") = "" & rs.Fields("planned_arrival_time").Value
                        objRow.Item("Duration") = "" & rs.Fields("flight_duration").Value
                        objRow.Item("AircraftType") = "" & rs.Fields("aircraft_type_rcd").Value
                        objRow.Item("FareCode") = "" & rs.Fields("fare_code").Value
                        objRow.Item("FareID") = "" & rs.Fields("fare_id").Value
                        objRow.Item("BookingClassRCD") = "" & rs.Fields("booking_class_rcd").Value
                        objRow.Item("BoardingClassRCD") = "" & rs.Fields("boarding_class_rcd").Value
                        objRow.Item("ClassBookableCapacity") = "" & rs.Fields("class_bookable_capacity").Value
                        objRow.Item("ClassOpenFlag") = "" & rs.Fields("class_open_flag").Value
                        objRow.Item("AdultFare") = "" & rs.Fields("adult_fare").Value
                        objRow.Item("ChildFare") = "" & rs.Fields("child_fare").Value
                        objRow.Item("InfantFare") = "" & rs.Fields("infant_fare").Value
                        objRow.Item("OtherFare") = "" & rs.Fields("other_fare").Value
                        objRow.Item("Status") = strStatus
                        .Rows.Add(objRow)
                    End With
                End If
                rs.MoveNext()
            Loop

            Dim strmBk As System.IO.MemoryStream = New System.IO.MemoryStream
            objDataSet.WriteXml(strmBk)
            strmBk.Position = 0
            Dim trBk As XmlTextReader = New XmlTextReader(strmBk)
            objXMLResponse.Load(trBk)

        Catch ex As Exception
            strReturn = ex.Message
        End Try
        Return strReturn

    End Function

    Public Function ProcessWEB() As String

        Dim strReturn As String = ""
        Try
            Dim rsA As ADODB.Recordset = Nothing
            Dim rsA2 As ADODB.Recordset = Nothing
            Dim clsRunCom As New clsRunComplus
            rsA = clsRunCom.GetAvailability(strOrigin, strDestination, dteFrom, #12/30/1899#, , , dteBooking, iAdult, _
                                            iChild, iInfant, iOther, strOtherPassengerType, bAllFlights, _
                                            bRefundable, strAgencyCode)
            If dteTo > #12/30/1899# Then
                rsA2 = clsRunCom.GetAvailability(strDestination, strOrigin, dteTo, #12/30/1899#, , , dteBooking, iAdult, _
                                                 iChild, iInfant, iOther, strOtherPassengerType, bAllFlights, _
                                                 bRefundable, strAgencyCode)
            End If
            clsRunCom = Nothing
            strReturn = ConvertRSIntoXMLResponse(rsA)
            If strReturn = "" Then
                If dteTo > #12/30/1899# Then
                    strReturn = ConvertRSIntoXMLResponse(rsA2, False)
                End If
            End If
        Catch EX As Exception
            strReturn = EX.Message
        Finally
        End Try
        ProcessWEB = strReturn

    End Function

    Public Sub New()
        strOrigin = ""
        strDestination = ""
        dteFrom = #12/30/1899#
        dteTo = #12/30/1899#
        dteBooking = #12/30/1899#
        iAdult = 1
        iChild = 0
        iInfant = 0
        iOther = 0
        strOtherPassengerType = ""
        bAllFlights = False
        bRefundable = False
        strAgencyCode = ""
    End Sub
End Class

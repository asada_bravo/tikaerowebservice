Imports System.Threading
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.Serialization

Public Class clsProcCBook

    Private objXMLResponse As New XmlDocument
    Private objXMLRequest As New XmlDocument

    Private strXMLPath As String
    Private strXSDPath As String

    Private strOrigin As String
    Private strDestination As String
    Private iAdult As Integer
    Private iChild As Integer
    Private iInfant As Integer
    Private iOther As Integer
    Private strOtherPassengerType As String
    Private bRefundable As Boolean
    Private bETicket As Boolean
    Private strAgencyCode As String
    Private rsBookFlight As New DataSet

    Public WriteOnly Property PathXMLLocation() As String
        Set(ByVal Value As String)
            strXMLPath = Value
        End Set
    End Property

    Public WriteOnly Property PathXSDLocation() As String
        Set(ByVal Value As String)
            strXSDPath = Value
        End Set
    End Property

    Public WriteOnly Property AgencyCode() As String
        Set(ByVal Value As String)
            strAgencyCode = Value
        End Set
    End Property

    Public ReadOnly Property XMLResponse() As XmlDocument
        Get
            Return objXMLResponse
        End Get
    End Property

    Private Function ConvertStrToDate(ByVal strIn As String) As Date
        Dim dtDate As Date
        Try
            dtDate = Date.Parse(strIn)
            If dtDate = #12:00:00 AM# Then
                dtDate = #12/30/1899#
            End If
        Catch
            dtDate = #12/30/1899#
        End Try
        Return dtDate
    End Function

    Public WriteOnly Property XMLRequest() As XmlDocument
        Set(ByVal objXML As XmlDocument)
            objXMLRequest = objXML
            Try
                Dim root As XmlElement = objXML.DocumentElement
                'Dim xmlNode As XmlNode
                Dim nsmgr As XmlNamespaceManager = New XmlNamespaceManager(objXML.NameTable)

                nsmgr.AddNamespace("ab", "http://tempuri.org/xmlRequestBooking.xsd")

                With objXML.DocumentElement.FirstChild
                    strOrigin = .ChildNodes(0).InnerText & ""
                    strDestination = .ChildNodes(1).InnerText & ""
                    iAdult = Integer.Parse(.ChildNodes(2).InnerText)
                    iChild = Integer.Parse(.ChildNodes(3).InnerText)
                    iInfant = Integer.Parse(.ChildNodes(4).InnerText)
                    iOther = Integer.Parse(.ChildNodes(5).InnerText)
                    strOtherPassengerType = .ChildNodes(6).InnerText & ""
                    bRefundable = Boolean.Parse(.ChildNodes(7).InnerText)
                    bETicket = Boolean.Parse(.ChildNodes(8).InnerText)
                End With

                Dim strm As System.IO.MemoryStream = New System.IO.MemoryStream
                objXML.Save(strm)
                strm.Position = 0
                Dim tr As XmlTextReader = New XmlTextReader(strm)

                rsBookFlight.ReadXml(tr, XmlReadMode.Auto)
            Catch
            End Try
        End Set
    End Property

    Public Function ConvertRSIntoXMLResponse() As String

        Dim strReturn As String = ""

        Try
            Dim objDataSet As New DataSet
            Dim objRow As DataRow

            objDataSet.ReadXmlSchema(strXSDPath & "xmlCreateBook.xsd")
            If objXMLResponse.DocumentElement Is Nothing Then
                objXMLResponse.Load(strXMLPath & "xmlCreateBook.xml")
            End If
            Dim strm As System.IO.MemoryStream = New System.IO.MemoryStream
            objXMLResponse.Save(strm)
            strm.Position = 0
            Dim tr As XmlTextReader = New XmlTextReader(strm)

            objDataSet.ReadXml(tr, XmlReadMode.Auto)

            Dim iHeaderID As Integer = -1

            With objDataSet.Tables("BookingHeader")
                Dim BkHeaderRows() As DataRow = .Select()
                iHeaderID = BkHeaderRows(0).Item("BookingHeader_Id")
                BkHeaderRows(0).Item("BookingID") = Guid.NewGuid
                BkHeaderRows(0).Item("AgencyCode") = strAgencyCode
                BkHeaderRows(0).Item("NumberAdult") = iAdult
                BkHeaderRows(0).Item("NumberChildren") = iChild
                BkHeaderRows(0).Item("NumberInfant") = iInfant
                BkHeaderRows(0).Item("NumberOther") = iOther
                BkHeaderRows(0).Item("OtherTypeRCD") = strOtherPassengerType & " "
                BkHeaderRows(0).Item("PhoneFax") = " "
            End With

            With objDataSet.Tables("Passenger")
                If .Rows.Count > 0 Then
                    .Rows.Clear()
                End If
                For i As Integer = 1 To iAdult
                    objRow = .NewRow
                    objRow.Item("BookingHeader_Id") = iHeaderID
                    objRow.Item("PassengerID") = Guid.NewGuid
                    objRow.Item("PassengerTypeRCD") = "ADULT"
                    objRow.Item("LastName") = ""
                    objRow.Item("FirstName") = ""
                    objRow.Item("TitleRCD") = ""
                    objRow.Item("GenderTypeRCD") = ""
                    objRow.Item("DateOfBirth") = ""
                    .Rows.Add(objRow)
                Next i
                For i As Integer = 1 To iChild
                    objRow = .NewRow
                    objRow.Item("BookingHeader_Id") = iHeaderID
                    objRow.Item("PassengerID") = Guid.NewGuid
                    objRow.Item("PassengerTypeRCD") = "CHD"
                    objRow.Item("LastName") = ""
                    objRow.Item("FirstName") = ""
                    objRow.Item("TitleRCD") = ""
                    objRow.Item("GenderTypeRCD") = ""
                    objRow.Item("DateOfBirth") = ""
                    .Rows.Add(objRow)
                Next i
                For i As Integer = 1 To iInfant
                    objRow = .NewRow
                    objRow.Item("BookingHeader_Id") = iHeaderID
                    objRow.Item("PassengerID") = Guid.NewGuid
                    objRow.Item("PassengerTypeRCD") = "INF"
                    objRow.Item("LastName") = ""
                    objRow.Item("FirstName") = ""
                    objRow.Item("TitleRCD") = ""
                    objRow.Item("GenderTypeRCD") = ""
                    objRow.Item("DateOfBirth") = ""
                    .Rows.Add(objRow)
                Next i
                For i As Integer = 1 To iOther
                    objRow = .NewRow
                    objRow.Item("BookingHeader_Id") = iHeaderID
                    objRow.Item("PassengerID") = Guid.NewGuid
                    objRow.Item("PassengerTypeRCD") = strOtherPassengerType
                    objRow.Item("LastName") = ""
                    objRow.Item("FirstName") = ""
                    objRow.Item("TitleRCD") = ""
                    objRow.Item("GenderTypeRCD") = ""
                    objRow.Item("DateOfBirth") = ""
                    .Rows.Add(objRow)
                Next i
            End With

            'Dim BSegClearRows() As DataRow = objDataSet.Tables("BookingSegment").Select()
            If objDataSet.Tables("BookingSegment").Rows.Count > 0 Then
                objDataSet.Tables("BookingSegment").Rows.Clear()
            End If
            If objDataSet.Tables("PassengerSegmentMapping").Rows.Count > 0 Then
                objDataSet.Tables("PassengerSegmentMapping").Rows.Clear()
            End If
            Dim iQuantity As Integer = iAdult + iChild + iOther

            If iQuantity > 0 Then
                Dim clsRunCom As New clsRunComplus
                Dim rsA As ADODB.Recordset = Nothing
                Dim rsB As ADODB.Recordset = Nothing

                Dim selectFlight() As DataRow = rsBookFlight.Tables("SelectFlight").Select
                For Each selectF As DataRow In selectFlight

                    Dim bsegGUID As Guid
                    Dim strAirline As String = selectF.Item("Airline")
                    Dim strFlight As String = selectF.Item("FlightNumber")
                    Dim strdtFlight As String = ConvertStrToDate(selectF.Item("FlightDate")).ToString("dd MMM yyyy")
                    Dim strFareId As String = selectF.Item("FareID")
                    Dim strFOrigRCD As String = ""
                    Dim strFDestRCD As String = ""

                    With objDataSet.Tables("BookingSegment")
                        ' Too lazy for rewrite loop and duplicate value (search without bookingid)
                        Dim BSegmentRows() As DataRow = .Select("((AirlineRCD = '" & strAirline & "') and " & _
                                                                " (FlightNumber = '" & strFlight & "') and " & _
                                                                " (DepartureDate = #" & strdtFlight & "#) and " & _
                                                                " (BookingHeader_Id = " & iHeaderID & "))", _
                                                                Nothing, _
                                                                DataViewRowState.CurrentRows)
                        If BSegmentRows.Length < 1 Then
                            rsA = clsRunCom.GetFlightSummary(strAirline, strFlight, ConvertStrToDate(strdtFlight))
                            If rsA Is Nothing Then
                            ElseIf rsA.EOF = False Then
                                bsegGUID = Guid.NewGuid
                                objRow = .NewRow
                                objRow.Item("BookingHeader_Id") = iHeaderID
                                objRow.Item("BookingSegmentID") = bsegGUID
                                objRow.Item("AirlineRCD") = strAirline
                                objRow.Item("FlightNumber") = strFlight
                                objRow.Item("BookingClassRCD") = " "
                                objRow.Item("DepartureDate") = strdtFlight
                                objRow.Item("DepartureTime") = rsA.Fields("planned_departure_time").Value & ""
                                objRow.Item("SegmentStatusRCD") = "HK"
                                objRow.Item("NumberOfUnits") = iQuantity
                                strFOrigRCD = rsA.Fields("origin_rcd").Value & ""
                                objRow.Item("OriginRCD") = strFOrigRCD
                                strFDestRCD = rsA.Fields("destination_rcd").Value & ""
                                objRow.Item("DestinationRCD") = strFDestRCD
                                .Rows.Add(objRow)
                            End If
                        Else
                            bsegGUID = BSegmentRows(0).Item("BookingSegmentID")
                            strFOrigRCD = BSegmentRows(0).Item("OriginRCD")
                            strFDestRCD = BSegmentRows(0).Item("DestinationRCD")
                        End If
                    End With
                    With objDataSet.Tables("Passenger")
                        Dim PassRows() As DataRow = .Select(Nothing, "PassengerTypeRCD ASC")
                        Dim strPType As String = ""
                        For Each pRow As DataRow In PassRows
                            If strPType <> pRow.Item("PassengerTypeRCD") Then
                                strPType = pRow.Item("PassengerTypeRCD")
                                rsB = clsRunCom.GetFareQuote(rsA.Fields("origin_rcd").Value & "", _
                                                             rsA.Fields("destination_rcd").Value & "", _
                                                             rsA.Fields("departure_date").Value & "", _
                                                             strPType, _
                                                             strAgencyCode, _
                                                             strFlight, _
                                                             Now, _
                                                             bRefundable:=bRefundable, _
                                                             strFareId:=strFareId)
                            End If
                            If rsB Is Nothing Then
                            ElseIf rsB.EOF = False Then
                                rsB.Filter = "passenger_type_rcd = '" & strPType & "'"
                                If rsB.EOF = True Then
                                    rsB.Filter = ADODB.FilterGroupEnum.adFilterNone
                                End If

                                objRow = objDataSet.Tables("PassengerSegmentMapping").NewRow
                                objRow.Item("BookingHeader_Id") = iHeaderID
                                objRow.Item("PassengerID") = pRow.Item("PassengerID")
                                objRow.Item("BookingSegmentID") = bsegGUID
                                objRow.Item("OriginRCD") = strFOrigRCD
                                objRow.Item("DestinationRCD") = strFDestRCD
                                objRow.Item("FareID") = Replace(Replace(rsB.Fields("fare_id").Value & "", "{", ""), "}", "")
                                objRow.Item("FareCode") = rsB.Fields("fare_code").Value & ""
                                objRow.Item("RefundableFlag") = rsB.Fields("refundable_flag").Value & ""
                                objRow.Item("FareDateTime") = Now.ToString("dd MMM yyyy hh:mm:ss tt")
                                objRow.Item("CurrencyRCD") = rsB.Fields("currency_rcd").Value & ""
                                objRow.Item("AirlineRCD") = strAirline
                                objRow.Item("FlightNumber") = strFlight
                                objRow.Item("DepartureDate") = strdtFlight
                                objRow.Item("BoardingClassRCD") = rsB.Fields("booking_class_rcd").Value & ""
                                objRow.Item("PassengerTypeRCD") = strPType
                                objRow.Item("PassengerStatusRCD") = "OK"
                                objRow.Item("ETicketFlag") = IIf(bETicket = True, 1, 0)
                                ' ????
                                If strPType = "INF" And _
                                   Not strPType = (rsB.Fields("passenger_type_rcd").Value & "") Then
                                    objRow.Item("NetTotal") = 0
                                    objRow.Item("FareAmount") = 0
                                    objRow.Item("TaxAmount") = 0
                                    objRow.Item("YqAmount") = 0
                                    objRow.Item("TicketingFeeAmount") = 0
                                    objRow.Item("BaseTicketingFeeAmount") = 0
                                    objRow.Item("ReservationFeeAmount") = 0
                                    objRow.Item("FareVat") = 0
                                    objRow.Item("TaxVat") = 0
                                    objRow.Item("YqVat") = 0
                                    objRow.Item("TicketFeeVat") = 0
                                    objRow.Item("ReservationFeeVat") = 0
                                    objRow.Item("BaggageWeight") = 0
                                Else
                                    objRow.Item("FareAmount") = rsB.Fields("fare_amount").Value & ""
                                    objRow.Item("TaxAmount") = rsB.Fields("tax_amount").Value & ""
                                    objRow.Item("YqAmount") = rsB.Fields("yq_amount").Value & ""
                                    If bETicket = False Then
                                        objRow.Item("TicketingFeeAmount") = rsB.Fields("ticketing_fee_amount").Value & ""
                                    Else
                                        objRow.Item("TicketingFeeAmount") = 0
                                    End If
                                    objRow.Item("BaseTicketingFeeAmount") = rsB.Fields("ticketing_fee_amount").Value & ""
                                    objRow.Item("ReservationFeeAmount") = rsB.Fields("reservation_fee_amount").Value & ""
                                    objRow.Item("FareVat") = rsB.Fields("fare_vat").Value & ""
                                    objRow.Item("TaxVat") = rsB.Fields("tax_vat").Value & ""
                                    objRow.Item("YqVat") = rsB.Fields("yq_vat").Value & ""
                                    objRow.Item("TicketFeeVat") = rsB.Fields("ticketing_fee_vat").Value & ""
                                    objRow.Item("ReservationFeeVat") = rsB.Fields("reservation_fee_vat").Value & ""
                                    objRow.Item("BaggageWeight") = rsB.Fields("weight_allowance").Value & ""
                                    If bETicket = False Then
                                        objRow.Item("NetTotal") = Val(rsB.Fields("fare_amount_incl").Value & "") + _
                                                                  Val(rsB.Fields("tax_amount_incl").Value & "") + _
                                                                  Val(rsB.Fields("yq_amount_incl").Value & "") + _
                                                                  Val(rsB.Fields("ticketing_fee_amount_incl").Value & "") + _
                                                                  Val(rsB.Fields("reservation_fee_amount_incl").Value & "")
                                    Else
                                        objRow.Item("NetTotal") = Val(rsB.Fields("fare_amount_incl").Value & "") + _
                                                                  Val(rsB.Fields("tax_amount_incl").Value & "") + _
                                                                  Val(rsB.Fields("yq_amount_incl").Value & "") + _
                                                                  Val(rsB.Fields("reservation_fee_amount_incl").Value & "")
                                    End If
                                End If
                                objDataSet.Tables("PassengerSegmentMapping").Rows.Add(objRow)
                            End If
                        Next pRow
                    End With
                Next selectF
                clsRunCom = Nothing
            End If

            Dim strmBk As System.IO.MemoryStream = New System.IO.MemoryStream
            objDataSet.WriteXml(strmBk)
            strmBk.Position = 0

            'Dim aFile As Byte()
            'Dim abc As System.Text.StringBuilder

            'aFile = strmBk.ToArray
            'Dim asciiEn As New System.Text.ASCIIEncoding
            'Dim myZZTop As String

            'myZZTop = asciiEn.GetString(aFile)

            Dim trBk As XmlTextReader = New XmlTextReader(strmBk)
            objXMLResponse.Load(trBk)

        Catch ex As Exception
            strReturn = ex.Message
        End Try
        Return strReturn

    End Function

    Public Sub New()
        strOrigin = ""
        strDestination = ""
        iAdult = 1
        iChild = 0
        iInfant = 0
        iOther = 0
        strOtherPassengerType = ""
        bRefundable = False
        bETicket = False
        strAgencyCode = ""
    End Sub

End Class

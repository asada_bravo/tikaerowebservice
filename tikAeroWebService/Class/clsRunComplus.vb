Imports System.Web
Imports System.Web.Security
Imports System.Security.Principal
Imports System.Runtime.InteropServices
Imports System.Configuration
Imports ADODB
Imports System.Data.OleDb
Imports System.Linq
Imports System.Collections.Generic

Friend Class clsRunComplus

    Private strUser As String
    Private strPassword As String
    Private strDomain As String
    Private strServer As String


    '---------------------------------------------------------------
    'Private tikDB As tikSQLDB.clsDBAccess
    '---------------------------------------------------------------

    Dim LOGON32_LOGON_INTERACTIVE As Integer = 2
    Dim LOGON32_PROVIDER_DEFAULT As Integer = 0

    Dim impersonationContext As WindowsImpersonationContext

    Declare Function LogonUserA Lib "advapi32.dll" (ByVal lpszUsername As String,
                            ByVal lpszDomain As String,
                            ByVal lpszPassword As String,
                            ByVal dwLogonType As Integer,
                            ByVal dwLogonProvider As Integer,
                            ByRef phToken As IntPtr) As Integer

    Declare Auto Function DuplicateToken Lib "advapi32.dll" (
                            ByVal ExistingTokenHandle As IntPtr,
                            ByVal ImpersonationLevel As Integer,
                            ByRef DuplicateTokenHandle As IntPtr) As Integer

    Declare Auto Function RevertToSelf Lib "advapi32.dll" () As Long
    Declare Auto Function CloseHandle Lib "kernel32.dll" (ByVal handle As IntPtr) As Long

    Private Function impersonateValidUser(ByVal userName As String, ByVal domain As String, ByVal password As String) As Boolean

        Try
            Dim tempWindowsIdentity As WindowsIdentity
            Dim token As IntPtr = IntPtr.Zero
            Dim tokenDuplicate As IntPtr = IntPtr.Zero
            impersonateValidUser = False

            If RevertToSelf() Then
                If LogonUserA(userName, domain, password, LOGON32_LOGON_INTERACTIVE,
                             LOGON32_PROVIDER_DEFAULT, token) <> 0 Then
                    If DuplicateToken(token, 2, tokenDuplicate) <> 0 Then
                        tempWindowsIdentity = New WindowsIdentity(tokenDuplicate)
                        impersonationContext = tempWindowsIdentity.Impersonate()
                        If Not impersonationContext Is Nothing Then
                            impersonateValidUser = True
                        End If
                    End If
                End If
            End If
            If Not tokenDuplicate.Equals(IntPtr.Zero) Then
                CloseHandle(tokenDuplicate)
            End If
            If Not token.Equals(IntPtr.Zero) Then
                CloseHandle(token)
            End If
        Catch
        End Try

    End Function

    Private Sub undoImpersonation()
        Try
            impersonationContext.Undo()
        Catch
        End Try
    End Sub

    Public Function Read(Optional ByRef strBookingId As String = "",
                         Optional ByRef strBookingReference As String = "",
                         Optional ByRef dblBookingNumber As Double = 0,
                         Optional ByRef rsHeader As ADODB.Recordset = Nothing,
                         Optional ByRef rsSegment As ADODB.Recordset = Nothing,
                         Optional ByRef rsPassenger As ADODB.Recordset = Nothing,
                         Optional ByRef rsRemark As ADODB.Recordset = Nothing,
                         Optional ByRef rsPayment As ADODB.Recordset = Nothing,
                         Optional ByRef rsMapping As ADODB.Recordset = Nothing) As Boolean

        Dim bResult As Boolean = False
        Dim objComBooking As tikAeroProcess.Booking = Nothing

        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If
            bResult = objComBooking.Read(strBookingId, strBookingReference, dblBookingNumber,
                                         rsHeader, rsSegment, rsPassenger, rsRemark,
                                         rsPayment, rsMapping)
            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
        End Try
        Return bResult

    End Function

    Public Function Save(Optional ByRef rsHeader As ADODB.Recordset = Nothing,
                         Optional ByRef rsSegment As ADODB.Recordset = Nothing,
                         Optional ByRef rsPassenger As ADODB.Recordset = Nothing,
                         Optional ByRef rsRemark As ADODB.Recordset = Nothing,
                         Optional ByRef rsPayment As ADODB.Recordset = Nothing,
                         Optional ByRef rsMapping As ADODB.Recordset = Nothing) As Boolean

        Dim bResult As Boolean = False
        Dim objComBooking As tikAeroProcess.Booking = Nothing

        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If
            bResult = objComBooking.Save(rsHeader, rsSegment, rsPassenger, rsRemark,
                                         rsPayment, rsMapping)
            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
        End Try
        Return bResult

    End Function

    Public Function SaveBookingHeader(Optional ByRef rsHeader As ADODB.Recordset = Nothing) As Boolean

        Dim bResult As Boolean = False
        Dim objComBooking As tikAeroProcess.Booking = Nothing

        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If
            bResult = objComBooking.Save(rsHeader, Nothing, Nothing, Nothing, Nothing, Nothing)
            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
        End Try
        Return bResult

    End Function

    Public Function GetClient(ByVal strClientId As String, ByVal strClientNumber As String, ByVal strPassengerId As String, ByRef rsClient As ADODB.Recordset, ByRef rsRemarks As ADODB.Recordset) As Boolean

        Dim bResult As Boolean = False
        Dim objComBooking As tikAeroProcess.Booking = Nothing
        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If
            bResult = objComBooking.GetClient(strClientId, strClientNumber, strPassengerId, rsClient, rsRemarks)
            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
        End Try
        Return bResult

    End Function

    Public Function GetClientPassenger(ByVal strBookingId As String, ByVal strClientProfileId As String, ByVal strClientNumber As String) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objComClient As tikAeroProcess.Client = Nothing

        Try
            If objComClient Is Nothing Then
                If strServer <> "" Then
                    objComClient = CreateObject("tikAeroProcess.Client", strServer)
                Else
                    objComClient = New tikAeroProcess.Client
                End If
            End If
            rsA = objComClient.GetClientPassenger(strBookingId, strClientProfileId, strClientNumber)
            Marshal.FinalReleaseComObject(objComClient)
            objComClient = Nothing
        Catch
            If IsNothing(objComClient) = False Then
                Marshal.FinalReleaseComObject(objComClient)
                objComClient = Nothing
            End If
        End Try
        Return rsA
    End Function

    '  Beware byref parameter and date conversion problem
    Public Function GetFareQuote(ByVal strOrigin As String,
                                   ByVal strDestination As String,
                                   ByVal dtDeparture As Date,
                                   Optional ByVal strPassengerType As String = "",
                                   Optional ByVal strAgencyCode As String = "",
                                   Optional ByVal strFlightNumber As String = "",
                                   Optional ByVal dtBooking As Date = #12/30/1899#,
                                   Optional ByVal dtTicketing As Date = #12/30/1899#,
                                   Optional ByVal strLanguage As String = "EN",
                                   Optional ByVal bRefundable As Boolean = False,
                                   Optional ByVal strFareCode As String = "",
                                   Optional ByVal strFareId As String = "",
                                   Optional ByVal bITFare As Boolean = False,
                                   Optional ByVal bStaffFare As Boolean = False,
                                   Optional ByVal bOpenReturn As Boolean = False,
                                   Optional ByVal strBoardingClass As String = "",
                                   Optional ByVal strBookingClass As String = "",
                                   Optional ByVal strCurrency As String = "",
                                   Optional ByVal strFareType As String = "FARE",
                                   Optional ByVal bNoVat As Boolean = False) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComInventory As tikAeroProcess.Inventory = Nothing

        Try
            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            rsA = objComInventory.GetFareQuote(strOrigin, strDestination, dtDeparture, strPassengerType,
                                    strAgencyCode, strFlightNumber, dtBooking, dtTicketing,
                                    strLanguage, bRefundable, bITFare, bStaffFare, bOpenReturn, strFareCode, strFareId, strBoardingClass, strBookingClass, strCurrency, strFareType, bNoVat)
            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing

        Catch
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function GetFareQuoteTax(ByVal strOrigin As String,
                                   ByVal strDestination As String,
                                   Optional ByVal strPassengerType As String = "",
                                   Optional ByVal strCurrency As String = "",
                                   Optional ByVal dtDeparture As Date = #12/30/1899#,
                                   Optional ByVal dtBooking As Date = #12/30/1899#,
                                   Optional ByVal strAgencyCode As String = "",
                                   Optional ByVal strLanguage As String = "EN",
                                   Optional ByVal bNoVat As Boolean = False) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComInventory As tikAeroProcess.Inventory = Nothing

        Try
            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            rsA = objComInventory.GetFareQuoteTax(strOrigin, strDestination, strPassengerType, strCurrency, dtDeparture, dtBooking, strAgencyCode, strLanguage, bNoVat)
            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing

        Catch
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
        End Try
        Return rsA

    End Function

    '  Beware byref parameter and date conversion problem
    Public Function GetAvailability(ByRef Origin As String,
                                    ByRef Destination As String,
                                    ByRef DepartDateFrom As Date,
                                    Optional ByRef DepartDateTo As Date = #12/30/1899#,
                                    Optional ByVal ReturnDateFrom As Date = #12/30/1899#,
                                    Optional ByRef ReturnDateTo As Date = #12/30/1899#,
                                    Optional ByRef DateBooking As Date = #12/30/1899#,
                                    Optional ByRef Adult As Short = 1,
                                    Optional ByRef Child As Short = 0,
                                    Optional ByRef Infant As Short = 0,
                                    Optional ByRef Other As Short = 0,
                                    Optional ByRef OtherPassengerType As String = "",
                                    Optional ByRef BoardingClass As String = "",
                                    Optional ByRef BookingClass As String = "",
                                    Optional ByRef DayTimeIndicator As String = "",
                                    Optional ByRef AgencyCode As String = "",
                                    Optional ByRef CurrencyCode As String = "",
                                    Optional ByRef FlightId As String = "",
                                    Optional ByRef FareId As String = "",
                                    Optional ByRef MaxAmount As Double = 0.0,
                                    Optional ByRef NonStopOnly As Boolean = False,
                                    Optional ByRef IncludeDeparted As Boolean = False,
                                    Optional ByRef IncludeCancelled As Boolean = False,
                                    Optional ByRef IncludeWaitlisted As Boolean = False,
                                    Optional ByRef IncludeSoldOut As Boolean = False,
                                    Optional ByRef Refundable As Boolean = False,
                                    Optional ByRef GroupFares As Boolean = False,
                                    Optional ByRef ItFaresOnly As Boolean = False,
                                    Optional ByRef rsReturn As ADODB.Recordset = Nothing,
                                    Optional ByVal PromotionCode As String = "",
                                    Optional ByVal FareType As String = "FARE",
                                    Optional ByVal strLanguage As String = "EN",
                                    Optional ByVal strIpAddress As String = "",
                                    Optional ByVal bReturnRefundable As Boolean = False,
                                    Optional ByVal bNoVat As Boolean = False) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComInventory As tikAeroProcess.Inventory = Nothing

        Try
            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            rsA = objComInventory.GetAvailability(Origin, Destination, DepartDateFrom, DepartDateTo, DateBooking, Adult, Child, Infant, Other, OtherPassengerType,
                                                  BoardingClass, BookingClass, DayTimeIndicator, AgencyCode, CurrencyCode, FlightId, FareId, MaxAmount,
                                                  NonStopOnly, IncludeDeparted, IncludeCancelled, IncludeWaitlisted, IncludeSoldOut, Refundable, GroupFares,
                                                  ItFaresOnly, False, False, False, "", ReturnDateFrom, ReturnDateTo,
                                                  rsReturn, True, bReturnRefundable, "", PromotionCode, FareType, strLanguage, strIpAddress, bNoVat)

            'Aurigny Special requirement to re-calculate tax for alternative route for london all airports
            'Check condition to for alternative route for now is LON (London all Airports)
            'Call function AlternativeCalculation to pass recordset outward and return to re-calculate tax 
            'by get New tax with correct route and calculate replace to total_adult_fare, total_child_fare, total_infant_fare, total_other_fare

            If ((Origin.ToUpper().Equals("LON") Or Destination.ToUpper().Equals("LON")) And rsA.RecordCount > 0) Then
                AlternativeCalculation(objComInventory, rsA, DateBooking, Adult, Child, Infant, Other, OtherPassengerType, AgencyCode, CurrencyCode, strLanguage, bNoVat)
                If (Not IsNothing(rsReturn)) Then
                    AlternativeCalculation(objComInventory, rsReturn, DateBooking, Adult, Child, Infant, Other, OtherPassengerType, AgencyCode, CurrencyCode, strLanguage, bNoVat)
                End If
            End If
            '-------------------------------------------------------------------------------------------------------------------------------------------'

            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing
        Catch e As Exception
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
            Throw
        End Try
        Return rsA
    End Function

    ''' <summary>
    ''' AlternativeCalculation to pass recordset outward and return to re-calculate tax
    ''' </summary>
    ''' <param name="objComInventory">Object inventory to call GetFareQuoteTax function to get taxs</param>
    ''' <param name="rs">Record set for outward and return</param>
    ''' <param name="DateBooking">Booking date</param>
    ''' <param name="Adult">Number of adult</param>
    ''' <param name="Child">Number of child</param>
    ''' <param name="Infant">Number of infant</param>
    ''' <param name="Other">Number of other passenger type</param>
    ''' <param name="OtherPassengerType">Other passenger type name</param>
    ''' <param name="AgencyCode">Agency code</param>
    ''' <param name="CurrencyCode">Currency code</param>
    ''' <param name="strLanguage">Language</param>
    ''' <param name="bNoVat">Vat</param>
    Public Sub AlternativeCalculation(objComInventory As tikAeroProcess.Inventory,
                                    ByRef rs As Recordset,
                                    Optional ByRef DateBooking As Date = #12/30/1899#,
                                    Optional ByRef Adult As Short = 1,
                                    Optional ByRef Child As Short = 0,
                                    Optional ByRef Infant As Short = 0,
                                    Optional ByRef Other As Short = 0,
                                    Optional ByRef OtherPassengerType As String = "",
                                    Optional ByRef AgencyCode As String = "",
                                    Optional ByRef CurrencyCode As String = "",
                                    Optional ByVal strLanguage As String = "EN",
                                    Optional ByVal bNoVat As Boolean = False)
        Dim dictionary As New Dictionary(Of String, Integer)
        dictionary.Add("ADULT", Adult)
        dictionary.Add("CHD", Child)
        dictionary.Add("INFANT", Infant)
        dictionary.Add(OtherPassengerType, Other)

        'standard case
        Dim dTax As Double = 0
        Dim dNonstop As Double = 0
        Dim dTransit As Double = 0
        Dim dFareAmount As Double = 0
        Dim dNoFareAmount As Double = 0

        Dim strTax As String = ""
        Dim strFareBasis As String = ""
        Dim strClass As String = ""

        Dim list As New List(Of Availability)
        Dim avai As Availability
        rs.MoveFirst()
        While Not rs.EOF
            avai = New Availability()
            avai.origin_rcd = rs.Fields("origin_rcd").Value
            avai.destination_rcd = rs.Fields("destination_rcd").Value
            avai.currency_rcd = rs.Fields("currency_rcd").Value
            avai.departure_date = rs.Fields("departure_date").Value
            avai.fare_code = rs.Fields("fare_code").Value
            avai.booking_class_rcd = rs.Fields("booking_class_rcd").Value
            avai.adult_fare = rs.Fields("adult_fare").Value
            avai.child_fare = rs.Fields("child_fare").Value
            avai.infant_fare = rs.Fields("infant_fare").Value
            avai.other_fare = rs.Fields("other_fare").Value
            list.Add(avai)
            rs.MoveNext()
        End While

        Dim group = (From g In list
                     Group g By g.origin_rcd,
                        g.destination_rcd,
                        g.currency_rcd,
                        g.departure_date Into gr = Group
                     Select New Availability With {.origin_rcd = origin_rcd,
                         .destination_rcd = destination_rcd,
                         .currency_rcd = currency_rcd,
                         .departure_date = departure_date}).ToList()

        Dim rsTax As Recordset
        Dim pair As KeyValuePair(Of String, Integer)
        Dim taxs As New List(Of AlternativeTax)
        Dim tax As AlternativeTax

        For Each item As Availability In group
            For Each pair In dictionary
                If (pair.Value > 0) Then
                    rsTax = objComInventory.GetFareQuoteTax(item.origin_rcd, item.destination_rcd, pair.Key, item.currency_rcd, item.departure_date, DateBooking, AgencyCode, strLanguage, bNoVat)
                    If (rsTax.RecordCount > 0) Then
                        tax = New AlternativeTax()
                        tax.origin_rcd = item.origin_rcd
                        tax.destination_rcd = item.destination_rcd
                        tax.currency_rcd = item.currency_rcd
                        tax.departure_date = item.departure_date
                        tax.passenger_type = pair.Key
                        tax.tax_set = rsTax
                        taxs.Add(tax)
                    End If
                End If
            Next
        Next

        Dim fare As Double
        rs.MoveFirst()
        While Not rs.EOF
            For i As Integer = 0 To taxs.Count - 1
                If (rs.Fields("origin_rcd").Value = taxs(i).origin_rcd And rs.Fields("destination_rcd").Value = taxs(i).destination_rcd And
                        rs.Fields("currency_rcd").Value = taxs(i).currency_rcd And rs.Fields("departure_date").Value = taxs(i).departure_date) Then
                    dTax = 0
                    dFareAmount = 0
                    dTransit = 0
                    dNonstop = 0
                    CalculatePassengerTax(taxs(i).tax_set,
                       rs.Fields("fare_code").Value,
                       rs.Fields("booking_class_rcd").Value,
                       dTax,
                       strTax,
                       CurrencyCode,
                       dFareAmount,
                       dTransit,
                       dNonstop,
                       bNoVat)
                    If (taxs(i).passenger_type = "ADULT") Then
                        fare = rs.Fields("adult_fare").Value
                        rs.Fields("total_adult_fare").Value = fare + dNonstop
                    ElseIf (taxs(i).passenger_type = "CHD") Then
                        fare = rs.Fields("child_fare").Value
                        rs.Fields("total_child_fare").Value = fare + dNonstop
                    ElseIf (taxs(i).passenger_type = "INFANT") Then
                        fare = rs.Fields("infant_fare").Value
                        rs.Fields("total_infant_fare").Value = fare + dNonstop
                    ElseIf (taxs(i).passenger_type = OtherPassengerType) Then
                        fare = rs.Fields("other_fare").Value
                        rs.Fields("total_other_fare").Value = fare + dNonstop
                    End If
                End If
            Next

            rs.MoveNext()
        End While
    End Sub


    Public Sub CalculatePassengerTax(ByVal rsTax As ADODB.Recordset,
                                 ByRef strFareBasis As String,
                                 ByRef strClass As String,
                                 ByRef dTax As Double,
                                 ByRef strTax As String,
                                 ByRef strCurrencyCode As String,
                                 ByRef dFareAmount As Double,
                                 ByRef dTransit As Double,
                                 ByRef dNonstop As Double,
                                 Optional bNoVat As Boolean = False)

        Dim dPercentageTax As Double

        Try
            Dim fare_code As String
            Dim valid_for_class As String

            With rsTax
                Do While .EOF = False
                    fare_code = Nz(.Fields("fare_code").Value)
                    valid_for_class = Nz(.Fields("valid_for_class").Value)

                    If Not IsNothing(fare_code) And Not Nz(fare_code) = strFareBasis Then
                        'we have a fare basis but no matching fare basis
                    ElseIf Not IsNothing(valid_for_class) And InStr(Nz(valid_for_class), strClass) = 0 Then
                        'we have a class mapping but fare class does not match
                    Else

                        If bNoVat = True Then
                            dTax = N0(.Fields("tax_amount").Value)
                        Else
                            dTax = N0(.Fields("tax_amount_incl").Value)
                        End If
                        strTax = strTax & "|" & .Fields("tax_rcd").Value

                        If strCurrencyCode = Nz(.Fields("tax_currency").Value) Then
                            'no need to convert the tax
                        Else
                            dTax = IIf(.Fields("exchange_to_accounting").Value = 0, dTax, .Fields("exchange_to_accounting").Value * dTax)
                            dTax = IIf(.Fields("exchange_from_accounting").Value = 0, dTax, .Fields("exchange_from_accounting").Value * dTax)
                            dTax = Math.Round(dTax, 2)
                        End If

                        '----------------------------------------------------------------------
                        'percentage handling
                        '----------------------------------------------------------------------

                        If Not .Fields("tax_percentage").Value = 0 Then

                            If Not dFareAmount = 0 Then
                                dPercentageTax = dFareAmount * .Fields("tax_percentage").Value / 100
                                dPercentageTax = dPercentageTax + (dPercentageTax * .Fields("vat_percentage").Value / 100)
                            End If

                            If .Fields("minimum_tax_amount_flag").Value = 0 Then
                                If dTax < dPercentageTax Then
                                    dTax = dPercentageTax
                                End If
                            Else
                                If dTax > dPercentageTax Then
                                    dTax = dPercentageTax
                                End If
                            End If

                            dTax = Math.Round(dTax, 2)
                            dPercentageTax = Math.Round(dPercentageTax, 2)

                        End If

                        '----------------------------------------------------------------------

                        dTransit = dTransit + Math.Round((dTax * .Fields("origin_tax_distribution").Value / 100), 2) + Math.Round((dTax * .Fields("destination_tax_distribution").Value / 100), 2)

                        If .Fields("transit_tax_only_flag").Value = 0 Then
                            'we do have a nonstop flag and this tax applies to
                            dNonstop = dNonstop + dTax
                        End If

                    End If

                    .MoveNext()

                Loop

                .Filter = ADODB.FilterGroupEnum.adFilterNone

            End With
        Catch ex As Exception
            Throw ex
        End Try
        '--------------------------------
        'ErrorHandling:
        '--------------------------------

    End Sub

    Public Shared Function Nz(ByVal Value As Object, Optional ByVal def As Object = Nothing) As Object
        If Value Is Nothing OrElse IsDBNull(Value) Then
            Return def
        Else
            Return Value
        End If
    End Function

    Private Function N0(Value As Object) As Double

        If IsNothing(Value) Then
            Return 0
        End If

        Return Value

    End Function

    Public Sub FilterAvailability(ByRef rsOutbound As ADODB.Recordset,
                                ByVal iAdult As Integer,
                                ByVal iChild As Integer,
                                ByVal iInfant As Integer,
                                ByVal iOther As Integer,
                                Optional ByRef rsReturn As ADODB.Recordset = Nothing,
                                Optional ByVal rsLogic As ADODB.Recordset = Nothing,
                                Optional ByVal strOrigin As String = "",
                                Optional ByVal strDestination As String = "",
                                Optional ByVal dtOutbound As Date = #12/30/1899#,
                                Optional ByVal dtReturn As Date = #12/30/1899#,
                                Optional ByVal dtBooking As Date = #12/30/1899#,
                                Optional ByVal bReturn As Boolean = False,
                                Optional ByVal bLowest As Boolean = False,
                                Optional ByVal bLowestClass As Boolean = False,
                                Optional ByVal bLowestGroup As Boolean = False,
                                Optional ByVal bShowClosed As Boolean = True,
                                Optional ByVal bSort As Boolean = True,
                                Optional ByVal bDelete As Boolean = True,
                                Optional ByVal bSkipFilterFareLogin As Boolean = False,
                                Optional ByVal bReturnRefundable As Boolean = False)

        Dim objComInventory As tikAeroProcess.Inventory = Nothing

        Try
            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            Call objComInventory.FilterAvailability(rsOutbound,
                                                    iAdult,
                                                    iChild,
                                                    iInfant,
                                                    iOther,
                                                    rsReturn,
                                                    rsLogic,
                                                    strOrigin,
                                                    strDestination,
                                                    dtOutbound,
                                                    dtReturn,
                                                    dtBooking,
                                                    bReturn,
                                                    bLowest,
                                                    bLowestClass,
                                                    bLowestGroup,
                                                    bShowClosed,
                                                    bSort,
                                                    bDelete,
                                                    bSkipFilterFareLogin,
                                                    bReturnRefundable)

            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing
        Catch
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
            Throw
        End Try
    End Sub

    '  Beware byref parameter and date conversion problem
    Public Function GetFlights(Optional ByVal strOrigin As String = "",
                               Optional ByVal strDestination As String = "",
                               Optional ByVal strAirline As String = "",
                               Optional ByVal strFlightNumber As String = "",
                               Optional ByVal dtFlightFrom As Date = #12/30/1899#,
                               Optional ByVal dtFlightTo As Date = #12/30/1899#,
                               Optional ByVal strFlightStatus As String = "",
                               Optional ByVal strCheckInStatus As String = "",
                               Optional ByVal strLanguage As String = "EN") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComInventory As tikAeroProcess.Inventory = Nothing

        Try
            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            rsA = objComInventory.GetFlights(strOrigin, strDestination, strAirline, strFlightNumber,
                                             dtFlightFrom, dtFlightTo, strFlightStatus, strCheckInStatus, strLanguage)
            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing
        Catch e As Exception
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function GetAirline(Optional ByVal strLanguage As String = "EN") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComInventory As tikAeroProcess.Inventory = Nothing

        Try

            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            rsA = objComInventory.GetAirlines(strLanguage)
            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing
        Catch e As Exception
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
        End Try

        Return rsA
    End Function

    Public Function GetDestination(Optional ByVal strLanguage As String = "EN",
                                   Optional ByVal b2cFlag As Boolean = False,
                                   Optional ByVal b2bFlag As Boolean = False,
                                   Optional ByVal b2eFlag As Boolean = False,
                                   Optional ByVal b2sFlag As Boolean = False,
                                   Optional ByVal apiFlag As Boolean = False) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComInventory As tikAeroProcess.Inventory = Nothing

        Try
            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            rsA = objComInventory.GetDestinations(strLanguage, b2cFlag, b2bFlag, b2eFlag, b2sFlag, apiFlag)
            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing
        Catch e As Exception
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function GetAirlines(Optional ByVal strLanguage As String = "EN") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComInventory As tikAeroProcess.Inventory = Nothing

        Try
            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            rsA = objComInventory.GetAirlines(strLanguage)
            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing
        Catch e As Exception
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function GetOrigins(Optional ByVal strLanguage As String = "EN",
                               Optional ByVal b2cFlag As Boolean = False,
                               Optional ByVal b2bFlag As Boolean = False,
                               Optional ByVal b2eFlag As Boolean = False,
                               Optional ByVal b2sFlag As Boolean = False,
                               Optional ByVal apiFlag As Boolean = False) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComInventory As tikAeroProcess.Inventory = Nothing
        Try

            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            rsA = objComInventory.GetOrigins(strLanguage, b2cFlag, b2bFlag, b2eFlag, b2sFlag, apiFlag)
            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing
        Catch
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function GetFlightSummary(ByVal strAirline As String,
                                     ByVal strFlightNumber As String,
                                     ByVal dtFlight As Date) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComInventory As tikAeroProcess.Inventory = Nothing
        Try

            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            rsA = objComInventory.GetFlightSummary(strAirline, strFlightNumber, dtFlight)
            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing
        Catch e As Exception
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetTicketSales(Optional ByVal AgencyCode As String = "", Optional ByVal UserId As String = "", Optional ByVal Origin As String = "", Optional ByVal Destination As String = "", Optional ByVal Airline As String = "", Optional ByVal FlightNumber As String = "", Optional ByVal FlightFrom As Date = #12/30/1899#, Optional ByVal FlightTo As Date = #12/30/1899#, Optional ByVal TicketingFrom As Date = #12/30/1899#, Optional ByVal TicketingTo As Date = #12/30/1899#, Optional ByVal PassengerType As String = "", Optional ByVal Language As String = "EN") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComReservation As tikAeroProcess.Reservation = Nothing
        Try

            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Reservation", strServer)
                Else
                    objComReservation = New tikAeroProcess.Reservation
                End If
            End If
            rsA = objComReservation.GetTicketSales(AgencyCode, UserId, Origin, Destination, Airline, FlightNumber, FlightFrom, FlightTo, TicketingFrom, TicketingTo, PassengerType, Language)
            Marshal.FinalReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch e As Exception
            If IsNothing(objComReservation) = False Then
                Marshal.FinalReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return rsA
    End Function


    Public Function GetBookings(Optional ByVal Airline As String = "",
                                Optional ByVal FlightNumber As String = "",
                                Optional ByVal FlightId As String = "",
                                Optional ByVal FlightFrom As Date = #12/30/1899#,
                                Optional ByVal FlightTo As Date = #12/30/1899#,
                                Optional ByVal RecordLocator As String = "",
                                Optional ByVal Origin As String = "",
                                Optional ByVal Destination As String = "",
                                Optional ByVal PassengerName As String = "",
                                Optional ByVal SeatNumber As String = "",
                                Optional ByVal TicketNumber As String = "",
                                Optional ByVal PhoneNumber As String = "",
                                Optional ByVal AgencyCode As String = "",
                                Optional ByVal ClientNumber As String = "",
                                Optional ByVal MemberNumber As String = "",
                                Optional ByVal ClientId As String = "",
                                Optional ByVal ShowHistory As Boolean = False,
                                Optional ByVal Language As String = "EN",
                                Optional ByVal bIndividual As Boolean = True,
                                Optional ByVal bGroup As Boolean = True,
                                Optional ByVal CreateFrom As Date = #12/30/1899#,
                                Optional ByVal CreateTo As Date = #12/30/1899#) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComReservation As tikAeroProcess.Reservation = Nothing
        Try

            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Reservation", strServer)
                Else
                    objComReservation = New tikAeroProcess.Reservation
                End If
            End If
            rsA = objComReservation.GetBookings(Airline, FlightNumber, FlightId, FlightFrom, FlightTo, RecordLocator, Origin, Destination, PassengerName, SeatNumber, TicketNumber, PhoneNumber, AgencyCode, ClientNumber, MemberNumber, ClientId, ShowHistory, bIndividual, bGroup, Language, CreateFrom, CreateTo)
            Marshal.FinalReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch e As Exception
            If IsNothing(objComReservation) = False Then
                Marshal.FinalReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function BookingLogon(ByRef RecordLocator As String, ByRef NameOrPhone As String) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objComSystem As tikAeroProcess.System = Nothing
        Try
            If objComSystem Is Nothing Then
                If strServer <> "" Then
                    objComSystem = CreateObject("tikAeroProcess.System", strServer)
                Else
                    objComSystem = New tikAeroProcess.System
                End If
            End If
            rsA = objComSystem.BookingLogon(RecordLocator, NameOrPhone)
            Marshal.FinalReleaseComObject(objComSystem)
            objComSystem = Nothing
        Catch e As Exception
            If IsNothing(objComSystem) = False Then
                Marshal.FinalReleaseComObject(objComSystem)
                objComSystem = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function TravelAgentLogon(ByVal agencyCode As String, ByVal agentLogon As String, ByVal agentPassword As String) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComSystem As tikAeroProcess.System = Nothing

        Try
            If objComSystem Is Nothing Then
                If strServer <> "" Then
                    objComSystem = CreateObject("tikAeroProcess.System", strServer)
                Else
                    objComSystem = New tikAeroProcess.System
                End If
            End If
            rsA = objComSystem.TravelAgentLogon(agencyCode, agentLogon, agentPassword)
            Marshal.FinalReleaseComObject(objComSystem)
            objComSystem = Nothing
        Catch e As Exception
            If IsNothing(objComSystem) = False Then
                Marshal.FinalReleaseComObject(objComSystem)
                objComSystem = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function ClientLogon(ByVal ClientNumber As String, ByVal ClientPassword As String) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComSystem As tikAeroProcess.System = Nothing

        Try
            If objComSystem Is Nothing Then
                If strServer <> "" Then
                    objComSystem = CreateObject("tikAeroProcess.System", strServer)
                Else
                    objComSystem = New tikAeroProcess.System
                End If
            End If
            rsA = objComSystem.ClientLogon(ClientNumber, ClientPassword)
            Marshal.FinalReleaseComObject(objComSystem)
            objComSystem = Nothing
        Catch e As Exception
            If IsNothing(objComSystem) = False Then
                Marshal.FinalReleaseComObject(objComSystem)
                objComSystem = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function GetFormOfPayments(Optional ByVal strLanguage As String = "EN") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objPayments As tikAeroProcess.Payment = Nothing

        Try
            If objPayments Is Nothing Then
                If strServer <> "" Then
                    objPayments = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objPayments = New tikAeroProcess.Payment
                End If
            End If
            rsA = objPayments.GetFormOfPayments(strLanguage)
            Marshal.FinalReleaseComObject(objPayments)
            objPayments = Nothing
        Catch e As Exception
            If IsNothing(objPayments) = False Then
                Marshal.FinalReleaseComObject(objPayments)
                objPayments = Nothing
            End If
        End Try
        Return rsA

    End Function
    Public Function GetFormOfPaymentSubTypes(Optional ByVal strType As String = "", Optional ByVal strLanguage As String = "EN") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objPayments As tikAeroProcess.Payment = Nothing

        Try
            If objPayments Is Nothing Then
                If strServer <> "" Then
                    objPayments = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objPayments = New tikAeroProcess.Payment
                End If
            End If
            rsA = objPayments.GetFormOfPaymentSubTypes(strType, strLanguage)
            Marshal.FinalReleaseComObject(objPayments)
            objPayments = Nothing
        Catch e As Exception
            If IsNothing(objPayments) = False Then
                Marshal.FinalReleaseComObject(objPayments)
                objPayments = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function ReadFormOfPayment(Optional ByVal strType As String = "") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objSettings As tikAeroProcess.Settings = Nothing

        Try
            If objSettings Is Nothing Then
                If strServer <> "" Then
                    objSettings = CreateObject("tikAeroProcess.settings", strServer)
                Else
                    objSettings = New tikAeroProcess.Settings
                End If
            End If
            rsA = objSettings.FormOfPaymentRead(strType, "", "", False)
            Marshal.FinalReleaseComObject(objSettings)
            objSettings = Nothing
        Catch e As Exception
            If IsNothing(objSettings) = False Then
                Marshal.FinalReleaseComObject(objSettings)
                objSettings = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function GetAirportCode(Optional ByVal strOdOrigin As String = "") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim rs0 As ADODB.Recordset = Nothing
        Dim rs1 As ADODB.Recordset = Nothing
        Dim objSettings As tikAeroProcess.Settings = Nothing

        Try
            If objSettings Is Nothing Then
                If strServer <> "" Then
                    objSettings = CreateObject("tikAeroProcess.settings", strServer)
                Else
                    objSettings = New tikAeroProcess.Settings
                End If
            End If
            rsA = objSettings.AirportRead(strOdOrigin, , , , , False)
            Marshal.FinalReleaseComObject(objSettings)
            objSettings = Nothing
        Catch e As Exception
            If IsNothing(objSettings) = False Then
                Marshal.FinalReleaseComObject(objSettings)
                objSettings = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function GetAirportTimezone(Optional ByVal strCode As String = "") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim rs0 As ADODB.Recordset = Nothing
        Dim rs1 As ADODB.Recordset = Nothing
        Dim objSettings As tikAeroProcess.Settings = Nothing

        Try
            If objSettings Is Nothing Then
                If strServer <> "" Then
                    objSettings = CreateObject("tikAeroProcess.settings", strServer)
                Else
                    objSettings = New tikAeroProcess.Settings
                End If
            End If
            rsA = objSettings.TimeZoneRead(strCode, , False, True)
            Marshal.FinalReleaseComObject(objSettings)
            objSettings = Nothing
        Catch e As Exception
            If IsNothing(objSettings) = False Then
                Marshal.FinalReleaseComObject(objSettings)
                objSettings = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function GetCurrencies(Optional ByVal strLanguage As String = "EN") As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objPayments As tikAeroProcess.Payment = Nothing

        Try
            If objPayments Is Nothing Then
                If strServer <> "" Then
                    objPayments = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objPayments = New tikAeroProcess.Payment
                End If
            End If
            rsA = objPayments.GetCurrencies(strLanguage)
            Marshal.FinalReleaseComObject(objPayments)
            objPayments = Nothing
        Catch e As Exception
            If IsNothing(objPayments) = False Then
                Marshal.FinalReleaseComObject(objPayments)
                objPayments = Nothing
            End If
        End Try
        Return rsA
    End Function
    Public Overridable Function GetFormOfPaymentSubtypeFees(Optional ByRef FormOfPayment As String = "",
                                                            Optional ByRef FormOfPaymentSubtype As String = "",
                                                            Optional ByRef CurrencyRcd As String = "",
                                                            Optional ByRef Agency As String = "",
                                                            Optional ByRef FeeDate As Date = #12/30/1899#) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objPayments As tikAeroProcess.Payment = Nothing
        Try
            If objPayments Is Nothing Then
                If strServer <> "" Then
                    objPayments = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objPayments = New tikAeroProcess.Payment
                End If
            End If
            rsA = objPayments.GetFormOfPaymentSubtypeFees(FormOfPayment, FormOfPaymentSubtype, CurrencyRcd, Agency, FeeDate)
            Marshal.FinalReleaseComObject(objPayments)
            objPayments = Nothing

            If IsNothing(rsA) = True Or rsA Is DBNull.Value Then
                Throw New NullReferenceException("GetFormOfPaymentSubtypeFees return null return set.")
            End If
        Catch e As Exception
            If IsNothing(objPayments) = False Then
                Marshal.FinalReleaseComObject(objPayments)
                objPayments = Nothing
            End If

            Throw
        End Try
        Return rsA

    End Function

    Public Function PaymentSave(ByVal rsPayments As ADODB.Recordset, ByVal rsAllocation As ADODB.Recordset, ByVal refundVoucher As String) As Boolean

        Dim objPayments As tikAeroProcess.Payment = Nothing
        Dim b As Boolean = False
        Try
            If objPayments Is Nothing Then
                If strServer <> "" Then
                    objPayments = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objPayments = New tikAeroProcess.Payment
                End If
            End If
            b = objPayments.Save(rsPayments, rsAllocation)
            Marshal.FinalReleaseComObject(objPayments)
            objPayments = Nothing
            Return b
        Catch e As Exception
            If IsNothing(objPayments) = False Then
                Marshal.FinalReleaseComObject(objPayments)
                objPayments = Nothing
            End If
            Return b
        End Try
    End Function

    Public Function GetLanguages(Optional ByVal strLanguage As String = "EN") As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objComReservation As tikAeroProcess.Reservation = Nothing

        Try
            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Reservation", strServer)
                Else
                    objComReservation = New tikAeroProcess.Reservation
                End If
            End If
            rsA = objComReservation.GetLanguages(strLanguage)
            Marshal.FinalReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch e As Exception
            If IsNothing(objComReservation) = False Then
                Marshal.FinalReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function GetXml(ByVal strBookingID As String, ByVal strPassengerId As String, ByVal strLanguageCode As String) As String
        Dim xml As String = ""
        Dim objComXml As tikAeroProcess.XML = Nothing

        Try
            If objComXml Is Nothing Then
                If strServer <> "" Then
                    objComXml = CreateObject("tikAeroProcess.XML", strServer)
                Else
                    objComXml = New tikAeroProcess.XML
                End If
            End If
            xml = objComXml.GetItinerary(strBookingID, strLanguageCode, strPassengerId)
            Marshal.FinalReleaseComObject(objComXml)
            objComXml = Nothing
        Catch e As Exception
            If IsNothing(objComXml) = False Then
                Marshal.FinalReleaseComObject(objComXml)
                objComXml = Nothing
            End If

            Throw
        End Try
        Return xml
    End Function

    Public Function GetPassengerTitles(Optional ByVal strLanguage As String = "EN") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComReservation As tikAeroProcess.Reservation = Nothing
        Try
            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Reservation", strServer)
                Else
                    objComReservation = New tikAeroProcess.Reservation
                End If
            End If
            rsA = objComReservation.GetPassengerTitles(strLanguage)
            Marshal.FinalReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch
            If IsNothing(objComReservation) = False Then
                Marshal.FinalReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return rsA

    End Function
    Public Function GetDocumentType(Optional ByVal strLanguage As String = "EN") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComInventory As tikAeroProcess.Inventory = Nothing

        Try
            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            rsA = objComInventory.GetDocumentType(strLanguage)
            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing
        Catch
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function GetPassengerTypes(Optional ByVal strLanguage As String = "EN") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComInventory As tikAeroProcess.Inventory = Nothing

        Try
            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            rsA = objComInventory.GetPassengerTypes(strLanguage)
            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing
        Catch
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function GetAgencyCode(ByVal strAgencyCode As String) As ADODB.Recordset

        Dim rsA As ADODB.Recordset
        Dim objComSetting As tikAeroProcess.Settings = Nothing

        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            rsA = objComSetting.AgencyRead(strAgencyCode)
            Marshal.FinalReleaseComObject(objComSetting)
            objComSetting = Nothing
        Catch e As Exception
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
                objComSetting = Nothing
            End If
            Throw New System.Exception(e.Message & " " & e.StackTrace)
        End Try
        Return rsA
    End Function

    Public Function GetItinerary(ByVal strBookingId As String,
                                    ByVal strLanguage As String,
                                    Optional ByRef strRecordLocator As String = "",
                                    Optional ByRef strNameOrPhone As String = "",
                                    Optional ByRef strAgency As String = "",
                                    Optional ByRef rsHeader As ADODB.Recordset = Nothing,
                                    Optional ByRef rsSegment As ADODB.Recordset = Nothing,
                                    Optional ByRef rsPassenger As ADODB.Recordset = Nothing,
                                    Optional ByRef rsRemark As ADODB.Recordset = Nothing,
                                    Optional ByRef rsPayment As ADODB.Recordset = Nothing,
                                    Optional ByRef rsMapping As ADODB.Recordset = Nothing,
                                    Optional ByRef rsService As ADODB.Recordset = Nothing,
                                    Optional ByRef rsTax As ADODB.Recordset = Nothing,
                                    Optional ByRef rsFee As ADODB.Recordset = Nothing) As Boolean

        Dim bResult As Boolean
        Dim objComBooking As tikAeroProcess.Booking = Nothing
        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If
            bResult = objComBooking.GetItinerary(strBookingId, strLanguage, strRecordLocator, strNameOrPhone, strAgency, rsHeader, rsSegment, rsPassenger, rsRemark, rsPayment, rsMapping, rsService, rsTax, rsFee)
            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch e As Exception
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
        End Try
        Return bResult
    End Function

    Public Function GetAgencyAccountTransactions(ByVal agencyCode As String, ByVal dateFrom As DateTime, ByVal dateTo As DateTime) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objComReservation As tikAeroProcess.Payment = Nothing

        Try
            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objComReservation = New tikAeroProcess.Payment
                End If
            End If
            rsA = objComReservation.GetAgencyAccountTransactions(agencyCode, dateTo)
            Marshal.ReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch e As Exception
            If IsNothing(objComReservation) = False Then
                Marshal.ReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function GetAgencyAccountTopUp(ByVal strAgencyCode As String, ByVal strCurrency As String, ByVal dtFrom As DateTime, ByVal dtTo As DateTime) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objComReservation As tikAeroProcess.Payment = Nothing

        Try
            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objComReservation = New tikAeroProcess.Payment
                End If
            End If
            rsA = objComReservation.GetAgencyAccountTopUp(strAgencyCode, strCurrency, dtFrom, dtTo)
            Marshal.ReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch e As Exception
            If IsNothing(objComReservation) = False Then
                Marshal.ReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return rsA

    End Function

    ' Start Yai add B2A
    Public Function GetAgencyAccountBalance(ByVal strAgencyCode As String, ByVal strAgencyName As String, ByVal strCurrency As String, ByVal strConsolidatorAgency As String) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objComReservation As tikAeroProcess.Payment = Nothing

        Try
            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objComReservation = New tikAeroProcess.Payment
                End If
            End If
            rsA = objComReservation.GetAgencyAccountBalance(strAgencyCode, strAgencyName, strCurrency, strConsolidatorAgency)
            Marshal.ReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch e As Exception
            If IsNothing(objComReservation) = False Then
                Marshal.ReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function AgencySave(ByRef rsAgency As ADODB.Recordset) As Boolean
        Dim bResult As Boolean = False
        Dim objComSetting As tikAeroProcess.Settings = Nothing

        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            bResult = objComSetting.AgencySave(rsAgency)
            Marshal.FinalReleaseComObject(objComSetting)
            objComSetting = Nothing
        Catch
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
                objComSetting = Nothing
            End If
        End Try
        Return bResult
    End Function

    Public Function AgencyDefaultUserSave(ByRef rsAgency As ADODB.Recordset,
                                          ByVal strAgencyType As String,
                                          ByVal strUserLogon As String,
                                          ByVal strPassword As String,
                                          ByVal strLastName As String,
                                          Optional ByVal strFirstName As String = "",
                                          Optional ByVal strTitle As String = "",
                                          Optional ByVal strUserId As String = "",
                                          Optional ByVal iChangeBooking As Short = 0,
                                          Optional ByVal iChangeSegment As Short = 0,
                                          Optional ByVal iDeleteSegment As Short = 0,
                                          Optional ByVal iTicketIssue As Short = 0) As Boolean

        Dim bResult As Boolean = False
        Dim objComSetting As tikAeroProcess.Settings = Nothing

        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            bResult = objComSetting.AgencyDefaultUserSave(rsAgency, strAgencyType, strUserLogon, strPassword, strLastName, strFirstName, strTitle, strUserId, iChangeBooking, iChangeSegment, iDeleteSegment, iTicketIssue)
            Marshal.FinalReleaseComObject(objComSetting)
            objComSetting = Nothing
        Catch
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
                objComSetting = Nothing
            End If
        End Try
        Return bResult
    End Function

    Public Function AgencyAccountSave(Optional ByRef rsAccount As ADODB.Recordset = Nothing) As Boolean
        Dim bResult As Boolean
        Dim objComSetting As tikAeroProcess.Settings = Nothing
        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            bResult = objComSetting.AgencyAccountSave(rsAccount)
            Marshal.FinalReleaseComObject(objComSetting)
            objComSetting = Nothing
        Catch
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
                objComSetting = Nothing
            End If
        End Try
        Return bResult
    End Function

    Public Function GetEmptyAccountBalance() As ADODB.Recordset
        Dim objComSettings As tikAeroProcess.Settings = Nothing
        Dim rsAccountBalance As ADODB.Recordset = Nothing
        Try
            If objComSettings Is Nothing Then
                If strServer <> "" Then
                    objComSettings = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSettings = New tikAeroProcess.Settings
                End If
            End If
            rsAccountBalance = objComSettings.AgencyAccountGetEmpty()
            Marshal.FinalReleaseComObject(objComSettings)
            objComSettings = Nothing
        Catch e As Exception
            If IsNothing(objComSettings) = False Then
                Marshal.FinalReleaseComObject(objComSettings)
                objComSettings = Nothing
            End If
        End Try
        Return rsAccountBalance
    End Function

    Public Function GetEmptyAgency() As ADODB.Recordset
        Dim objComSettings As tikAeroProcess.Settings = Nothing
        Dim rsAgency As ADODB.Recordset = Nothing
        Try
            If objComSettings Is Nothing Then
                If strServer <> "" Then
                    objComSettings = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSettings = New tikAeroProcess.Settings
                End If
            End If
            rsAgency = objComSettings.AgencyGetEmpty()
            Marshal.FinalReleaseComObject(objComSettings)
            objComSettings = Nothing
        Catch e As Exception
            If IsNothing(objComSettings) = False Then
                Marshal.FinalReleaseComObject(objComSettings)
                objComSettings = Nothing
            End If
        End Try
        Return rsAgency
    End Function

    Public Function AgencyRead(ByVal strAgencyCode As String) As ADODB.Recordset
        Dim rsA As ADODB.Recordset
        Dim objComSetting As tikAeroProcess.Settings = Nothing
        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            rsA = objComSetting.AgencyRead(strAgencyCode)
            Marshal.FinalReleaseComObject(objComSetting)
            objComSetting = Nothing
        Catch e As Exception
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
                objComSetting = Nothing
            End If
            Throw New System.Exception("No found agency information" & " " & e.Message)
        End Try
        Return rsA
    End Function
    ' End Yai add B2A

    Public Overridable Function GetSeatMap(Optional ByRef strOrigin As String = "", Optional ByRef strDestination As String = "", Optional ByRef strFlightId As String = "", Optional ByRef strBoardingClass As String = "", Optional ByRef strBookingClass As String = "", Optional ByRef strLanguage As String = "") As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objComCheckIn As tikAeroProcess.CheckIn = Nothing
        Try
            If objComCheckIn Is Nothing Then
                If strServer <> "" Then
                    objComCheckIn = CreateObject("tikAeroProcess.CheckIn", strServer)
                Else
                    objComCheckIn = New tikAeroProcess.CheckIn
                End If
            End If

            If String.IsNullOrEmpty(strLanguage) Then
                strLanguage = "EN"
            End If
            rsA = objComCheckIn.GetSeatMap(strOrigin, strDestination, strFlightId, strBoardingClass, strBookingClass, strLanguage)
            Marshal.FinalReleaseComObject(objComCheckIn)
            objComCheckIn = Nothing
        Catch e As Exception
            If IsNothing(objComCheckIn) = False Then
                Marshal.FinalReleaseComObject(objComCheckIn)
                objComCheckIn = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Overridable Function GetSeatMapLayout(Optional ByRef strFlightId As String = "",
                                                 Optional ByRef strOrigin As String = "",
                                                 Optional ByRef strDestination As String = "",
                                                 Optional ByRef strBoardingClass As String = "",
                                                 Optional ByRef strConfiguration As String = "",
                                                 Optional ByRef strLanguage As String = "",
                                                 Optional ByRef rsCompartment As ADODB.Recordset = Nothing,
                                                 Optional ByRef rsSeatmap As ADODB.Recordset = Nothing,
                                                 Optional ByRef rsAttribute As ADODB.Recordset = Nothing) As Boolean
        Dim bResult As Boolean
        Dim objComFlights As tikAeroProcess.Flights = Nothing

        Try
            If objComFlights Is Nothing Then
                If strServer <> "" Then
                    objComFlights = CreateObject("tikAeroProcess.Flights", strServer)
                Else
                    objComFlights = New tikAeroProcess.Flights
                End If
            End If
            bResult = objComFlights.GetSeatmapLayout(strFlightId,
                                                     strOrigin,
                                                     strDestination,
                                                     strBoardingClass,
                                                     strConfiguration,
                                                     strLanguage,
                                                     rsCompartment,
                                                     rsSeatmap,
                                                     rsAttribute)
            Marshal.ReleaseComObject(objComFlights)
            objComFlights = Nothing
        Catch
            If IsNothing(objComFlights) = False Then
                Marshal.ReleaseComObject(objComFlights)
                objComFlights = Nothing
            End If
        End Try
        Return bResult
    End Function

    Public Overridable Function GetBookingSegmentCheckIn(Optional ByVal strBookingID As String = "", Optional ByVal strClientID As String = "", Optional ByVal strLanguageCode As String = "EN") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComCheckIn As tikAeroProcess.CheckIn = Nothing
        Try
            If objComCheckIn Is Nothing Then
                If strServer <> "" Then
                    objComCheckIn = CreateObject("tikAeroProcess.CheckIn", strServer)
                Else
                    objComCheckIn = New tikAeroProcess.CheckIn
                End If
            End If
            rsA = objComCheckIn.GetBookingSegmentCheckIn(strBookingID, strClientID, strLanguageCode)
            Marshal.FinalReleaseComObject(objComCheckIn)
            objComCheckIn = Nothing
        Catch e As Exception
            If IsNothing(objComCheckIn) = False Then
                Marshal.FinalReleaseComObject(objComCheckIn)
                objComCheckIn = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Overridable Function BoardPassengers(ByVal strFlightID As String, ByVal strOrigin As String, ByVal strBoard As String, ByVal strUserId As String, ByVal bBoard As Boolean) As String

        Dim strResult As String = String.Empty
        Dim rsFlight As ADODB.Recordset = Nothing
        Dim rsPassenger As ADODB.Recordset = Nothing
        Dim objComCheckIn As tikAeroProcess.CheckIn = Nothing
        Try
            If objComCheckIn Is Nothing Then
                If strServer <> "" Then
                    objComCheckIn = CreateObject("tikAeroProcess.CheckIn", strServer)
                Else
                    objComCheckIn = New tikAeroProcess.CheckIn
                End If
            End If
            strResult = objComCheckIn.BoardPassengers(strFlightID, strOrigin, strBoard, strUserId, rsFlight, rsPassenger, bBoard)
            Marshal.FinalReleaseComObject(objComCheckIn)
            objComCheckIn = Nothing
        Catch e As Exception
            If IsNothing(objComCheckIn) = False Then
                Marshal.FinalReleaseComObject(objComCheckIn)
                objComCheckIn = Nothing
            End If
        End Try
        Return strResult
    End Function

    Public Overridable Function OffloadPassenger(ByVal rsPassengerOffload As ADODB.Recordset, ByVal autoBaggageFlag As Boolean, ByVal strUserId As String) As String

        Dim strResult As String = String.Empty
        Dim rsMapping As ADODB.Recordset = Nothing
        Dim rsPassenger As ADODB.Recordset = Nothing
        Dim objComCheckIn As tikAeroProcess.CheckIn = Nothing
        Try
            If objComCheckIn Is Nothing Then
                If strServer <> "" Then
                    objComCheckIn = CreateObject("tikAeroProcess.CheckIn", strServer)
                Else
                    objComCheckIn = New tikAeroProcess.CheckIn
                End If
            End If
            strResult = objComCheckIn.PassengerOffload(rsPassengerOffload, strUserId, autoBaggageFlag)
            Marshal.FinalReleaseComObject(objComCheckIn)
            objComCheckIn = Nothing
        Catch e As Exception
            If IsNothing(objComCheckIn) = False Then
                Marshal.FinalReleaseComObject(objComCheckIn)
                objComCheckIn = Nothing
            End If
        End Try
        Return strResult
    End Function

    Public Function GetPassengerDetails(Optional ByRef strPassengerId As String = "",
                                        Optional ByRef strBookingSegmentID As String = "",
                                        Optional ByRef strFlightID As String = "",
                                        Optional ByRef rsIDs As ADODB.Recordset = Nothing,
                                        Optional ByRef rsPassenger As ADODB.Recordset = Nothing,
                                        Optional ByRef rsRemarks As ADODB.Recordset = Nothing,
                                        Optional ByRef rsServices As ADODB.Recordset = Nothing,
                                        Optional ByRef rsBaggage As ADODB.Recordset = Nothing,
                                        Optional ByRef rsSegment As ADODB.Recordset = Nothing,
                                        Optional ByRef rsFee As ADODB.Recordset = Nothing,
                                        Optional ByRef bPassenger As Boolean = False,
                                        Optional ByRef bRemarks As Boolean = False,
                                        Optional ByRef bServices As Boolean = False,
                                        Optional ByRef bBaggage As Boolean = False,
                                        Optional ByRef bSegment As Boolean = False,
                                        Optional ByRef bFee As Boolean = False,
                                        Optional ByRef bBookingDetails As Boolean = False,
                                        Optional ByRef strLanguageCode As String = "EN",
                                        Optional ByRef strOrigin As String = "") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComCheckIn As tikAeroProcess.CheckIn = Nothing
        Try
            If objComCheckIn Is Nothing Then
                If strServer <> "" Then
                    objComCheckIn = CreateObject("tikAeroProcess.CheckIn", strServer)
                Else
                    objComCheckIn = New tikAeroProcess.CheckIn
                End If
            End If
            rsA = objComCheckIn.GetPassengerDetails(strPassengerId, strBookingSegmentID, strFlightID, rsIDs, rsPassenger, rsRemarks, rsServices, rsBaggage, rsSegment, rsFee, bPassenger, bRemarks, bServices, bBaggage, bSegment, bFee, bBookingDetails, strLanguageCode, strOrigin)
            Marshal.FinalReleaseComObject(objComCheckIn)
            objComCheckIn = Nothing
        Catch e As Exception
            If IsNothing(objComCheckIn) = False Then
                Marshal.FinalReleaseComObject(objComCheckIn)
                objComCheckIn = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function CheckInSave(ByVal rsPassengerSegment As ADODB.Recordset,
                                ByVal rsBaggageTag As ADODB.Recordset,
                                ByVal rsSeatAssignment As ADODB.Recordset,
                                ByVal rsPassenger As ADODB.Recordset,
                                ByVal rsService As ADODB.Recordset,
                                ByVal rsRemarks As ADODB.Recordset,
                                ByVal rsBookingSegment As ADODB.Recordset,
                                ByVal rsFee As ADODB.Recordset,
                                ByVal bCheckDuplicateSeat As Boolean) As Boolean

        Dim b As Boolean = False
        Dim objComCheckIn As tikAeroProcess.CheckIn = Nothing
        Try
            If objComCheckIn Is Nothing Then
                If strServer <> "" Then
                    objComCheckIn = CreateObject("tikAeroProcess.CheckIn", strServer)
                Else
                    objComCheckIn = New tikAeroProcess.CheckIn
                End If
            End If
            b = objComCheckIn.Save(rsPassengerSegment, rsBaggageTag, rsSeatAssignment, rsPassenger, rsService, rsRemarks, rsBookingSegment, rsFee, bCheckDuplicateSeat)
            Marshal.FinalReleaseComObject(objComCheckIn)
            objComCheckIn = Nothing
        Catch e As Exception
            If IsNothing(objComCheckIn) = False Then
                Marshal.FinalReleaseComObject(objComCheckIn)
                objComCheckIn = Nothing
            End If
        End Try
        Return b
    End Function

    Public Overridable Function GetBooking(ByVal strBookingID As String, Optional ByRef rsHeader As ADODB.Recordset = Nothing, Optional ByRef rsSegment As ADODB.Recordset = Nothing, Optional ByRef rsPassenger As ADODB.Recordset = Nothing, Optional ByRef rsRemark As ADODB.Recordset = Nothing, Optional ByRef rsPayment As ADODB.Recordset = Nothing, Optional ByRef rsMapping As ADODB.Recordset = Nothing, Optional ByRef rsServices As ADODB.Recordset = Nothing, Optional ByRef rsTax As ADODB.Recordset = Nothing, Optional ByRef rsFree As ADODB.Recordset = Nothing, Optional ByRef rsQuote As ADODB.Recordset = Nothing) As Boolean
        Dim b As Boolean = False
        Dim objComBooking As tikAeroProcess.Booking = Nothing
        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If
            b = objComBooking.Read(strBookingID, , , rsHeader, rsSegment, rsPassenger, rsRemark, rsPayment, rsMapping, rsServices, rsTax, rsFree, rsQuote, , False)
            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch e As Exception
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
            Throw
        End Try
        Return b
    End Function

    Public Function CalculateBookingCreateFees(ByVal AgencyCode As String,
                                                ByVal strCurrency As String,
                                                ByVal strBookingId As String,
                                                ByRef rsHeader As ADODB.Recordset,
                                                ByRef rsSegment As ADODB.Recordset,
                                                ByRef rsPassenger As ADODB.Recordset,
                                                ByRef rsFees As ADODB.Recordset,
                                                ByRef rsRemark As ADODB.Recordset,
                                                ByRef rsPayment As ADODB.Recordset,
                                                ByRef rsMapping As ADODB.Recordset,
                                                ByRef rsService As ADODB.Recordset,
                                                ByRef rsTax As ADODB.Recordset,
                                                ByVal strLanguage As String,
                                                ByVal bNoVat As Boolean) As Boolean
        Dim b As Boolean = False
        Dim objComFees As tikAeroProcess.Fees = Nothing
        Try
            If objComFees Is Nothing Then
                If strServer <> "" Then
                    objComFees = CreateObject("tikAeroProcess.Fees", strServer)
                Else
                    objComFees = New tikAeroProcess.Fees
                End If
            End If

            b = objComFees.BookingCreate(AgencyCode, strCurrency, strBookingId, rsHeader, rsSegment, rsPassenger, rsFees, rsRemark, rsMapping, rsService, strLanguage, bNoVat)
            Marshal.FinalReleaseComObject(objComFees)
            objComFees = Nothing
        Catch e As Exception
            If IsNothing(objComFees) = False Then
                Marshal.FinalReleaseComObject(objComFees)
                objComFees = Nothing
            End If
        End Try
        Return b
    End Function
    Public Function CalculateBookingChangeFees(ByVal AgencyCode As String,
                                               ByVal strCurrency As String,
                                               ByVal strBookingId As String,
                                               ByRef rsHeader As ADODB.Recordset,
                                               ByRef rsSegment As ADODB.Recordset,
                                               ByRef rsPassenger As ADODB.Recordset,
                                               ByRef rsFees As ADODB.Recordset,
                                               ByRef rsRemark As ADODB.Recordset,
                                               ByRef rsPayment As ADODB.Recordset,
                                               ByRef rsMapping As ADODB.Recordset,
                                               ByRef rsService As ADODB.Recordset,
                                               ByRef rsTax As ADODB.Recordset,
                                               ByVal strLanguage As String,
                                               ByVal bNoVat As Boolean) As Boolean
        Dim b As Boolean = False
        Dim objComFees As tikAeroProcess.Fees = Nothing
        Try
            If objComFees Is Nothing Then
                If strServer <> "" Then
                    objComFees = CreateObject("tikAeroProcess.Fees", strServer)
                Else
                    objComFees = New tikAeroProcess.Fees
                End If
            End If


            b = objComFees.BookingChange(AgencyCode, strCurrency, strBookingId, rsHeader, rsSegment, rsPassenger, rsFees, rsRemark, rsMapping, rsService, strLanguage, bNoVat)

            Marshal.FinalReleaseComObject(objComFees)
            objComFees = Nothing
        Catch e As Exception
            If IsNothing(objComFees) = False Then
                Marshal.FinalReleaseComObject(objComFees)
                objComFees = Nothing
            End If
            Throw
        End Try
        Return b
    End Function
    Public Function CalculateNameChangeFees(ByVal AgencyCode As String,
                                           ByVal strCurrency As String,
                                           ByVal strBookingId As String,
                                           ByRef rsHeader As ADODB.Recordset,
                                           ByRef rsSegment As ADODB.Recordset,
                                           ByRef rsPassenger As ADODB.Recordset,
                                           ByRef rsFees As ADODB.Recordset,
                                           ByRef rsRemark As ADODB.Recordset,
                                           ByRef rsPayment As ADODB.Recordset,
                                           ByRef rsMapping As ADODB.Recordset,
                                           ByRef rsService As ADODB.Recordset,
                                           ByRef rsTax As ADODB.Recordset,
                                           ByVal strLanguage As String,
                                           ByVal bNoVat As Boolean) As Boolean
        Dim b As Boolean = False
        Dim objComFees As tikAeroProcess.Fees = Nothing
        Try
            If objComFees Is Nothing Then
                If strServer <> "" Then
                    objComFees = CreateObject("tikAeroProcess.Fees", strServer)
                Else
                    objComFees = New tikAeroProcess.Fees
                End If
            End If

            b = objComFees.NameChange(AgencyCode, strCurrency, strBookingId, rsHeader, rsSegment, rsPassenger, rsFees, rsRemark, rsMapping, rsService, strLanguage, bNoVat)

            Marshal.FinalReleaseComObject(objComFees)
            objComFees = Nothing
        Catch e As Exception
            If IsNothing(objComFees) = False Then
                Marshal.FinalReleaseComObject(objComFees)
                objComFees = Nothing
            End If
        End Try
        Return b
    End Function
    Public Function CalculateSeatAssignmentFees(ByVal AgencyCode As String,
                                               ByVal strCurrency As String,
                                               ByVal strBookingId As String,
                                               ByRef rsHeader As ADODB.Recordset,
                                               ByRef rsSegment As ADODB.Recordset,
                                               ByRef rsPassenger As ADODB.Recordset,
                                               ByRef rsFees As ADODB.Recordset,
                                               ByRef rsRemark As ADODB.Recordset,
                                               ByRef rsPayment As ADODB.Recordset,
                                               ByRef rsMapping As ADODB.Recordset,
                                               ByRef rsService As ADODB.Recordset,
                                               ByRef rsTax As ADODB.Recordset,
                                               ByVal strLanguage As String,
                                               ByVal bNoVat As Boolean) As Boolean
        Dim b As Boolean = False
        Dim objComFees As tikAeroProcess.Fees = Nothing
        Try
            If objComFees Is Nothing Then
                If strServer <> "" Then
                    objComFees = CreateObject("tikAeroProcess.Fees", strServer)
                Else
                    objComFees = New tikAeroProcess.Fees
                End If
            End If

            b = objComFees.SeatAssignment(AgencyCode, strCurrency, strBookingId, rsHeader, rsSegment, rsPassenger, rsFees, rsRemark, rsMapping, rsService, strLanguage, bNoVat)

            Marshal.FinalReleaseComObject(objComFees)
            objComFees = Nothing
        Catch e As Exception
            If IsNothing(objComFees) = False Then
                Marshal.FinalReleaseComObject(objComFees)
                objComFees = Nothing
            End If
        End Try
        Return b
    End Function
    Public Function CalculateSpecialServiceFees(ByVal AgencyCode As String,
                                                ByVal strCurrency As String,
                                                ByVal strBookingId As String,
                                                ByRef rsHeader As ADODB.Recordset,
                                                ByRef rsService As ADODB.Recordset,
                                                ByRef rsFees As ADODB.Recordset,
                                                ByRef rsRemark As ADODB.Recordset,
                                                ByRef rsMapping As ADODB.Recordset,
                                                ByVal strLanguage As String,
                                                ByVal bNoVat As Boolean) As Boolean
        Dim b As Boolean = False
        Dim objComFees As tikAeroProcess.Fees = Nothing
        Try
            If objComFees Is Nothing Then
                If strServer <> "" Then
                    objComFees = CreateObject("tikAeroProcess.Fees", strServer)
                Else
                    objComFees = New tikAeroProcess.Fees
                End If
            End If

            b = objComFees.SpecialService(AgencyCode, strCurrency, strBookingId, rsHeader, rsService, rsFees, rsRemark, rsMapping, strLanguage, bNoVat)

            Marshal.FinalReleaseComObject(objComFees)
            objComFees = Nothing
        Catch e As Exception
            If IsNothing(objComFees) = False Then
                Marshal.FinalReleaseComObject(objComFees)
                objComFees = Nothing
            End If
        End Try
        Return b
    End Function
    Public Function AddFee(ByVal AgencyCode As String,
                                          ByVal strCurrency As String,
                                          ByVal strBookingId As String,
                                          ByVal strFee As String,
                                          ByRef rsHeader As ADODB.Recordset,
                                          ByRef rsSegment As ADODB.Recordset,
                                          ByRef rsPassenger As ADODB.Recordset,
                                          ByRef rsFees As ADODB.Recordset,
                                          ByRef rsRemark As ADODB.Recordset,
                                          ByRef rsPayment As ADODB.Recordset,
                                          ByRef rsMapping As ADODB.Recordset,
                                          ByRef rsService As ADODB.Recordset,
                                          ByRef rsTax As ADODB.Recordset,
                                          ByVal strLanguage As String,
                                          ByVal bNoVat As Boolean) As Boolean
        Dim b As Boolean = False
        Dim objComFees As tikAeroProcess.Fees = Nothing
        Try
            If objComFees Is Nothing Then
                If strServer <> "" Then
                    objComFees = CreateObject("tikAeroProcess.Fees", strServer)
                Else
                    objComFees = New tikAeroProcess.Fees
                End If
            End If

            b = objComFees.Add(AgencyCode, strCurrency, strBookingId, strFee, rsHeader, rsSegment, rsPassenger, rsFees, rsRemark, rsMapping, rsService, strLanguage, bNoVat)

            Marshal.FinalReleaseComObject(objComFees)
            objComFees = Nothing
        Catch e As Exception
            If IsNothing(objComFees) = False Then
                Marshal.FinalReleaseComObject(objComFees)
                objComFees = Nothing
            End If
        End Try
        Return b
    End Function

    Public Sub UpdatePassengerNames(ByRef rsPassenger As ADODB.Recordset, ByRef rsMapping As ADODB.Recordset, Optional ByRef rsHeader As ADODB.Recordset = Nothing, Optional ByRef rsFees As ADODB.Recordset = Nothing, Optional ByRef rsSegment As ADODB.Recordset = Nothing, Optional ByRef strAgency As String = "", Optional ByRef strCurrency As String = "", Optional ByRef strBookingID As String = "", Optional ByRef bAllowNameChange As Boolean = True)
        Dim objComBooking As tikAeroProcess.Booking = Nothing
        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If
            Call objComBooking.UpdatePassengerNames(rsPassenger, rsMapping, rsHeader, rsFees, rsSegment, strAgency, strCurrency, strBookingID, bAllowNameChange)
            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch e As Exception
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
        End Try
    End Sub

    Public Overridable Function SaveBooking(Optional ByRef rsHeader As ADODB.Recordset = Nothing,
                                            Optional ByRef rsSegment As ADODB.Recordset = Nothing,
                                            Optional ByRef rsPassenger As ADODB.Recordset = Nothing,
                                            Optional ByRef rsRemark As ADODB.Recordset = Nothing,
                                            Optional ByRef rsPayment As ADODB.Recordset = Nothing,
                                            Optional ByRef rsMapping As ADODB.Recordset = Nothing,
                                            Optional ByRef rsService As ADODB.Recordset = Nothing,
                                            Optional ByRef rsTax As ADODB.Recordset = Nothing,
                                            Optional ByVal rsFee As ADODB.Recordset = Nothing,
                                            Optional ByRef CreateTickets As Boolean = False,
                                            Optional ByVal bReadBooking As Boolean = False,
                                            Optional ByVal bReadOnly As Boolean = False,
                                            Optional ByVal bSetLock As Boolean = False,
                                            Optional ByVal bCheckSeatAssignment As Boolean = False,
                                            Optional ByVal bCheckSessionTimeOut As Boolean = False) As Short
        Dim b As Short = -1
        Dim objComBooking As tikAeroProcess.Booking = Nothing
        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If
            b = objComBooking.Save(rsHeader, rsSegment, rsPassenger, rsRemark, rsPayment, rsMapping, rsService, rsTax, rsFee, False, bReadBooking, bReadOnly, bSetLock, bCheckSeatAssignment, bCheckSessionTimeOut)
            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch e As Exception
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If

            Throw
        End Try
        Return b
    End Function

    Public Overridable Function TicketCreate(ByVal AgencyCode As String,
                                             ByVal UserID As String,
                                             Optional ByRef rs As ADODB.Recordset = Nothing,
                                             Optional ByVal BookingID As String = "",
                                             Optional ByVal ProcessPayment As Boolean = False) As Boolean
        Dim b As Boolean = False
        Dim objComBooking As tikAeroProcess.Booking = Nothing
        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If
            b = objComBooking.TicketCreate(AgencyCode, UserID, BookingID)
            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch e As Exception
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
        End Try
        Return b
    End Function

    Public Overridable Function GetRemarkTypes(Optional ByRef strLanguage As String = "EN") As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objComReservation As tikAeroProcess.Reservation = Nothing
        Try
            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Reservation", strServer)
                Else
                    objComReservation = New tikAeroProcess.Reservation
                End If
            End If
            rsA = objComReservation.GetRemarkTypes(strLanguage)
            Marshal.FinalReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch e As Exception
            If IsNothing(objComReservation) = False Then
                Marshal.FinalReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function AddChangeFlight(ByRef rsPassengers As ADODB.Recordset,
                                    ByRef rsSegments As ADODB.Recordset,
                                    ByRef rsMappings As ADODB.Recordset,
                                    ByRef rsTaxes As ADODB.Recordset,
                                    ByRef rsServices As ADODB.Recordset,
                                    ByRef strAgencyCode As String,
                                    Optional ByRef rsQuotes As ADODB.Recordset = Nothing,
                                    Optional ByRef rsRemarks As ADODB.Recordset = Nothing,
                                    Optional ByRef rsFlights As ADODB.Recordset = Nothing,
                                    Optional ByVal dtFlight As Date = #12/30/1899#,
                                    Optional ByVal strBookingID As String = "",
                                    Optional ByVal bGroupBooking As Boolean = False,
                                    Optional ByVal bWaitlist As Boolean = False,
                                    Optional ByVal UserId As String = "",
                                    Optional ByVal strLanguage As String = "EN",
                                    Optional ByVal strCurrencyCode As String = "",
                                    Optional ByVal bNoVat As Boolean = False) As Boolean
        Dim b As Boolean = False
        Dim objComBooking As tikAeroProcess.Booking = Nothing
        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If
            b = objComBooking.AddFlight(rsPassengers, rsSegments, rsMappings, rsTaxes, rsServices, strAgencyCode, rsQuotes, rsRemarks, rsFlights, dtFlight, strBookingID, , , , , , , , , strLanguage, strCurrencyCode, , , bGroupBooking, bWaitlist, , , , , , , , UserId, , , , bNoVat)
            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch e As Exception
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
            Throw
        End Try
        Return b
    End Function

    Public Function GetEmpty(ByVal strAgencyCode As String,
                                         ByVal strCurrency As String,
                                         Optional ByRef rsHeader As ADODB.Recordset = Nothing,
                                         Optional ByRef rsSegments As ADODB.Recordset = Nothing,
                                         Optional ByRef rsPassengers As ADODB.Recordset = Nothing,
                                         Optional ByRef rsRemarks As ADODB.Recordset = Nothing,
                                         Optional ByRef rsPayments As ADODB.Recordset = Nothing,
                                         Optional ByRef rsMappings As ADODB.Recordset = Nothing,
                                         Optional ByRef rsServices As ADODB.Recordset = Nothing,
                                         Optional ByRef rsTaxes As ADODB.Recordset = Nothing,
                                         Optional ByRef rsFees As ADODB.Recordset = Nothing,
                                         Optional ByVal rsFlights As ADODB.Recordset = Nothing,
                                         Optional ByRef rsQuotes As ADODB.Recordset = Nothing,
                                         Optional ByVal strBookingID As String = "",
                                         Optional ByVal iAdults As Integer = 0,
                                         Optional ByVal iChildren As Integer = 0,
                                         Optional ByVal iInfants As Integer = 0,
                                         Optional ByVal iOthers As Integer = 0,
                                         Optional ByVal strOthers As String = "",
                                         Optional ByVal strUserId As String = "",
                                         Optional ByVal strIpAddress As String = "",
                                         Optional ByVal strLanguageCode As String = "",
                                         Optional ByVal bNoVat As Boolean = False) As Boolean
        Dim objComBooking As tikAeroProcess.Booking = Nothing
        Dim b As Boolean
        Dim result As String
        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If
            result = objComBooking.GetEmpty(rsHeader,
                                       rsSegments,
                                       rsPassengers,
                                       rsRemarks,
                                       rsPayments,
                                       rsMappings,
                                       rsServices,
                                       rsTaxes,
                                       rsFees,
                                       rsFlights,
                                       rsQuotes,
                                       strBookingID,
                                       strAgencyCode,
                                       strCurrency,
                                       iAdults,
                                       iChildren,
                                       iInfants,
                                       iOthers,
                                       strOthers,
                                       strUserId,
                                       strIpAddress,
                                       strLanguageCode,
                                       bNoVat)

            If String.IsNullOrEmpty(result) Or result <> "00000" Then
                b = False
            Else
                b = True
            End If

            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch e As Exception
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
        End Try

        Return b

    End Function


    Public Function GetDemo() As ADODB.Recordset

        Dim objComBooking As tikAeroProcess.Booking = Nothing
        Dim rsA As ADODB.Recordset = Nothing
        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If
            rsA = objComBooking.PassengerGetEmpty
            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function BookingSearch(Optional ByVal strAirline As String = "",
                                    Optional ByVal strFlightNumber As String = "",
                                    Optional ByVal dtFlightFrom As Date = #12/30/1899#,
                                    Optional ByVal dtFlightTo As Date = #12/30/1899#,
                                    Optional ByVal strRecordLocator As String = "",
                                    Optional ByVal strOrigin As String = "",
                                    Optional ByVal strDestination As String = "",
                                    Optional ByVal strPassengerName As String = "",
                                    Optional ByVal strSeatNumber As String = "",
                                    Optional ByVal strTicketNumber As String = "",
                                    Optional ByVal strPhoneNumber As String = "",
                                    Optional ByVal strLanguage As String = "EN") As ADODB.Recordset

        Dim objComReservation As tikAeroProcess.Reservation = Nothing
        Dim rsA As ADODB.Recordset = Nothing
        Try
            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Reservation", strServer)
                Else
                    objComReservation = New tikAeroProcess.Reservation
                End If
            End If
            rsA = objComReservation.GetBookings(strAirline, strFlightNumber, dtFlightFrom, dtFlightTo, strRecordLocator, strOrigin, strDestination, strPassengerName, strSeatNumber, strTicketNumber, strPhoneNumber, strLanguage)
            Marshal.FinalReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch e As Exception
            If IsNothing(objComReservation) = False Then
                Marshal.FinalReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function GetCountry(Optional ByVal strLanguage As String = "EN") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComInventory As tikAeroProcess.Inventory = Nothing
        Try
            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            rsA = objComInventory.GetCountry(strLanguage)
            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing
        Catch
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
        End Try
        Return rsA

    End Function
    Public Function GetTourOperators(Optional ByVal strLanguage As String = "EN") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComVendor As tikAeroProcess.Vendor = Nothing
        Try
            If objComVendor Is Nothing Then
                If strServer <> "" Then
                    objComVendor = CreateObject("tikAeroProcess.Vendor", strServer)
                Else
                    objComVendor = New tikAeroProcess.Vendor

                End If
            End If
            rsA = objComVendor.TourOperatorsRead()

            Marshal.FinalReleaseComObject(objComVendor)
            objComVendor = Nothing
        Catch
            If IsNothing(objComVendor) = False Then
                Marshal.FinalReleaseComObject(objComVendor)
                objComVendor = Nothing
            End If
        End Try
        Return rsA

    End Function
    Public Function GetVendorTourOperator(Optional ByVal strVendorRcd As String = "") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComVendor As tikAeroProcess.Vendor = Nothing
        Try
            If objComVendor Is Nothing Then
                If strServer <> "" Then
                    objComVendor = CreateObject("tikAeroProcess.Vendor", strServer)
                Else
                    objComVendor = New tikAeroProcess.Vendor

                End If
            End If
            rsA = objComVendor.VendorTourOperatorGet(strVendorRcd)

            Marshal.FinalReleaseComObject(objComVendor)
            objComVendor = Nothing
        Catch
            If IsNothing(objComVendor) = False Then
                Marshal.FinalReleaseComObject(objComVendor)
                objComVendor = Nothing
            End If
        End Try
        Return rsA

    End Function
    Public Function GetTourOperatorCodeMappingRead(Optional ByVal strTourOperatorId As String = "", Optional ByVal bInclude As Boolean = True) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComVendor As tikAeroProcess.Vendor = Nothing
        Try
            If objComVendor Is Nothing Then
                If strServer <> "" Then
                    objComVendor = CreateObject("tikAeroProcess.Vendor", strServer)
                Else
                    objComVendor = New tikAeroProcess.Vendor

                End If
            End If
            rsA = objComVendor.TourOperatorCodeMappingRead(strTourOperatorId, bInclude)

            Marshal.FinalReleaseComObject(objComVendor)
            objComVendor = Nothing
        Catch
            If IsNothing(objComVendor) = False Then
                Marshal.FinalReleaseComObject(objComVendor)
                objComVendor = Nothing
            End If
        End Try
        Return rsA

    End Function
    Public Function GetActivityTypes(Optional ByVal strLanguage As String = "EN") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComReservation As tikAeroProcess.Reservation = Nothing
        Try
            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Reservation", strServer)
                Else
                    objComReservation = New tikAeroProcess.Reservation
                End If
            End If
            rsA = objComReservation.GetRemarkTypes
            Marshal.FinalReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch
            If IsNothing(objComReservation) = False Then
                Marshal.FinalReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return rsA

    End Function

    Function CompleteRemark(Optional ByVal Remark As ADODB.Recordset = Nothing, Optional ByVal RemarkId As String = "", Optional ByVal UserId As String = "") As Boolean
        Dim b As Boolean = False
        Dim objComReservation As tikAeroProcess.Reservation = Nothing
        Try
            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Reservation", strServer)
                Else
                    objComReservation = New tikAeroProcess.Reservation
                End If
            End If
            b = objComReservation.CompleteRemark(Remark, RemarkId, UserId)
            Marshal.FinalReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch e As Exception
            If IsNothing(objComReservation) = False Then
                Marshal.FinalReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return b
    End Function

    Public Function ReleaseFlightInventorySession(ByVal BookingID As String) As Boolean
        Dim b As Boolean = False
        Dim objComInventory As tikAeroProcess.Inventory = Nothing
        Try
            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            b = objComInventory.ReleaseFlightInventorySession(BookingID, , , , False, True, False)
            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing
        Catch
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
        End Try
        Return b
    End Function

    Public Sub New()
        MyBase.New()
        Try
            strServer = System.Configuration.ConfigurationManager.AppSettings("ComServer")
            strUser = System.Configuration.ConfigurationManager.AppSettings("ComUser")
            strPassword = System.Configuration.ConfigurationManager.AppSettings("ComPassword")
            strDomain = System.Configuration.ConfigurationManager.AppSettings("ComDomain")
            If Len(strUser) > 0 And Len(strPassword) > 0 And Len(strDomain) > 0 Then
                impersonateValidUser(strUser, strDomain, strPassword)
            End If
        Catch
        End Try
    End Sub

    Function ValidCreditCard(ByVal rsPayment As ADODB.Recordset,
                            ByVal rsSegment As ADODB.Recordset,
                            ByVal rsAllocation As ADODB.Recordset,
                            ByVal rsVoucher As ADODB.Recordset,
                            ByVal rsMapping As ADODB.Recordset,
                            Optional ByVal securityToken As String = "",
                            Optional ByVal authenticationToken As String = "",
                            Optional ByVal commerceIndicator As String = "",
                            Optional ByVal strBookingReference As String = "",
                            Optional ByVal rsFees As ADODB.Recordset = Nothing,
                            Optional ByVal rsHeader As ADODB.Recordset = Nothing,
                            Optional ByVal rsTaxes As ADODB.Recordset = Nothing,
                            Optional ByVal strRequestSourcd As String = "",
                            Optional ByVal bWeb As Boolean = False) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComCreditCard As tikAeroProcess.clsCreditCard = Nothing
        Try
            If objComCreditCard Is Nothing Then
                If strServer <> "" Then
                    objComCreditCard = CreateObject("tikAeroProcess.clsCreditCard", strServer)
                Else
                    objComCreditCard = New tikAeroProcess.clsCreditCard
                End If
            End If
            rsA = objComCreditCard.Authorize(rsPayment,
                                            rsSegment,
                                            rsAllocation,
                                            rsVoucher,
                                            rsMapping,
                                            rsFees,
                                            rsHeader,
                                            rsTaxes,
                                            securityToken,
                                            authenticationToken,
                                            commerceIndicator,
                                            strBookingReference,
                                            strRequestSourcd,
                                            bWeb)

            Marshal.FinalReleaseComObject(objComCreditCard)
            objComCreditCard = Nothing
        Catch e As Exception
            If IsNothing(objComCreditCard) = False Then
                Marshal.FinalReleaseComObject(objComCreditCard)
                objComCreditCard = Nothing
            End If

            Throw
        End Try
        Return rsA
    End Function

    Function UserList(Optional ByRef UserLogon As String = "",
                      Optional ByRef UserCode As String = "",
                      Optional ByRef LastName As String = "",
                      Optional ByRef FirstName As String = "",
                      Optional ByRef AgencyCode As String = "",
                      Optional ByRef StatusCode As String = "",
                      Optional ByRef bAirlineUsers As Boolean = True) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objComSetting As tikAeroProcess.Settings = Nothing
        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            rsA = objComSetting.UserList(UserLogon, UserCode, LastName, FirstName, AgencyCode, StatusCode, bAirlineUsers)
            Marshal.FinalReleaseComObject(objComSetting)
            objComSetting = Nothing
        Catch
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
                objComSetting = Nothing
            End If
        End Try
        Return rsA
    End Function

    Function UserRead(ByRef UserId As String) As ADODB.Recordset
        Dim objComSetting As tikAeroProcess.Settings = Nothing
        Dim rsA As ADODB.Recordset = Nothing
        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            rsA = objComSetting.UserRead(UserId)
            Marshal.FinalReleaseComObject(objComSetting)
            objComSetting = Nothing
        Catch
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
                objComSetting = Nothing
            End If
        End Try
        Return rsA
    End Function

    Function UserGetEmpty() As ADODB.Recordset
        Dim objComSetting As tikAeroProcess.Settings = Nothing
        Dim rsA As ADODB.Recordset = Nothing
        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            rsA = objComSetting.UserGetEmpty()
            Marshal.FinalReleaseComObject(objComSetting)
            objComSetting = Nothing
        Catch
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
                objComSetting = Nothing
            End If
        End Try
        Return rsA
    End Function

    Function AgencyUserMappingGetEmpty() As ADODB.Recordset
        Dim objComSetting As tikAeroProcess.Settings = Nothing
        Dim rsA As ADODB.Recordset = Nothing
        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            rsA = objComSetting.AgencyUserMappingGetEmpty()
            Marshal.FinalReleaseComObject(objComSetting)
            objComSetting = Nothing
        Catch
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
                objComSetting = Nothing
            End If
        End Try
        Return rsA
    End Function

    Function AgencyUserMappingRead(ByRef UserId As String, ByRef AgencyCode As String) As Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objComSetting As tikAeroProcess.Settings = Nothing
        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            rsA = objComSetting.AgencyUserMappingRead(UserId, AgencyCode)
            Marshal.FinalReleaseComObject(objComSetting)
            objComSetting = Nothing
        Catch
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
                objComSetting = Nothing
            End If
        End Try
        Return rsA
    End Function

    Function AgencyUserMappingSave(ByVal rsAgencyUserMapping As ADODB.Recordset) As Boolean
        Dim b As Boolean = False
        Dim objComSetting As tikAeroProcess.Settings = Nothing
        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            b = objComSetting.AgencyUserMappingSave(rsAgencyUserMapping)
            Marshal.FinalReleaseComObject(objComSetting)
            objComSetting = Nothing
        Catch
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
                objComSetting = Nothing
            End If
        End Try
        Return b
    End Function

    Function UserSave(ByVal rsUser As ADODB.Recordset) As Boolean
        Dim b As Boolean = False
        Dim objComSetting As tikAeroProcess.Settings = Nothing
        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            b = objComSetting.UserSave(rsUser)
            Marshal.FinalReleaseComObject(objComSetting)
            objComSetting = Nothing
        Catch
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
                objComSetting = Nothing
            End If
        End Try
        Return b
    End Function

    Function GetActivities(Optional ByVal AgencyCode As String = "",
                           Optional ByVal RemarkType As String = "",
                           Optional ByVal Nickname As String = "",
                           Optional ByVal TimelimitFrom As Date = #12/30/1899#,
                           Optional ByVal TimelimitTo As Date = #12/30/1899#,
                           Optional ByVal PendingOnly As Boolean = True,
                           Optional ByVal IncompleteOnly As Boolean = True,
                           Optional ByVal IncludeRemarks As Boolean = False) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objComReservation As tikAeroProcess.Reservation = Nothing
        Try
            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Reservation", strServer)
                Else
                    objComReservation = New tikAeroProcess.Reservation
                End If
            End If
            rsA = objComReservation.GetActivities(AgencyCode, RemarkType, Nickname, TimelimitFrom, TimelimitTo, PendingOnly, IncompleteOnly, IncludeRemarks)
            Marshal.FinalReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch
            If IsNothing(objComReservation) = False Then
                Marshal.FinalReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetAgencySessionProfile(ByRef agencyCode As String, ByVal UserAccountID As String) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objComSystem As tikAeroProcess.System = Nothing
        Try
            If objComSystem Is Nothing Then
                If strServer <> "" Then
                    objComSystem = CreateObject("tikAeroProcess.System", strServer)
                Else
                    objComSystem = New tikAeroProcess.System
                End If
            End If
            rsA = objComSystem.GetAgencySessionProfile(agencyCode, UserAccountID)
            Marshal.FinalReleaseComObject(objComSystem)
            objComSystem = Nothing
        Catch
            If IsNothing(objComSystem) = False Then
                Marshal.FinalReleaseComObject(objComSystem)
                objComSystem = Nothing
            End If

            Throw
        End Try
        Return rsA
    End Function

    Public Function SegmentCancel(ByVal SegmentId As String,
                                  ByRef rsSegments As ADODB.Recordset,
                                  ByRef rsMappings As ADODB.Recordset,
                                  Optional ByRef rsServices As ADODB.Recordset = Nothing,
                                  Optional ByRef rsPayments As ADODB.Recordset = Nothing,
                                  Optional ByVal rsTaxes As ADODB.Recordset = Nothing,
                                  Optional ByRef rsQuotes As ADODB.Recordset = Nothing,
                                  Optional ByVal sUserID As String = "",
                                  Optional ByVal AgencyCode As String = "",
                                  Optional ByVal WaiveFee As Boolean = False,
                                  Optional ByVal ProcessTicketRefund As Boolean = False,
                                  Optional ByVal IncludeRefundableOnly As Boolean = True,
                                  Optional ByVal NoShowSegmentDateTime As Date = #12/30/1899#,
                                  Optional ByVal Void As Boolean = False) As Boolean
        Dim b As Boolean = False
        Dim objComBooking As tikAeroProcess.Booking = Nothing
        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If

            b = objComBooking.SegmentCancel(SegmentId, rsSegments, rsMappings, rsServices, rsPayments, rsTaxes, rsQuotes, sUserID, AgencyCode, WaiveFee, ProcessTicketRefund, IncludeRefundableOnly, NoShowSegmentDateTime, Void)
            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch e As Exception
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
        End Try
        Return b
    End Function


    Public Function GetVouchers(Optional ByVal strRecordLocator As String = "",
                                Optional ByVal strVoucherNumber As String = "",
                                Optional ByVal strVoucherID As String = "",
                                Optional ByVal strStatus As String = "",
                                Optional ByVal strRecipient As String = "",
                                Optional ByVal strFOPSubType As String = "",
                                Optional ByVal strClientProfileId As String = "",
                                Optional ByVal strCurrency As String = "",
                                Optional ByVal strPassword As String = "",
                                Optional ByVal bIncludeOpenVoucher As Boolean = False,
                                Optional ByVal bIncludeExpiredVoucher As Boolean = False,
                                Optional ByVal bIncludeUsedVoucher As Boolean = False,
                                Optional ByVal bIncludeVoidedVoucher As Boolean = False,
                                Optional ByVal bIncludeRefundable As Boolean = False,
                                Optional ByVal bIncludeFareOnly As Boolean = False,
                                Optional ByRef bWrite As Boolean = False) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objMiscellaneous As tikAeroProcess.Miscellaneous = Nothing
        Try
            If objMiscellaneous Is Nothing Then
                If strServer <> "" Then
                    objMiscellaneous = CreateObject("tikAeroProcess.Miscellaneous", strServer)
                Else
                    objMiscellaneous = New tikAeroProcess.Miscellaneous
                End If
            End If
            rsA = objMiscellaneous.GetVouchers(strRecordLocator, strVoucherNumber, strVoucherID, strStatus, strRecipient, strFOPSubType, strClientProfileId, strCurrency, strPassword, bIncludeOpenVoucher, bIncludeExpiredVoucher, bIncludeUsedVoucher, bIncludeVoidedVoucher, bIncludeRefundable, bIncludeFareOnly, bWrite)
            Marshal.FinalReleaseComObject(objMiscellaneous)
            objMiscellaneous = Nothing
        Catch e As Exception
            If IsNothing(objMiscellaneous) = False Then
                Marshal.FinalReleaseComObject(objMiscellaneous)
                objMiscellaneous = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function CalculateExchange(ByVal currencyFrom As String,
                                      ByVal currencyTo As String,
                                      ByVal amount As Double,
                                      Optional ByVal systemCurrency As String = "",
                                      Optional ByVal dateOfExchange As Date = #12/30/1899#,
                                      Optional ByVal reverse As Boolean = False) As Double
        Dim dbl As Double
        Dim objPayments As tikAeroProcess.Payment = Nothing
        Try
            If objPayments Is Nothing Then
                If strServer <> "" Then
                    objPayments = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objPayments = New tikAeroProcess.Payment
                End If
            End If
            dbl = objPayments.CalculateExchange(currencyFrom, currencyTo, amount, systemCurrency, dateOfExchange, reverse)
            Marshal.FinalReleaseComObject(objPayments)
            objPayments = Nothing
        Catch e As Exception
            If IsNothing(objPayments) = False Then
                Marshal.FinalReleaseComObject(objPayments)
                objPayments = Nothing
            End If
        End Try

        Return dbl
    End Function

    Public Function GetBookingFees(ByVal strCurrency As String,
                                   ByVal dt As Date,
                                   Optional ByVal strAgency As String = "",
                                   Optional ByVal strType As String = "") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComReservation As tikAeroProcess.Reservation = Nothing

        Try
            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Reservation", strServer)
                Else
                    objComReservation = New tikAeroProcess.Reservation
                End If
            End If

            rsA = objComReservation.GetBookingFees(strCurrency, dt, strAgency, strType)
            Marshal.FinalReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch e As Exception
            If IsNothing(objComReservation) = False Then
                Marshal.FinalReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetFees(ByVal strCurrency As String,
                            ByVal dt As Date,
                            Optional ByVal strAgency As String = "",
                            Optional ByVal strType As String = "",
                            Optional ByVal strClass As String = "",
                            Optional ByVal strFareBasis As String = "",
                            Optional ByVal strOrigin As String = "",
                            Optional ByVal strDestination As String = "",
                            Optional ByVal strFlightNumber As String = "",
                            Optional ByVal strLanguage As String = "EN",
                            Optional ByVal bNoVat As Boolean = False) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComSetting As tikAeroProcess.Settings = Nothing

        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            rsA = objComSetting.GetFee(strType, strCurrency, strAgency, strClass, strFareBasis, strOrigin, strDestination, strFlightNumber, dt, strLanguage, bNoVat)
            Marshal.FinalReleaseComObject(objComSetting)
        Catch e As Exception
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
            End If
        End Try
        Return rsA
    End Function

    Public Function GetFeesDefinition(ByVal strLanguage As String) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComInventory As tikAeroProcess.Inventory = Nothing

        Try
            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            rsA = objComInventory.GetFee(strLanguage)
            Marshal.FinalReleaseComObject(objComInventory)
        Catch e As Exception
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
            End If
        End Try
        Return rsA
    End Function
    Public Function AddInfant(ByRef rsPassenger As ADODB.Recordset,
                              ByRef rsSegment As ADODB.Recordset,
                              ByRef rsMapping As ADODB.Recordset,
                              ByRef rsTax As ADODB.Recordset,
                              ByRef rsService As ADODB.Recordset,
                              ByRef rsQuote As ADODB.Recordset,
                              ByVal strBookingId As String,
                              ByVal strAgencyCode As String,
                              Optional ByVal strPassengerType As String = "INF",
                              Optional ByVal bGroupBooking As Boolean = False,
                              Optional ByVal bNoVat As Boolean = False) As Boolean

        Dim bSuccess As Boolean = False
        Dim objComBooking As tikAeroProcess.Booking = Nothing
        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If

            bSuccess = objComBooking.AddInfant(rsPassenger, rsSegment, rsMapping, rsTax, rsService, strBookingId, strAgencyCode, rsQuote, strPassengerType, , bGroupBooking, bNoVat)
            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch e As Exception
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
        End Try
        Return bSuccess
    End Function

    Public Function GetClientSessionProfile(ByVal clientProfileId As String) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objComSystem As tikAeroProcess.System = Nothing
        Try
            If objComSystem Is Nothing Then
                If strServer <> "" Then
                    objComSystem = CreateObject("tikAeroProcess.System", strServer)
                Else
                    objComSystem = New tikAeroProcess.System
                End If
            End If
            rsA = objComSystem.GetClientSessionProfile(clientProfileId)
            Marshal.FinalReleaseComObject(objComSystem)
            objComSystem = Nothing
        Catch
            If IsNothing(objComSystem) = False Then
                Marshal.FinalReleaseComObject(objComSystem)
                objComSystem = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetCorporateSessionProfile(ByRef clientId As String, Optional ByVal LastName As String = "") As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objComClient As tikAeroProcess.Client = Nothing

        Try
            If objComClient Is Nothing Then
                If strServer <> "" Then
                    objComClient = CreateObject("tikAeroProcess.Client", strServer)
                Else
                    objComClient = New tikAeroProcess.Client
                End If
            End If
            rsA = objComClient.GetCorporateClients(clientId, LastName)
            Marshal.FinalReleaseComObject(objComClient)
            objComClient = Nothing
        Catch
            If IsNothing(objComClient) = False Then
                Marshal.FinalReleaseComObject(objComClient)
                objComClient = Nothing
            End If
        End Try
        Return rsA
    End Function
    Public Function ClientGetEmpty(Optional ByRef rsClient As ADODB.Recordset = Nothing,
                                   Optional ByRef rsPassenger As ADODB.Recordset = Nothing,
                                   Optional ByRef sBookingRemark As ADODB.Recordset = Nothing) As Boolean

        Dim objComClient As tikAeroProcess.Client = Nothing
        Dim bSuccess As Boolean = False

        Try
            If objComClient Is Nothing Then
                If strServer <> "" Then
                    objComClient = CreateObject("tikAeroProcess.Client", strServer)
                Else
                    objComClient = New tikAeroProcess.Client
                End If
            End If
            bSuccess = objComClient.GetEmpty(rsClient, rsPassenger, sBookingRemark)
            Marshal.FinalReleaseComObject(objComClient)
            objComClient = Nothing
        Catch
            If IsNothing(objComClient) = False Then
                Marshal.FinalReleaseComObject(objComClient)
                objComClient = Nothing
            End If
        End Try
        Return bSuccess
    End Function

    Public Function ClientRead(Optional ByVal strClientProfileId As String = "",
                               Optional ByRef rsClient As ADODB.Recordset = Nothing,
                               Optional ByRef rsEmployee As ADODB.Recordset = Nothing,
                               Optional ByRef rsPassenger As ADODB.Recordset = Nothing,
                               Optional ByRef rsBookingRemark As ADODB.Recordset = Nothing,
                               Optional ByRef rsClientSegment As ADODB.Recordset = Nothing) As Boolean

        Dim bSuccess As Boolean = False
        Dim objComClient As tikAeroProcess.Client = Nothing

        Try
            If objComClient Is Nothing Then
                If strServer <> "" Then
                    objComClient = CreateObject("tikAeroProcess.Client", strServer)
                Else
                    objComClient = New tikAeroProcess.Client
                End If
            End If
            bSuccess = objComClient.Read(strClientProfileId, rsClient, rsEmployee, rsPassenger, rsBookingRemark, rsClientSegment)
            Marshal.FinalReleaseComObject(objComClient)
            objComClient = Nothing
        Catch
            If IsNothing(objComClient) = False Then
                Marshal.FinalReleaseComObject(objComClient)
                objComClient = Nothing
            End If
        End Try
        Return bSuccess
    End Function

    Public Function ClientSave(Optional ByRef rsClient As ADODB.Recordset = Nothing,
                               Optional ByRef rsPassenger As ADODB.Recordset = Nothing,
                               Optional ByRef rsBookingRemark As ADODB.Recordset = Nothing) As Boolean

        Dim bSuccess As Boolean = False
        Dim objComClient As tikAeroProcess.Client = Nothing

        Try
            If objComClient Is Nothing Then
                If strServer <> "" Then
                    objComClient = CreateObject("tikAeroProcess.Client", strServer)
                Else
                    objComClient = New tikAeroProcess.Client
                End If
            End If
            bSuccess = objComClient.Save(rsClient, rsPassenger, rsBookingRemark)
            Marshal.FinalReleaseComObject(objComClient)
            objComClient = Nothing
        Catch
            If IsNothing(objComClient) = False Then
                Marshal.FinalReleaseComObject(objComClient)
                objComClient = Nothing
            End If
        End Try

        Return bSuccess
    End Function

    Public Function GetTicketsIssued(ByVal dtReportFrom As DateTime,
                                     ByVal dtReportTo As DateTime,
                                     ByVal dtFlightFrom As DateTime,
                                     ByVal dtFlightTo As DateTime,
                                     ByVal strOrigin As String,
                                     ByVal strDestination As String,
                                     ByVal strAgency As String,
                                     ByVal strAirline As String,
                                     ByVal strFlight As String) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objMiscellaneous As tikAeroProcess.Miscellaneous = Nothing

        Try
            If objMiscellaneous Is Nothing Then
                If strServer <> "" Then
                    objMiscellaneous = CreateObject("tikAeroProcess.Miscellaneous", strServer)
                Else
                    objMiscellaneous = New tikAeroProcess.Miscellaneous
                End If
            End If
            rsA = objMiscellaneous.GetTicketsIssued(strOrigin,
                                                    strDestination,
                                                    strAgency,
                                                    strAirline,
                                                    strFlight,
                                                    dtReportFrom,
                                                    dtReportTo,
                                                    dtFlightFrom,
                                                    dtFlightTo)
            Marshal.FinalReleaseComObject(objMiscellaneous)
            objMiscellaneous = Nothing
        Catch
            If IsNothing(objMiscellaneous) = False Then
                Marshal.FinalReleaseComObject(objMiscellaneous)
                objMiscellaneous = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetTicketsUsed(ByVal dtReportFrom As DateTime,
                                   ByVal dtReportTo As DateTime,
                                   ByVal dtFlightFrom As DateTime,
                                   ByVal dtFlightTo As DateTime,
                                   ByVal strOrigin As String,
                                   ByVal strDestination As String,
                                   ByVal strAgency As String,
                                   ByVal strAirline As String,
                                   ByVal strFlight As String) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objMiscellaneous As tikAeroProcess.Miscellaneous = Nothing

        Try
            If objMiscellaneous Is Nothing Then
                If strServer <> "" Then
                    objMiscellaneous = CreateObject("tikAeroProcess.Miscellaneous", strServer)
                Else
                    objMiscellaneous = New tikAeroProcess.Miscellaneous
                End If
            End If
            rsA = objMiscellaneous.GetTicketsUsed(strOrigin,
                                                  strDestination,
                                                  strAgency,
                                                  strAirline,
                                                  strFlight,
                                                  dtReportFrom,
                                                  dtReportTo,
                                                  dtFlightFrom,
                                                  dtFlightTo)
            Marshal.FinalReleaseComObject(objMiscellaneous)
            objMiscellaneous = Nothing
        Catch
            If IsNothing(objMiscellaneous) = False Then
                Marshal.FinalReleaseComObject(objMiscellaneous)
                objMiscellaneous = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetTicketsRefunded(ByVal dtReportFrom As DateTime,
                                       ByVal dtReportTo As DateTime,
                                       ByVal dtFlightFrom As DateTime,
                                       ByVal dtFlightTo As DateTime,
                                       ByVal strOrigin As String,
                                       ByVal strDestination As String,
                                       ByVal strAgency As String,
                                       ByVal strAirline As String,
                                       ByVal strFlight As String) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objMiscellaneous As tikAeroProcess.Miscellaneous = Nothing
        Try
            If objMiscellaneous Is Nothing Then
                If strServer <> "" Then
                    objMiscellaneous = CreateObject("tikAeroProcess.Miscellaneous", strServer)
                Else
                    objMiscellaneous = New tikAeroProcess.Miscellaneous
                End If
            End If
            rsA = objMiscellaneous.GetTicketsRefunded(strOrigin,
                                                      strDestination,
                                                      strAgency,
                                                      strAirline,
                                                      strFlight,
                                                      dtReportFrom,
                                                      dtReportTo,
                                                      dtFlightFrom,
                                                      dtFlightTo)
            Marshal.FinalReleaseComObject(objMiscellaneous)
            objMiscellaneous = Nothing
        Catch
            If IsNothing(objMiscellaneous) = False Then
                Marshal.FinalReleaseComObject(objMiscellaneous)
                objMiscellaneous = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetTicketsExpired(ByVal dtReportFrom As DateTime,
                                      ByVal dtReportTo As DateTime,
                                      ByVal dtFlightFrom As DateTime,
                                      ByVal dtFlightTo As DateTime,
                                      ByVal strOrigin As String,
                                      ByVal strDestination As String,
                                      ByVal strAgency As String,
                                      ByVal strAirline As String,
                                      ByVal strFlight As String) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objMiscellaneous As tikAeroProcess.Miscellaneous = Nothing

        Try
            If objMiscellaneous Is Nothing Then
                If strServer <> "" Then
                    objMiscellaneous = CreateObject("tikAeroProcess.Miscellaneous", strServer)
                Else
                    objMiscellaneous = New tikAeroProcess.Miscellaneous
                End If
            End If
            rsA = objMiscellaneous.GetTicketsExpired(strOrigin,
                                                     strDestination,
                                                     strAgency,
                                                     strAirline,
                                                     strFlight,
                                                     dtReportFrom,
                                                     dtReportTo,
                                                     dtFlightFrom,
                                                     dtFlightTo)
            Marshal.FinalReleaseComObject(objMiscellaneous)
            objMiscellaneous = Nothing
        Catch
            If IsNothing(objMiscellaneous) = False Then
                Marshal.FinalReleaseComObject(objMiscellaneous)
                objMiscellaneous = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetTicketsCancelled(Optional ByVal strOrigin As String = "",
                                        Optional ByVal strDestination As String = "",
                                        Optional ByVal strAgency As String = "",
                                        Optional ByVal strAirline As String = "",
                                        Optional ByVal strFlight As String = "",
                                        Optional ByVal dtReportFrom As Date = #12/30/1899#,
                                        Optional ByVal dtReportTo As Date = #12/30/1899#,
                                        Optional ByVal dtFlightFrom As Date = #12/30/1899#,
                                        Optional ByVal dtFlightTo As Date = #12/30/1899#,
                                        Optional ByVal intTicketonly As Integer = 1,
                                        Optional ByVal intRefundable As Integer = 0,
                                        Optional ByVal strProfileID As String = "",
                                        Optional ByVal strTicketNumber As String = "",
                                        Optional ByVal strFirstName As String = "",
                                        Optional ByVal strLastName As String = "",
                                        Optional ByVal strPassengerId As String = "",
                                        Optional ByVal strBookingSegmentID As String = "") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objMiscellaneous As tikAeroProcess.Miscellaneous = Nothing

        Try
            If objMiscellaneous Is Nothing Then
                If strServer <> "" Then
                    objMiscellaneous = CreateObject("tikAeroProcess.Miscellaneous", strServer)
                Else
                    objMiscellaneous = New tikAeroProcess.Miscellaneous
                End If
            End If
            rsA = objMiscellaneous.GetTicketsCancelled(strOrigin,
                                                       strDestination,
                                                       strAgency,
                                                       strAirline,
                                                       strFlight,
                                                       dtReportFrom,
                                                       dtReportTo,
                                                       dtFlightFrom,
                                                       dtFlightTo,
                                                       intTicketonly,
                                                       intRefundable,
                                                       strProfileID,
                                                       strTicketNumber,
                                                       strFirstName,
                                                       strLastName,
                                                       strPassengerId,
                                                       strBookingSegmentID)
            Marshal.FinalReleaseComObject(objMiscellaneous)
            objMiscellaneous = Nothing
        Catch
            If IsNothing(objMiscellaneous) = False Then
                Marshal.FinalReleaseComObject(objMiscellaneous)
                objMiscellaneous = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetTicketsNotFlown(Optional ByVal strOrigin As String = "",
                                       Optional ByVal strDestination As String = "",
                                       Optional ByVal strAgency As String = "",
                                       Optional ByVal strAirline As String = "",
                                       Optional ByVal strFlight As String = "",
                                       Optional ByVal dtReportFrom As Date = #12/30/1899#,
                                       Optional ByVal dtReportTo As Date = #12/30/1899#,
                                       Optional ByVal dtFlightFrom As Date = #12/30/1899#,
                                       Optional ByVal dtFlightTo As Date = #12/30/1899#,
                                       Optional ByVal bUnflown As Boolean = True,
                                       Optional ByVal bNoShow As Boolean = True) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objMiscellaneous As tikAeroProcess.Miscellaneous = Nothing
        Try
            If objMiscellaneous Is Nothing Then
                If strServer <> "" Then
                    objMiscellaneous = CreateObject("tikAeroProcess.Miscellaneous", strServer)
                Else
                    objMiscellaneous = New tikAeroProcess.Miscellaneous
                End If
            End If
            rsA = objMiscellaneous.GetTicketsNotFlown(strOrigin,
                                                      strDestination,
                                                      strAgency,
                                                      strAirline,
                                                      strFlight,
                                                      dtReportFrom,
                                                      dtReportTo,
                                                      dtFlightFrom,
                                                      dtFlightTo,
                                                      bUnflown,
                                                      bNoShow)
            Marshal.FinalReleaseComObject(objMiscellaneous)
            objMiscellaneous = Nothing
        Catch
            If IsNothing(objMiscellaneous) = False Then
                Marshal.FinalReleaseComObject(objMiscellaneous)
                objMiscellaneous = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetPaymentApprovals(Optional ByVal dtPaymentFrom As Date = #12/30/1899#,
                                        Optional ByVal dtPaymentTo As Date = #12/30/1899#,
                                        Optional ByVal strDocumentFirst As String = "",
                                        Optional ByVal strDocumentlast As String = "",
                                        Optional ByVal strApprovalCode As String = "",
                                        Optional ByVal strPaymentReference As String = "",
                                        Optional ByVal strBookingReference As String = "",
                                        Optional ByVal strNameOnCard As String = "",
                                        Optional ByVal strStatus As String = "",
                                        Optional ByVal strType As String = "",
                                        Optional ByVal strError As String = "",
                                        Optional ByVal strSubTypes As String = "",
                                        Optional ByVal lPaymentNumber As Long = 0,
                                        Optional ByVal dPaymentAmount As Double = 0,
                                        Optional ByVal bExcludeSubTypes As Boolean = False) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objPayments As tikAeroProcess.Payment = Nothing

        Try
            If objPayments Is Nothing Then
                If strServer <> "" Then
                    objPayments = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objPayments = New tikAeroProcess.Payment
                End If
            End If
            rsA = objPayments.GetPaymentApprovals(dtPaymentFrom,
                                                  dtPaymentTo,
                                                  strDocumentFirst,
                                                  strDocumentlast,
                                                  strApprovalCode,
                                                  strPaymentReference,
                                                  strBookingReference,
                                                  strNameOnCard,
                                                  strStatus,
                                                  strType,
                                                  strError,
                                                  strSubTypes,
                                                  lPaymentNumber,
                                                  dPaymentAmount,
                                                  bExcludeSubTypes)
            Marshal.FinalReleaseComObject(objPayments)
            objPayments = Nothing
        Catch
            If IsNothing(objPayments) = False Then
                Marshal.FinalReleaseComObject(objPayments)
                objPayments = Nothing
            End If
            Throw
        End Try
        Return rsA
    End Function

    Public Function TicketRead(Optional ByRef strBookingId As String = "",
                               Optional ByRef strPassengerId As String = "",
                               Optional ByRef strSegmentId As String = "",
                               Optional ByRef strTicketNumber As String = "",
                               Optional ByRef rsTax As ADODB.Recordset = Nothing,
                               Optional ByRef bReadOnly As Boolean = False,
                               Optional ByRef bReturnTax As Boolean = True) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComBooking As tikAeroProcess.Booking = Nothing

        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If
            rsA = objComBooking.TicketRead(strBookingId,
                                           strPassengerId,
                                           strSegmentId,
                                           strTicketNumber,
                                           rsTax,
                                           bReadOnly,
                                           bReturnTax)
            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetCashbookPayments(Optional ByVal strAgency As String = "",
                                      Optional ByVal strGroup As String = "",
                                      Optional ByVal strUserId As String = "",
                                      Optional ByVal dtPaymentFrom As Date = #12/30/1899#,
                                      Optional ByVal dtPaymentTo As Date = #12/30/1899#) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objPayments As tikAeroProcess.Payment = Nothing

        Try
            If objPayments Is Nothing Then
                If strServer <> "" Then
                    objPayments = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objPayments = New tikAeroProcess.Payment
                End If
            End If
            rsA = objPayments.GetCashbookPaymentsAll(strAgency,
                                                  strGroup,
                                                  strUserId,
                                                  dtPaymentFrom,
                                                  dtPaymentTo)

            Marshal.FinalReleaseComObject(objPayments)
            objPayments = Nothing
        Catch
            If IsNothing(objPayments) = False Then
                Marshal.FinalReleaseComObject(objPayments)
                objPayments = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetCashbookPaymentSummary(Optional ByRef rsCashbookPaymentsAll As ADODB.Recordset = Nothing) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objPayments As tikAeroProcess.Payment = Nothing

        Try
            If objPayments Is Nothing Then
                If strServer <> "" Then
                    objPayments = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objPayments = New tikAeroProcess.Payment
                End If
            End If
            rsA = objPayments.GetCashbookPaymentSummary(rsCashbookPaymentsAll)
            Marshal.FinalReleaseComObject(objPayments)
            objPayments = Nothing
        Catch
            If IsNothing(objPayments) = False Then
                Marshal.FinalReleaseComObject(objPayments)
                objPayments = Nothing
            End If
        End Try
        Return rsA

    End Function


    '13012010
    Public Function GetCashbookCharges(Optional ByRef rsCashbookCharges As ADODB.Recordset = Nothing,
                                       Optional ByRef strCashbookId As String = "") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objPayments As tikAeroProcess.Payment = Nothing

        Try
            If objPayments Is Nothing Then
                If strServer <> "" Then
                    objPayments = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objPayments = New tikAeroProcess.Payment
                End If
            End If
            rsA = objPayments.GetCashbookCharges(rsCashbookCharges, strCashbookId)
            Marshal.FinalReleaseComObject(objPayments)
            objPayments = Nothing
        Catch
            If IsNothing(objPayments) = False Then
                Marshal.FinalReleaseComObject(objPayments)
                objPayments = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function GetBookingFeeAccounted(Optional ByVal strAgencyCode As String = "",
                                           Optional ByVal strUserId As String = "",
                                           Optional ByVal strFee As String = "",
                                           Optional ByVal dtFrom As Date = #12/30/1899#,
                                           Optional ByVal dtTo As Date = #12/30/1899#,
                                           Optional ByVal strAirline As String = "",
                                           Optional ByVal strFlightNumber As String = "",
                                           Optional ByVal strOrigin As String = "",
                                           Optional ByVal strDestination As String = "",
                                           Optional ByVal dtFlightFrom As Date = #12/30/1899#,
                                           Optional ByVal dtFlightTo As Date = #12/30/1899#,
                                           Optional ByVal strLanguage As String = "") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objFees As tikAeroProcess.Fees = Nothing

        Try
            If objFees Is Nothing Then
                If strServer <> "" Then
                    objFees = CreateObject("tikAeroProcess.Fees", strServer)
                Else
                    objFees = New tikAeroProcess.Fees
                End If
            End If
            rsA = objFees.GetBookingFeeAccounted(strAgencyCode,
                                                          strUserId,
                                                          strFee,
                                                          dtFrom,
                                                          dtTo,
                                                          strAirline,
                                                          strFlightNumber,
                                                          strOrigin,
                                                          strDestination,
                                                          dtFlightFrom,
                                                          dtFlightTo,
                                                          strLanguage)
            Marshal.FinalReleaseComObject(objFees)
            objFees = Nothing
        Catch
            If IsNothing(objFees) = False Then
                Marshal.FinalReleaseComObject(objFees)
                objFees = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetBookingFeeBooked(Optional ByVal strAgencyCode As String = "",
                                        Optional ByVal strUserId As String = "",
                                        Optional ByVal strFee As String = "",
                                        Optional ByVal dtFrom As Date = #12/30/1899#,
                                        Optional ByVal dtTo As Date = #12/30/1899#,
                                        Optional ByVal strAirline As String = "",
                                        Optional ByVal strFlightNumber As String = "",
                                        Optional ByVal strOrigin As String = "",
                                        Optional ByVal strDestination As String = "",
                                        Optional ByVal dtFlightFrom As Date = #12/30/1899#,
                                        Optional ByVal dtFlightTo As Date = #12/30/1899#,
                                        Optional ByVal strLanguage As String = "") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objFees As tikAeroProcess.Fees = Nothing

        Try
            If objFees Is Nothing Then
                If strServer <> "" Then
                    objFees = CreateObject("tikAeroProcess.Fees", strServer)
                Else
                    objFees = New tikAeroProcess.Fees
                End If
            End If
            rsA = objFees.GetBookingFeeBooked(strAgencyCode,
                                                       strUserId,
                                                       strFee,
                                                       dtFrom,
                                                       dtTo,
                                                       strAirline,
                                                       strFlightNumber,
                                                       strOrigin,
                                                       strDestination,
                                                       dtFlightFrom,
                                                       dtFlightTo,
                                                       strLanguage)
            Marshal.FinalReleaseComObject(objFees)
            objFees = Nothing
        Catch
            If IsNothing(objFees) = False Then
                Marshal.FinalReleaseComObject(objFees)
                objFees = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetBookingFeeVoided(Optional ByVal strAgencyCode As String = "",
                                        Optional ByVal strUserId As String = "",
                                        Optional ByVal strFee As String = "",
                                        Optional ByVal dtFrom As Date = #12/30/1899#,
                                        Optional ByVal dtTo As Date = #12/30/1899#,
                                        Optional ByVal strAirline As String = "",
                                        Optional ByVal strFlightNumber As String = "",
                                        Optional ByVal strOrigin As String = "",
                                        Optional ByVal strDestination As String = "",
                                        Optional ByVal dtFlightFrom As Date = #12/30/1899#,
                                        Optional ByVal dtFlightTo As Date = #12/30/1899#,
                                        Optional ByVal strLanguage As String = "") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objFees As tikAeroProcess.Fees = Nothing
        Try
            If objFees Is Nothing Then
                If strServer <> "" Then
                    objFees = CreateObject("tikAeroProcess.Fees", strServer)
                Else
                    objFees = New tikAeroProcess.Fees
                End If
            End If
            rsA = objFees.GetBookingFeeVoided(strAgencyCode,
                                                       strUserId,
                                                       strFee,
                                                       dtFrom,
                                                       dtTo,
                                                       strAirline,
                                                       strFlightNumber,
                                                       strOrigin,
                                                       strDestination,
                                                       dtFlightFrom,
                                                       dtFlightTo,
                                                       strLanguage)
            Marshal.FinalReleaseComObject(objFees)
            objFees = Nothing
        Catch
            If IsNothing(objFees) = False Then
                Marshal.FinalReleaseComObject(objFees)
                objFees = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function CreditCardPayment(Optional ByRef strCCNumber As String = "",
                                      Optional ByRef strTransType As String = "",
                                      Optional ByRef strTransStatus As String = "",
                                      Optional ByRef dtFrom As Date = #12/30/1899#,
                                      Optional ByRef dtTo As Date = #12/30/1899#,
                                      Optional ByRef strCCType As String = "",
                                      Optional ByRef strAgency As String = "") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objPayments As tikAeroProcess.Payment = Nothing

        Try
            If objPayments Is Nothing Then
                If strServer <> "" Then
                    objPayments = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objPayments = New tikAeroProcess.Payment
                End If
            End If
            rsA = objPayments.CreditCardPayment(strCCNumber,
                                                strTransType,
                                                strTransStatus,
                                                dtFrom,
                                                dtTo,
                                                strCCType,
                                                strAgency)
            Marshal.FinalReleaseComObject(objPayments)
            objPayments = Nothing
        Catch
            If IsNothing(objPayments) = False Then
                Marshal.FinalReleaseComObject(objPayments)
                objPayments = Nothing
            End If
        End Try
        Return rsA

    End Function
    Public Function GetServiceFees(ByVal strXml As String) As String
        Using objOut As New DataSet
            Dim rsA As ADODB.Recordset = Nothing
            Dim objPayments As tikAeroProcess.Payment = Nothing
            Try
                If objPayments Is Nothing Then
                    If strServer <> "" Then
                        objPayments = CreateObject("tikAeroProcess.Payment", strServer)
                    Else
                        objPayments = New tikAeroProcess.Payment
                    End If
                End If

                If strXml.Length > 0 Then
                    Dim xmlDoc As New System.Xml.XPath.XPathDocument(New System.IO.StringReader(strXml))
                    Dim nv As System.Xml.XPath.XPathNavigator = xmlDoc.CreateNavigator

                    Dim strOrigin As String
                    Dim strDestination As String
                    Dim strCurrency As String
                    Dim strAgencyCode As String
                    Dim strServiceGroup As String
                    Dim dtFee As DateTime
                    Dim da As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter

                    objOut.DataSetName = "ServiceFees"
                    For Each n As System.Xml.XPath.XPathNavigator In nv.Select("services/service")

                        strOrigin = n.SelectSingleNode("origin_rcd").InnerXml
                        strDestination = n.SelectSingleNode("destination_rcd").InnerXml
                        strCurrency = n.SelectSingleNode("currency_rcd").InnerXml
                        strAgencyCode = n.SelectSingleNode("agency_code").InnerXml
                        strServiceGroup = n.SelectSingleNode("service_group").InnerXml
                        dtFee = Convert.ToDateTime(n.SelectSingleNode("departure_date").InnerXml)

                        rsA = objPayments.GetServiceFees(strOrigin, strDestination, strCurrency, strAgencyCode, strServiceGroup, dtFee)

                        If Not rsA Is Nothing Then
                            If rsA.RecordCount > 0 Then
                                da.Fill(objOut, rsA, 0)
                                objOut.Tables(objOut.Tables.Count - 1).TableName = strOrigin + strDestination + strServiceGroup
                            End If
                            Helper.Check.ClearRecordset(rsA)
                        End If
                    Next
                End If

            Catch e As Exception
            Finally
                If IsNothing(objPayments) = False Then
                    Marshal.FinalReleaseComObject(objPayments)
                    objPayments = Nothing
                End If
            End Try
            Return objOut.GetXml
        End Using
    End Function
    Public Function GetServiceFees(ByRef strOrigin As String,
                                   ByRef strDestination As String,
                                   ByRef strCurrency As String,
                                   ByRef strAgency As String,
                                   ByRef strServiceGroup As String,
                                   Optional ByRef dtFee As Date = #12/30/1899#) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objPayments As tikAeroProcess.Payment = Nothing

        Try
            If objPayments Is Nothing Then
                If strServer <> "" Then
                    objPayments = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objPayments = New tikAeroProcess.Payment
                End If
            End If
            rsA = objPayments.GetServiceFees(strOrigin, strDestination, strCurrency, strAgency, strServiceGroup, dtFee)
            Marshal.FinalReleaseComObject(objPayments)
            objPayments = Nothing
        Catch
            If IsNothing(objPayments) = False Then
                Marshal.FinalReleaseComObject(objPayments)
                objPayments = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetBookingsThisUser(Optional ByRef strAgencyCode As String = "",
                                        Optional ByRef strUserId As String = "",
                                        Optional ByRef strAirline As String = "",
                                        Optional ByRef strFlightNumber As String = "",
                                        Optional ByRef dtFlightFrom As Date = #12/30/1899#,
                                        Optional ByRef dtFlightTo As Date = #12/30/1899#,
                                        Optional ByRef strRecordLocator As String = "",
                                        Optional ByRef strOrigin As String = "",
                                        Optional ByRef strDestination As String = "",
                                        Optional ByRef strPassengerName As String = "",
                                        Optional ByRef strSeatNumber As String = "",
                                        Optional ByRef strTicketNumber As String = "",
                                        Optional ByRef strPhoneNumber As String = "",
                                        Optional ByRef dtCreateFrom As Date = #12/30/1899#,
                                        Optional ByRef dtCreateTo As Date = #12/30/1899#) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComReservation As tikAeroProcess.Reservation = Nothing

        Try
            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Reservation", strServer)
                Else
                    objComReservation = New tikAeroProcess.Reservation
                End If
            End If
            rsA = objComReservation.GetBookingsThisUser(strAgencyCode,
                                                        strUserId,
                                                        strAirline,
                                                        strFlightNumber,
                                                        dtFlightFrom,
                                                        dtFlightTo,
                                                        strRecordLocator,
                                                        strOrigin,
                                                        strDestination,
                                                        strPassengerName,
                                                        strSeatNumber,
                                                        strTicketNumber,
                                                        strPhoneNumber,
                                                        dtCreateFrom,
                                                        dtCreateTo)
            Marshal.FinalReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch
            If IsNothing(objComReservation) = False Then
                Marshal.FinalReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetPassengerProfileSegments(Optional ByRef strPassengerProfileId As String = "") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComReservation As tikAeroProcess.Reservation = Nothing

        Try
            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Reservation", strServer)
                Else
                    objComReservation = New tikAeroProcess.Reservation
                End If
            End If
            rsA = objComReservation.GetPassengerProfileSegments(strPassengerProfileId)
            Marshal.FinalReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch
            If IsNothing(objComReservation) = False Then
                Marshal.FinalReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetOutstanding(Optional ByVal strAgencyCode As String = "",
                                    Optional ByVal strAirline As String = "",
                                    Optional ByVal strFlightNumber As String = "",
                                    Optional ByVal dtFlightFrom As Date = #12/30/1899#,
                                    Optional ByVal dtFlightTo As Date = #12/30/1899#,
                                    Optional ByVal strOrigin As String = "",
                                    Optional ByVal strDestination As String = "",
                                    Optional ByVal bOffices As Boolean = True,
                                    Optional ByVal bAgencies As Boolean = True,
                                    Optional ByVal bLastTwentyFourHours As Boolean = False,
                                    Optional ByVal bTicketedOnly As Boolean = True,
                                    Optional ByVal iOlderThanHours As Integer = 0,
                                    Optional ByVal strLanguage As String = "EN",
                                    Optional ByVal bAccountsPayable As Boolean = False) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objPayments As tikAeroProcess.Payment = Nothing
        Try
            If objPayments Is Nothing Then
                If strServer <> "" Then
                    objPayments = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objPayments = New tikAeroProcess.Payment
                End If
            End If
            rsA = objPayments.GetOutstanding(strAgencyCode,
                                             strAirline,
                                             strFlightNumber,
                                             dtFlightFrom,
                                             dtFlightTo,
                                             strOrigin,
                                             strDestination,
                                             bOffices,
                                             bAgencies,
                                             bLastTwentyFourHours,
                                             bTicketedOnly,
                                             iOlderThanHours,
                                             strLanguage,
                                             bAccountsPayable)
            Marshal.FinalReleaseComObject(objPayments)
            objPayments = Nothing
        Catch
            If IsNothing(objPayments) = False Then
                Marshal.FinalReleaseComObject(objPayments)
                objPayments = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetCorporateAgencyClients(ByRef AgencyCode As String) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComClient As tikAeroProcess.Client = Nothing
        Try
            If objComClient Is Nothing Then
                If strServer <> "" Then
                    objComClient = CreateObject("tikAeroProcess.Client", strServer)
                Else
                    objComClient = New tikAeroProcess.Client
                End If
            End If
            rsA = objComClient.GetCorporateAgencyClients(AgencyCode)
            Marshal.FinalReleaseComObject(objComClient)
            objComClient = Nothing
        Catch
            If IsNothing(objComClient) = False Then
                Marshal.FinalReleaseComObject(objComClient)
                objComClient = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function SplitBooking(ByVal rsSplitPassenger As ADODB.Recordset,
                                 ByVal strBookingIdOld As String,
                                 ByVal strReceivedFrom As String,
                                 ByVal strUserId As String,
                                 ByVal strAgencyCode As String,
                                 ByVal strUserCode As String,
                                 ByVal bCopyRemark As Boolean) As String

        Dim BookingId As String = ""
        Dim objComBooking As tikAeroProcess.Booking = Nothing

        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If

            BookingId = objComBooking.Split(rsSplitPassenger,
                                            strBookingIdOld,
                                            strReceivedFrom,
                                            strUserId,
                                            strAgencyCode,
                                            strUserCode,
                                            bCopyRemark)
            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
        End Try

        Return BookingId
    End Function

    Function AgencyRegistrationInsert(Optional ByVal strAgencyName As String = "",
                                      Optional ByVal strLegalName As String = "",
                                      Optional ByVal strAgencyType As String = "",
                                      Optional ByVal strIATA As String = "",
                                      Optional ByVal strTaxId As String = "",
                                      Optional ByVal strMail As String = "",
                                      Optional ByVal strFax As String = "",
                                      Optional ByVal strPhone As String = "",
                                      Optional ByVal strAddress1 As String = "",
                                      Optional ByVal strAddress2 As String = "",
                                      Optional ByVal strStreet As String = "",
                                      Optional ByVal strState As String = "",
                                      Optional ByVal strDistrict As String = "",
                                      Optional ByVal strProvince As String = "",
                                      Optional ByVal strCity As String = "",
                                      Optional ByVal strZipCode As String = "",
                                      Optional ByVal strPoBox As String = "",
                                      Optional ByVal strWebsite As String = "",
                                      Optional ByVal strContactPerson As String = "",
                                      Optional ByVal strLastName As String = "",
                                      Optional ByVal strFirstName As String = "",
                                      Optional ByVal strTitle As String = "",
                                      Optional ByVal strUserLogon As String = "",
                                      Optional ByVal strPassword As String = "",
                                      Optional ByVal strCountry As String = "",
                                      Optional ByVal strCurrency As String = "",
                                      Optional ByVal strLanguage As String = "",
                                      Optional ByVal strComment As String = "") As Boolean

        Dim b As Boolean = False
        Dim objComSetting As tikAeroProcess.Settings = Nothing

        Try

            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            b = objComSetting.AgencyRegistrationInsert(strAgencyName,
                                                          strLegalName,
                                                          strAgencyType,
                                                          strIATA,
                                                          strTaxId,
                                                          strMail,
                                                          strFax,
                                                          strPhone,
                                                          strAddress1,
                                                          strAddress2,
                                                          strStreet,
                                                          strState,
                                                          strDistrict,
                                                          strProvince,
                                                          strCity,
                                                          strZipCode,
                                                          strPoBox,
                                                          strWebsite,
                                                          strContactPerson,
                                                          strLastName,
                                                          strFirstName,
                                                          strTitle,
                                                          strUserLogon,
                                                          strPassword,
                                                          strCountry,
                                                          strCurrency,
                                                          strLanguage,
                                                          strComment)
            Marshal.FinalReleaseComObject(objComSetting)
            objComSetting = Nothing
        Catch
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
                objComSetting = Nothing
            End If
        End Try
        Return b
    End Function

    Public Function QueueMail(ByVal strFromAddress As String,
                                ByVal strFromName As String,
                                ByVal strToAddress As String,
                                ByVal strToAddressCC As String,
                                ByVal strToAddressBCC As String,
                                ByVal strReplyToAddress As String,
                                ByVal strSubject As String,
                                ByVal strBody As String,
                                Optional ByVal strDocumentType As String = "",
                                Optional ByVal strAttachmentStream As String = "",
                                Optional ByVal strAttachmentFileName As String = "",
                                Optional ByVal strAttachmentFileType As String = "",
                                Optional ByVal strAttachmentParser As String = "",
                                Optional ByVal bHtmlBody As Boolean = False,
                                Optional ByVal bConvertAttachmentFromHTML2PDF As Boolean = False,
                                Optional ByVal bRemoveFromQueue As Boolean = True,
                                Optional ByVal lMailPriority As tikAeroProcess.MAIL_PRIORITY = tikAeroProcess.MAIL_PRIORITY.NORMAL_PRIORITY,
                                Optional ByVal strUserId As String = "",
                                Optional ByVal strBookingId As String = "",
                                Optional ByVal strVoucherId As String = "",
                                Optional ByVal strBookingSegmentID As String = "",
                                Optional ByVal strPassengerId As String = "",
                                Optional ByVal strClientProfileId As String = "",
                                Optional ByVal strDocumentId As String = "",
                                Optional ByVal strLanguageCode As String = "") As Boolean
        Dim b As Boolean = False
        Dim objMessage As tikAeroProcess.Message = Nothing
        Try
            If objMessage Is Nothing Then
                If strServer <> "" Then
                    objMessage = CreateObject("tikAeroProcess.Message", strServer)
                Else
                    objMessage = New tikAeroProcess.Message
                End If
            End If

            b = objMessage.QueueMail(strFromAddress,
                                    strFromName,
                                    strToAddress,
                                    strToAddressCC,
                                    strToAddressBCC,
                                    strReplyToAddress,
                                    strSubject,
                                    strBody,
                                    strDocumentType,
                                    strAttachmentStream,
                                    strAttachmentFileName,
                                    strAttachmentFileType,
                                    strAttachmentParser,
                                    bHtmlBody,
                                    bConvertAttachmentFromHTML2PDF,
                                    bRemoveFromQueue,
                                    lMailPriority,
                                    strUserId,
                                    strBookingId,
                                    strVoucherId,
                                    strBookingSegmentID,
                                    strPassengerId,
                                    strClientProfileId,
                                    strDocumentId,
                                    strLanguageCode)

            Marshal.FinalReleaseComObject(objMessage)
            objMessage = Nothing
        Catch e As Exception
            If IsNothing(objMessage) = False Then
                Marshal.FinalReleaseComObject(objMessage)
                objMessage = Nothing
            End If
        End Try
        Return b
    End Function

    Public Function GetTransaction(Optional ByVal strOrigin As String = "",
                                    Optional ByVal strDestination As String = "",
                                    Optional ByVal strAirline As String = "",
                                    Optional ByVal strFlight As String = "",
                                    Optional ByVal strSegmentType As String = "",
                                    Optional ByVal strClientProfileId As String = "",
                                    Optional ByVal strPassengerProfileId As String = "",
                                    Optional ByVal strVendor As String = "",
                                    Optional ByVal strCreditDebit As String = "",
                                    Optional ByVal dtFlightFrom As Date = #12/30/1899#,
                                    Optional ByVal dtFlightTo As Date = #12/30/1899#,
                                    Optional ByVal dtTransactionFrom As Date = #12/30/1899#,
                                    Optional ByVal dtTransactionTo As Date = #12/30/1899#,
                                    Optional ByVal dtExpiryFrom As Date = #12/30/1899#,
                                    Optional ByVal dtExpiryTo As Date = #12/30/1899#,
                                    Optional ByVal dtVoidFrom As Date = #12/30/1899#,
                                    Optional ByVal dtVoidTo As Date = #12/30/1899#,
                                    Optional ByVal iBatch As Integer = 0,
                                    Optional ByVal bAllVoid As Boolean = False,
                                    Optional ByVal bAllExpired As Boolean = False,
                                    Optional ByVal bAuto As Boolean = True,
                                    Optional ByVal bManual As Boolean = True,
                                    Optional ByVal bAllPoint As Boolean = False) As ADODB.Recordset


        Dim rsA As ADODB.Recordset = Nothing
        Dim objComClient As tikAeroProcess.Client = Nothing

        Try
            If objComClient Is Nothing Then
                If strServer <> "" Then
                    objComClient = CreateObject("tikAeroProcess.Client", strServer)
                Else
                    objComClient = New tikAeroProcess.Client
                End If
            End If
            rsA = objComClient.GetTransaction(strOrigin,
                                                strDestination,
                                                strAirline,
                                                strFlight,
                                                strSegmentType,
                                                strClientProfileId,
                                                strPassengerProfileId,
                                                strVendor,
                                                strCreditDebit,
                                                dtFlightFrom,
                                                dtFlightTo,
                                                dtTransactionFrom,
                                                dtTransactionTo,
                                                dtExpiryFrom,
                                                dtExpiryTo,
                                                dtVoidFrom,
                                                dtVoidTo,
                                                iBatch,
                                                bAllVoid,
                                                bAllExpired,
                                                bAuto,
                                                bManual,
                                                bAllPoint)

            Marshal.FinalReleaseComObject(objComClient)
            objComClient = Nothing
        Catch
            If IsNothing(objComClient) = False Then
                Marshal.FinalReleaseComObject(objComClient)
                objComClient = Nothing
            End If
        End Try
        Return rsA
    End Function
    Public Function PassengerRoleRead(Optional ByVal strPaxRoleCode As String = "",
                                        Optional ByVal strPaxRole As String = "",
                                        Optional ByVal strStatus As String = "",
                                        Optional ByVal strLanguage As String = "EN",
                                        Optional ByVal bWrite As Boolean = False) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComSetting As tikAeroProcess.Settings = Nothing

        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            rsA = objComSetting.PassengerRoleRead(strPaxRoleCode, strPaxRole, strStatus, bWrite)
            Marshal.FinalReleaseComObject(objComSetting)
        Catch e As Exception
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
            End If
        End Try
        Return rsA

    End Function

    Public Sub UpdateApproval(ByVal strBookingId As String, ByVal iApproval As Integer)

        Dim objComSetting As tikAeroProcess.CheckIn = Nothing

        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.CheckIn", strServer)
                Else
                    objComSetting = New tikAeroProcess.CheckIn
                End If
            End If
            objComSetting.UpdateApproval(strBookingId, iApproval)
            Marshal.FinalReleaseComObject(objComSetting)
        Catch e As Exception
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
            End If
        End Try

    End Sub

    Public Function MemberLevelRead(Optional ByVal strMemberLevelCode As String = "",
                                    Optional ByVal strMemberLevel As String = "",
                                    Optional ByVal strStatus As String = "",
                                    Optional ByVal bWrite As Boolean = False) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objComSetting As tikAeroProcess.Settings = Nothing

        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            rsA = objComSetting.MemberLevelRead(strMemberLevelCode, strMemberLevel, strStatus, bWrite)
            Marshal.FinalReleaseComObject(objComSetting)
        Catch e As Exception
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
            End If
        End Try
        Return rsA
    End Function
    Public Function CheckUniqueMailAddress(ByVal strMail As String, Optional ByVal strClientProfileId As String = "") As Boolean
        Dim objComClient As tikAeroProcess.Client = Nothing
        Dim bResult As Boolean
        Try
            If objComClient Is Nothing Then
                If strServer <> "" Then
                    objComClient = CreateObject("tikAeroProcess.Client", strServer)
                Else
                    objComClient = New tikAeroProcess.Client
                End If
            End If
            bResult = objComClient.CheckUniqueMailAddress(strMail, strClientProfileId)
            Marshal.FinalReleaseComObject(objComClient)
            objComClient = Nothing
        Catch
            If IsNothing(objComClient) = False Then
                Marshal.FinalReleaseComObject(objComClient)
                objComClient = Nothing
            End If
        End Try
        Return bResult
    End Function

    Public Function AccuralQuote(ByVal rsPassenger As ADODB.Recordset, ByVal rsMapping As ADODB.Recordset, Optional ByVal strClientId As String = "") As ADODB.Recordset
        Dim objComFares As tikAeroProcess.Fares = Nothing
        Dim rsA As ADODB.Recordset = Nothing
        Try
            If objComFares Is Nothing Then
                If strServer <> "" Then
                    objComFares = CreateObject("tikAeroProcess.Fares", strServer)
                Else
                    objComFares = New tikAeroProcess.Fares
                End If
            End If
            rsA = objComFares.AccrualQuote(rsPassenger, rsMapping, strClientId)
            Marshal.FinalReleaseComObject(objComFares)
        Catch
            If IsNothing(objComFares) = False Then
                Marshal.FinalReleaseComObject(objComFares)
                objComFares = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetFlightDailyCount(Optional ByVal dtFrom As Date = #12/30/1899#,
                                    Optional ByVal dtTo As Date = #12/30/1899#,
                                    Optional ByVal strFrom As String = "",
                                    Optional ByVal strTo As String = "") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComFlights As tikAeroProcess.Flights = Nothing
        Try
            If objComFlights Is Nothing Then
                If strServer <> "" Then
                    objComFlights = CreateObject("tikAeroProcess.Flights", strServer)
                Else
                    objComFlights = New tikAeroProcess.Flights
                End If
            End If
            rsA = objComFlights.GetFlightDailyCount(dtFrom, dtTo, strFrom, strTo)
            Marshal.ReleaseComObject(objComFlights)
            objComFlights = Nothing
        Catch
            If IsNothing(objComFlights) = False Then
                Marshal.ReleaseComObject(objComFlights)
                objComFlights = Nothing
            End If
        End Try
        Return rsA
    End Function

    Protected Overrides Sub Finalize()
        Try
            'objComSetting = Nothing
            'objComInventory = Nothing
            'objComBooking = Nothing
            'objComReservation = Nothing
            'objComCreditCard = Nothing
            'objComCheckIn = Nothing
            'objPayments = Nothing

            undoImpersonation()
            RevertToSelf()
        Catch
        End Try
        MyBase.Finalize()
    End Sub

    Public Function GetBinRangeSearch(Optional ByRef strCardType As String = "", Optional ByRef strStatusCode As String = "") As ADODB.Recordset
        Dim rsA As ADODB.Recordset
        Dim objComSetting As tikAeroProcess.Settings = Nothing

        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            rsA = objComSetting.BinRangeSearch(strCardType, strStatusCode)
            Marshal.FinalReleaseComObject(objComSetting)
            objComSetting = Nothing
        Catch e As Exception
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
                objComSetting = Nothing
            End If
            Throw New System.Exception("No Bin-Range Search information" & " " & e.Message)
        End Try
        Return rsA
    End Function

    Public Function GetAgencyCommissionDetails(ByVal strAgency As String,
                                        ByVal strOrigin As String,
                                        ByVal strDestination As String,
                                        ByVal strAirline As String,
                                        ByVal strFlight As String,
                                        ByVal dtFlightFrom As DateTime,
                                        ByVal dtFlightTo As DateTime,
                                        ByVal dtSalesFrom As DateTime,
                                        ByVal dtSalesTo As DateTime,
                                        ByVal strLanguage As String,
                                        ByVal strStatus As String,
                                        ByVal strOwner As String,
                                        ByVal strCurrency As String) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComReservation As tikAeroProcess.Reservation = Nothing

        Try
            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Reservation", strServer)
                Else
                    objComReservation = New tikAeroProcess.Reservation
                End If
            End If

            rsA = objComReservation.GetAgencyCommissionDetails(strAgency,
                                                                strOrigin,
                                                                strDestination,
                                                                strAirline,
                                                                strFlight,
                                                                dtFlightFrom,
                                                                dtFlightTo,
                                                                dtSalesFrom,
                                                                dtSalesTo,
                                                                strLanguage,
                                                                strStatus,
                                                                strOwner,
                                                                strCurrency)
            Marshal.ReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch e As Exception
            If IsNothing(objComReservation) = False Then
                Marshal.ReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function GetAgencyTicketSales(ByVal strAgencyCode As String,
                                       ByVal strCurrency As String,
                                       ByVal dtFrom As DateTime,
                                       ByVal dtTo As DateTime) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objReport As tikAeroProcess.Report = Nothing

        Try
            If objReport Is Nothing Then
                If strServer <> "" Then
                    objReport = CreateObject("tikAeroProcess.Report", strServer)
                Else
                    objReport = New tikAeroProcess.Report
                End If
            End If

            rsA = objReport.GetAgencyTicketSales(strAgencyCode,
                                                    strCurrency,
                                                    dtFrom,
                                                    dtTo)
            Marshal.ReleaseComObject(objReport)
            objReport = Nothing
        Catch e As Exception
            If IsNothing(objReport) = False Then
                Marshal.ReleaseComObject(objReport)
                objReport = Nothing
            End If
        End Try
        Return rsA
    End Function

    Public Function FareQuote(ByRef rsPassenger As ADODB.Recordset,
                                ByVal strAgencyCode As String,
                                Optional ByRef rsTax As ADODB.Recordset = Nothing,
                                Optional ByRef rsMapping As ADODB.Recordset = Nothing,
                                Optional ByRef rsQuote As ADODB.Recordset = Nothing,
                                Optional ByVal rsFlights As ADODB.Recordset = Nothing,
                                Optional ByVal strFlightId As String = "",
                                Optional ByVal strBoardpoint As String = "",
                                Optional ByVal strOffpoint As String = "",
                                Optional ByVal strAirline As String = "",
                                Optional ByVal strFlight As String = "",
                                Optional ByVal strBoardingClass As String = "",
                                Optional ByVal strBookingClass As String = "",
                                Optional ByVal dtFlight As Date = #12/30/1899#,
                                Optional ByVal strFareId As String = "",
                                Optional ByVal strCurrencyCode As String = "",
                                Optional ByVal bRefundable As Boolean = False,
                                Optional ByVal bGroupBooking As Boolean = False,
                                Optional ByVal bNonRevenue As Boolean = False,
                                Optional ByVal strSegmentId As String = "",
                                Optional ByVal iIdReduction As Integer = 0,
                                Optional ByVal strFareType As String = "FARE",
                                Optional ByVal strLanguage As String = "EN",
                                Optional ByVal bNoVat As Boolean = False) As Boolean

        Dim objComFares As tikAeroProcess.Fares = Nothing
        Dim bResult As Boolean = False
        Try
            If objComFares Is Nothing Then
                If strServer <> "" Then
                    objComFares = CreateObject("tikAeroProcess.Fares", strServer)
                Else
                    objComFares = New tikAeroProcess.Fares
                End If
            End If
            bResult = objComFares.FareQuote(rsPassenger, strAgencyCode, rsTax, rsMapping, rsQuote, rsFlights, strFlightId, strBoardpoint, strOffpoint, strAirline, strFlight, strBoardingClass, strBookingClass, dtFlight, strFareId, strCurrencyCode, bRefundable, bGroupBooking, bNonRevenue, strSegmentId, iIdReduction, strFareType, strLanguage, bNoVat)

            Marshal.FinalReleaseComObject(objComFares)
        Catch
            If IsNothing(objComFares) = False Then
                Marshal.FinalReleaseComObject(objComFares)
                objComFares = Nothing
            End If
        End Try
        Return bResult
    End Function
    Public Function GetBookingHistory(ByVal strBookingID As String,
                                Optional ByVal rsHeader As ADODB.Recordset = Nothing,
                                Optional ByRef rsPassenger As ADODB.Recordset = Nothing,
                                Optional ByRef rsSegment As ADODB.Recordset = Nothing,
                                Optional ByVal rsPayment As ADODB.Recordset = Nothing,
                                Optional ByVal rsRemark As ADODB.Recordset = Nothing,
                                Optional ByRef rsMapping As ADODB.Recordset = Nothing,
                                Optional ByRef rsTax As ADODB.Recordset = Nothing,
                                Optional ByVal rsFee As ADODB.Recordset = Nothing,
                                Optional ByVal rsService As ADODB.Recordset = Nothing,
                                Optional ByVal rsIndex As ADODB.Recordset = Nothing) As DataSet
        Dim xml As String = ""
        Dim objOut As New DataSet
        Dim objComBooking As tikAeroProcess.Booking = Nothing
        Dim bResult As Boolean = False
        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If
            Dim da As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter
            objComBooking.GetHistory(strBookingID, rsHeader, rsPassenger, rsSegment, rsPayment, rsRemark, rsMapping, rsTax, rsFee, rsService, rsIndex)
            Marshal.FinalReleaseComObject(objComBooking)

            If Not rsHeader Is Nothing Then
                If rsHeader.RecordCount > 0 Then
                    da.Fill(objOut, rsHeader, 0)
                    objOut.Tables(objOut.Tables.Count - 1).TableName = "rsHeader"

                End If
            End If
            If Not rsPassenger Is Nothing Then
                If rsPassenger.RecordCount > 0 Then
                    da.Fill(objOut, rsPassenger, 1)
                    objOut.Tables(objOut.Tables.Count - 1).TableName = "rsPassenger"
                End If
            End If

            If Not rsSegment Is Nothing Then
                If rsSegment.RecordCount > 0 Then
                    da.Fill(objOut, rsSegment, 2)
                    objOut.Tables(objOut.Tables.Count - 1).TableName = "rsSegment"
                End If
            End If
            If Not rsPayment Is Nothing Then
                If rsPayment.RecordCount > 0 Then
                    da.Fill(objOut, rsPayment, 3)
                    objOut.Tables(objOut.Tables.Count - 1).TableName = "rsPayment"
                End If
            End If
            If Not rsRemark Is Nothing Then
                If rsRemark.RecordCount > 0 Then
                    da.Fill(objOut, rsRemark, 4)
                    objOut.Tables(objOut.Tables.Count - 1).TableName = "rsRemark"
                End If
            End If
            If Not rsMapping Is Nothing Then
                If rsMapping.RecordCount > 0 Then
                    da.Fill(objOut, rsMapping, 5)
                    objOut.Tables(objOut.Tables.Count - 1).TableName = "rsMapping"
                End If
            End If
            'If Not rsTax Is Nothing Then
            '    If rsTax.RecordCount > 0 Then
            '        da.Fill(objOut, rsTax, 6)
            '        objOut.Tables(objOut.Tables.Count - 1).TableName = "rsTax"
            '    End If
            'End If
            'If Not rsFee Is Nothing Then
            '    If rsFee.RecordCount > 0 Then
            '        da.Fill(objOut, rsFee, 7)
            '        objOut.Tables(objOut.Tables.Count - 1).TableName = "rsFee"
            '    End If
            'End If
            If Not rsService Is Nothing Then
                If rsService.RecordCount > 0 Then
                    da.Fill(objOut, rsService, 8)
                    objOut.Tables(objOut.Tables.Count - 1).TableName = "rsService"
                End If
            End If
            If Not rsIndex Is Nothing Then
                If rsIndex.RecordCount > 0 Then
                    da.Fill(objOut, rsIndex, 9)
                    objOut.Tables(objOut.Tables.Count - 1).TableName = "rsIndex"
                End If
            End If
            objComBooking = Nothing
        Catch e As Exception
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
        End Try
        Return objOut
    End Function

    Public Function GetFlightLegInfants(ByVal strFlightId As String, ByVal strOrigin As String, ByVal strDestination As String, ByVal strBoardingClass As String) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComInventory As tikAeroProcess.Inventory = Nothing
        Try
            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            rsA = objComInventory.GetFlightLegInfants(strFlightId, strOrigin, strDestination, strBoardingClass)
            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing
        Catch
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
        End Try
        Return rsA

    End Function
    Public Function GetRecordLocator(ByRef strRecordLocator As String, ByRef iBookingNumber As Integer) As Boolean

        Dim bResult As Boolean = False
        Dim objComBooking As tikAeroProcess.Booking = Nothing

        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If
            bResult = objComBooking.GetRecordLocator(strRecordLocator, iBookingNumber)
            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
        End Try
        Return bResult

    End Function

    Public Function ValidateVoucher(Optional ByRef rsVoucher As ADODB.Recordset = Nothing,
                                    Optional ByRef rsMapping As ADODB.Recordset = Nothing,
                                    Optional ByRef rsFee As ADODB.Recordset = Nothing,
                                    Optional ByRef strVoucherId As String = "") As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objPayments As tikAeroProcess.Payment = Nothing

        Try
            If objPayments Is Nothing Then
                If strServer <> "" Then
                    objPayments = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objPayments = New tikAeroProcess.Payment
                End If
            End If
            rsA = objPayments.ValidateVoucher(rsVoucher, rsMapping, rsFee, strVoucherId)
            Marshal.FinalReleaseComObject(objPayments)
            objPayments = Nothing
        Catch e As Exception
            If IsNothing(objPayments) = False Then
                Marshal.FinalReleaseComObject(objPayments)
                objPayments = Nothing
            End If
        End Try
        Return rsA

    End Function

    Public Function SegmentFee(ByVal strAgencyCode As String,
                             ByVal strCurrency As String,
                             ByVal xmlSegmentFee As String,
                             ByVal strLanguage As String,
                             ByVal bCalculateSpecialService As Boolean,
                             ByVal bNoVat As Boolean) As DataSet

        Dim b As Boolean = False
        Dim objComFees As tikAeroProcess.Fees = Nothing

        Dim ds As New DataSet

        Try
            If objComFees Is Nothing Then
                If strServer <> "" Then
                    objComFees = CreateObject("tikAeroProcess.Fees", strServer)
                Else
                    objComFees = New tikAeroProcess.Fees
                End If
            End If

            'Fill xml Data in to recordset
            Dim da As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter

            Dim rsSegment As ADODB.Recordset = Nothing
            Dim xmlDoc As New System.Xml.XPath.XPathDocument(New System.IO.StringReader(xmlSegmentFee))
            Dim nv As System.Xml.XPath.XPathNavigator = xmlDoc.CreateNavigator

            ds.DataSetName = "ServiceFees"
            For Each nf As System.Xml.XPath.XPathNavigator In nv.Select("booking/*")
                rsSegment = objComFees.GetSegmentFeeRecordset()

                For Each n As System.Xml.XPath.XPathNavigator In nf.Select("segment_fee")
                    rsSegment.AddNew()
                    If n.SelectSingleNode("flight_connection_id").InnerXml.Length > 0 Then
                        rsSegment.Fields("flight_connection_id").Value = "{" + n.SelectSingleNode("flight_connection_id").InnerXml + "}"
                    End If

                    rsSegment.Fields("origin_rcd").Value = n.SelectSingleNode("origin_rcd").InnerXml
                    rsSegment.Fields("destination_rcd").Value = n.SelectSingleNode("destination_rcd").InnerXml
                    rsSegment.Fields("od_origin_rcd").Value = n.SelectSingleNode("od_origin_rcd").InnerXml
                    rsSegment.Fields("od_destination_rcd").Value = n.SelectSingleNode("od_destination_rcd").InnerXml
                    rsSegment.Fields("booking_class_rcd").Value = n.SelectSingleNode("booking_class_rcd").InnerXml
                    rsSegment.Fields("fare_code").Value = n.SelectSingleNode("fare_code").InnerXml
                    rsSegment.Fields("airline_rcd").Value = n.SelectSingleNode("airline_rcd").InnerXml
                    rsSegment.Fields("flight_number").Value = n.SelectSingleNode("flight_number").InnerXml
                    rsSegment.Fields("departure_date").Value = n.SelectSingleNode("departure_date").InnerXml
                    If Convert.ToInt16(n.SelectSingleNode("pax_count").InnerXml) > 0 Then
                        rsSegment.Fields("pax_count").Value = n.SelectSingleNode("pax_count").InnerXml
                    End If
                    If Convert.ToInt16(n.SelectSingleNode("inf_count").InnerXml) > 0 Then
                        rsSegment.Fields("inf_count").Value = n.SelectSingleNode("inf_count").InnerXml
                    End If
                Next

                If rsSegment.RecordCount > 0 Then

                    If bCalculateSpecialService = True Then
                        b = objComFees.SpecialServiceFee(strAgencyCode, strCurrency, nf.Name, rsSegment, strLanguage, bNoVat)
                    Else
                        b = objComFees.SegmentFee(strAgencyCode, strCurrency, rsSegment, nf.Name, strLanguage, bNoVat)
                    End If

                    If b = True Then
                        'Fill recordset to dataset
                        da.Fill(ds, rsSegment, 0)
                        ds.Tables(ds.Tables.Count - 1).TableName = nf.Name
                    End If
                End If

                Helper.Check.ClearRecordset(rsSegment)
            Next

            'Marshal.FinalReleaseComObject(rsSegmentFeeStruct)
            Marshal.FinalReleaseComObject(objComFees)
            objComFees = Nothing
        Catch e As Exception
            If IsNothing(objComFees) = False Then
                Marshal.FinalReleaseComObject(objComFees)
                objComFees = Nothing
            End If
        End Try

        Return ds
    End Function
    Public Function SpecialServiceRead(Optional ByVal strSpecialServiceCode As String = "",
                                        Optional ByVal strSpecialServiceGroupCode As String = "",
                                        Optional ByVal strSpecialService As String = "",
                                        Optional ByVal strStatus As String = "",
                                        Optional ByVal bTextAllowed As Boolean = False,
                                        Optional ByVal bTextRequired As Boolean = False,
                                        Optional ByVal bServiceOnRequest As Boolean = False,
                                        Optional ByVal bManifest As Boolean = False,
                                        Optional ByVal bWrite As Boolean = False,
                                        Optional ByVal strLanguage As String = "EN") As ADODB.Recordset


        Dim rsA As ADODB.Recordset
        Dim rsLanguage As ADODB.Recordset = Nothing

        Dim objComSetting As tikAeroProcess.Settings = Nothing

        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            rsA = objComSetting.SpecialServiceRead(strSpecialServiceCode,
                                                    strSpecialServiceGroupCode,
                                                    strSpecialService,
                                                    strStatus,
                                                    bTextAllowed,
                                                    bTextRequired,
                                                    bServiceOnRequest,
                                                    bManifest,
                                                    bWrite,
                                                    rsLanguage,
                                                    strLanguage)

            Marshal.FinalReleaseComObject(objComSetting)
            objComSetting = Nothing
        Catch e As Exception
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
                objComSetting = Nothing
            End If
            Throw New System.Exception("No found agency information" & " " & e.Message)
        End Try
        If rsLanguage.RecordCount > 0 Then
            Helper.Check.ClearRecordset(rsA)
            Return rsLanguage
        Else
            Helper.Check.ClearRecordset(rsLanguage)
            Return rsA
        End If

    End Function

    Function InsertPaymentApproval(ByVal strPaymentApprovalID As String,
                                    ByVal strCardRcd As String,
                                    ByVal strCardNumber As String,
                                    ByVal strNameOnCard As String,
                                    ByVal iExpiryMonth As Integer,
                                    ByVal iExpiryYear As Integer,
                                    ByVal iIssueMonth As Integer,
                                    ByVal iIssueYear As Integer,
                                    ByVal iIssueNumber As Integer,
                                    ByVal strPaymentStateCode As String,
                                    ByVal strBookingPaymentId As String,
                                    ByVal strCurrencyRcd As String,
                                    ByVal dPaymentAmount As Double,
                                    ByVal strIpAddress As String) As Boolean

        Dim b As Boolean = False
        Dim objComPayment As tikAeroProcess.Payment = Nothing

        Try

            If objComPayment Is Nothing Then
                If strServer <> "" Then
                    objComPayment = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objComPayment = New tikAeroProcess.Payment
                End If
            End If
            b = objComPayment.InsertPaymentApproval(strPaymentApprovalID,
                                                    strCardRcd,
                                                    strCardNumber,
                                                    strNameOnCard,
                                                    iExpiryMonth,
                                                    iExpiryYear,
                                                    iIssueMonth,
                                                    iIssueYear,
                                                    iIssueNumber,
                                                    strPaymentStateCode,
                                                    strBookingPaymentId,
                                                    strCurrencyRcd,
                                                    dPaymentAmount,
                                                    strIpAddress)
            Marshal.FinalReleaseComObject(objComPayment)
            objComPayment = Nothing
        Catch
            If IsNothing(objComPayment) = False Then
                Marshal.FinalReleaseComObject(objComPayment)
                objComPayment = Nothing
            End If
            Throw
        End Try
        Return b
    End Function

    Function UpdatePaymentApproval(ByVal strApprovalCode As String,
                                   ByVal strPaymentReference As String,
                                   ByVal strTransactionReference As String,
                                   ByVal strTransactionDescription As String,
                                   ByVal strAvsCode As String,
                                   ByVal strAvsResponse As String,
                                   ByVal strCvvCode As String,
                                   ByVal strCvvResponse As String,
                                   ByVal strErrorCode As String,
                                   ByVal strErrorResponse As String,
                                   ByVal strPaymentStateCode As String,
                                   ByVal strResponseCode As String,
                                   ByVal strReturnCode As String,
                                   ByVal strResponseText As String,
                                   ByVal strPaymentApprovalId As String,
                                   ByVal strRequestStreamText As String,
                                   ByVal strReplyStreamText As String,
                                   ByVal strCardNumber As String,
                                   ByVal strCardType As String) As Boolean

        Dim b As Boolean = False
        Dim objComPayment As tikAeroProcess.Payment = Nothing

        Try

            If objComPayment Is Nothing Then
                If strServer <> "" Then
                    objComPayment = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objComPayment = New tikAeroProcess.Payment
                End If
            End If
            b = objComPayment.UpdatePaymentApproval(strApprovalCode,
                                                    strPaymentReference,
                                                    strTransactionReference,
                                                    strTransactionDescription,
                                                    strAvsCode,
                                                    strAvsResponse,
                                                    strCvvCode,
                                                    strCvvResponse,
                                                    strErrorCode,
                                                    strErrorResponse,
                                                    strPaymentStateCode,
                                                    strResponseCode,
                                                    strReturnCode,
                                                    strResponseText,
                                                    strPaymentApprovalId,
                                                    strRequestStreamText,
                                                    strReplyStreamText,
                                                    strCardNumber,
                                                    strCardType)
            Marshal.FinalReleaseComObject(objComPayment)
            objComPayment = Nothing
        Catch
            If IsNothing(objComPayment) = False Then
                Marshal.FinalReleaseComObject(objComPayment)
                objComPayment = Nothing
            End If
            Throw
        End Try
        Return b
    End Function
    Public Function ExchangeRateRead(ByVal strOriginCurrencyCode As String, ByVal strDestCurrencyCode As String) As Double
        Dim rsA As ADODB.Recordset = Nothing
        Dim ret As Double = 1D
        Dim ds As DataSet = New DataSet
        Dim da As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter
        Dim strSystemCurrency As String = ""

        Dim objComSetting As tikAeroProcess.Settings = Nothing
        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If

            If (strOriginCurrencyCode <> strDestCurrencyCode) Then
                rsA = objComSetting.SystemRead()
                ds = New DataSet
                da.Fill(ds, rsA, 0)
                Helper.Check.ClearRecordset(rsA)
                strSystemCurrency = ds.Tables(0).Rows(0)("currency_rcd").ToString
                If (strSystemCurrency <> strOriginCurrencyCode) Then
                    rsA = objComSetting.ExchangeRateRead(strOriginCurrencyCode)
                    ds = New DataSet
                    da.Fill(ds, rsA, 0)
                    ret = Convert.ToDouble(ds.Tables(0).Rows(0)("exchange_to_accounting"))
                    Helper.Check.ClearRecordset(rsA)
                End If
                If (strSystemCurrency <> strDestCurrencyCode) Then
                    rsA = objComSetting.ExchangeRateRead(strDestCurrencyCode)
                    ds = New DataSet
                    da.Fill(ds, rsA, 0)
                    ret = ret * Convert.ToDouble(ds.Tables(0).Rows(0)("exchange_from_accounting"))
                    Helper.Check.ClearRecordset(rsA)
                End If
            End If

            Marshal.FinalReleaseComObject(objComSetting)
            objComSetting = Nothing
            da.Dispose()
            ds.Dispose()
        Catch e As Exception
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
                objComSetting = Nothing
            End If
            Throw New System.Exception("Invalid currency type." & e.Message)
        End Try
        Return ret
    End Function


    Public Function AgencyAccountAdd(ByVal strAgencyCode As String,
                                     ByVal strCurrency As String,
                                     Optional ByVal strUserId As String = "",
                                     Optional ByVal strComment As String = "",
                                     Optional ByVal dAmount As Double = 0.0,
                                     Optional ByVal strExternalReference As String = "",
                                     Optional ByVal strInternalReference As String = "",
                                     Optional ByVal strTransactionReference As String = "",
                                     Optional ByVal bExternalTopup As Boolean = False) As Boolean

        Dim bResult As Boolean = False
        Dim objSettings As tikAeroProcess.Settings = Nothing
        Try
            If objSettings Is Nothing Then
                If Not String.IsNullOrEmpty(strServer) Then
                    objSettings = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objSettings = New tikAeroProcess.Settings()
                End If
            End If
            bResult = objSettings.AgencyAccountAdd(strAgencyCode,
                                                  strCurrency,
                                                  strUserId,
                                                  strComment,
                                                  dAmount,
                                                  strExternalReference,
                                                  strInternalReference,
                                                  strTransactionReference,
                                                  bExternalTopup)
        Catch e As Exception
            Throw New Exception(e.Message)
        Finally
            If IsNothing(objSettings) = False Then
                Marshal.FinalReleaseComObject(objSettings)
                objSettings = Nothing
            End If
        End Try
        Return bResult
    End Function


    Public Function AgencyAccountVoid(ByVal strAgencyAccountId As String, ByVal strUserId As String) As Boolean
        Dim bResult As Boolean = False
        Dim objSettings As tikAeroProcess.Settings = Nothing
        Try
            If objSettings Is Nothing Then
                If Not String.IsNullOrEmpty(strServer) Then
                    objSettings = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objSettings = New tikAeroProcess.Settings()
                End If
            End If
            bResult = objSettings.AgencyAccountVoid(strAgencyAccountId, strUserId)
        Catch e As Exception
            Throw New Exception(e.Message)
        Finally
            If IsNothing(objSettings) = False Then
                Marshal.FinalReleaseComObject(objSettings)
                objSettings = Nothing
            End If
        End Try
        Return bResult
    End Function

    Public Function ExternalPaymentListAgencyTopUp(ByVal strAgencyCode As String) As ADODB.Recordset
        Dim rsResult As ADODB.Recordset
        Dim objPayment As tikAeroProcess.Payment = Nothing
        Try
            If objPayment Is Nothing Then
                If Not String.IsNullOrEmpty(strServer) Then
                    objPayment = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objPayment = New tikAeroProcess.Payment()
                End If
            End If
            rsResult = objPayment.ExternalPaymentListAgencyTopUp(strAgencyCode)
        Catch e As Exception
            Throw New Exception(e.Message)
        Finally
            If IsNothing(objPayment) = False Then
                Marshal.FinalReleaseComObject(objPayment)
                objPayment = Nothing
            End If
        End Try
        Return rsResult
    End Function

    Public Function GetBaggageFeeOptions(ByVal rsMapping As ADODB.Recordset,
                                         ByVal strSegmentId As String,
                                         ByVal strPassengerId As String,
                                         Optional ByVal strAgencyCode As String = "",
                                         Optional ByVal strLanguage As String = "EN",
                                         Optional ByVal lMaxUnits As Long = 0,
                                         Optional ByVal rsFees As ADODB.Recordset = Nothing,
                                         Optional ByVal bApplySegmentFee As Boolean = False,
                                         Optional ByVal bNoVat As Boolean = False) As ADODB.Recordset

        Dim rsResult As ADODB.Recordset = Nothing
        Dim objComFees As tikAeroProcess.Fees = Nothing

        Try
            If objComFees Is Nothing Then
                If strServer <> "" Then
                    objComFees = CreateObject("tikAeroProcess.Fees", strServer)
                Else
                    objComFees = New tikAeroProcess.Fees
                End If
            End If

            rsResult = objComFees.GetBaggageFeeOptions(rsMapping, strSegmentId, strPassengerId, strAgencyCode, strLanguage, lMaxUnits, rsFees, bApplySegmentFee, bNoVat)

            Marshal.FinalReleaseComObject(objComFees)
            objComFees = Nothing
        Catch e As Exception

            If IsNothing(objComFees) = False Then
                Marshal.FinalReleaseComObject(objComFees)
                objComFees = Nothing
            End If
        End Try

        Return rsResult
    End Function

    Public Function ExternalPaymantAddPayment(ByVal strBookingId As String,
                                              ByVal strAgencyCode As String,
                                              ByVal strFormOfPayment As String,
                                              ByVal strCurrencyCode As String,
                                              ByVal dAmount As Decimal,
                                              ByVal strFormOfPaymentSubtype As String,
                                              ByVal strUserId As String,
                                              ByVal strDocumentNumber As String,
                                              ByVal strApprovalCode As String,
                                              ByVal strRemark As String,
                                              ByVal strLanguage As String,
                                              ByVal dtPayment As DateTime,
                                              ByVal bReturnItinerary As Boolean) As String

        Dim strResult As String = String.Empty
        Dim objPayments As tikAeroProcess.Payment = Nothing

        Try
            If objPayments Is Nothing Then
                If strServer <> "" Then
                    objPayments = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objPayments = New tikAeroProcess.Payment
                End If
            End If
            strResult = objPayments.ExternalPaymentAddPayment(strBookingId,
                                                              strAgencyCode,
                                                              strFormOfPayment,
                                                              strCurrencyCode,
                                                              dAmount,
                                                              strFormOfPaymentSubtype,
                                                              strUserId,
                                                              strDocumentNumber,
                                                              strApprovalCode,
                                                              strRemark,
                                                              strLanguage,
                                                              dtPayment,
                                                              bReturnItinerary)

            Marshal.FinalReleaseComObject(objPayments)
            objPayments = Nothing
        Catch e As Exception
            If IsNothing(objPayments) = False Then
                Marshal.FinalReleaseComObject(objPayments)
                objPayments = Nothing
            End If
        End Try
        Return strResult
    End Function

    'add GetAvailabilety for check SSR inventory availability
    Public Function GetAvailabilety(ByVal strFlightID As String,
                                ByVal strOriginRcd As String,
                                ByVal strDestinationRcd As String,
                                ByVal strSpecialServiceRcd As String,
                                ByVal strBoardingClassRcd As String
                                ) As ADODB.Recordset
        Dim rsResult As ADODB.Recordset = Nothing
        Dim objSpecialService As tikAeroProcess.SpecialService = Nothing

        Try
            If objSpecialService Is Nothing Then
                If strServer <> "" Then
                    objSpecialService = CreateObject("tikAeroProcess.SpecialService", strServer)
                Else
                    objSpecialService = New tikAeroProcess.SpecialService
                End If
            End If

            rsResult = objSpecialService.GetAvailabilety(strFlightID, strOriginRcd, strDestinationRcd, strSpecialServiceRcd, strBoardingClassRcd)

            Marshal.FinalReleaseComObject(objSpecialService)
            objSpecialService = Nothing
        Catch e As Exception

            If IsNothing(objSpecialService) = False Then
                Marshal.FinalReleaseComObject(objSpecialService)
                objSpecialService = Nothing
            End If
        End Try

        Return rsResult
    End Function
    Public Function SpecialServiceInventoryFlightGet(ByVal strFlightID As String, ByVal bWrite As Boolean) As ADODB.Recordset
        Dim rsResult As ADODB.Recordset = Nothing
        Dim objSpecialService As tikAeroProcess.SpecialService = Nothing

        Try
            If objSpecialService Is Nothing Then
                If strServer <> "" Then
                    objSpecialService = CreateObject("tikAeroProcess.SpecialService", strServer)
                Else
                    objSpecialService = New tikAeroProcess.SpecialService
                End If
            End If
            rsResult = objSpecialService.SpecialServiceInventoryFlightGet(strFlightID, bWrite)

            Marshal.FinalReleaseComObject(objSpecialService)
            objSpecialService = Nothing
        Catch e As Exception

            If IsNothing(objSpecialService) = False Then
                Marshal.FinalReleaseComObject(objSpecialService)
                objSpecialService = Nothing
            End If
        End Try

        Return rsResult
    End Function
    Public Function GetPassengerRole(ByVal strLanguage As String) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComReservation As tikAeroProcess.Reservation = Nothing
        Try
            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Reservation", strServer)
                Else
                    objComReservation = New tikAeroProcess.Reservation
                End If
            End If
            rsA = objComReservation.GetPassengerRole(strLanguage)
            Marshal.FinalReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch e As Exception
            If IsNothing(objComReservation) = False Then
                Marshal.FinalReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return rsA
    End Function
    Public Overridable Function ItiniraryRead(ByVal strBookingID As String,
                                             ByVal strRecordLocator As String,
                                             ByVal AgencyCode As String,
                                             Optional ByRef rsHeader As ADODB.Recordset = Nothing,
                                             Optional ByRef rsSegment As ADODB.Recordset = Nothing,
                                             Optional ByRef rsPassenger As ADODB.Recordset = Nothing,
                                             Optional ByRef rsRemark As ADODB.Recordset = Nothing,
                                             Optional ByRef rsPayment As ADODB.Recordset = Nothing,
                                             Optional ByRef rsMapping As ADODB.Recordset = Nothing,
                                             Optional ByRef rsServices As ADODB.Recordset = Nothing,
                                             Optional ByRef rsTax As ADODB.Recordset = Nothing,
                                             Optional ByRef rsFee As ADODB.Recordset = Nothing,
                                             Optional ByRef rsQuote As ADODB.Recordset = Nothing,
                                             Optional ByVal strLanguage As String = "EN") As Boolean

        Dim b As Boolean = False
        Dim objComConversion As tikAeroConversions.Booking = Nothing

        Try

            If objComConversion Is Nothing Then
                objComConversion = New tikAeroConversions.Booking
            End If

            b = GetItinerary(strBookingID, strLanguage, , , AgencyCode, rsHeader, rsSegment, rsPassenger, rsRemark, rsPayment, rsMapping, rsServices, rsTax, rsFee)
            If b = True Then
                rsQuote = objComConversion.SummarizeQuote(rsMapping, rsTax, , False, True)
            End If

            Marshal.FinalReleaseComObject(objComConversion)
            objComConversion = Nothing
        Catch e As Exception
            If IsNothing(objComConversion) = False Then
                Marshal.FinalReleaseComObject(objComConversion)
                objComConversion = Nothing
            End If
        End Try
        Return b
    End Function

    Public Function GetSpecialServices(ByVal strLanguage As String) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComReservation As tikAeroProcess.Reservation = Nothing
        Try
            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Reservation", strServer)
                Else
                    objComReservation = New tikAeroProcess.Reservation
                End If
            End If
            rsA = objComReservation.GetSpecialServices(strLanguage)
            Marshal.FinalReleaseComObject(objComReservation)
            objComReservation = Nothing
        Catch e As Exception
            If IsNothing(objComReservation) = False Then
                Marshal.FinalReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        End Try
        Return rsA
    End Function
    Public Function ProcessRefund(ByVal strBookingPaymentId As String,
                                  ByVal strBookingId As String,
                                  ByVal strUserId As String,
                                  ByVal strFormOfPayment As String,
                                  ByVal strAgencyCode As String,
                                  ByVal strSalesCurrency As String,
                                  ByVal strPaymentCurrency As String,
                                  ByVal dSalesAmount As Double,
                                  ByVal dPaymentAmount As Double,
                                  Optional ByVal rsCreditCard As ADODB.Recordset = Nothing,
                                  Optional ByVal rsAllocation As ADODB.Recordset = Nothing,
                                  Optional ByVal strVoucherID As String = "",
                                  Optional ByVal strRefundVoucher As String = "",
                                  Optional ByVal strClientID As String = "",
                                  Optional ByVal strNameOnVoucher As String = "",
                                  Optional ByVal strDebitAgencyCode As String = "",
                                  Optional ByVal strOriginBookingPaymentId As String = "",
                                  Optional ByVal strDocumentNumber As String = "",
                                  Optional ByVal dtPayment As Date = #12/30/1899#) As String

        Dim objPayments As tikAeroProcess.Payment = Nothing

        Try
            If objPayments Is Nothing Then
                If strServer <> "" Then
                    objPayments = CreateObject("tikAeroProcess.Payment", strServer)
                Else
                    objPayments = New tikAeroProcess.Payment
                End If
            End If

            If objPayments.ProcessRefund(strBookingPaymentId,
                                          strBookingId,
                                          strUserId,
                                          strFormOfPayment,
                                          strAgencyCode,
                                          strSalesCurrency,
                                          strPaymentCurrency,
                                          dSalesAmount,
                                          dPaymentAmount,
                                          rsCreditCard,
                                          rsAllocation,
                                          strVoucherID,
                                          strRefundVoucher,
                                          strClientID,
                                          strNameOnVoucher,
                                          strDebitAgencyCode,
                                          strOriginBookingPaymentId,
                                          strDocumentNumber,
                                          dtPayment) = True Then
                'Get Voucher xml.
                Return ReadVoucher(strVoucherID, 0)
            Else
                Return String.Empty
            End If

        Catch e As Exception
        Finally
            If IsNothing(objPayments) = False Then
                Marshal.FinalReleaseComObject(objPayments)
                objPayments = Nothing
            End If
        End Try
        Return String.Empty
    End Function

    Public Function ReadVoucher(ByVal strVoucherID As String, ByVal strVoucherNumber As String) As String
        Dim rsA As ADODB.Recordset = Nothing
        Dim objMiscellaneous As tikAeroProcess.Miscellaneous = Nothing
        Try
            If objMiscellaneous Is Nothing Then
                If strServer <> "" Then
                    objMiscellaneous = CreateObject("tikAeroProcess.Miscellaneous", strServer)
                Else
                    objMiscellaneous = New tikAeroProcess.Miscellaneous
                End If
            End If
            rsA = objMiscellaneous.ReadVoucher(strVoucherID, strVoucherNumber)
            If IsNothing(rsA) = False Then
                If rsA.RecordCount > 0 Then
                    Dim stb As New StringBuilder
                    Dim FieldName As String
                    Using stw As New System.IO.StringWriter(stb)
                        Using xtw As System.Xml.XmlWriter = System.Xml.XmlWriter.Create(stw)

                            rsA.MoveFirst()
                            xtw.WriteStartElement("Voucher")
                            While Not rsA.EOF
                                xtw.WriteStartElement("Details")
                                '********************Field*****************************

                                For i As Integer = 0 To rsA.Fields.Count - 1
                                    FieldName = rsA.Fields(i).Name
                                    xtw.WriteStartElement(FieldName)
                                    If rsA.Fields(FieldName).Value Is System.DBNull.Value Then
                                        xtw.WriteValue(String.Empty)
                                    Else
                                        xtw.WriteValue(rsA.Fields(FieldName).Value)
                                    End If
                                    xtw.WriteEndElement()
                                Next

                                '********************Field*****************************
                                xtw.WriteEndElement() 'Details

                                rsA.MoveNext()
                            End While
                            xtw.WriteEndElement() 'Voucher
                        End Using

                    End Using

                    Return stb.ToString()
                End If
            End If
            Return String.Empty
        Catch e As Exception
        Finally
            If IsNothing(objMiscellaneous) = False Then
                Marshal.FinalReleaseComObject(objMiscellaneous)
                objMiscellaneous = Nothing
            End If
            Helper.Check.ClearRecordset(rsA)
        End Try

        Return String.Empty
    End Function
    Public Function GetActiveBookings(ByVal strClientProfileId As String) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objComClient As tikAeroProcess.Client = Nothing
        Try

            If objComClient Is Nothing Then
                If strServer <> "" Then
                    objComClient = CreateObject("tikAeroProcess.Client", strServer)
                Else
                    objComClient = New tikAeroProcess.Client
                End If
            End If
            rsA = objComClient.GetActiveBookings("@@@" + strClientProfileId)
            Marshal.FinalReleaseComObject(objComClient)
            objComClient = Nothing
        Catch e As Exception
            If IsNothing(objComClient) = False Then
                Marshal.FinalReleaseComObject(objComClient)
                objComClient = Nothing
            End If
        End Try
        Return rsA
    End Function
    Public Function GetFlownBookings(ByVal strClientProfileId As String) As ADODB.Recordset
        Dim rsA As ADODB.Recordset = Nothing
        Dim objComClient As tikAeroProcess.Client = Nothing
        Try

            If objComClient Is Nothing Then
                If strServer <> "" Then
                    objComClient = CreateObject("tikAeroProcess.Client", strServer)
                Else
                    objComClient = New tikAeroProcess.Client
                End If
            End If
            rsA = objComClient.GetFlownBookings("@@@" + strClientProfileId)
            Marshal.FinalReleaseComObject(objComClient)
            objComClient = Nothing
        Catch e As Exception
            If IsNothing(objComClient) = False Then
                Marshal.FinalReleaseComObject(objComClient)
                objComClient = Nothing
            End If
        End Try
        Return rsA
    End Function
    Public Function UpdatePassengerCheckinDetails(ByVal strPassengerId As String,
                                                  ByVal gUpdateBy As Guid,
                                                  ByVal strGenderTypeRcd As String,
                                                  ByVal dtDateOfBirth As DateTime,
                                                  ByVal strNationalityRcd As String,
                                                  ByVal strPassportIssueCountryRcd As String,
                                                  ByVal dtPassportExpiryDate As DateTime,
                                                  ByVal strDocumentNumber As String,
                                                  ByVal strDocumentTypeRcd As String) As Boolean
        Dim bResult As Boolean = False
        Dim objComCheckIn As tikAeroProcess.CheckIn = Nothing
        Try
            If objComCheckIn Is Nothing Then
                If strServer <> "" Then
                    objComCheckIn = CreateObject("tikAeroProcess.CheckIn", strServer)
                Else
                    objComCheckIn = New tikAeroProcess.CheckIn
                End If
            End If
            bResult = objComCheckIn.UpdatePassengerCheckinDetails(strPassengerId,
                                                                  gUpdateBy.ToString(),
                                                                  strGenderTypeRcd,
                                                                  dtDateOfBirth,
                                                                  strNationalityRcd,
                                                                  strPassportIssueCountryRcd,
                                                                  dtPassportExpiryDate,
                                                                  strDocumentNumber,
                                                                  strDocumentTypeRcd)
            Marshal.FinalReleaseComObject(objComCheckIn)
            objComCheckIn = Nothing
        Catch e As Exception
            If IsNothing(objComCheckIn) = False Then
                Marshal.FinalReleaseComObject(objComCheckIn)
                objComCheckIn = Nothing
            End If
        End Try
        Return bResult
    End Function
    Public Function BookingVerifyForChange(ByVal BookingId As Guid) As Boolean
        Dim bResult As Boolean = False
        Dim objComBooking As tikAeroProcess.Booking = Nothing

        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If
            If BookingId.Equals(Guid.Empty) = False Then
                bResult = objComBooking.BookingVerifyForChange("{" + BookingId.ToString() + "}")
            End If
            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
        End Try
        Return bResult
    End Function

    Public Function GetBookingClasses(Optional ByVal strBoardingclass As String = "Y", Optional ByVal strLanguage As String = "EN", Optional ByVal bWrite As Boolean = False) As ADODB.Recordset

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComInventory As tikAeroProcess.Inventory = Nothing
        Try
            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            rsA = objComInventory.GetBookingClasses(strBoardingclass, strLanguage, bWrite)
            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing
        Catch
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
        End Try
        Return rsA

    End Function
    Public Function VerifyFlightInventorySession(ByVal rsSegment As ADODB.Recordset) As Boolean

        Dim bResult As Boolean = False
        Dim objComInventory As tikAeroProcess.Inventory = Nothing
        Try
            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            bResult = objComInventory.VerifyFlightInventorySession(rsSegment)
            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing
        Catch
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
        End Try
        Return bResult

    End Function
    Public Function VerifySeatAssignment(ByVal rsMapping As ADODB.Recordset, ByVal rsSegment As ADODB.Recordset) As Boolean

        Dim rsA As ADODB.Recordset = Nothing
        Dim objComReservation As tikAeroProcess.Reservation = Nothing
        Dim bResult As Boolean
        Try

            If objComReservation Is Nothing Then
                If strServer <> "" Then
                    objComReservation = CreateObject("tikAeroProcess.Reservation", strServer)
                Else
                    objComReservation = New tikAeroProcess.Reservation
                End If
            End If
            rsA = objComReservation.VerifySeatAssignment(rsMapping, rsSegment)
            Marshal.FinalReleaseComObject(objComReservation)
            objComReservation = Nothing

            If rsA Is Nothing Then
            ElseIf rsA.EOF = False Then
                bResult = True
            End If

        Catch e As Exception
            If IsNothing(objComReservation) = False Then
                Marshal.FinalReleaseComObject(objComReservation)
                objComReservation = Nothing
            End If
        Finally
            Helper.Check.ClearRecordset(rsA)
        End Try
        Return bResult
    End Function
    Public Function ValidAmount(ByVal rsPayment As ADODB.Recordset, ByVal rsMappings As ADODB.Recordset, ByVal rsFees As ADODB.Recordset, ByVal strAgencyCode As String, ByVal strIpAddress As String, ByVal strUserId As String, ByVal strVoucherSessionId As String) As Boolean
        Dim bValidateResult As Boolean = True
        Dim dclVoucherBalance As Decimal
        Dim dclPaymentAmount As Decimal
        Dim dclAgencyAccountBalance As Decimal

        Dim rsA As ADODB.Recordset = Nothing
        Dim rsV As ADODB.Recordset = Nothing

        'fix NP
        Dim payment_total As Decimal

        Try
            If IsNothing(rsPayment) = False AndAlso rsPayment.RecordCount > 0 Then
                '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                'Check voucher

                dclVoucherBalance = 0
                dclPaymentAmount = 0

                rsPayment.Filter = "form_of_payment_rcd = 'VOUCHER'"
                If rsPayment.RecordCount > 0 Then
                    While Not rsPayment.EOF

                        rsV = GetVouchers("", rsPayment.Fields("document_number").Value, "", "", "", "", "", "", "", True)
                        If IsNothing(rsV) = False AndAlso rsV.RecordCount > 0 Then
                            '1. Check voucher session.
                            If Not rsV.Fields("voucher_session_id").Value Is DBNull.Value Then
                                bValidateResult = False
                            End If

                            '2. Validate voucher
                            If bValidateResult = True Then
                                Dim rsVoucherAllocation As ADODB.Recordset = ValidateVoucher(rsV, rsMappings, rsFees, String.Empty)
                                If IsNothing(rsVoucherAllocation) = True Then
                                    bValidateResult = False
                                Else
                                    If rsVoucherAllocation.RecordCount = 0 Then
                                        bValidateResult = False
                                    Else

                                        'fix NP
                                        rsVoucherAllocation.Filter = "dif_amount > 0"
                                        If rsVoucherAllocation.RecordCount > 0 Then
                                            bValidateResult = False
                                        End If

                                        rsVoucherAllocation.Filter = 0

                                End If
                            End If
                                Helper.Check.ClearRecordset(rsVoucherAllocation)
                            End If

                            '3. Insert voucher session and sum total voucher amount.
                            If bValidateResult = True Then
                                InsertVoucherSession(strVoucherSessionId, rsV.Fields("voucher_id").Value, strAgencyCode, strIpAddress, strUserId)

                                'fix NP
                                If IsDBNull(rsV.Fields("payment_total").Value) Then
                                    payment_total = 0.0
                                Else
                                    payment_total = rsV.Fields("payment_total").Value
                                End If

                                dclVoucherBalance = dclVoucherBalance + (CType(rsV.Fields("voucher_value").Value, Decimal) - CType(payment_total, Decimal))

                            End If
                        End If

                        dclPaymentAmount = dclPaymentAmount + rsPayment.Fields("payment_amount").Value

                        'Clear recordset
                        Helper.Check.ClearRecordset(rsV)
                        rsPayment.MoveNext()
                    End While

                    '4. Check if the voucher balance is enough
                    If bValidateResult = True Then
                        If dclPaymentAmount > dclVoucherBalance Then
                            bValidateResult = False
                        End If
                    End If
                End If

                '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                'Check Credit Agency
                If bValidateResult = True Then
                    rsPayment.Filter = "form_of_payment_rcd = 'INV' OR form_of_payment_rcd = 'CRAGT'"
                    If rsPayment.RecordCount > 0 Then
                        'Read agency information for validate.
                        rsA = GetAgencySessionProfile(strAgencyCode, String.Empty)
                        If IsNothing(rsA) = False Then
                            If rsA.RecordCount > 0 Then
                                dclAgencyAccountBalance = CType(rsA.Fields("agency_account").Value, Decimal) - CType(rsA.Fields("booking_payment").Value, Decimal)
                            End If

                            'Clear recordset
                            Helper.Check.ClearRecordset(rsA)
                        End If

                        'Check account balance
                        While Not rsPayment.EOF
                            If dclAgencyAccountBalance < CType(rsPayment.Fields("payment_amount").Value, Decimal) Then
                                bValidateResult = False
                            End If
                            rsPayment.MoveNext()
                        End While


                    End If
                End If
            End If
        Catch ex As Exception
            If IsNothing(rsA) = False Then
                Helper.Check.ClearRecordset(rsA)
            End If
        End Try

        rsPayment.Filter = 0
        Return bValidateResult
    End Function
    Public Function InsertVoucherSession(Optional ByVal strVoucherSessionId As String = "",
                                        Optional ByVal strVoucherID As String = "",
                                        Optional ByVal strAgencyCode As String = "",
                                        Optional ByVal strIpAddress As String = "",
                                        Optional ByVal strRequestBy As String = "") As Boolean
        Dim bResult As Boolean
        Dim objMiscellaneous As tikAeroProcess.Miscellaneous = Nothing
        Try
            If objMiscellaneous Is Nothing Then
                If strServer <> "" Then
                    objMiscellaneous = CreateObject("tikAeroProcess.Miscellaneous", strServer)
                Else
                    objMiscellaneous = New tikAeroProcess.Miscellaneous
                End If
            End If
            bResult = objMiscellaneous.InsertVoucherSession(strVoucherSessionId, strVoucherID, strAgencyCode, strIpAddress, strRequestBy)
            Marshal.FinalReleaseComObject(objMiscellaneous)
            objMiscellaneous = Nothing
        Catch e As Exception
            If IsNothing(objMiscellaneous) = False Then
                Marshal.FinalReleaseComObject(objMiscellaneous)
                objMiscellaneous = Nothing
            End If
        End Try
        Return bResult
    End Function

    Public Function DeleteVoucherSession(Optional ByVal strVoucherSessionId As String = "",
                                        Optional ByVal rsPayment As ADODB.Recordset = Nothing) As Boolean
        Dim bResult As Boolean
        Dim objMiscellaneous As tikAeroProcess.Miscellaneous = Nothing
        Dim rsV As ADODB.Recordset = Nothing
        Try
            If IsNothing(rsPayment) = False AndAlso rsPayment.RecordCount > 0 Then
                '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                'Check voucher
                rsPayment.Filter = "form_of_payment_rcd = 'VOUCHER'"
                If rsPayment.RecordCount > 0 Then
                    While Not rsPayment.EOF
                        rsV = GetVouchers("", rsPayment.Fields("document_number").Value, "", "", "", "", "", "", "", True)
                        If IsNothing(rsV) = False AndAlso rsV.RecordCount > 0 Then
                            If objMiscellaneous Is Nothing Then
                                If strServer <> "" Then
                                    objMiscellaneous = CreateObject("tikAeroProcess.Miscellaneous", strServer)
                                Else
                                    objMiscellaneous = New tikAeroProcess.Miscellaneous
                                End If
                            End If
                            bResult = objMiscellaneous.ReleaseVoucherSession(strVoucherSessionId, rsV.Fields("voucher_id").Value)
                        End If
                        'Clear recordset
                        Helper.Check.ClearRecordset(rsV)
                        rsPayment.MoveNext()
                    End While
                End If
            End If

            Marshal.FinalReleaseComObject(objMiscellaneous)
            objMiscellaneous = Nothing
        Catch e As Exception
            If IsNothing(objMiscellaneous) = False Then
                Marshal.FinalReleaseComObject(objMiscellaneous)
                objMiscellaneous = Nothing
            End If
        End Try
        Return bResult
    End Function
    Public Function GetFlightsFLIFO(Optional ByVal strOrigin As String = "",
                                    Optional ByVal strDestination As String = "",
                                    Optional ByVal strAirline As String = "",
                                    Optional ByVal strFlightNumber As String = "",
                                    Optional ByVal dtFlightFrom As Date = #12/12/2008#,
                                    Optional ByVal dtFlightTo As Date = #12/12/2008#,
                                    Optional ByVal strLanguage As String = "EN") As String


        Dim xml As String = ""
        Dim objComXml As tikAeroProcess.XML = Nothing

        Try
            If objComXml Is Nothing Then
                If strServer <> "" Then
                    objComXml = CreateObject("tikAeroProcess.XML", strServer)
                Else
                    objComXml = New tikAeroProcess.XML
                End If
            End If
            xml = objComXml.GetFlightsFLIFO(strOrigin, strDestination, strAirline, strFlightNumber, dtFlightFrom, dtFlightTo, strLanguage)
            Marshal.FinalReleaseComObject(objComXml)
            objComXml = Nothing
        Catch e As Exception
            If IsNothing(objComXml) = False Then
                Marshal.FinalReleaseComObject(objComXml)
                objComXml = Nothing
            End If
        End Try
        Return xml
    End Function
    Public Function LowFareFinderAllow(ByVal strAgencyCode As String) As Boolean
        Dim bResult As Boolean
        Dim objComInventory As tikAeroProcess.Inventory = Nothing

        Try
            If objComInventory Is Nothing Then
                If strServer <> "" Then
                    objComInventory = CreateObject("tikAeroProcess.Inventory", strServer)
                Else
                    objComInventory = New tikAeroProcess.Inventory
                End If
            End If
            bResult = objComInventory.LowFareFinderAllow(strAgencyCode)

            Marshal.FinalReleaseComObject(objComInventory)
            objComInventory = Nothing
        Catch
            If IsNothing(objComInventory) = False Then
                Marshal.FinalReleaseComObject(objComInventory)
                objComInventory = Nothing
            End If
        End Try
        Return bResult
    End Function
    Public Function BoardingClassRead(Optional ByVal strBoardingClassCode As String = "",
                                      Optional ByVal strBoardingClass As String = "",
                                      Optional ByVal strSortSeq As String = "",
                                      Optional ByVal strStatus As String = "",
                                      Optional ByVal bWrite As Boolean = False) As ADODB.Recordset


        Dim rsA As ADODB.Recordset
        Dim objComSetting As tikAeroProcess.Settings = Nothing

        Try
            If objComSetting Is Nothing Then
                If strServer <> "" Then
                    objComSetting = CreateObject("tikAeroProcess.Settings", strServer)
                Else
                    objComSetting = New tikAeroProcess.Settings
                End If
            End If
            rsA = objComSetting.BoardingClassRead(strBoardingClassCode,
                                                  strBoardingClass,
                                                  strSortSeq,
                                                  strStatus,
                                                  bWrite)

            Marshal.FinalReleaseComObject(objComSetting)
            objComSetting = Nothing
        Catch e As Exception
            If IsNothing(objComSetting) = False Then
                Marshal.FinalReleaseComObject(objComSetting)
                objComSetting = Nothing
            End If
            Throw e
        End Try

        Return rsA
    End Function

    Public Function RemarkAdd(ByVal strRemarkType As String,
                                          ByVal strBookingRemarkId As String,
                                          ByVal strBookingId As String,
                                          ByVal strClientProfileId As String,
                                          ByRef strNickname As String,
                                          ByRef strRemarkText As String,
                                          ByRef strAgencyCode As String,
                                          ByRef strAddedBy As String,
                                          ByRef strUserId As String,
                                          ByRef bProtected As Boolean,
                                          ByRef bWarning As Boolean,
                                          ByRef bProcessMessage As Boolean,
                                          ByRef bSystemRemark As Boolean,
                                          ByVal timelimit As Date,
                                          ByVal timelimitUTC As Date) As String
        'Dim b As Boolean = False
        Dim objComBooking As tikAeroProcess.Booking = Nothing
        Dim result As String = String.Empty
        Try
            If objComBooking Is Nothing Then
                If strServer <> "" Then
                    objComBooking = CreateObject("tikAeroProcess.Booking", strServer)
                Else
                    objComBooking = New tikAeroProcess.Booking
                End If
            End If

            result = objComBooking.RemarkAdd(strRemarkType,
                                            strBookingRemarkId,
                                            strBookingId,
                                            strClientProfileId,
                                            strNickname,
                                            strRemarkText,
                                            strAgencyCode,
                                            strAddedBy,
                                            strUserId,
                                            bProtected,
                                            bWarning,
                                            bProcessMessage,
                                            bSystemRemark,
                                            timelimit,
                                            timelimitUTC)

            Marshal.FinalReleaseComObject(objComBooking)
            objComBooking = Nothing
        Catch e As Exception
            If IsNothing(objComBooking) = False Then
                Marshal.FinalReleaseComObject(objComBooking)
                objComBooking = Nothing
            End If
            Throw
        End Try
        Return result

    End Function
End Class
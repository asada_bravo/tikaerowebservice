﻿Public Class Availability
    Public origin_rcd As String
    Public destination_rcd As String
    Public currency_rcd As String
    Public departure_date As DateTime
    Public fare_code As String
    Public booking_class_rcd As String
    Public adult_fare As Double
    Public child_fare As Double
    Public infant_fare As Double
    Public other_fare As Double
End Class

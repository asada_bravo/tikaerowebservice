Imports System.Reflection
Imports System.Data.SqlTypes
Imports System.Configuration
Imports TikAero.DAL

Namespace Helper
    Public Class RecordSet

        Public Property DefaultCreateById() As Guid
            Get
                Return _DefaultCreateById
            End Get
            Set(ByVal Value As Guid)
                _DefaultCreateById = Value
            End Set
        End Property
        Private _DefaultCreateById As Guid


        Public Sub New()
            If IsNothing(System.Web.HttpContext.Current.Session("CreateById")) = False Then
                If System.Web.HttpContext.Current.Session("CreateById").ToString.Length > 0 Then
                    Dim g As New Guid(System.Web.HttpContext.Current.Session("CreateById").ToString)
                    If g.Equals(Guid.Empty) = False Then
                        _DefaultCreateById = g
                    Else
                        If Not Helper.Check.IsGuid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy"), _DefaultCreateById) Then
                            _DefaultCreateById = Nothing
                        End If
                    End If
                End If
            Else
                If Not Helper.Check.IsGuid(System.Configuration.ConfigurationManager.AppSettings("DefaultCreateBy"), _DefaultCreateById) Then
                    _DefaultCreateById = Nothing
                End If
            End If

        End Sub

        Public Sub Fill(ByVal rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObject, ByVal d As BusinessObjects.BusinessObject)
            Dim isUpdate As Boolean = False
            Update(rs, o, d, isUpdate)
            If isUpdate Then
                rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                rs("update_date_time").Value = Now()
            End If
        End Sub

        Public Sub Fill(ByVal rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObject)
            Call Add(rs, o)
            rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
            rs("create_date_time").Value = Now()
            rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
            rs("update_date_time").Value = Now()
        End Sub
        Public Sub FillAccountBy(ByVal rs As ADODB.Recordset)
            If Not rs.EOF Then
                rs.MoveFirst()
                While Not rs.EOF

                    If rs("account_fee_by").Value Is System.DBNull.Value Then
                        rs("account_fee_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                    End If

                    rs.MoveNext()
                End While
                rs.MoveFirst()
                rs.Filter = 0
            End If
        End Sub
        Public Sub FillCreateDate(ByVal rs As ADODB.Recordset)
            If Not rs.EOF Then
                rs.MoveFirst()
                While Not rs.EOF
                    If rs("create_by").Value Is System.DBNull.Value Then
                        rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                        rs("create_date_time").Value = Now()
                    End If
                    If rs("update_by").Value Is System.DBNull.Value Then
                        rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                    End If

                    rs("update_date_time").Value = Now()
                    rs.MoveNext()
                End While
                rs.MoveFirst()
                rs.Filter = 0
            End If
        End Sub
        Public Sub Fill(ByVal rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObjectCollection, ByVal d As BusinessObjects.BusinessObjectCollection)
            If Not rs.EOF Then
                rs.MoveFirst()
                For i As Integer = 0 To o.Count - 1
                    Dim isUpdate As Boolean = False
                    Call Update(rs, o(i), d(i), isUpdate)
                    If isUpdate Then
                        rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                        rs("update_date_time").Value = Now()
                    End If
                    rs.MoveNext()
                Next
                rs.MoveFirst()
            End If
        End Sub

        ' Start Yai add B2A
        Public Sub FillAccountBalance(ByRef rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObjectCollection, Optional ByVal indication As Boolean = True)
            For i As Integer = 0 To o.Count - 1
                Call Add(rs, o(i))
            Next
        End Sub

        Public Sub FillAgency(ByRef rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObjectCollection, Optional ByVal indication As Boolean = True)
            For i As Integer = 0 To o.Count - 1
                Call Add(rs, o(i))
            Next
        End Sub

        Public Sub FillAgencyUpdate(ByRef rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObjectCollection)
            For i As Integer = 0 To o.Count - 1
                Dim isUpdate As Boolean = False
                Call Update(rs, o(i), isUpdate)
                If isUpdate Then
                    If rs("update_by").Value Is System.DBNull.Value Then
                        rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                    End If
                    rs("update_date_time").Value = Now()
                End If
            Next
        End Sub
        ' End Yai add B2A

        Public Sub FillItinerary(ByRef rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObjectCollection, ByVal d As BusinessObjects.BusinessObjectCollection, ByVal IdKey As String)
            If Not rs.EOF Then
                For i As Integer = 0 To o.Count - 1
                    rs.Filter = rs.Fields(0).Name & " = '{" & DirectCast(o, TikAero.DAL.BusinessObjects.Itinerary).GetKey(i).ToString & "}'"
                    If rs.RecordCount = 0 Then
                        Call Add(rs, o(i))
                        rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                        rs("create_date_time").Value = Now()
                        rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                        rs("update_date_time").Value = Now()
                    Else
                        Dim isUpdate As Boolean = False
                        Call Update(rs, o(i), d(i), isUpdate)
                        If isUpdate Then
                            If rs("create_by").Value Is System.DBNull.Value Then
                                rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                                rs("create_date_time").Value = Now()
                            End If
                            rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                            rs("update_date_time").Value = Now()
                        End If
                    End If

                    rs.Filter = 0
                Next
            End If
        End Sub

        Public Sub FillPayments(ByRef rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObjectCollection)
            Try
                For i As Integer = 0 To o.Count - 1
                    If rs.EOF Then
                        Call Add(rs, o(i))
                        rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                        rs("create_date_time").Value = Now()
                        rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                        rs("update_date_time").Value = Now()
                    Else
                        rs.Filter = "(booking_payment_id = '{" & DirectCast(o, TikAero.DAL.BusinessObjects.PaymentsUpdate).GetKey(i).ToString & "}') "
                        If rs.RecordCount = 0 Then
                            Call Add(rs, o(i))
                            rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                            rs("create_date_time").Value = Now()
                            rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                            rs("update_date_time").Value = Now()
                        Else
                            Dim isUpdate As Boolean = False
                            Call Update(rs, o(i), isUpdate)
                            If isUpdate Then
                                If rs("create_by").Value Is System.DBNull.Value Then
                                    rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                                    rs("create_date_time").Value = Now()
                                End If
                                rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                                rs("update_date_time").Value = Now()
                            End If
                        End If
                        rs.Filter = 0
                    End If
                Next
            Catch ex As Exception

            End Try
        End Sub

        Public Sub FillPassengers(ByRef rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObjectCollection)
            If Not rs.EOF Then
                For i As Integer = 0 To o.Count - 1
                    rs.Filter = "(passenger_id= '{" & DirectCast(o, TikAero.DAL.BusinessObjects.Passengers).GetKey(i).ToString & "}') "
                    If rs.RecordCount = 0 Then
                        Call Add(rs, o(i))
                        If rs("create_by").Value Is System.DBNull.Value Then
                            rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                            rs("create_date_time").Value = Now()
                        End If
                        If rs("update_by").Value Is System.DBNull.Value Then
                            rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                        End If
                        rs("update_date_time").Value = Now()
                    Else
                        Dim isUpdate As Boolean = False
                        Call Update(rs, o(i), isUpdate)
                        If isUpdate Then
                            If rs("create_by").Value Is System.DBNull.Value Then
                                rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                                rs("create_date_time").Value = Now()
                            End If
                            If rs("update_by").Value Is System.DBNull.Value Then
                                rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                            End If
                            rs("update_date_time").Value = Now()
                        End If
                    End If
                    rs.Filter = 0
                Next
            End If
        End Sub

        Public Sub FillRemarks(ByRef rs As ADODB.Recordset, ByVal o As BusinessObjects.Remarks)
            For i As Integer = 0 To o.Count - 1
                If o(i).booking_remark_id.Equals(Guid.Empty) = False Then
                    rs.Filter = "(booking_remark_id = '{" & o(i).booking_remark_id.ToString & "}') "
                    If rs.RecordCount = 0 Then
                        Call Add(rs, o(i))
                        If rs("create_by").Value Is System.DBNull.Value Then
                            rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                            rs("create_date_time").Value = Now()
                        End If
                        If rs("update_by").Value Is System.DBNull.Value Then
                            rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                        End If
                        rs("update_date_time").Value = Now()
                    Else
                        Dim isUpdate As Boolean = False
                        Call Update(rs, o(i), isUpdate)
                        If isUpdate Then
                            If rs("create_by").Value Is System.DBNull.Value Then
                                rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                                rs("create_date_time").Value = Now()
                            End If
                            If rs("update_by").Value Is System.DBNull.Value Then
                                rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                            End If
                            rs("update_date_time").Value = Now()
                        End If
                    End If
                    rs.Filter = 0
                End If
            Next
        End Sub

        Public Sub FillMapping(ByRef rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObjectCollection)
            If Not rs.EOF Then
                For i As Integer = 0 To o.Count - 1
                    rs.Filter = "(booking_segment_id = '{" & DirectCast(o, TikAero.DAL.BusinessObjects.MappingsUpdate).GetKey(i)(0).ToString & "}') " &
                                "AND (passenger_id = '{" & DirectCast(o, TikAero.DAL.BusinessObjects.MappingsUpdate).GetKey(i)(1).ToString & "}')"
                    If rs.RecordCount = 0 Then
                        Call Add(rs, o(i))
                        If rs("create_by").Value Is System.DBNull.Value Then
                            rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                            rs("create_date_time").Value = Now()
                        End If
                        If rs("update_by").Value Is System.DBNull.Value Then
                            rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                        End If
                        rs("update_date_time").Value = Now()
                    Else
                        Dim isUpdate As Boolean = False
                        Call Update(rs, o(i), isUpdate)
                        If isUpdate Then
                            If rs("create_by").Value Is System.DBNull.Value Then
                                rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                                rs("create_date_time").Value = Now()
                            End If
                            If rs("update_by").Value Is System.DBNull.Value Then
                                rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                            End If
                            rs("update_date_time").Value = Now()
                        End If

                    End If
                    rs.Filter = 0
                Next
            End If
        End Sub

        Public Function UpdateBookingHeader(ByRef rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObject) As Boolean
            Dim isUpdate As Boolean = False
            If Not rs.EOF Then
                If rs.RecordCount > 0 Then
                    Call Update(rs, o, isUpdate)
                    If isUpdate Then
                        If rs("create_by").Value Is System.DBNull.Value Then
                            rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                            rs("create_date_time").Value = Now()
                        End If
                        If rs("update_by").Value Is System.DBNull.Value Then
                            rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                        End If
                        rs("update_date_time").Value = Now()
                    End If
                End If
            End If
            Return isUpdate
        End Function

        Public Function UpdateMapping(ByRef rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObjectCollection) As Boolean
            Dim isUpdate As Boolean = False
            If Not rs.EOF Then
                For i As Integer = 0 To o.Count - 1
                    rs.Filter = "(booking_segment_id = '{" & DirectCast(o, TikAero.DAL.BusinessObjects.MappingsUpdate).GetKey(i)(0).ToString & "}') " & _
                                "AND (passenger_id = '{" & DirectCast(o, TikAero.DAL.BusinessObjects.MappingsUpdate).GetKey(i)(1).ToString & "}')"
                    If rs.RecordCount > 0 Then
                        Call Update(rs, o(i), isUpdate)
                        If isUpdate Then
                            If rs("create_by").Value Is System.DBNull.Value Then
                                rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                                rs("create_date_time").Value = Now()
                            End If
                            If rs("update_by").Value Is System.DBNull.Value Then
                                rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                            End If
                            rs("update_date_time").Value = Now()
                        End If
                    End If
                    rs.Filter = 0
                Next
            End If

            Return isUpdate
        End Function

        Public Sub FillTaxes(ByRef rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObjectCollection)
            If Not rs.EOF Then
                For i As Integer = 0 To o.Count - 1
                    rs.Filter = "(passenger_segment_tax_id= '{" & DirectCast(o, TikAero.DAL.BusinessObjects.Taxes).GetKey(i).ToString & "}') "
                    If rs.RecordCount = 0 Then
                        Call Add(rs, o(i))
                        rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                        rs("create_date_time").Value = Now()
                        rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                        rs("update_date_time").Value = Now()
                    Else
                        Dim isUpdate As Boolean = False
                        Call Update(rs, o(i), isUpdate)
                        If isUpdate Then
                            If rs("create_by").Value Is System.DBNull.Value Then
                                rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                                rs("create_date_time").Value = Now()
                            End If
                            rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                            rs("update_date_time").Value = Now()
                        End If
                    End If
                    rs.Filter = 0
                Next
            End If
        End Sub

        Public Sub FillFees(ByRef rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObjectCollection)
            For i As Integer = 0 To o.Count - 1
                rs.Filter = "(booking_fee_id = '{" & DirectCast(o, TikAero.DAL.BusinessObjects.Fees).GetKey(i).ToString & "}') "
                'If DirectCast(o, TikAero.DAL.BusinessObjects.Fees)(i).fee_id.ToString = "00000000-0000-0000-0000-000000000000" Then
                If rs.RecordCount = 0 Then
                    Call Add(rs, o(i))
                    If rs("create_by").Value Is System.DBNull.Value Then
                        rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                        rs("create_date_time").Value = Now()
                    End If

                    If rs("update_by").Value Is System.DBNull.Value Then
                        rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                    End If
                    rs("update_date_time").Value = Now()
                Else
                    Dim isUpdate As Boolean = False
                    Call Update(rs, o(i), isUpdate)
                    If isUpdate Then
                        If rs("create_by").Value Is System.DBNull.Value Then
                            rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                            rs("create_date_time").Value = Now()
                        End If
                        If rs("update_by").Value Is System.DBNull.Value Then
                            rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                        End If
                        rs("update_date_time").Value = CastType(rs("update_date_time").Value, rs("update_date_time").Type)
                    End If
                End If
                rs.Filter = 0
                'End If
            Next
        End Sub
        Public Sub FillServices(ByRef rs As ADODB.Recordset, ByVal o As BusinessObjects.Services)
            For i As Integer = 0 To o.Count - 1
                If o(i).passenger_segment_service_id.Equals(Guid.Empty) = False Then
                    rs.Filter = "(passenger_segment_service_id = '{" & o(i).passenger_segment_service_id.ToString & "}') "
                    If rs.RecordCount = 0 Then
                        Call Add(rs, o(i))
                        If rs("create_by").Value Is System.DBNull.Value Then
                            rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                            rs("create_date_time").Value = Now()
                        End If

                        If rs("update_by").Value Is System.DBNull.Value Then
                            rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                        End If
                        rs("update_date_time").Value = Now()
                    Else
                        Dim isUpdate As Boolean = False
                        Call Update(rs, o(i), isUpdate)
                        If isUpdate Then
                            If rs("create_by").Value Is System.DBNull.Value Then
                                rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                                rs("create_date_time").Value = Now()
                            End If
                            If rs("update_by").Value Is System.DBNull.Value Then
                                rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                            End If
                            rs("update_date_time").Value = CastType(rs("update_date_time").Value, rs("update_date_time").Type)
                        End If
                    End If
                    rs.Filter = 0
                    'End If
                End If
            Next
        End Sub
        Public Sub ClearFees(ByRef rsFees As ADODB.Recordset, ByVal o As BusinessObjects.Fees)
            'Dim bFound As Boolean
            If IsNothing(rsFees) = False Then
                If rsFees.RecordCount > 0 And IsNothing(o) = False Then
                    rsFees.Filter = 0
                    While Not rsFees.EOF
                        If rsFees.Status = 1 Then
                            'bFound = False
                            'For Each f As BusinessObjects.Fee In o
                            '    If f.fee_id.Equals(New Guid(rsFees.Fields("fee_id").Value.ToString())) Then
                            '        bFound = True
                            '        Exit For
                            '    End If
                            'Next
                            'If bFound = False Then
                            rsFees.Delete()
                            'End If
                        End If
                        rsFees.MoveNext()
                    End While
                End If
                rsFees.Filter = 0
            End If
        End Sub
        Public Sub ClearServices(ByRef rsServices As ADODB.Recordset, ByVal o As BusinessObjects.Services)
            Dim bFound As Boolean
            If IsNothing(rsServices) = False Then
                If rsServices.RecordCount > 0 And IsNothing(o) = False Then
                    rsServices.Filter = 0
                    While Not rsServices.EOF
                        If rsServices.Status = 1 Then
                            bFound = False
                            For Each s As BusinessObjects.Service In o
                                If s.passenger_segment_service_id.Equals(New Guid(rsServices.Fields("passenger_segment_service_id").Value.ToString())) Then
                                    bFound = True
                                    Exit For
                                End If
                            Next
                            If bFound = False Then
                                rsServices.Delete()
                            End If
                        End If
                        rsServices.MoveNext()
                    End While
                End If
                rsServices.Filter = 0
            End If
        End Sub

        Public Sub FillHeader(ByVal rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObject)
            Dim isUpdate As Boolean = False
            Update(rs, o, isUpdate)
            If isUpdate Then
                If rs("create_by").Value Is System.DBNull.Value Then
                    rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                    rs("create_date_time").Value = Now()
                End If
                If rs("update_by").Value Is System.DBNull.Value Then
                    rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                End If
                rs("update_date_time").Value = Now()
            End If
        End Sub
        Public Sub FillUpdatePassengerProfile(ByRef rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObjectCollection)
            If Not rs.EOF Then
                For i As Integer = 0 To o.Count - 1
                    rs.Filter = "(passenger_profile_id= '{" & DirectCast(o, TikAero.DAL.BusinessObjects.Passengers)(i).passenger_profile_id.ToString & "}') "
                    Dim isUpdate As Boolean = False
                    Call Update(rs, o(i), isUpdate)
                    If isUpdate Then
                        rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                        rs("update_date_time").Value = Now()
                    End If
                    rs.Filter = 0
                Next
            End If
        End Sub
        Public Sub UpdateHistory(ByVal rs As ADODB.Recordset)
            If rs("create_by").Value Is System.DBNull.Value Then
                rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                rs("create_date_time").Value = Now()
            End If
            rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
            rs("update_date_time").Value = Now()
        End Sub


        Public Sub FillQuote(ByRef rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObjectCollection)
            'If Not rs.EOF Then
            For i As Integer = 0 To o.Count - 1
                Call Add(rs, o(i))
            Next
            ' End If
        End Sub


        Public Sub Fill(ByRef rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObjectCollection, Optional ByVal indication As Boolean = True)
            For i As Integer = 0 To o.Count - 1
                Call Add(rs, o(i))
                If indication Then
                    If rs("create_by").Value Is System.DBNull.Value Then
                        rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                        rs("create_date_time").Value = Now()
                    End If
                    If rs("update_by").Value Is System.DBNull.Value Then
                        rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                    End If
                    rs("update_date_time").Value = Now()
                End If
            Next
        End Sub

        Public Sub FillItinerary(ByRef rs As ADODB.Recordset, ByVal o As BusinessObjects.BusinessObjectCollection)
            For i As Integer = 0 To o.Count - 1
                rs.Filter = "(booking_segment_id = '{" & DirectCast(o, TikAero.DAL.BusinessObjects.ItineraryUpdate).GetKey(i).ToString & "}') "
                If rs.RecordCount = 0 Then
                    Call Add(rs, o(i))
                    If rs("create_by").Value Is System.DBNull.Value Then
                        rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                        rs("create_date_time").Value = Now()
                    End If

                    If rs("update_by").Value Is System.DBNull.Value Then
                        rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                    End If
                    rs("update_date_time").Value = Now()
                Else
                    Dim isUpdate As Boolean = False
                    Call Update(rs, o(i), isUpdate)
                    If isUpdate Then
                        If rs("update_by").Value Is System.DBNull.Value Then
                            rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                        End If
                        rs("update_date_time").Value = Now()
                    End If
                End If
                rs.Filter = 0
                'End If
            Next
        End Sub

        Public Sub ClearNewRecordset(ByRef rs As ADODB.Recordset)
            If rs.RecordCount > 0 Then
                rs.MoveFirst()
                While Not rs.EOF
                    If rs.Status = 1 Then
                        rs.Delete()
                    End If
                    rs.MoveNext()
                End While
                rs.MoveFirst()
                rs.Filter = 0
            End If
        End Sub

        Public Function FeeChange(ByRef rs As ADODB.Recordset, ByVal o As Object) As Boolean
            Dim bChange As Boolean = False

            For i As Integer = 0 To o.Count - 1
                Dim ot As Type = o(i).GetType()
                rs.Filter = "(booking_fee_id = '{" & DirectCast(o, TikAero.DAL.BusinessObjects.Fees)(i).booking_fee_id.ToString & "}') "
                bChange = FindChange(rs, o(i), ot)
                If bChange = True Then
                    Exit For
                End If
            Next
            rs.Filter = 0
            Return bChange
        End Function

        Public Function SegmentChange(ByRef rs As ADODB.Recordset, ByVal o As Object) As Boolean
            Dim bChange As Boolean = False

            For i As Integer = 0 To o.Count - 1
                Dim ot As Type = o(i).GetType()
                rs.Filter = "(booking_segment_id = '{" & DirectCast(o, TikAero.DAL.BusinessObjects.Itinerary)(i).booking_segment_id.ToString & "}') "
                If rs.RecordCount = 0 Then
                    Call Add(rs, o(i))
                    If rs("create_by").Value Is System.DBNull.Value Then
                        rs("create_by").Value = CastType(DefaultCreateById, rs("create_by").Type)
                        rs("create_date_time").Value = Now()
                    End If
                    If rs("update_by").Value Is System.DBNull.Value Then
                        rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                    End If
                    rs("update_date_time").Value = Now()
                Else
                    bChange = FindChange(rs, o(i), ot)
                End If
                rs.Filter = 0
            Next

            Return bChange
        End Function

        Private Function FindChange(ByRef rs As ADODB.Recordset, ByVal o As Object, ByVal ot As Object) As Boolean

            Dim bChange As Boolean = False
            For j As Integer = 0 To rs.Fields.Count - 1
                Dim FieldName As String = rs.Fields(j).Name
                If Not ot.GetProperty(FieldName) Is Nothing Then
                    Dim op As PropertyInfo = ot.GetProperty(FieldName)
                    Dim oValue = CastType(op.GetValue(o, Nothing), rs(FieldName).Type)
                    Dim rsValue = CastType(rs.Fields(FieldName).Value, rs(FieldName).Type)
                    If FieldName <> "create_date_time" And FieldName <> "update_date_time" And FieldName <> "create_by" And FieldName <> "update_by" Then
                        If Not String.Compare(ClearString(oValue.ToString), ClearString(rsValue.ToString), True) = 0 Then
                            bChange = True
                            Exit For
                        End If
                    Else
                        If ClearString(rsValue.ToString) = String.Empty Then
                            bChange = True
                            Exit For
                        End If
                    End If
                End If
            Next
            Return bChange
        End Function
        Private Function ClearString(ByVal Value As String) As String

            If IsNumeric(Value) = False Then
                If Value.ToUpper = "TRUE" Then
                    Return 1
                ElseIf Value.ToUpper = "FALSE" Then
                    Return String.Empty
                Else
                    Return Value.Replace("{", "").Replace("}", "").ToUpper
                End If
            Else
                Dim dclValue As Decimal
                If Decimal.TryParse(Value, dclValue) = True Then
                    If dclValue > 0 Then
                        Return dclValue
                    Else
                        Return String.Empty
                    End If
                Else
                    Return Value
                End If                
            End If
        End Function
        Private Sub Add(ByRef rs As ADODB.Recordset, ByVal o As Object)
            Try
                Dim t As Type = o.GetType()
                rs.AddNew()
                For i As Integer = 0 To rs.Fields.Count - 1
                    Dim FieldName As String = rs.Fields(i).Name
                    If Not t.GetProperty(FieldName) Is Nothing Then
                        Dim pi As PropertyInfo = t.GetProperty(FieldName)
                        Try
                            rs.Fields(i).Value = CastType(pi.GetValue(o, Nothing), rs(i).Type)
                        Catch e As Exception

                        End Try
                    End If
                Next
            Catch ex As Exception

            End Try
        End Sub

        Private Sub Update(ByVal rs As ADODB.Recordset, ByVal o As Object, ByVal d As Object, ByRef isUpdate As Boolean)
            Dim ot As Type = o.GetType()
            Dim dt As Type = d.GetType()
            isUpdate = False
            For i As Integer = 0 To rs.Fields.Count - 1
                Dim FieldName As String = rs.Fields(i).Name
                If Not ot.GetProperty(FieldName) Is Nothing Then
                    Dim op As PropertyInfo = ot.GetProperty(FieldName)
                    Dim dp As PropertyInfo = dt.GetProperty(FieldName)
                    Dim oValue = CastType(op.GetValue(o, Nothing), rs(i).Type)
                    Dim dValue = CastType(dp.GetValue(d, Nothing), rs(i).Type)
                    If Not oValue.ToString = dValue.ToString Then
                        Try
                            If FieldName.ToLower = "create_by" Then
                                rs.Fields(i).Value = dValue
                            Else
                                rs.Fields(i).Value = CastType(op.GetValue(o, Nothing), rs(i).Type)
                            End If

                            isUpdate = True
                        Catch e As Exception

                        End Try
                    End If
                End If
            Next
        End Sub

        Private Sub Update(ByRef rs As ADODB.Recordset, ByVal o As Object, ByRef isUpdate As Boolean)
            Dim ot As Type = o.GetType()
            isUpdate = False
            For i As Integer = 0 To rs.Fields.Count - 1
                Dim FieldName As String = rs.Fields(i).Name
                If Not ot.GetProperty(FieldName) Is Nothing Then
                    Dim op As PropertyInfo = ot.GetProperty(FieldName)
                    If Not op.GetValue(o, Nothing) Is Nothing Then
                        Dim oValue = CastType(op.GetValue(o, Nothing), rs(FieldName).Type)
                        Dim rsValue = CastType(rs.Fields(FieldName).Value, rs(FieldName).Type)
                        If FieldName <> "create_date_time" And FieldName <> "update_date_time" And FieldName <> "create_by" And FieldName <> "update_by" Then
                            If Not String.Compare(ClearString(oValue.ToString), ClearString(rsValue.ToString), True) = 0 Then
                                Try
                                    rs.Fields(i).Value = CastType(op.GetValue(o, Nothing), rs(i).Type)
                                    'Update User that update data.
                                    rs("update_by").Value = CastType(DefaultCreateById, rs("update_by").Type)
                                    isUpdate = True
                                Catch e As Exception

                                End Try
                            End If
                        End If
                    End If
                End If
            Next
        End Sub

        Private Function CastType(ByVal o As Object, ByVal rsType As ADODB.DataTypeEnum) As Object
            Dim v As Object
            Try
                If o Is Nothing Then
                    v = System.DBNull.Value
                ElseIf o.ToString.Length = 0 Then
                    v = System.DBNull.Value
                ElseIf rsType = ADODB.DataTypeEnum.adGUID Then
                    If Not o.ToString = "00000000-0000-0000-0000-000000000000" Then
                        v = "{" & o.ToString & "}"
                    Else
                        v = System.DBNull.Value
                    End If
                ElseIf rsType = ADODB.DataTypeEnum.adDBTimeStamp Or rsType = ADODB.DataTypeEnum.adDBDate Then
                    v = Format(o, "yyyy/MM/dd HH:mm:ss")
                    If v = "0001/01/01 00:00:00" Then
                        v = System.DBNull.Value
                    End If
                ElseIf rsType = ADODB.DataTypeEnum.adInteger Then
                    v = o
                Else
                    v = o.ToString
                End If
            Catch ex As Exception
                v = System.DBNull.Value
            End Try
            Return v
        End Function

        Private Function FormatYYYYYMMDDtoDate(ByVal s As String) As Date
            Try
                Return System.Convert.ToDateTime(SqlDateTime.Parse(s).ToString)
            Catch ex As Exception
                Return Nothing
            End Try
        End Function
    End Class
End Namespace
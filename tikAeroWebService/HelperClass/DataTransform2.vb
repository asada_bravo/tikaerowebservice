Imports System
Imports System.Web
Imports System.Xml
Imports System.Xml.Xsl
Imports System.Xml.XPath
Imports System.Data
Imports System.IO


Namespace Helper
    Public Class DataTransform
        Public Enum DataSetExportType
            XML
            ExcelXML
            TabDelimited
            CommaDelimited
            ADORecordSet
        End Enum

        Public Shared XSLTPaths As String = System.Configuration.ConfigurationManager.AppSettings("XSLT")
        Public Shared XSLTPath As String = "C:\DEVELOPMENT\tikAeroWebservices\XSLT\"

        Public Shared Function GetADORecordSet(ByVal ds As DataSet) As String
            Dim transformedDataSet As String = DataTransformXSL.TransformToString(ds.GetXml, XSLTPath + "ADORecordset.xsl", True)
            Dim aMemStr As System.IO.MemoryStream = New System.IO.MemoryStream
            Dim xwriter As XmlTextWriter = New XmlTextWriter(aMemStr, System.Text.Encoding.Default)
            WriteADONamespaces(xwriter)
            WriteSchemaElement(ds, ds.Tables(0).TableName, xwriter)
            RewriteFullElements(xwriter, transformedDataSet)
            xwriter.Flush()
            xwriter.Close()
            Dim strXml As String = System.Text.Encoding.UTF8.GetString(aMemStr.ToArray)
            Return strXml
        End Function

        Private Shared Sub RewriteFullElements(ByRef wrt As XmlTextWriter, ByVal ADOXmlString As String)
            Dim rdr As XmlTextReader = New XmlTextReader(ADOXmlString, XmlNodeType.Document, Nothing)
            Dim outStream As MemoryStream = New MemoryStream
            rdr.MoveToContent()
            While Not (rdr.ReadState = ReadState.EndOfFile)
                If rdr.Name = "s:Schema" Then
                    wrt.WriteNode(rdr, False)
                    wrt.Flush()
                Else
                    If rdr.Name = "z:row" AndAlso rdr.NodeType = XmlNodeType.Element Then
                        wrt.WriteStartElement("z", "row", "#RowsetSchema")
                        rdr.MoveToFirstAttribute()
                        wrt.WriteAttributes(rdr, True)
                        wrt.Flush()
                    Else
                        If rdr.Name = "z:row" AndAlso rdr.NodeType = XmlNodeType.EndElement Then
                            wrt.WriteEndElement()
                            wrt.Flush()
                        Else
                            If rdr.Name = "rs:data" AndAlso rdr.NodeType = XmlNodeType.Element Then
                                wrt.WriteStartElement("rs", "data", "urn:schemas-microsoft-com:rowset")
                            Else
                                If rdr.Name = "rs:data" AndAlso rdr.NodeType = XmlNodeType.EndElement Then
                                    wrt.WriteEndElement()
                                    wrt.Flush()
                                End If
                            End If
                        End If
                    End If
                End If
                rdr.Read()
            End While
            wrt.WriteEndElement()
            wrt.Flush()
        End Sub

        Private Shared Sub WriteADONamespaces(ByRef writer As XmlTextWriter)
            writer.WriteProcessingInstruction("xml", "version='1.0' encoding='ISO-8859-1'")
            writer.WriteStartElement("", "xml", "")
            writer.WriteAttributeString("xmlns", "s", Nothing, "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882")
            writer.WriteAttributeString("xmlns", "dt", Nothing, "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882")
            writer.WriteAttributeString("xmlns", "rs", Nothing, "urn:schemas-microsoft-com:rowset")
            writer.WriteAttributeString("xmlns", "z", Nothing, "#RowsetSchema")
            writer.Flush()
        End Sub

        Private Shared Sub WriteSchemaElement(ByVal ds As DataSet, ByVal dbname As String, ByRef writer As XmlTextWriter)
            writer.WriteStartElement("s", "Schema", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882")
            writer.WriteAttributeString("id", "RowsetSchema")
            writer.WriteStartElement("s", "ElementType", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882")
            writer.WriteAttributeString("name", "", "row")
            writer.WriteAttributeString("content", "", "eltOnly")
            writer.WriteAttributeString("rs", "updatable", "urn:schemas-microsoft-com:rowset", "true")
            WriteSchema(ds, dbname, writer)
            writer.WriteFullEndElement()
            writer.WriteFullEndElement()
            writer.Flush()
        End Sub

        Private Shared Sub WriteSchema(ByVal ds As DataSet, ByVal dbname As String, ByRef writer As XmlTextWriter)
            Dim i As Int32 = 1
            For Each dc As DataColumn In ds.Tables(0).Columns
                dc.ColumnMapping = MappingType.Attribute
                writer.WriteStartElement("s", "AttributeType", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882")
                writer.WriteAttributeString("name", "", XmlConvert.EncodeName(dc.ColumnName))
                writer.WriteAttributeString("rs", "number", "urn:schemas-microsoft-com:rowset", i.ToString)
                writer.WriteStartElement("s", "datatype", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882")
                Select Case dc.DataType.ToString
                    Case ("System.String")
                        writer.WriteAttributeString("dt", "type", "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882", "string")
                        writer.WriteAttributeString("dt", "maxlength", "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882", "255")
                        writer.WriteAttributeString("rs", "maybenull", "urn:schemas-microsoft-com:rowset", dc.AllowDBNull.ToString)
                        ' break
                    Case ("System.Int16"), ("System.Int32")
                        writer.WriteAttributeString("dt", "type", "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882", "number")
                        writer.WriteAttributeString("rs", "dbtype", "urn:schemas-microsoft-com:rowset", "numeric")
                        writer.WriteAttributeString("dt", "maxlength", "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882", "19")
                        writer.WriteAttributeString("rs", "scale", "urn:schemas-microsoft-com:rowset", "0")
                        writer.WriteAttributeString("rs", "precision", "urn:schemas-microsoft-com:rowset", "9")
                        writer.WriteAttributeString("rs", "fixedlength", "urn:schemas-microsoft-com:rowset", "true")
                        writer.WriteAttributeString("rs", "maybenull", "urn:schemas-microsoft-com:rowset", dc.AllowDBNull.ToString)
                        ' break
                    Case ("System.DateTime")
                        writer.WriteAttributeString("dt", "type", "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882", "dateTime")
                        writer.WriteAttributeString("rs", "dbtype", "urn:schemas-microsoft-com:rowset", "variantdate")
                        writer.WriteAttributeString("dt", "maxlength", "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882", "16")
                        writer.WriteAttributeString("rs", "fixedlength", "urn:schemas-microsoft-com:rowset", dc.AllowDBNull.ToString)
                        ' break
                End Select
                writer.WriteEndElement()
                writer.WriteEndElement()
                writer.Flush()
                System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
            Next
            writer.WriteStartElement("s", "AttributeType", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882")
            writer.WriteAttributeString("name", "", "TotalCount")
            writer.WriteAttributeString("rs", "number", "urn:schemas-microsoft-com:rowset", i.ToString)
            writer.WriteStartElement("s", "datatype", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882")
            writer.WriteAttributeString("dt", "type", "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882", "number")
            writer.WriteAttributeString("rs", "dbtype", "urn:schemas-microsoft-com:rowset", "numeric")
            writer.WriteAttributeString("dt", "maxlength", "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882", "19")
            writer.WriteAttributeString("rs", "scale", "urn:schemas-microsoft-com:rowset", "0")
            writer.WriteAttributeString("rs", "precision", "urn:schemas-microsoft-com:rowset", "9")
            writer.WriteAttributeString("rs", "fixedlength", "urn:schemas-microsoft-com:rowset", "true")
            writer.WriteAttributeString("rs", "maybenull", "urn:schemas-microsoft-com:rowset", "false")
            writer.WriteEndElement()
            writer.WriteEndElement()
            writer.Flush()
        End Sub

        Private Shared Function GetDatatype(ByVal dtype As String) As String
            Select Case dtype
                Case "System.Int32", "System.Int16"
                    Return "int"
                Case "System.DateTime"
                    Return "dateTime"
                Case Else
                    Return "string"
            End Select
        End Function

        Public Shared Sub ExportDataSet(ByVal ds As DataSet, ByVal fileName As String, ByVal type As DataSetExportType, ByRef XML As String)
            Dim dv As DataView = New DataView
            Select Case type
                'Case DataSetExportType.ExcelXML
                '    context.Response.ClearHeaders()
                '    context.Response.ContentType = "Application/x-msexcel"
                '    context.Response.AddHeader("content-disposition", "attachment; filename=" + fileName + ".xls")
                '    context.Response.Output.Write(DataTransformXSL.TransformToString(ds.GetXml, context.Request.MapPath(XSLTPath) + type.ToString + ".xsl", False))
                '    ' break
                'Case DataSetExportType.CommaDelimited
                '    context.Response.ClearHeaders()
                '    context.Response.ContentType = "application/vnd.ms-excel"
                '    context.Response.AddHeader("content-disposition", "attachment; filename=" + fileName + ".csv")
                '    context.Response.Output.Write(DataTransformXSL.TransformToString(ds.GetXml, context.Request.MapPath(XSLTPath) + type.ToString + ".xsl", True))
                '    ' break
                'Case DataSetExportType.TabDelimited
                '    context.Response.ClearHeaders()
                '    context.Response.ContentType = "Application/x-msexcel"
                '    context.Response.AddHeader("content-disposition", "attachment; filename=" + fileName + ".txt")
                '    context.Response.Output.Write(DataTransformXSL.TransformToString(ds.GetXml, context.Request.MapPath(XSLTPath) + type.ToString + ".xsl", True))
                '    ' break
                'Case DataSetExportType.XML
                '    context.Response.ClearHeaders()
                '    context.Response.ContentType = "text/xml"
                '    context.Response.AddHeader("content-disposition", "attachment; filename=" + fileName + ".xml")
                '    context.Response.Output.Write(ds.GetXml)
                '    ' break
            Case DataSetExportType.ADORecordSet
                    'context.Response.ClearHeaders()
                    'context.Response.ContentType = "text/xml"
                    'context.Response.AddHeader("content-disposition", "attachment; filename=" + fileName + ".xml")
                    XML = GetADORecordSet(ds)
                    ' break
            End Select
        End Sub
        Public Shared Function CompressString(ByVal str As String) As String
            Dim buffer As Byte() = Encoding.UTF8.GetBytes(str)
            Using ms As New System.IO.MemoryStream()
                Using zip As New System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Compress, True)
                    zip.Write(buffer, 0, buffer.Length)
                End Using

                ms.Position = 0
                Dim outStream As New System.IO.MemoryStream()

                Dim compressed As Byte() = New Byte(ms.Length - 1) {}
                ms.Read(compressed, 0, compressed.Length)

                Dim gzBuffer As Byte() = New Byte(compressed.Length + 3) {}
                System.Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length)
                System.Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gzBuffer, 0, 4)
                Return Convert.ToBase64String(gzBuffer)
            End Using
        End Function

        Public Shared Function DecompressString(ByVal compressedText As String) As String
            Dim gzBuffer As Byte() = Convert.FromBase64String(compressedText)
            Using ms As New System.IO.MemoryStream()
                Dim msgLength As Integer = BitConverter.ToInt32(gzBuffer, 0)
                ms.Write(gzBuffer, 4, gzBuffer.Length - 4)

                Dim buffer As Byte() = New Byte(msgLength - 1) {}

                ms.Position = 0
                Using zip As New System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Decompress)
                    zip.Read(buffer, 0, buffer.Length)
                End Using

                Return Encoding.UTF8.GetString(buffer)
            End Using
        End Function
    End Class
End Namespace

Imports System.IO
Imports System.Data
Imports System.Xml
Imports System.Xml.XPath
'Imports System.Xml.Xsl
Imports System.Xml.xsl

Namespace Helper
    Public Class DataTransformXSL
        Public Shared Function TransformToString(ByVal xmlData As String, ByVal xslFilePath As String) As String
            Return TransformToString(xmlData, xslFilePath, True)
        End Function

        Public Shared Function TransformToString(ByVal xmlData As String, ByVal xslFilePath As String, ByVal OmitXMLDeclartion As Boolean) As String
            'Dim xslt As XslTransform = New XslTransform
            Dim xslt As XslCompiledTransform = New XslCompiledTransform
            xslt.Load(xslFilePath)
            Dim xr As XmlTextReader = New XmlTextReader(New StringReader(xmlData))
            'Dim mydata As XPathDocument = New XPathDocument(xr)
            Dim aMemStr As System.IO.MemoryStream = New System.IO.MemoryStream
            Dim writer As XmlWriter = New XmlTextWriter(aMemStr, Nothing)

            'xslt.Transform(mydata, Nothing, writer, Nothing)
            xslt.Transform(xr, Nothing, writer, Nothing)
            writer.Close()
            Dim strXml As String = System.Text.Encoding.UTF8.GetString(aMemStr.ToArray)
            Return Microsoft.VisualBasic.IIf((OmitXMLDeclartion), strXml, "<?xml version=""1.0"" encoding=""utf-8""?>" + strXml)
        End Function

        Public Shared Function TransformToMemoryStream(ByVal xmlData As String, ByVal xslFilePath As String) As MemoryStream
            Dim xslt As XslCompiledTransform = New XslCompiledTransform
            xslt.Load(xslFilePath)
            Dim xr As XmlTextReader = New XmlTextReader(New StringReader(xmlData))
            'Dim mydata As XPathDocument = New XPathDocument(xr)
            Dim aMemStr As System.IO.MemoryStream = New System.IO.MemoryStream
            Dim writer As XmlWriter = XmlWriter.Create(aMemStr)

            'xslt.Transform(mydata, Nothing, aMemStr, Nothing)
            xslt.Transform(xr, Nothing, writer, Nothing)
            Return aMemStr
        End Function

        Public Shared Function TransformDataSet(ByVal ds As DataSet, ByVal xstFilePath As String) As System.IO.MemoryStream
            Dim instream As MemoryStream = New MemoryStream
            Dim outstream As MemoryStream = New MemoryStream
            ds.WriteXml(instream, XmlWriteMode.IgnoreSchema)
            instream.Position = 0
            Dim xslt As XslCompiledTransform = New XslCompiledTransform
            xslt.Load(xstFilePath)
            Dim xmltr As XmlTextReader = New XmlTextReader(instream)
            Dim xpathdoc As XPathDocument = New XPathDocument(xmltr)
            Dim nav As XPathNavigator = xpathdoc.CreateNavigator

            Dim xslArg As XsltArgumentList = New XsltArgumentList
            xslArg.AddParam("tablename", "", ds.Tables(0).TableName)

            Dim writer As XmlWriter = XmlWriter.Create(outstream)
            'xslt.Transform(nav, xslArg, outstream, Nothing)
            xslt.Transform(xmltr, xslArg, writer, Nothing)
            Return outstream
        End Function

        Public Shared Sub TransformToFile(ByVal xmlData As String, ByVal xslFilePath As String, ByVal resultFileName As String)
            Dim xslt As XslCompiledTransform = New XslCompiledTransform
            xslt.Load(xslFilePath)
            Dim xr As XmlTextReader = New XmlTextReader(New StringReader(xmlData))
            'Dim mydata As XPathDocument = New XPathDocument(xr)
            Dim writer As XmlWriter = New XmlTextWriter(resultFileName, System.Text.Encoding.UTF8)
            'xslt.Transform(mydata, Nothing, writer, Nothing)
            xslt.Transform(xr, Nothing, writer, Nothing)
            writer.Close()

        End Sub
    End Class
End Namespace


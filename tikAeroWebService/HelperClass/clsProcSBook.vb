Imports System.Threading
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.Serialization

Public Class clsProcSBook

    Private objXMLResponse As New XmlDocument
    Private objXMLRequest As New XmlDocument

    Private strXMLPath As String
    Private strXSDPath As String

    Private objRequestCBook As New DataSet

    Private strAgencyCode As String
    Private strBookingID As String

    Private rsHeader As ADODB.Recordset
    Private rsSegment As ADODB.Recordset
    Private rsPassenger As ADODB.Recordset
    Private rsRemark As ADODB.Recordset
    Private rsPayment As ADODB.Recordset
    Private rsMapping As ADODB.Recordset

    Public WriteOnly Property PathXMLLocation() As String
        Set(ByVal Value As String)
            strXMLPath = Value
        End Set
    End Property

    Public WriteOnly Property PathXSDLocation() As String
        Set(ByVal Value As String)
            strXSDPath = Value
        End Set
    End Property

    Public WriteOnly Property AgencyCode() As String
        Set(ByVal Value As String)
            strAgencyCode = Value
        End Set
    End Property

    Public ReadOnly Property XMLResponse() As XmlDocument
        Get
            Return objXMLResponse
        End Get
    End Property

    Public ReadOnly Property BookingID() As String
        Get
            Return strBookingID
        End Get
    End Property

    Private Function CnvStrToDate(ByVal objIn As Object) As Date
        Dim dtDate As Date
        Try
            If IsDBNull(objIn) = True Then
                dtDate = #12/30/1899#
            Else
                If IsDate(objIn) = True Then
                    dtDate = CType(objIn, Date)
                Else
                    dtDate = Date.Parse(objIn & "")
                End If
                If dtDate = #12:00:00 AM# Then
                    dtDate = #12/30/1899#
                End If
            End If
        Catch
            dtDate = #12/30/1899#
        End Try
        Return dtDate
    End Function

    Public WriteOnly Property XMLRequest() As XmlDocument
        Set(ByVal objXML As XmlDocument)
            objXMLRequest = objXML
            Try
                objRequestCBook.ReadXmlSchema(strXSDPath & "xmlCreateBook.xsd")

                Dim strm As System.IO.MemoryStream = New System.IO.MemoryStream
                objXMLRequest.Save(strm)
                strm.Position = 0
                Dim tr As XmlTextReader = New XmlTextReader(strm)

                objRequestCBook.ReadXml(tr, XmlReadMode.Auto)
                If objRequestCBook.Tables("BookingHeader") Is Nothing Then
                Else
                    If objRequestCBook.Tables("BookingHeader").Rows.Count > 0 Then
                        strBookingID = objRequestCBook.Tables("BookingHeader").Rows(0).Item("BookingID")
                    End If
                End If
            Catch
            End Try
        End Set
    End Property

    Private Function CnvToGuid(ByVal objIn As Object) As String
        Dim objGuid As Guid
        Dim strOut As String = ""
        Try
            Dim strIn As String = objIn & ""
            objGuid = New Guid(strIn)
            strOut = objGuid.ToString
        Catch
        End Try
        Return strOut
    End Function

    Private Function ReturnBookingRecordset() As String

        Dim strReturn As String = ""

        Try
            Dim objDataSet As New DataSet
            Dim objRow As DataRow = Nothing

            objDataSet.ReadXmlSchema(strXSDPath & "xmlCreateBook.xsd")
            If objXMLResponse.DocumentElement Is Nothing Then
                objXMLResponse.Load(strXMLPath & "xmlCreateBook.xml")
            End If
            Dim strm As System.IO.MemoryStream = New System.IO.MemoryStream
            objXMLResponse.Save(strm)
            strm.Position = 0
            Dim tr As XmlTextReader = New XmlTextReader(strm)

            objDataSet.ReadXml(tr, XmlReadMode.Auto)

            Dim iHeaderID As Integer = -1

            With objDataSet.Tables("BookingHeader")
                If .Rows.Count > 0 Then
                    .Rows.Clear()
                End If
                Dim objNewRow As DataRow
                objNewRow = .NewRow
                objNewRow.Item("BookingID") = CnvToGuid(rsHeader.Fields("booking_id").Value)
                objNewRow.Item("AgencyCode") = rsHeader.Fields("agency_code").Value & ""
                objNewRow.Item("NumberAdult") = rsHeader.Fields("number_of_adults").Value & ""
                objNewRow.Item("NumberChildren") = rsHeader.Fields("number_of_children").Value & ""
                objNewRow.Item("NumberInfant") = rsHeader.Fields("number_of_infants").Value & ""
                objNewRow.Item("ContactName") = rsHeader.Fields("contact_name").Value & ""
                objNewRow.Item("PhoneHome") = rsHeader.Fields("phone_home").Value & ""
                objNewRow.Item("PhoneBusiness") = rsHeader.Fields("phone_business").Value & ""
                objNewRow.Item("PhoneMobile") = rsHeader.Fields("phone_mobile").Value & ""
                objNewRow.Item("ContactEmail") = rsHeader.Fields("contact_email").Value & ""
                objNewRow.Item("LanguageRCD") = rsHeader.Fields("language_rcd").Value & ""
                objNewRow.Item("ReceiveFrom") = rsHeader.Fields("received_from").Value & ""
                objNewRow.Item("NotifyByEmailFlag") = rsHeader.Fields("notify_by_email_flag").Value & ""
                objNewRow.Item("NotifyBySMSFlag") = rsHeader.Fields("notify_by_sms_flag").Value & ""
                .Rows.Add(objNewRow)
                iHeaderID = .Rows(0).Item("BookingHeader_Id")
            End With

            With objDataSet.Tables("Passenger")
                If .Rows.Count > 0 Then
                    .Rows.Clear()
                End If
                'Dim iRow As Integer
                Do While rsPassenger.EOF = False
                    Dim objNewRow As DataRow
                    objNewRow = .NewRow
                    objNewRow.Item("PassengerID") = CnvToGuid(rsPassenger.Fields("passenger_id").Value)
                    objNewRow.Item("PassengerTypeRCD") = rsPassenger.Fields("passenger_type_rcd").Value & ""
                    objNewRow.Item("LastName") = rsPassenger.Fields("lastname").Value & ""
                    objNewRow.Item("FirstName") = rsPassenger.Fields("firstname").Value & ""
                    objNewRow.Item("TitleRCD") = rsPassenger.Fields("title_rcd").Value & ""
                    objNewRow.Item("GenderTypeRCD") = rsPassenger.Fields("gender_type_rcd").Value & ""
                    objNewRow.Item("DateOfBirth") = CnvStrToDate(rsPassenger.Fields("date_of_birth").Value).ToString("dd MMM yyyy")
                    objNewRow.Item("BookingHeader_Id") = iHeaderID
                    .Rows.Add(objNewRow)
                    rsPassenger.MoveNext()
                Loop
            End With

            With objDataSet.Tables("BookingSegment")
                If .Rows.Count > 0 Then
                    .Rows.Clear()
                End If
                'Dim iRow As Integer
                Do While rsSegment.EOF = False
                    Dim objNewRow As DataRow
                    objNewRow = .NewRow
                    objNewRow.Item("BookingHeader_Id") = iHeaderID
                    objNewRow.Item("BookingSegmentID") = CnvToGuid(rsSegment.Fields("booking_segment_id").Value)
                    objNewRow.Item("AirlineRCD") = rsSegment.Fields("airline_rcd").Value & ""
                    objNewRow.Item("FlightNumber") = rsSegment.Fields("flight_number").Value & ""
                    objNewRow.Item("DepartureDate") = CnvStrToDate(rsSegment.Fields("departure_date").Value).ToString("dd MMM yyyy")
                    objNewRow.Item("BookingClassRCD") = rsSegment.Fields("booking_class_rcd").Value & ""
                    objNewRow.Item("DepartureTime") = rsSegment.Fields("departure_time").Value & ""
                    objNewRow.Item("SegmentStatusRCD") = rsSegment.Fields("segment_status_rcd").Value & ""
                    objNewRow.Item("NumberOfUnits") = rsSegment.Fields("number_of_units").Value & ""
                    objNewRow.Item("OriginRCD") = rsSegment.Fields("origin_rcd").Value & ""
                    objNewRow.Item("DestinationRCD") = rsSegment.Fields("destination_rcd").Value & ""
                    .Rows.Add(objNewRow)
                    rsSegment.MoveNext()
                Loop
            End With

            With objDataSet.Tables("BookingRemark")
                If .Rows.Count > 0 Then
                    .Rows.Clear()
                End If
                'Dim iRow As Integer
                Do While rsRemark.EOF = False
                    Dim objNewRow As DataRow
                    objNewRow = .NewRow
                    objNewRow.Item("BookingHeader_Id") = iHeaderID
                    objNewRow.Item("BookingRemarkID") = CnvToGuid(rsRemark.Fields("booking_remark_id").Value)
                    objNewRow.Item("RemarkTypeRCD") = rsRemark.Fields("remark_type_rcd").Value & ""
                    objNewRow.Item("TimelimitDateTime") = CnvStrToDate(rsRemark.Fields("timelimit_date_time").Value).ToString("dd MMM yyyy hh:mm:ss tt")
                    objNewRow.Item("CompleteFlag") = rsRemark.Fields("complete_flag").Value & ""
                    objNewRow.Item("RemarkText") = rsRemark.Fields("remark_text").Value & ""
                    objNewRow.Item("AddedBy") = rsRemark.Fields("added_by").Value & ""
                    .Rows.Add(objNewRow)
                    rsRemark.MoveNext()
                Loop
            End With

            With objDataSet.Tables("PassengerSegmentMapping")
                If .Rows.Count > 0 Then
                    .Rows.Clear()
                End If
                'Dim iRow As Integer
                Do While rsMapping.EOF = False
                    Dim objNewRow As DataRow
                    objNewRow = .NewRow
                    objNewRow.Item("BookingHeader_Id") = iHeaderID
                    objNewRow.Item("PassengerID") = CnvToGuid(rsMapping.Fields("passenger_id").Value)
                    objNewRow.Item("BookingSegmentID") = CnvToGuid(rsMapping.Fields("booking_segment_id").Value)
                    objNewRow.Item("FareID") = CnvToGuid(rsMapping.Fields("fare_id").Value)
                    objNewRow.Item("FareCode") = rsMapping.Fields("fare_code").Value & ""
                    objNewRow.Item("RefundableFlag") = rsMapping.Fields("refundable_flag").Value & ""
                    objNewRow.Item("ETicketFlag") = rsMapping.Fields("e_ticket_flag").Value & ""
                    objNewRow.Item("OriginRCD") = rsMapping.Fields("origin_rcd").Value & ""
                    objNewRow.Item("DestinationRCD") = rsMapping.Fields("destination_rcd").Value & ""
                    objNewRow.Item("NetTotal") = rsMapping.Fields("net_total").Value & ""
                    objNewRow.Item("TaxAmount") = rsMapping.Fields("tax_amount").Value & ""
                    objNewRow.Item("FareAmount") = rsMapping.Fields("fare_amount").Value & ""
                    objNewRow.Item("YqAmount") = rsMapping.Fields("yq_amount").Value & ""
                    objNewRow.Item("FareDateTime") = CnvStrToDate(rsMapping.Fields("fare_date_time").Value).ToString("dd MMM yyyy hh:mm:ss tt")
                    objNewRow.Item("CurrencyRCD") = rsMapping.Fields("currency_rcd").Value & ""
                    objNewRow.Item("BaggageWeight") = rsMapping.Fields("baggage_weight").Value & ""
                    objNewRow.Item("AirlineRCD") = rsMapping.Fields("airline_rcd").Value & ""
                    objNewRow.Item("TicketingFeeAmount") = rsMapping.Fields("ticketing_fee_amount").Value & ""
                    objNewRow.Item("ReservationFeeAmount") = rsMapping.Fields("reservation_fee_amount").Value & ""
                    objNewRow.Item("FlightNumber") = rsMapping.Fields("flight_number").Value & ""
                    objNewRow.Item("LastName") = rsMapping.Fields("lastname").Value & ""
                    objNewRow.Item("FirstName") = rsMapping.Fields("firstname").Value & ""
                    objNewRow.Item("TitleRCD") = rsMapping.Fields("title_rcd").Value & ""
                    objNewRow.Item("DateOfBirth") = CnvStrToDate(rsMapping.Fields("date_of_birth").Value).ToString("dd MMM yyyy")
                    objNewRow.Item("GenderTypeRCD") = rsMapping.Fields("gender_type_rcd").Value & ""
                    objNewRow.Item("PassengerTypeRCD") = rsMapping.Fields("passenger_type_rcd").Value & ""
                    objNewRow.Item("BaseTicketingFeeAmount") = rsMapping.Fields("base_ticketing_fee_amount").Value & ""
                    objNewRow.Item("DepartureDate") = CnvStrToDate(rsMapping.Fields("departure_date").Value).ToString("dd MMM yyyy")
                    objNewRow.Item("FareVat") = rsMapping.Fields("fare_vat").Value & ""
                    objNewRow.Item("TaxVat") = rsMapping.Fields("tax_vat").Value & ""
                    objNewRow.Item("YqVat") = rsMapping.Fields("yq_vat").Value & ""
                    objNewRow.Item("TicketFeeVat") = rsMapping.Fields("ticketing_fee_vat").Value & ""
                    objNewRow.Item("ReservationFeeVat") = rsMapping.Fields("reservation_fee_vat").Value & ""
                    objNewRow.Item("PassengerStatusRCD") = rsMapping.Fields("passenger_status_rcd").Value & ""
                    .Rows.Add(objNewRow)
                    rsMapping.MoveNext()
                Loop
            End With

            Dim strmBk As System.IO.MemoryStream = New System.IO.MemoryStream
            objDataSet.WriteXml(strmBk)
            strmBk.Position = 0
            Dim trBk As XmlTextReader = New XmlTextReader(strmBk)
            objXMLResponse.Load(trBk)

        Catch ex As Exception
            strReturn = ex.Message
        End Try
        Return strReturn

    End Function

    Private Function FormatGUID(ByVal strIn As String) As String
        Dim strOut As String = ""
        Try
            Dim gOut As Guid = New Guid(strIn)
            strOut = "{" & gOut.ToString & "}"
        Catch
        End Try
        Return strOut
    End Function

    Private Function DumpRecordsetToString(ByVal rs As ADODB.Recordset) As String
        On Error Resume Next
        Dim i%
        Dim rslocal As ADODB.Recordset
        Dim strRS As String = ""
        Dim iMax%

        If rs Is Nothing Then
            strRS = strRS & vbCrLf & vbCrLf & "Nothing in recordset."
            DumpRecordsetToString = strRS
            Exit Function
        End If

        strRS = vbCrLf & "=======================================" & vbCrLf & vbCrLf
        strRS = strRS & "RecordCount = " & rs.RecordCount & vbCrLf
        rslocal = rs.Clone
        rslocal.MoveFirst()
        iMax% = 1
        Do While Not rslocal.EOF And iMax% <= 50
            strRS = strRS & vbCrLf
            strRS = strRS & "Row = " & iMax% & vbCrLf
            strRS = strRS & "rs.RecordState = " & rs.State & vbCrLf
            strRS = strRS & "rs.RecordStatus = " & rs.Status & vbCrLf
            For i% = 0 To rslocal.Fields.Count - 1
                strRS = strRS & Chr(9) & rslocal.Fields(i%).Name & " = " & Trim("" & rslocal.Fields(i%).Value) & vbCrLf
            Next i%
            iMax% = iMax% + 1
            rslocal.MoveNext()
        Loop
        strRS = strRS & vbCrLf & "======================================="
        DumpRecordsetToString = strRS
    End Function

    Private Function CnvStrtoNull(ByVal strIn As Object) As Object
        Dim objOut As Object = System.DBNull.Value
        Try
            If strIn <> "" Then
                objOut = strIn
            End If
        Catch
        End Try
        Return objOut
    End Function

    Private Function UpdateBookingRecordset() As String

        Dim strResult As String = ""
        Try
            If objRequestCBook.Tables("BookingHeader") Is Nothing Then
                strResult = "No found booking header request information"
            Else
                Dim bookRows() As DataRow = objRequestCBook.Tables("BookingHeader").Select
                For Each bookHRow As DataRow In bookRows
                    Dim iHeaderID As Integer = bookHRow.Item("BookingHeader_Id")
                    rsHeader.AddNew()
                    rsHeader.Fields("booking_id").Value = FormatGUID(bookHRow.Item("BookingID"))
                    rsHeader.Fields("number_of_adults").Value = CnvStrtoNull(bookHRow.Item("NumberAdult"))
                    rsHeader.Fields("number_of_children").Value = CnvStrtoNull(bookHRow.Item("NumberChildren"))
                    rsHeader.Fields("number_of_infants").Value = CnvStrtoNull(bookHRow.Item("NumberInfant"))
                    rsHeader.Fields("language_rcd").Value = CnvStrtoNull(bookHRow.Item("LanguageRCD"))
                    rsHeader.Fields("agency_code").Value = CnvStrtoNull(bookHRow.Item("AgencyCode"))
                    rsHeader.Fields("contact_name").Value = CnvStrtoNull(bookHRow.Item("ContactName"))
                    rsHeader.Fields("contact_email").Value = CnvStrtoNull(bookHRow.Item("ContactEmail"))
                    rsHeader.Fields("phone_mobile").Value = CnvStrtoNull(bookHRow.Item("PhoneMobile"))
                    rsHeader.Fields("phone_home").Value = CnvStrtoNull(bookHRow.Item("PhoneHome"))
                    rsHeader.Fields("phone_business").Value = CnvStrtoNull(bookHRow.Item("PhoneBusiness"))
                    rsHeader.Fields("received_from").Value = CnvStrtoNull(bookHRow.Item("ReceiveFrom"))
                    rsHeader.Fields("notify_by_email_flag").Value = CnvStrtoNull(bookHRow.Item("NotifyByEmailFlag"))
                    rsHeader.Fields("notify_by_sms_flag").Value = CnvStrtoNull(bookHRow.Item("NotifyBySMSFlag"))
                    rsHeader.Update()

                    Dim PassRows() As DataRow = objRequestCBook.Tables("Passenger").Select("BookingHeader_Id = " & iHeaderID)
                    For Each PassRow As DataRow In PassRows
                        rsPassenger.AddNew()
                        rsPassenger.Fields("passenger_id").Value = FormatGUID(PassRow.Item("PassengerID"))
                        rsPassenger.Fields("booking_id").Value = FormatGUID(bookHRow.Item("BookingID"))
                        rsPassenger.Fields("passenger_type_rcd").Value = CnvStrtoNull(PassRow.Item("PassengerTypeRCD"))
                        rsPassenger.Fields("lastname").Value = CnvStrtoNull(PassRow.Item("LastName"))
                        rsPassenger.Fields("firstname").Value = CnvStrtoNull(PassRow.Item("FirstName"))
                        rsPassenger.Fields("date_of_birth").Value = CnvStrtoNull(PassRow.Item("DateOfBirth"))
                        rsPassenger.Fields("title_rcd").Value = CnvStrtoNull(PassRow.Item("TitleRCD"))
                        rsPassenger.Fields("gender_type_rcd").Value = CnvStrtoNull(PassRow.Item("GenderTypeRCD"))
                        rsPassenger.Update()
                    Next PassRow

                    Dim SegmentRows() As DataRow = objRequestCBook.Tables("BookingSegment").Select("BookingHeader_Id = " & iHeaderID)
                    For Each SegmentRow As DataRow In SegmentRows
                        rsSegment.AddNew()
                        rsSegment.Fields("booking_segment_id").Value = FormatGUID(SegmentRow.Item("BookingSegmentID"))
                        rsSegment.Fields("booking_id").Value = FormatGUID(bookHRow.Item("BookingID"))
                        rsSegment.Fields("airline_rcd").Value = CnvStrtoNull(SegmentRow.Item("AirlineRCD"))
                        rsSegment.Fields("flight_number").Value = CnvStrtoNull(SegmentRow.Item("FlightNumber"))
                        rsSegment.Fields("departure_date").Value = CnvStrtoNull(SegmentRow.Item("DepartureDate"))
                        rsSegment.Fields("booking_class_rcd").Value = CnvStrtoNull(SegmentRow.Item("BookingClassRCD"))
                        rsSegment.Fields("departure_time").Value = CnvStrtoNull(SegmentRow.Item("DepartureTime"))
                        rsSegment.Fields("segment_status_rcd").Value = CnvStrtoNull(SegmentRow.Item("SegmentStatusRCD"))
                        rsSegment.Fields("number_of_units").Value = CnvStrtoNull(SegmentRow.Item("NumberOfUnits"))
                        rsSegment.Fields("origin_rcd").Value = CnvStrtoNull(SegmentRow.Item("OriginRCD"))
                        rsSegment.Fields("destination_rcd").Value = CnvStrtoNull(SegmentRow.Item("DestinationRCD"))
                        rsSegment.Update()
                    Next SegmentRow

                    Dim RemarkRows() As DataRow = objRequestCBook.Tables("BookingRemark").Select("BookingHeader_Id = " & iHeaderID)
                    For Each RemarkRow As DataRow In RemarkRows
                        rsRemark.AddNew()
                        rsRemark.Fields("booking_remark_id").Value = FormatGUID(RemarkRow.Item("BookingRemarkID"))
                        rsRemark.Fields("booking_id").Value = FormatGUID(bookHRow.Item("BookingID"))
                        rsRemark.Fields("remark_type_rcd").Value = CnvStrtoNull(RemarkRow.Item("RemarkTypeRCD"))
                        rsRemark.Fields("timelimit_date_time").Value = CnvStrtoNull(RemarkRow.Item("TimelimitDateTime"))
                        rsRemark.Fields("complete_flag").Value = CnvStrtoNull(RemarkRow.Item("CompleteFlag"))
                        rsRemark.Fields("remark_text").Value = CnvStrtoNull(RemarkRow.Item("RemarkText"))
                        rsRemark.Fields("added_by").Value = CnvStrtoNull(RemarkRow.Item("AddedBy"))
                        rsRemark.Update()
                    Next RemarkRow

                    Dim MappingRows() As DataRow = objRequestCBook.Tables("PassengerSegmentMapping").Select("BookingHeader_Id = " & iHeaderID)
                    For Each MappingRow As DataRow In MappingRows
                        Dim PassSubRows() As DataRow = objRequestCBook.Tables("Passenger").Select("PassengerID = '" & MappingRow.Item("PassengerID") & "'")
                        If PassSubRows.Length = 0 Then
                            Throw New System.Exception("No match mapping and passenger information")
                        End If

                        rsMapping.AddNew()
                        rsMapping.Fields("passenger_id").Value = FormatGUID(MappingRow.Item("PassengerID"))
                        rsMapping.Fields("booking_segment_id").Value = FormatGUID(MappingRow.Item("BookingSegmentID"))
                        rsMapping.Fields("fare_id").Value = FormatGUID(MappingRow.Item("FareID"))
                        rsMapping.Fields("fare_code").Value = CnvStrtoNull(MappingRow.Item("FareCode"))
                        rsMapping.Fields("refundable_flag").Value = CnvStrtoNull(MappingRow.Item("RefundableFlag"))
                        rsMapping.Fields("e_ticket_flag").Value = CnvStrtoNull(MappingRow.Item("ETicketFlag"))
                        ''rsMapping.Fields("origin_rcd").Value = MappingRow.Item("OriginRCD")
                        ''rsMapping.Fields("destination_rcd").Value = MappingRow.Item("DestinationRCD")
                        rsMapping.Fields("net_total").Value = CnvStrtoNull(MappingRow.Item("NetTotal"))
                        rsMapping.Fields("tax_amount").Value = CnvStrtoNull(MappingRow.Item("TaxAmount"))
                        rsMapping.Fields("fare_amount").Value = CnvStrtoNull(MappingRow.Item("FareAmount"))
                        rsMapping.Fields("yq_amount").Value = CnvStrtoNull(MappingRow.Item("YqAmount"))
                        rsMapping.Fields("fare_date_time").Value = CnvStrtoNull(MappingRow.Item("FareDateTime"))
                        rsMapping.Fields("currency_rcd").Value = CnvStrtoNull(MappingRow.Item("CurrencyRCD"))
                        rsMapping.Fields("baggage_weight").Value = CnvStrtoNull(MappingRow.Item("BaggageWeight"))
                        rsMapping.Fields("airline_rcd").Value = CnvStrtoNull(MappingRow.Item("AirlineRCD"))
                        rsMapping.Fields("ticketing_fee_amount").Value = CnvStrtoNull(MappingRow.Item("TicketingFeeAmount"))
                        rsMapping.Fields("reservation_fee_amount").Value = CnvStrtoNull(MappingRow.Item("ReservationFeeAmount"))

                        rsMapping.Fields("flight_number").Value = CnvStrtoNull(MappingRow.Item("FlightNumber"))
                        rsMapping.Fields("lastname").Value = CnvStrtoNull(PassSubRows(0).Item("LastName"))
                        rsMapping.Fields("firstname").Value = CnvStrtoNull(PassSubRows(0).Item("FirstName"))
                        rsMapping.Fields("title_rcd").Value = CnvStrtoNull(PassSubRows(0).Item("TitleRCD"))
                        rsMapping.Fields("date_of_birth").Value = CnvStrtoNull(PassSubRows(0).Item("DateOfBirth"))
                        rsMapping.Fields("gender_type_rcd").Value = CnvStrtoNull(PassSubRows(0).Item("GenderTypeRCD"))
                        rsMapping.Fields("passenger_type_rcd").Value = CnvStrtoNull(MappingRow.Item("PassengerTypeRCD"))
                        rsMapping.Fields("base_ticketing_fee_amount").Value = CnvStrtoNull(MappingRow.Item("BaseTicketingFeeAmount"))
                        rsMapping.Fields("departure_date").Value = CnvStrtoNull(MappingRow.Item("DepartureDate"))
                        rsMapping.Fields("fare_vat").Value = CnvStrtoNull(MappingRow.Item("FareVat"))
                        rsMapping.Fields("agency_code").Value = strAgencyCode
                        rsMapping.Fields("tax_vat").Value = CnvStrtoNull(MappingRow.Item("TaxVat"))
                        rsMapping.Fields("yq_vat").Value = CnvStrtoNull(MappingRow.Item("YqVat"))
                        rsMapping.Fields("ticketing_fee_vat").Value = CnvStrtoNull(MappingRow.Item("TicketFeeVat"))
                        rsMapping.Fields("reservation_fee_vat").Value = CnvStrtoNull(MappingRow.Item("ReservationFeeVat"))
                        rsMapping.Fields("passenger_status_rcd").Value = CnvStrtoNull(MappingRow.Item("PassengerStatusRCD"))
                        rsMapping.Update()
                    Next MappingRow
                Next bookHRow
            End If
        Catch ex As Exception
            strResult = ex.Message
        End Try
        Return strResult
    End Function

    'Public Function SaveBookings() As String

    '    Dim strReturn As String = ""

    '    Try
    '        Dim bRun As Boolean = False

    '        Dim clsRunCom As New clsRunComplus
    '        bRun = clsRunCom.GetEmpty(rsHeader, rsSegment, rsPassenger, rsRemark, rsPayment, rsMapping)
    '        If bRun = False Then
    '            strReturn = "Could not create empty recordset for booking"
    '        Else
    '            strReturn = UpdateBookingRecordset()
    '            If strReturn = "" Then
    '                'Dim strAll As String
    '                'strAll += ("rsHeader recordset : " & vbCrLf & DumpRecordsetToString(rsHeader))
    '                'strAll += ("rsSegment recordset : " & vbCrLf & DumpRecordsetToString(rsSegment))
    '                'strAll += ("rsPassenger recordset : " & vbCrLf & DumpRecordsetToString(rsPassenger))
    '                'strAll += ("rsRemark recordset : " & vbCrLf & DumpRecordsetToString(rsRemark))
    '                'strAll += ("rsPayment recordset : " & vbCrLf & DumpRecordsetToString(rsPayment))
    '                'strAll += ("rsMapping recordset : " & vbCrLf & DumpRecordsetToString(rsMapping))
    '                bRun = clsRunCom.Save(rsHeader, rsSegment, rsPassenger, rsRemark, rsPayment, rsMapping)
    '                If bRun = False Then
    '                    strReturn = "Could not save booking recordset"
    '                Else
    '                    bRun = clsRunCom.Read(strBookingID, "", 0, rsHeader, rsSegment, rsPassenger, rsRemark, rsPayment, rsMapping)
    '                    If bRun = False Then
    '                        strReturn = "Failed to retrieve booking information back"
    '                    Else
    '                        strReturn = ReturnBookingRecordset()
    '                    End If
    '                End If
    '            End If
    '        End If
    '        clsRunCom = Nothing
    '    Catch ex As Exception
    '        strReturn = ex.Message
    '    End Try
    '    Return strReturn

    'End Function

    Public Sub New()
        strBookingID = ""
    End Sub

End Class

<?xml version="1.0" encoding="UTF-8"?>
<!-- This style sheet is used with the Employees.xml file exported
    from Microsoft Access to produce a listing of employee names,
    addresses, and phone numbers in Microsoft Excel.

    Used with:
        Employees.xml

    Output:
        Table
 -->
<xsl:stylesheet version="1.0"
    xmlns="urn:schemas-microsoft-com:office:spreadsheet"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxsl="urn:schemas-microsoft-com:xslt"
	xmlns:user="urn:my-scripts"
	xmlns:o="urn:schemas-microsoft-com:office:office"
	xmlns:x="urn:schemas-microsoft-com:office:excel"
	xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" >	
	<xsl:output method="xml" omit-xml-declaration="no" indent="yes" encoding="utf-8"/>	
	<xsl:template match="/">		
		<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">			
			<Styles>				
				<Style ss:ID="Default" ss:Name="Normal">					
					<Alignment ss:Vertical="Bottom"/>					
					<Borders/>					
					<Font/>					
					<Interior/>					
					<NumberFormat/>					
					<Protection/>				
				</Style>				
				<Style ss:ID="AltItem">					
					<Alignment ss:Vertical="Top" ss:WrapText="1"/>					
					<Borders>						
						<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>						
						<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>						
						<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>						
						<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"  ss:Color="#C0C0C0"/>
					</Borders>					
					<Font ss:Color="#4A3C8C" ss:Size="7"/>					
					<Interior/>				
				</Style>				
				<Style ss:ID="Header">					
					<Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>					
					<Borders>						
						<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"    ss:Color="#C0C0C0"/>						
						<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"    ss:Color="#C0C0C0"/>						
						<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"  ss:Color="#C0C0C0"/>						
						<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"  ss:Color="#C0C0C0"/>					
					</Borders>					
					<Font ss:Size="7" ss:Color="#F7F7F7" ss:Bold="1"/>					
					<Interior ss:Color="#4A3C8C" ss:Pattern="Solid"/>				
				</Style>				
				<Style ss:ID="Item">
					<Alignment ss:Vertical="Top" ss:WrapText="1"/>					
					<Borders>						
						<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>						
						<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>						
						<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>						
						<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>					
					</Borders>					
					<Font ss:Color="#4A3C8C" ss:Size="7"/>					
					<Interior ss:Color="#CCCCFF" ss:Pattern="Solid"/>				
				</Style>				
			</Styles>			
			<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">				
				<Author>Exported from VIT Web Application</Author>				
				<Created>2003-04-22T13:43:53Z</Created>				
				<Version>10.4219</Version>			
			</DocumentProperties>			
			<Worksheet ss:Name="Sheet">				
				<Table>					
					<xsl:for-each select="*/*">						
						<xsl:if test="position()=1">							
							<xsl:for-each select="*">								
								<Column ss:Width="82.5"/>							
							</xsl:for-each>						
						</xsl:if>					
					</xsl:for-each>					
					<xsl:for-each select="*/*">						
						<xsl:if test="position()=1">								
							<Row ss:Height="13.5">							
								<xsl:for-each select="*">									
									<Cell ss:StyleID="Header">										
										<Data ss:Type="String">											
											<xsl:value-of select="name(current())"/>										
										</Data>									
									</Cell>							
								</xsl:for-each>								
							</Row>						
						</xsl:if>					
					</xsl:for-each>					
					<xsl:for-each select="./*">						
						<xsl:apply-templates />					
					</xsl:for-each>				
				</Table>			
			</Worksheet>		
		</Workbook>	
	</xsl:template>	
	<xsl:template match="*">		
		<Row>			
			<xsl:choose>				
				<xsl:when test="position() mod 2 = 0">					
					<xsl:for-each select="*">						
						<Cell ss:StyleID="AltItem">							
							<Data ss:Type="String">								
								<xsl:value-of select="text()"/>							
							</Data>						
						</Cell>					
					</xsl:for-each>				
				</xsl:when>				
				<xsl:otherwise>					
					<xsl:for-each select="*">						
						<Cell ss:StyleID="Item">							
							<Data ss:Type="String">								
								<xsl:value-of select="text()"/>							
							</Data>						
						</Cell>					
					</xsl:for-each>				
				</xsl:otherwise>			
			</xsl:choose>		
		</Row>	
	</xsl:template>
</xsl:stylesheet>
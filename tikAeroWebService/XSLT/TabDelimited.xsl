<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="http://mycompany.com/mynamespace" version="1.0">
	<xsl:output method="text" omit-xml-declaration="yes" indent="yes" encoding="utf-8"/>
	<xsl:template match="/">
		<xsl:for-each select="*/*">
			<xsl:if test="position()=1">
				<xsl:for-each select="*"><xsl:value-of select="name(current())"/><xsl:if test="count(following-sibling::*) != 0">&#09;</xsl:if></xsl:for-each>
				<xsl:text>&#13;&#10;</xsl:text>	
			</xsl:if>
		</xsl:for-each>	
		<xsl:for-each select="./*">
			<xsl:apply-templates />
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="*">
		<xsl:for-each select="*"><xsl:value-of select="text()"/><xsl:if test="count(following-sibling::*) != 0">&#09;</xsl:if></xsl:for-each>
		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:template>
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<!-- 
Project: CTQ
File name: en_email_b2c.xsl
-VAT

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
	<xsl:key name="passenger_type_rcd_group" match="//Passengers/Passenger" use="passenger_type_rcd"/>
	<!--Variable-->
	<xsl:variable name="BarPDF417URL">http://www.tikaero.com/TikAeroBarcode/TikAeroBarcodePDF417.ashx?</xsl:variable>
	<xsl:variable name="BarURL">http://www.tikaero.com/TikAeroBarcode/TikAeroBarcode.ashx?valueToEncode=</xsl:variable>
	<xsl:variable name="pass_id" select="//Booking/Header/BookingHeader/booking_number"/>
	<xsl:variable name="BaseURL">http://www.tikaero.com/XSLImages/CTQ/</xsl:variable>
	<xsl:variable name="Titel">TikAero Itinerary</xsl:variable>
	<xsl:variable name="passenger_id" select="Booking/Tickets/Ticket/passenger_id"/>
	<xsl:variable name="booking_segment_id_tax" select="Booking/Tickets/Ticket[(passenger_status_rcd = 'OK')]/booking_segment_id"/>
	<xsl:variable name="booking_segment_id" select="Booking/TicketTaxes/TicketTaxes[booking_segment_id = $booking_segment_id_tax]"/>
	<xsl:variable name="sum_net_total" select="sum(Booking/Tickets/Ticket/net_total)"/>
	<xsl:variable name="sum_payment_amount" select="sum(Booking/Payments/Payment/payment_amount)"/>
	<!--Template-->

	<!-- Format String -->
	<xsl:template name="CDate2Julian">
		<xsl:param name="date"/>
		<xsl:param name="format" select="000"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="format-number(substring($date,3,2),'00')"/>
		<xsl:variable name="month" select="substring(substring-after('01/00002/03103/05904/09005/12006/15107/18108/21209/24310/27311/30412/334', concat($month1,'/')), 1, 3)"/>
		<xsl:value-of select="$day + $month"/>
	</xsl:template>
	<xsl:template name="formatdate">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month1" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,3,2)"/>
		<xsl:variable name="month" select="substring(substring-after('010102020303040405050606070708080909101011111212', $month1), 1, 2)"/>
		<xsl:value-of select="concat($day, '/', $month, '/', $year)"/>
	</xsl:template>
	<xsl:template name="format-date">
		<xsl:param name="date"/>
		<xsl:param name="format" select="0"/>
		<xsl:variable name="day" select="substring($date, 7,2)"/>
		<xsl:variable name="month" select="substring($date,5,2)"/>
		<xsl:variable name="year" select="substring($date,3,2)"/>
		<xsl:value-of select="concat($day, '.', $month, '.', $year)"/>
	</xsl:template>
	<xsl:template match="/">
		<HTML>
			<HEAD>
				<TITLE>
					<xsl:value-of select="$Titel"/>
				</TITLE>

				<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				<!--Style Sheet-->
				<STYLE>.HeaderBackground { background: url(http://www.tikaero.com/xslimages/AAS/Images/0/H/hhe.gif) fixed no-repeat left center; }
					.base { color: ; font-family: 12pt helvetica, Tahoma, Arial, sans-serif, verdana; font-size: 9pt; width: 100%; height:100%}
					.base .content{ color: ; float: left;  font-size: 9pt; }
					.dummy{ }
					.base .content .contentHeader{ margin: 5px 0px 0px 0px; padding: 0px 0px 0px 5px; }
					.base .quickSearch { float: left; width: 170px; }					
					.base .quickSearch DIV.input { font-size: 8pt; float: left; padding: 2px 4px 2px 5px; text-align: left; }
					.base .quickSearch DIV.label{ float: left; padding: 2px 4px 2px 5px; text-align: right; vertical-align: middle; }
					.BarcodeHeader	{ color: ; font-size: 7pt; font-style: normal; font-weight: bold; padding: 0px 0px 0px 12px; text-align: left; width: 100%; }
					.PanelItinerary .Barcode	{ padding: 1 5 1 5; }
					.footer A	{ height: 20px; text-align: right; width: 50%; }
					.footer	SPAN	{ height: 20px; padding-top: 3px; width: 50%; }
					.grid TD.gridItems0Button	{ padding: 1px 5px 1px 1px; text-align: right; }
					.grid TR.GridFooter TD	{ background-color: transparent; border-bottom: 1px solid  #B9CAEB; color: #666666; font: bold 8pt ; border-top: 1px solid #B9CAEB; height: 22px;  }
					.grid TR.GridFooter TD.Summary	{ background-color: transparent; }
					.grid TR.gridHeader	{background:#FF7F00;color:#ffffff;;font-size:11px;font-weight:bold;padding:4px 2px 4px 5px;}
					.grid TR.GridPrice	{ background-color: tranparent; color: ; font-size: 11px; font-weight: bold; padding: 2px 2px 2px 5px; }
					.grid TR.gridHeader TD	{ padding: 7px 2px 1px 5px; border-bottom: 1px solid #FF7F00; }
					.grid TR.gridHeaderFight { background-color: #FED684; color: ; font-size: 11px; font-weight: bold; padding: 2px 2px 2px 5px; }
					.grid TR.gridItems0 TD { border-bottom: 1px solid #B9CAEB; color: ; cursor: pointer; font-size: 11px; font-weight: normal; height: 15px; padding: 0px 1px 0px 5px; }
					.grid TR.gridItems1 TD { border-top: 1px solid  #B9CAEB; color: ; cursor: pointer; font-size: 11px; font-weight: normal; height: 15px; padding: 0px 1px 0px 5px; }
					.grid tr.griditems2 td{color:;cursor:pointer;font-size:11px;font-weight:normal;height:15px;padding:0px 1px 0px 5px;}
					.grid TR.gridItems02 TD { color:  ; cursor: pointer; font-size: 11px; font-weight: normal; height: 15px; padding: 0px 1px 0px 5px; }
					.grid TR.gridItems0 TD { color: ; cursor: pointer; font-size: 11px; font-weight: normal; height: 22px; }
					.grid TR.gridItems01 TD { color: ; cursor: pointer;  font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems0o TD{ background-color: #FFBD3B; border-bottom: 1px solid #FFCC00; color: ; cursor: pointer;  font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems01 TD	{ color: ; cursor: pointer; font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems01o TD	{ background-color: #CEE6F7; color: ; cursor: pointer; font-family: Tahoma, Arial, sans-serif, verdana; font-size: 11px; font-weight: normal; height: 22px; padding: 1px 1px 1px 5px; }
					.grid TR.gridItems0Input	{ color: ;font-size: 11px; font-weight: normal; height: 22px; padding: 5px 1px 1px 5px; }
					.header	{ width: 100%; }
					.header .subHeader	{ background-color: ; }
					/* End Forms \*/
					/* Headers \*/
					.headers	{color:#C4102F; font: bold 12pt /*background: url(../Images/0/H/hbh.jpg);*/ margin: 5px 0px 5px 5px; width: 100%; }
					.headers DIV	{ font: bold 8pt  margin: 3px 0px 3px 0px; padding: 5px 0px 5px 17px; }
					.headers DIV SPAN.S0{ color: #FF6600; }
					.headers SPAN.Button { width: 49%; }
					.Itinerary	{ color: ; font-size: 8pt; font-style: normal; padding: 10px 12px 0px 12px; text-align: left; width: 100%; }
					.Itinerary Span.O	{ color: #FF6600; font-weight: bold; }
					.Itinerary Span.1	{ color: #FF6600; font-weight: bold; text-decoration: underline; }
					.Itinerary Span.2 { color: ; font-weight: bold; }
					.welcometext,.
					.commenttext{font-family:verdana,arial,helvetica,sans-serif;font-size:12px;padding:1 10 1 5;}
					p.msonormal	{ color: #1E336C; font-family: "Verdana"; font-size: 8pt; margin-bottom: .0001pt; margin-left: 0in; margin-right: 0in; margin-top: 0in; mso-style-parent: ""; text-align: justify; }</STYLE>
				<!--End Style Sheet-->
			</HEAD>
			<BODY leftmargin="10px;" topmargin="5" rightmargin="5" bottommargin="5" marginwidth="5" marginheight="5">
				<DIV style="width:100%;height:100%">
					<DIV class="header" align="left" style="width:100%;">
						<BR/>
						<img src="{$BaseURL}CTQ_logo.jpg"/>
						<BR/>
						<BR/>
						<BR/>
						<table border="0" id="table1" width="100%" height="99" cellspacing="0" cellpadding="0">
							<tr>
								<td valign="top" width="300">
									<DIV class="headers" style="width: 250px; height: 89px;font-family: helvetica, Tahoma, Arial, sans-serif, verdana;">
										<table style="width: 100%" class="headers">
											<tr>
												<td style="width: 183px">
													<SPAN style="font-family: helvetica, Tahoma, Arial, sans-serif, verdana;font-size: 12pt;font-weight: 400">
														<font color="#000000">Your Booking:</font>
													</SPAN>
												</td>
												<td style="width: 183px">
													<xsl:value-of select="Booking/Header/BookingHeader/record_locator"/>
												</td>
											</tr>
											<tr>
												<td style="width: 183px"></td>
												<td style="width: 183px"></td>
											</tr>
											<tr>
												<td style="width: 183px">
													<SPAN style="font-family: helvetica, Tahoma, Arial, sans-serif, verdana;font-size: 10pt;font-weight: 400">
														<font color="#000000">Agency Code:</font>&#32;
													</SPAN>
												</td>
												<td style="width: 183px">
													<SPAN style="font-family: helvetica, Tahoma, Arial, sans-serif, verdana;font-size: 10pt;">
														<xsl:value-of select="Booking/Header/BookingHeader/agency_code"/>
													</SPAN>
												</td>
											</tr>
											<tr>
												<td style="width: 183px">
													<SPAN style="font-family: helvetica, Tahoma, Arial, sans-serif, verdana;font-size: 10pt;font-weight: 400">
														<font color="#000000">Booking Date:</font>&#32;
													</SPAN>
												</td>
												<td style="width: 183px">
													<SPAN style="font-family: helvetica, Tahoma, Arial, sans-serif, verdana;font-size: 10pt;">
														<xsl:call-template name="formatdate">
															<xsl:with-param name="date" select="Booking/Header/BookingHeader/create_date_time"/>
														</xsl:call-template>
													</SPAN>
												</td>
											</tr>
										</table>
										<BR/>
									</DIV>
								</td>
								<td align="center">
									<div class="style1" style="width: 274px">
										<span style="font-family: helvetica, Tahoma, Arial, sans-serif, verdana; font-size: 12pt; font-weight: 400;color:#C4102F;">
											<xsl:choose>
												<xsl:when test="//Payments/Payment = false()">
													<xsl:text>ITINERARY</xsl:text>
												</xsl:when>
												<xsl:otherwise>
													<xsl:variable name="Payment_total" select="sum(//Booking/Payments/Payment[string-length(void_by)!=38]/payment_amount)"/>
													<xsl:variable name="Ticket_total"
													              select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount_incl)"/>
													<xsl:choose>
														<xsl:when test="($Payment_total  &gt;=  $Ticket_total) ">
															<xsl:text>RECEIPT/TICKET</xsl:text>
														</xsl:when>
														<xsl:otherwise>
															<xsl:text>ITINERARY</xsl:text>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:otherwise>
											</xsl:choose>
										</span>
									</div>
								</td>
								<td align="center" valign="top" width="14%">
									<xsl:for-each select="//Booking/Itinerary/FlightSegment[not(segment_status_rcd='XX')]">
										<xsl:if test="position()=1">
											<div class="Barcode" style="height: 19px">
												<table width="100%" border="0">
													<tbody>
														<tr>
															<td align="center" width="100%">
																<img hspace="0" src="{concat($BarURL,$pass_id,'0')}" width="164" height="57"/>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</xsl:if>
									</xsl:for-each>
								</td>
							</tr>
						</table>
					</DIV>
					<DIV class="base">
						<!--Passengers-->
						<DIV class="headers">
							<SPAN style="font-size: 10pt;">Passengers&#32;</SPAN>
							<TABLE class="Grid" cellspacing="0" border="0" style="border-width:0px;width:100%;border-collapse:collapse;">
								<TR class="gridHeader">
									<TD>&#32;
									</TD>
									<TD>Lastname</TD>
									<TD>Firstname</TD>
									<TD>Title</TD>
									<TD>Type</TD>
								</TR>
								<xsl:for-each select="Booking/Passengers/Passenger">
									<xsl:variable name="date_of_birth" select="date_of_birth"/>
									<TR class="gridItems0">
										<TD>
											<xsl:choose>
												<xsl:when test="position()&gt;9">
													<xsl:value-of select="concat('0','',string(position()))"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="concat('00','',string(position()))"/>
												</xsl:otherwise>
											</xsl:choose>
										</TD>
										<TD>
											<xsl:value-of select="lastname"/>
										</TD>
										<TD>
											<xsl:value-of select="firstname"/>
										</TD>
										<TD>
											<xsl:value-of select="title_rcd"/>
										</TD>
										<TD>
											<xsl:value-of select="passenger_type_rcd"/>
										</TD>
									</TR>
								</xsl:for-each>
							</TABLE>
						</DIV>
						<br/>
						<!--End Passengers-->
						<!--Flights-->
						<DIV class="headers">
							<SPAN style="font-size: 10pt;">Flights&#32;</SPAN>
							<TABLE class="Grid" cellspacing="0" rules="rows" border="0" style="border-width:0px;width:100%;border-collapse:collapse;">
								<TR class="gridHeader">
									<TD/>
									<TD>Flight</TD>
									<TD>From</TD>
									<TD>To</TD>
									<TD>Date</TD>
									<TD>Dep</TD>
									<TD>Arr</TD>
									<TD>Class</TD>
									<TD>Status</TD>
								</TR>
								<xsl:for-each select="Booking/Itinerary/FlightSegment[segment_status_rcd != 'XX']">
									<xsl:variable name="booking_segment_id" select="booking_segment_id"/>
									<TR class="gridItems0">
										<TD>
											<xsl:value-of select="concat('00','',string(position()))"/>
										</TD>
										<TD>
											<SPAN>
												<xsl:value-of select="airline_rcd"/>&#32;
												<xsl:value-of select="flight_number"/>
											</SPAN>
										</TD>
										<TD>
											<xsl:value-of select="origin_name"/>&#32;(<xsl:value-of select="origin_rcd"/>)</TD>
										<TD>
											<xsl:value-of select="destination_name"/>&#32;(<xsl:value-of select="destination_rcd"/>)</TD>
										<TD>
											<xsl:if test="string-length(departure_date) &gt; '0'">
												<xsl:call-template name="formatdate">
													<xsl:with-param name="date" select="departure_date"/>
												</xsl:call-template>
											</xsl:if>
											<xsl:if test="string-length(departure_date) = '0'">
												<xsl:text>OPEN</xsl:text>
											</xsl:if>
										</TD>
										<TD>
											<xsl:if test="string(departure_time) != '0'">
												<xsl:value-of select="substring(format-number(number(departure_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(departure_time), '0000'),3,4)"/></xsl:if>
										</TD>
										<TD>
											<xsl:if test="string(planned_arrival_time) != '0'">
												<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),1,2)"/>:<xsl:value-of select="substring(format-number(number(planned_arrival_time), '0000'),3,4)"/></xsl:if>
										</TD>
										<TD>
											<xsl:value-of select="class_name"/>
										</TD>
										<td>
											<xsl:choose>
												<xsl:when test="flight_id != ''">
													<xsl:value-of select="status_name"/>
													<!--<xsl:value-of select="segment_status_rcd"/>-->
												</xsl:when>
												<xsl:when test="not(string-length(departure_date) &gt; '0')">
													<xsl:text>&#32;</xsl:text>
												</xsl:when>
												<xsl:otherwise>
													<xsl:text>Info</xsl:text>
												</xsl:otherwise>
											</xsl:choose>
										</td>
									</TR>
								</xsl:for-each>
							</TABLE>
						</DIV>
						<br/>
						<!--End Flights-->
						<!--Auxiliaries-->
						<xsl:for-each select="Booking/Remarks/Remark[substring(remark_type_rcd,1,3)='AUX']">
							<xsl:if test="position()=1">
								<div class="headers">
									<span style="font-size: 10pt">Auxiliaries</span>
									<table class="Grid" id="Itinerary1_grdFares" cellSpacing="0" border="0" style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; ; width: 100%; border-collapse: collapse; border-right-width: 0px">
										<tbody>
											<tr class="gridHeader">
												<td align="left">Code</td>
												<td align="left">Description</td>
											</tr>
											<xsl:for-each select="/Booking/Remarks/Remark[substring(remark_type_rcd,1,3)='AUX']">
												<tr class="gridItems0">
													<td align="left" valign="top">
														<xsl:value-of select="display_name"/>
													</td>
													<td align="left" valign="top">
														<xsl:value-of select="remark_text"/>
													</td>
												</tr>
											</xsl:for-each>
										</tbody>
									</table>
								</div>
								<br/>
							</xsl:if>
						</xsl:for-each>
						<!--End Auxiliaries-->
						<!--Seat number-->
						<xsl:for-each select="Booking/Tickets/Ticket[not(passenger_status_rcd='XX')]">
							<xsl:sort select="seat_number" order="descending"/>
							<xsl:sort select="ticket_number" order="descending"/>
							<xsl:sort select="restriction_text" order="descending"/>
							<xsl:sort select="endorsement_text" order="descending"/>
							<xsl:if test="(string-length(seat_number) &gt;0) or (string-length(ticket_number) &gt;0) or (string-length(restriction_text) &gt;0) or (string-length(endorsement_text) &gt;0)">
								<xsl:if test="position()=1">
									<div class="headers">
										<span style="font-size: 10pt">Tickets and Seats</span>
										<table class="Grid" id="Itinerary1_grdFares" cellSpacing="0" border="0"
										       style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; ; width: 100%; border-collapse: collapse; border-right-width: 0px">
											<tbody>
												<tr class="gridHeader">
													<td align="left">Flight</td>
													<td align="left">Passenger Name</td>
													<td align="left">Ticket</td>
													<td align="left">Class</td>
													<td align="left">Seat</td>
													<td align="left">Endorsement &amp; Restrictions</td>
													<!--<td align="right" width="5%">&#160;</td>-->
												</tr>
												<xsl:for-each select="/Booking/Tickets/Ticket[not(passenger_status_rcd='XX')]">
													<xsl:if test="(string-length(seat_number) &gt;0) or (string-length(ticket_number) &gt;0) or (string-length(restriction_text) &gt;0) or (string-length(endorsement_text) &gt;0)">
														<tr class="gridItems0">
															<td align="left" valign="top">
																<xsl:value-of select="airline_rcd"/>&#32;
																<xsl:value-of select="flight_number"/>
															</td>
															<td align="left" valign="top">
																<xsl:value-of select="lastname"/>/&#32;<xsl:value-of select="firstname"/></td>
															<td align="left" valign="top">
																<xsl:value-of select="ticket_number"/>
															</td>
															<td align="left" valign="top">
																<xsl:value-of select="boarding_class_rcd"/>-<xsl:value-of select="booking_class_rcd"/></td>
															<td align="left" valign="top">
																<xsl:value-of select="seat_number"/>
															</td>
															<td align="left" valign="top">
																<xsl:choose>
																	<xsl:when test="(string-length(restriction_text) &gt;0) and (string-length(endorsement_text) &gt;0)">
																		<xsl:value-of select="endorsement_text"/>
																		<br/>
																		<xsl:value-of select="restriction_text"/>
																	</xsl:when>
																	<xsl:when test="(string-length(restriction_text) &gt;0) and (string-length(endorsement_text) =0)">
																		<xsl:value-of select="restriction_text"/>
																	</xsl:when>
																	<xsl:when test="(string-length(restriction_text) =0) and (string-length(endorsement_text) &gt;0)">
																		<xsl:value-of select="endorsement_text"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:value-of select="restriction_text"/>
																	</xsl:otherwise>
																</xsl:choose>
															</td>
														</tr>
													</xsl:if>
												</xsl:for-each>
											</tbody>
										</table>
									</div>
									<br/>
								</xsl:if>
							</xsl:if>
						</xsl:for-each>
						<!--End Seat number-->
						<!--Special Services-->
						<xsl:for-each select="/Booking/SpecialServices/SpecialService[not(special_service_status_rcd='XX')]">
							<xsl:sort select="departure_date" order="descending"/>
							<xsl:if test="position()=1">
								<div class="headers">
									<span style="font-size: 10pt">Special Services&#32;</span>
									<table class="Grid" id="Itinerary1_grdFares" cellSpacing="0" border="0" style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; ; width: 100%; border-collapse: collapse; border-right-width: 0px">
										<tbody>
											<tr class="gridHeader">
												<td>&#32;
												</td>
												<td>Flight</td>
												<td>Passenger Name</td>
												<td>Status</td>
												<td>Special Service</td>
												<td>Text</td>
											</tr>
											<xsl:for-each select="/Booking/SpecialServices/SpecialService[not(special_service_status_rcd = 'XX')]">
												<tr class="gridItems0">
													<td>
														<xsl:value-of select="concat('00','',string(position()))"/>
													</td>
													<td>
														<xsl:value-of select="airline_rcd"/>&#32;
														<xsl:value-of select="flight_number"/>
													</td>
													<td>
														<xsl:value-of select="lastname"/>/&#32;<xsl:value-of select="firstname"/></td>
													<td>
														<xsl:value-of select="special_service_status_rcd"/>
													</td>
													<td>
														<xsl:value-of select="special_service_rcd"/>&#32;&#32;
														<xsl:value-of select="display_name"/>
													</td>
													<td>
														<xsl:value-of select="service_text"/>
													</td>
												</tr>
											</xsl:for-each>
										</tbody>
									</table>
								</div>
								<br/>
							</xsl:if>
						</xsl:for-each>
						<!--End Special Services-->
						<!--Quote-->
						<!--AAS, ABA, AEK, GMG, PMT, BEA, IRS, CTS -->
						<xsl:if test="//TicketQuotes/Total = true()">
							<DIV class="headers">
								<SPAN style="font-size: 10pt">Quotes</SPAN>
								<TABLE cellspacing="0" border="0" class="Grid" style="border-width:0px;width:100%;border-collapse:collapse;">
									<TR class="GridHeader">
										<TD>Passenger</TD>
										<td align="center">Units</td>
										<TD>Charge</TD>
										<td align="right">Amount</td>
										<td align="right">VAT</td>
										<td align="right">Total</td>
									</TR>
									<xsl:for-each select="//Passengers/Passenger[count(. | key('passenger_type_rcd_group', passenger_type_rcd)[1]) = 1]">
										<xsl:variable name="passenger_type_rcd" select="passenger_type_rcd"/>
										<xsl:for-each select="//TicketQuotes/Total[passenger_type_rcd=$passenger_type_rcd]">
											<xsl:variable name="TotalCharge">
												<xsl:if test="charge_type != 'REFUND'">
													<xsl:value-of select="sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/charge_amount)"/>
												</xsl:if>
												<xsl:if test="charge_type = 'REFUND'">
													<xsl:value-of select="sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/charge_amount)"/>
												</xsl:if>
											</xsl:variable>
											<xsl:if test="position()=1">
												<xsl:if test="position()!=last()">
													<tr class="gridItems1">
														<td>
															<xsl:value-of select="passenger_type_rcd"/>
														</td>
														<td align="center">
															<xsl:value-of select="sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/passenger_count)"/>
														</td>
														<td>
															<xsl:choose>
																<xsl:when test="charge_type != 'Fare'">
																	<xsl:text>Fare</xsl:text>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:value-of select="charge_name"/>
																</xsl:otherwise>
															</xsl:choose>
														</td>
														<td align="right">
															<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>
														</td>
														<td align="right">
															<xsl:if test="number(tax_amount) != 0">
																<xsl:if test="number(tax_amount) &gt;= 0">
																	<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
																</xsl:if>
															</xsl:if>
														</td>
														<td align="right">
															<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#32;&#32;
															<xsl:value-of select="currency_rcd"/>
														</td>
													</tr>
												</xsl:if>
												<xsl:if test="position()=last()">
													<tr class="gridItems1">
														<td>
															<xsl:value-of select="passenger_type_rcd"/>
														</td>
														<td align="center">
															<xsl:value-of select="passenger_count"/>
														</td>
														<td>
															<xsl:value-of select="charge_name"/>
														</td>
														<td align="right">
															<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>
														</td>
														<td align="right">
															<xsl:if test="number(tax_amount) != 0">
																<xsl:if test="number(tax_amount) &gt;= 0">
																	<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
																</xsl:if>
															</xsl:if>
														</td>
														<td align="right">
															<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type != 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#32;&#32;
															<xsl:value-of select="currency_rcd"/>
														</td>
													</tr>
												</xsl:if>
											</xsl:if>
											<xsl:if test="position()!=1">
												<xsl:if test="position()=last()">
													<xsl:if test="charge_type != 'REFUND'">
														<tr class="gridItems02">
															<td>&#32;
															</td>
															<td>&#32;
															</td>
															<td>
																<xsl:value-of select="charge_name"/>
															</td>
															<td align="right">
																<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>
															</td>
															<td align="right">
																<xsl:if test="number(tax_amount) != 0">
																	<xsl:if test="number(tax_amount) &gt;= 0">
																		<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
																	</xsl:if>
																</xsl:if>
															</td>
															<td>&#32;
															</td>
														</tr>
													</xsl:if>
													<xsl:if test="charge_type = 'REFUND'">
														<tr class="gridItems02">
															<td>&#32;
															</td>
															<td align="center">&#32;
															</td>
															<td>
																<xsl:value-of select="charge_name"/>
															</td>
															<td align="right">&#32;
															</td>
															<td align="right">&#32;
															</td>
															<td align="right">-<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#32;&#32;<xsl:value-of select="currency_rcd"/></td>
														</tr>
													</xsl:if>
												</xsl:if>
												<xsl:if test="position()!=last()">
													<xsl:if test="charge_type != 'REFUND'">
														<tr class="gridItems02">
															<td>&#32;
															</td>
															<td>&#32;
															</td>
															<td>
																<xsl:value-of select="charge_name"/>
															</td>
															<td align="right">
																<xsl:value-of select="format-number(charge_amount,'#,##0.00')"/>
															</td>
															<td align="right">
																<xsl:if test="number(tax_amount) != 0">
																	<xsl:if test="number(tax_amount) &gt;= 0">
																		<xsl:value-of select="format-number(tax_amount,'###,##0.00')"/>
																	</xsl:if>
																</xsl:if>
															</td>
															<td>&#32;
															</td>
														</tr>
													</xsl:if>
													<xsl:if test="charge_type = 'REFUND'">
														<tr class="gridItems02">
															<td>&#32;
															</td>
															<td align="center">&#32;
															</td>
															<td>
																<xsl:value-of select="charge_name"/>
															</td>
															<td align="right">&#32;
															</td>
															<td align="right">&#32;
															</td>
															<td align="right">-<xsl:value-of select="format-number($TotalCharge + sum(//TicketQuotes/Total[charge_type = 'REFUND'][passenger_type_rcd = $passenger_type_rcd]/tax_amount),'#,##0.00')"/>&#32;&#32;<xsl:value-of select="currency_rcd"/></td>
														</tr>
													</xsl:if>
												</xsl:if>
											</xsl:if>
										</xsl:for-each>
									</xsl:for-each>
									<xsl:for-each select="//Fees/Fee[void_date_time = '']">
										<xsl:if test="position()=1">
											<tr class="gridItems1">
												<td>&#32;
												</td>
												<td>&#32;
												</td>
												<td>
													<xsl:value-of select="display_name"/>
												</td>
												<td align="right">
													<xsl:value-of select="format-number(fee_amount,'#,##0.00')"/>
												</td>
												<td align="right">
													<xsl:choose>
														<xsl:when test="format-number(fee_amount_incl - fee_amount,'0.00') = 0">&#32;
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="format-number(fee_amount_incl - fee_amount,'#,##0.00')"/>
														</xsl:otherwise>
													</xsl:choose>
												</td>
												<td align="right">
													<xsl:value-of select="format-number(sum(//Fees/Fee[void_date_time = '']/fee_amount_incl),'#,##0.00')"/>&#32;&#32;
													<xsl:value-of select="currency_rcd"/>
												</td>
											</tr>
										</xsl:if>
										<xsl:if test="position()!=1">
											<tr class="gridItems02">
												<td>&#32;
												</td>
												<td>&#32;
												</td>
												<td>
													<xsl:value-of select="display_name"/>
												</td>
												<td align="right">
													<xsl:value-of select="format-number(fee_amount,'#,##0.00')"/>
												</td>
												<td align="right">
													<xsl:choose>
														<xsl:when test="format-number(fee_amount_incl - fee_amount,'0.00') = 0">&#32;
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="format-number(fee_amount_incl - fee_amount,'#,##0.00')"/>
														</xsl:otherwise>
													</xsl:choose>
												</td>
												<td>&#32;
												</td>
											</tr>
										</xsl:if>
									</xsl:for-each>
									<tr class="GridFooter">
										<td>&#32;
										</td>
										<td>&#32;
										</td>
										<td>&#32;
										</td>
										<td>&#32;
										</td>
										<td class="Summary" align="right">
											<span>Total</span>
										</td>
										<td align="right">
											<SPAN class="FooterTotalLabel">
												<xsl:value-of select="format-number((sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount_incl),'#,##0.00')"/>&#32;&#32;
												<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
											</SPAN>
										</td>
									</tr>
								</TABLE>
							</DIV>
						</xsl:if>
						<!--End Quote-->
						<!--Payments-->
						<xsl:if test="//Payments/Payment = true()">
							<br/>
							<div class="headers">
								<span style="font-size: 10pt">Payments</span>
								<table class="Grid" width="100%" border="0" ellspacing="0" style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; ; border-collapse: collapse; border-right-width: 0px">
									<tbody>
										<tr class="gridHeader">
											<td align="left" width="30%">
												<span class="gridHeader">Description</span>
											</td>
											<td align="left" width="30%">
												<span class="gridHeader">&#32;
												</span>
											</td>
											<td align="left" width="12%">
												<span class="GridHeader">Status, Date</span>
											</td>
											<td align="right" width="15%">
												<span class="gridHeader">Credit</span>
											</td>
											<td align="right" width="15%">
												<span class="gridHeader">Debit</span>
											</td>
										</tr>
										<xsl:variable name="Payment_total" select="sum(//Booking/Payments/Payment[string-length(void_by)!=38]/payment_amount)"/>
										<xsl:variable name="Ticket_total"
										              select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount_incl)"/>
										<xsl:choose>
											<xsl:when test="($Payment_total  &gt;=  $Ticket_total) ">
												<tr class="gridItems0" vAlign="middle" align="left">
													<TD align="left">Ticket Cost &amp; Fee</TD>
													<TD align="left">&#32;
													</TD>
													<TD align="left">&#32;
													</TD>
													<xsl:if test="starts-with(string($Ticket_total), '-')">
														<TD align="left">&#32;
														</TD>
														<TD align="right">
															<xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0.00')"/>&#32;
															<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
														</TD>
													</xsl:if>
													<xsl:if test="not(starts-with(string($Ticket_total), '-'))">
														<TD align="right">
															<xsl:value-of select="format-number($Ticket_total,'#,##0.00')"/>&#32;
															<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
														</TD>
														<TD align="left">&#32;
														</TD>
													</xsl:if>
												</tr>
												<xsl:for-each select="Booking/Payments/Payment[void_date_time='']">
													<xsl:if test="form_of_payment_rcd !=''">
														<TR valign="middle" align="left" bgcolor="#ffffff" class="gridItems0">
															<TD align="left">
																<xsl:if test="form_of_payment_rcd ='CASH'">
																	<xsl:value-of select="form_of_payment_rcd"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='VOUCHER'">
																	<xsl:value-of select="form_of_payment_subtype"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CC'">
																	<xsl:value-of select="form_of_payment_subtype"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='TKT'">
																	<xsl:value-of select="form_of_payment"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CRAGT'">
																	<xsl:value-of select="form_of_payment"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='MANUAL'">
																	<xsl:value-of select="form_of_payment_subtype"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CHEQUE'">
																	<xsl:value-of select="form_of_payment_subtype"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='BANK'">
																	<xsl:value-of select="form_of_payment"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='INV'">
																	<xsl:value-of select="form_of_payment"/>
																</xsl:if>
															</TD>
															<TD align="left">
																<xsl:if test="form_of_payment_rcd ='INV'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='BANK'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CHEQUE'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='MANUAL'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='VOUCHER'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CC'">
																	<xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
																</xsl:if>
															</TD>
															<TD align="left">
																<xsl:choose>
																	<xsl:when test="void_date_time != ''">
																		<xsl:text>XX</xsl:text>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:if test="payment_date_time != ''">
																			<xsl:call-template name="formatdate">
																				<xsl:with-param name="date" select="payment_date_time"/>
																			</xsl:call-template>
																		</xsl:if>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="not(payment_amount &gt;= 0)">
																		<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0.00')"/>&#32;&#32;
																		<xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="payment_amount &gt; 0">
																		<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#32;&#32;
																		<xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
														</TR>
													</xsl:if>
												</xsl:for-each>
												<tr class="gridItems0" vAlign="middle" align="left">
													<xsl:variable name="SubTotal" select="$Ticket_total - $Payment_total"/>
													<TD align="left">
														<xsl:choose>
															<xsl:when test="number($SubTotal) &lt; 0">
																<SPAN class="Summary">
																	<xsl:text>OVERPAID BALANCE</xsl:text>
																</SPAN>
															</xsl:when>
															<xsl:otherwise>
																<SPAN class="Summary">
																	<xsl:text>OUTSTANDING BALANCE</xsl:text>
																</SPAN>
															</xsl:otherwise>
														</xsl:choose>
													</TD>
													<TD align="left">&#32;
													</TD>
													<TD align="left">&#32;
													</TD>

													<xsl:if test="number($SubTotal) = 0">
														<TD align="left">&#32;
														</TD>
														<TD align="right" style="FONT-WEIGHT: bold; color: #666666;">
															<SPAN class="Summary">0.00&#32;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN>
														</TD>
													</xsl:if>
													<xsl:if test="number($SubTotal) != 0">
														<xsl:if test="number($SubTotal) &gt;= 0">
															<TD align="left">&#32;
															</TD>
															<TD align="right">
																<SPAN class="Summary">
																	<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#32;
																	<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																</SPAN>
															</TD>
														</xsl:if>
														<xsl:if test="number($SubTotal) &lt; 0">
															<TD align="right" style="FONT-WEIGHT: bold; color: #666666;">
																<SPAN class="Summary">
																	<xsl:value-of select="format-number(substring($SubTotal,2,string-length($SubTotal)),'#,##0.00')"/>&#32;
																	<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																</SPAN>
															</TD>
															<TD align="left">&#32;
															</TD>
														</xsl:if>
													</xsl:if>
												</tr>
											</xsl:when>
											<xsl:otherwise>
												<TR valign="middle" align="left" class="gridItems0">
													<TD align="left">Ticket Cost &amp; Fee</TD>
													<TD align="left">&#32;
													</TD>
													<TD align="left">&#32;
													</TD>
													<xsl:if test="starts-with(string($Ticket_total), '-')">
														<TD align="left">&#32;
														</TD>
														<TD align="right">
															<xsl:value-of select="format-number(substring($Ticket_total,2,string-length($Ticket_total)),'#,##0.00')"/>&#32;
															<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
														</TD>
													</xsl:if>
													<xsl:if test="not(starts-with(string($Ticket_total), '-'))">
														<TD align="right">
															<xsl:value-of select="format-number($Ticket_total,'#,##0.00')"/>&#32;
															<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
														</TD>
														<TD align="left">&#32;
														</TD>
													</xsl:if>
												</TR>
												<xsl:for-each select="Booking/Payments/Payment[(substring(void_date_time,5,2) = '')][not(booking_id = ticket_booking_id)]">
													<xsl:if test="form_of_payment_subtype !=''">
														<TR valign="middle" align="left" bgcolor="#ffffff" class="gridItems0">
															<TD align="left">
																<xsl:value-of select="form_of_payment_subtype"/>
															</TD>
															<TD align="left">
																<xsl:if test="form_of_payment_rcd ='INV'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='BANK'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CHEQUE'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='MANUAL'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='VOUCHER'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CC'">
																	<xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
																</xsl:if>
															</TD>
															<TD align="left">
																<xsl:choose>
																	<xsl:when test="void_date_time != ''">
																		<xsl:text>XX</xsl:text>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:if test="payment_date_time != ''">
																			<xsl:call-template name="formatdate">
																				<xsl:with-param name="date" select="payment_date_time"/>
																			</xsl:call-template>
																		</xsl:if>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="not(payment_amount &gt;= 0)">
																		<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0.00')"/>&#32;&#32;
																		<xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="payment_amount &gt; 0">
																		<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#32;&#32;
																		<xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
														</TR>
													</xsl:if>
													<xsl:if test="form_of_payment_subtype =''">
														<TR valign="middle" align="left" bgcolor="#ffffff" class="gridItems0">
															<TD align="left">
																<xsl:value-of select="form_of_payment_rcd"/>
															</TD>
															<TD align="left">
																<xsl:if test="form_of_payment_rcd ='INV'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='BANK'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CHEQUE'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='MANUAL'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='VOUCHER'">
																	<xsl:value-of select="document_number"/>
																</xsl:if>
																<xsl:if test="form_of_payment_rcd ='CC'">
																	<xsl:value-of select="concat(substring(document_number, 0,5),'XXXXXXXX',substring(document_number,string-length(document_number)-3,5))"/>
																</xsl:if>
															</TD>
															<TD align="left">
																<xsl:if test="payment_date_time != ''">
																	<xsl:call-template name="formatdate">
																		<xsl:with-param name="date" select="payment_date_time"/>
																	</xsl:call-template>
																</xsl:if>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="not(payment_amount &gt;= 0)">
																		<xsl:value-of select="format-number(substring(payment_amount,2,100),'#,##0.00')"/>&#32;&#32;
																		<xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
															<TD align="right">
																<xsl:choose>
																	<xsl:when test="payment_amount &gt; 0">
																		<xsl:value-of select="format-number(payment_amount,'#,##0.00')"/>&#32;&#32;
																		<xsl:value-of select="currency_rcd"/>
																		<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																	</xsl:when>
																	<xsl:otherwise>
																	</xsl:otherwise>
																</xsl:choose>
															</TD>
														</TR>
													</xsl:if>
												</xsl:for-each>
												<TR valign="middle" align="left" class="gridItems0">
													<TD align="left">
														<SPAN class="Summary">OUTSTANDING BALANCE</SPAN>
													</TD>
													<TD align="left">&#32;
													</TD>
													<TD align="left">&#32;
													</TD>
													<xsl:variable name="SubTotal" select="number($Ticket_total) - number($Payment_total)"/>
													<xsl:if test="number($SubTotal) = 0">
														<TD align="left">&#32;
														</TD>\
														<TD align="right">
															<SPAN class="Summary">0.00&#32;<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/></SPAN>
														</TD>
													</xsl:if>
													<xsl:if test="number($SubTotal) != 0">
														<xsl:if test="number($SubTotal) &gt;= 0">
															<TD align="left">&#32;
															</TD>
															<TD align="right" style="FONT-WEIGHT: bold; color: #666666;">
																<SPAN class="Summary">
																	<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#32;
																	<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																</SPAN>
															</TD>
														</xsl:if>
														<xsl:if test="number($SubTotal) &lt; 0">
															<TD align="right">
																<SPAN class="Summary">
																	<xsl:value-of select="format-number($SubTotal,'#,##0.00')"/>&#32;
																	<xsl:value-of select="Booking/Tickets/Ticket/currency_rcd"/>
																</SPAN>
															</TD>
															<TD align="left">&#32;
															</TD>
														</xsl:if>
													</xsl:if>
												</TR>
											</xsl:otherwise>
										</xsl:choose>
									</tbody>
								</table>
							</div>
						</xsl:if>
						<!--End Payments-->
						<br/>
						<br/>
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<xsl:choose>
									<xsl:when test="//Payments/Payment = false()">
										<xsl:text>&#32;</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:variable name="Payment_total" select="sum(//Booking/Payments/Payment[string-length(void_by)!=38]/payment_amount)"/>
										<xsl:variable name="Ticket_total"
										              select="(sum(//TicketQuotes/Total[charge_type != 'REFUND']/charge_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/charge_amount)) + (sum(//TicketQuotes/Total[charge_type != 'REFUND']/tax_amount) - sum(//TicketQuotes/Total[charge_type = 'REFUND']/tax_amount)) + sum(//Fees/Fee[void_date_time = '']/fee_amount_incl)"/>
										<xsl:choose>
											<xsl:when test="($Payment_total  &gt;=  $Ticket_total) ">

												<td class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-align: justify;line-height:normal">
													<p class="MsoNormal">
														<strong>PLEASE PRODUCE THIS RECEIPT AND YOUR IDENTITY CARD 
AT THE CHECK-IN COUNTER ON THE DAY OF YOUR TRAVEL.</strong>
													</p>
													<p class="MsoNormal">&#32;
													</p>
													<p class="MsoNormal">
														<strong>NOTICE:</strong>
													</p>
													<p class="MsoNormal">Carriage and other services provided by CiTylinK are subject 
to the Conditions of Carriage below.</p>
													<p class="MsoNormal">&#32;
													</p>
													<br/>
													<p class="MsoNormal">
														<strong>CONDITIONS OF CONTRACT:</strong>
													</p>
													<ol class="MsoNormal">
														<li style="font-size: 8pt;color:#1E336C; ">
															<p class="MsoNormal">As used in this contract "ticket" means this receipt if 
	applicable, in the case of an electronic ticket, of which these conditions form 
	part, "carriage" is equivalent to "transportation", "carrier" means all air 
	carriers that carry or undertake to carry the passenger or his baggage hereunder 
	or perform any other service incidental to such air carriage, "electronic ticket" 
	means the itinerary / receipt issued by or on behalf of Carrier, the Electronic 
	Coupons and, if applicable, a boarding documents. "WARSAW CONVENTION" means 
	the Convention for the Unification of Certain Rules for International Carriage 
	by Air signed at 
	 Warsaw , October 12, 1929, or that Convention 
	means amended at 
	 
	The Hague , September 28, 1955, whichever may be 
	applicable. "Montreal Convention" means the Convention for the Unification of 
	Certain Rules for International Carriage by air at 
	 
	Montreal , 28th May 1999.</p>
														</li>
														<li style="font-size: 8pt;color:#1E336C; ">
															<p class="MsoNormal">Carriage hereunder is subject to the rules and limitations 
	relating to liability establish by either the (amended) Warsaw Convention or 
	the Montreal Convention unless such carriage is not "international carriage" 
	as defined by any of the aforementioned conventions.</p>
														</li>
														<li style="font-size: 8pt;color:#1E336C; ">
															<p class="MsoNormal">To the extent not in conflict with the foregoing, carriage 
	and other services performed by each carrier are subject to: (i)<span style="mso-spacerun: yes">&#32;</span>provisions contained in the ticket,<span style="mso-spacerun: yes">&#32;&#32;&#32;&#32;&#32;</span>(ii) applicable tariffs, (iii) carrier"s conditions of carriage and related 
	regulations which are made part hereof (and are made available on application 
	at the offices of carrier), except in transportation between a place in the 
	United States or Canada and any place outside thereof to which tariffs in force 
	in those countries apply.</p>
														</li>
														<li style="font-size: 8pt;color:#1E336C; ">
															<p class="MsoNormal">In case of damage to baggage moving in international transportation 
	complaint must be made in writing to carrier forthwith after discovery of damage 
	and, at latest, within seven days from receipt; in case of delay, complaints 
	must the date the baggage was delivered.</p>
														</li>
														<li style="font-size: 8pt;color:#1E336C; ">
															<p class="MsoNormal">Passenger shall comply with<span style="mso-spacerun: yes">&#32;</span>Government travel requirements, present exit, entry and other required 
	documents and arrive at airport by time fixed by carrier or, if no time is fixed, 
	early enough to complete departure<span style="mso-spacerun: yes">&#32;</span>procedures.</p>
														</li>
														<li style="font-size: 8pt;color:#1E336C; ">
															<p class="MsoNormal">No agent, subcontractor, servant or representative of carrier 
	has authority to alter, modify or waive any portion of this<span style="mso-spacerun: yes">&#32;</span>contract.</p>
														</li>
													</ol>
													<p class="MsoNormal">&#32;
													</p>
													<p class="MsoNormal">&#32;
													</p>
													<p class="MsoNormal">
														<strong>NOTICE OF BAGGAGE LIABILITY LIMITATIONS</strong>
													</p>
													<p class="MsoNormal">&#32;
													</p>
													<p class="MsoNormal">Liability for loss, delay, or damage to baggage is limited 
unless a higher value is declared in advance and additional charges are paid. For 
most international travel (including domestic portions of international journeys), 
the Montréal Convention will apply, with a liability limit of 1,000 Special Drawing 
Rights (approximately US $ 1,375) for checked and unchecked baggage. In some cases, 
the (amended) Warsaw Convention may apply, with a liability limit of approximately 
US$9.07 per pound (US$20.00 per kilo) for checked baggage and US$400.00 for unchecked 
baggage. Excess valuation may be declared on certain types of articles. Some carriers 
assume no liability for fragile, valuable or perishable articles. Further information 
may be obtained from the carrier.</p>
													<br/>
													<p class="MsoNormal">&#32;
													</p>
													<p class="MsoNormal">
														<strong>STANDARD CONDITIONS</strong>
													</p>
													<p class="MsoNormal">&#32;
													</p>
													<ol>
														<li style="font-size: 8pt;color:#1E336C; ">
															<p class="MsoNormal">Allowed weight for your luggage is 15KG plus 5KG as hand 
	luggage.</p>
														</li>
														<li style="font-size: 8pt;color:#1E336C; ">
															<p class="MsoNormal">Reconfirmation: Please reconfirm your booking at least 
	48 hours before departure.</p>
														</li>
														<li style="font-size: 8pt;color:#1E336C; ">
															<p class="MsoNormal">Failure to cancel your booking at least 48 hours before 
	departure would result in a "No Show" fee charge of 30% of the original fare.</p>
														</li>
														<li style="font-size: 8pt;color:#1E336C; ">
															<p class="MsoNormal">CiTylinK does not refund unused tickets.</p>
														</li>
														<li style="font-size: 8pt;color:#1E336C; ">
															<p class="MsoNormal">Change in name and reservation attracts a fee of $20</p>
														</li>
														<li style="font-size: 8pt;color:#1E336C; ">
															<p class="MsoNormal">Ticket is valid for six months.</p>
														</li>
														<li style="font-size: 8pt;color:#1E336C; ">
															<p class="MsoNormal">You may make only one change on issued tickets.</p>
														</li>
													</ol>
													<p class="MsoNormal">&#32;
													</p>
													<p class="MsoNormal">
														<strong>DANGEROUS GOODS:</strong>
													</p>
													<p class="MsoNormal">For safety reasons, dangerous articles such as compressed gas...(<strong>list 
all standard banned items</strong>)...must not be carried in your baggage or onto 
the aircraft.</p>
													<br/>
													<p class="MsoNormal">&#32;
													</p>
													<p class="MsoNormal">
														<strong>SAFTEY AND SECURITY NOTICES</strong>
													</p>
													<p class="MsoNormal">All safety and security notices published by the Ghana Airports 
Company Limited apply your travel with CiTylinK.</p>
													<p class="MsoNormal">&#32;
													</p>
												</td>
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>&#32;</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:otherwise>
								</xsl:choose>
							</tr>
						</table>
					</DIV>
				</DIV>
			</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2007. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="no" externalpreview="no" url="file:///c:/EXEs/XML/FO63YD.XML" htmlbaseurl="" outputurl="" processortype="internal" useresolver="yes" profilemode="0" profiledepth="" profilelength=""
		          urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->